﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebService.Models.Base.Search;

namespace WebService.Services.Base
{
    public interface ISearchable<S, L>
         where S : ListRequestModel
    {
        Task<L> Get();
        Task<L> Get(S searchModel);
    }
}
