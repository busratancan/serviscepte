﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebService.Models.Base;

namespace WebService.Services.Base
{
    public interface IServiceable<T>
        where T : BaseModel
    {
        Task<T> Get(int id);
        Task<T> Post(T model);
        Task<T> Put(int id, T model);
        void Delete(int id);
    }
}
