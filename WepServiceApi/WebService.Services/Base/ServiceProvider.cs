﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using WebService.Services.Customer;
using WebService.Services.Service;
using WebService.Services.Service.Interface;

namespace WebService.Services.Base
{
    public class ServiceProvider
    {
        public static void SetServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ICategoryDefinitionService, CategoryDefinitionService>();
            serviceCollection.AddTransient<IControlDefineService, ControlDefineService>();
            serviceCollection.AddTransient<IModuleDefineService, ModuleDefineService>();
            serviceCollection.AddTransient<IRequestDefinationService, RequestDefinationService>();
            //serviceCollection.AddTransient<IServiceStartService, ServiceStartService>();
            serviceCollection.AddTransient<ISubControlDefinesService, SubControlDefinesService>();
            serviceCollection.AddTransient<ISubLastStatusService, SubLastStatusService>();
            serviceCollection.AddTransient<ICustomerService, CustomerService>();
            //serviceCollection.AddTransient<IAccountActivitiesService, AccountActivitiesService>();
            //serviceCollection.AddTransient<IUserPictureService, UserPictureService>();
            
        }
    }
}
