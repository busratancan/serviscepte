﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebService.Models.Customer;
using WebService.Repositories.Customer;
using WepService.Data;

namespace WebService.Services.Customer
{
    public class CustomerService : ICustomerService
    {
        private ICustomerRepository customerRepository;

        public CustomerService(
            ICustomerRepository customerRepository)
        {
            this.customerRepository = customerRepository;
        }

        public async void Delete(int id)
        {
            CustomerEntity entity = await customerRepository.GetAsync(id, null);
            entity.Deletion = 1;

            customerRepository.Update(entity);
        }

        public async Task<CustomerModel> Get(int id)
        {
            return new CustomerModel(await customerRepository.GetAsync(id, null));
        }

        public async Task<CustomerListModel> Get(CustomerListRequestModel searchModel)
        {
            CustomerListModel listModel = new CustomerListModel();

            var list = customerRepository.GetQueryPaged(
                    searchModel.PageNumber,
                    searchModel.PageCount,
                    searchModel.GetFilter(),
                    searchModel.GetOrderBy()
                        //null
                        //c =>
                        //c.Include(a => a.Transaction)
                );

            listModel.List = (await list.ToListAsync()).Select(customer => new CustomerItemModel(customer)).ToList();
            //listModel.Map(list);
            listModel.Count = await customerRepository.QueryCountAsync(searchModel.GetFilter());
            listModel.CurrentPage = searchModel.PageNumber;
            listModel.PageCount = searchModel.PageCount;
            //listModel.TotalPage = listModel.Count / listModel.PageCount;

            return listModel;
        }

        public async Task<CustomerListModel> Get()
        {
            return await Get(new CustomerListRequestModel());
        }

        public Task<CustomerModel> Post(CustomerModel model)
        {
            customerRepository.Add(model.Convert());

            return Task.FromResult(model);
        }

        public Task<CustomerModel> Put(int id, CustomerModel model)
        {
            model.Id = id;

            customerRepository.Update(model.Convert());

            return Task.FromResult(model);
        }
    }
}
