﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Models.Customer;
using WebService.Services.Base;

namespace WebService.Services.Customer
{
    public interface ICustomerService :
        IServiceable<CustomerModel>,
        ISearchable<CustomerListRequestModel, CustomerListModel>
    {
    }
}
