﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebService.Models.Customer;
using WebService.Models.Service;
using WebService.Repositories.Services;
using WepService.Data;

namespace WebService.Services.Service
{
    public class ControlDefineService : IControlDefineService
    {
        private IControlDefinesRepo controlDefinesRepo;

        public ControlDefineService(
            IControlDefinesRepo controlDefinesRepo)
        {
            this.controlDefinesRepo = controlDefinesRepo;
        }

        public async void Delete(int id)
        {
            ServiceControlDefines entity = await controlDefinesRepo.GetAsync(id, null);
            entity.Deletion = 1;

            controlDefinesRepo.Update(entity);
        }

        public async Task<ControlDefineModel> Get(int id)
        {
            return new ControlDefineModel(await controlDefinesRepo.GetAsync(id, null));
        }

        public async Task<ControlDefineList> Get(ControlDefineRequestModel searchModel)
        {
            ControlDefineList listModel = new ControlDefineList();

            var list = controlDefinesRepo.GetQueryPaged(
                    searchModel.PageNumber,
                    searchModel.PageCount,
                    searchModel.GetFilter(),
                    searchModel.GetOrderBy()
                );
            
            listModel.List = (await list.ToListAsync()).Select(k => new ControlDefineItemModel(k)).ToList();
            listModel.Count = await controlDefinesRepo.QueryCountAsync(searchModel.GetFilter());
            listModel.CurrentPage = searchModel.PageNumber;
            listModel.PageCount = searchModel.PageCount;

            return listModel;
        }

        public async Task<ControlDefineList> Get()
        {
            return await Get(new ControlDefineRequestModel());
        }

        public Task<ControlDefineModel> Post(ControlDefineModel model)
        {
            controlDefinesRepo.Add(model.Convert());

            return Task.FromResult(model);
        }

        public Task<ControlDefineModel> Put(int id, ControlDefineModel model)
        {
            model.Id = id;

            controlDefinesRepo.Update(model.Convert());

            return Task.FromResult(model);
        }
    }
}

