﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebService.Models.Service;
using WebService.Models.Service.ItemModels;
using WebService.Models.Service.ListModels;
using WebService.Models.Service.RequestList;
using WebService.Repositories.Services.InterfaceRepo;
using WebService.Services.Service.Interface;
using WepService.Data;

namespace WebService.Services.Service
{
    public class LastStatusService : ILastStatusService
    {
        private ILastStatusRepo lastStatusRepo;

        public LastStatusService(
            ILastStatusRepo lastStatusRepo)
        {
            this.lastStatusRepo = lastStatusRepo;
        }

        public async void Delete(int id)
        {
            ServicesLastStatusDefination entity = await lastStatusRepo.GetAsync(id, null);
            entity.Deletion = 1;

            lastStatusRepo.Update(entity);
        }

        public async Task<LastStatusModel> Get(int id)
        {
            return new LastStatusModel(await lastStatusRepo.GetAsync(id, null));
        }

        public async Task<LastStatusListModel> Get(LastStatusRequestModel searchModel)
        {
            LastStatusListModel listModel = new LastStatusListModel();

            var list = lastStatusRepo.GetQueryPaged(
                    searchModel.PageNumber,
                    searchModel.PageCount,
                    searchModel.GetFilter(),
                    searchModel.GetOrderBy()
                );

            listModel.List = (await list.ToListAsync()).Select(k => new LastStatusItemModel(k)).ToList();
            listModel.Count = await lastStatusRepo.QueryCountAsync(searchModel.GetFilter());
            listModel.CurrentPage = searchModel.PageNumber;
            listModel.PageCount = searchModel.PageCount;

            return listModel;
        }

        public async Task<LastStatusListModel> Get()
        {
            return await Get(new LastStatusRequestModel());
        }

        public Task<LastStatusModel> Post(LastStatusModel model)
        {
            lastStatusRepo.Add(model.Convert());

            return Task.FromResult(model);
        }

        public Task<LastStatusModel> Put(int id, LastStatusModel model)
        {
            model.Id = id;

            lastStatusRepo.Update(model.Convert());

            return Task.FromResult(model);
        }
    }
}