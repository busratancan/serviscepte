﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Models.Service;
using WebService.Models.Service.ListModels;
using WebService.Models.Service.RequestList;
using WebService.Services.Base;

namespace WebService.Services.Service.Interface
{
    interface ICategoryDefinitionService :
        IServiceable<CategoryDefinitionModel>,
        ISearchable<CategoryDefinitionRequestModel, CategoryDefinitionListModel>
    {
    }
}
