﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Models.Service;
using WebService.Services.Base;

namespace WebService.Services.Service
{
    interface ISubControlDefinesService :
        IServiceable<SubControlModel>,
        ISearchable<SubControlListRequestModel, SubControlListModel>
    {
    }
}
