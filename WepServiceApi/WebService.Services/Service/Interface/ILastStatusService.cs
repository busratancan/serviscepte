﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Models.Service;
using WebService.Models.Service.ListModels;
using WebService.Models.Service.RequestList;
using WebService.Services.Base;

namespace WebService.Services.Service.Interface
{
    public interface ILastStatusService :
        IServiceable<LastStatusModel>,
        ISearchable<LastStatusRequestModel, LastStatusListModel>
    {
    }
}
