﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebService.Models.Service;
using WebService.Repositories.Services;
using WepService.Data;

namespace WebService.Services.Service
{
    public class SubControlDefinesService : ISubControlDefinesService
    {
        private ISubControlDefinesRepo subControlDefinesRepo;

        public SubControlDefinesService(
            ISubControlDefinesRepo subControlDefinesRepo)
        {
            this.subControlDefinesRepo = subControlDefinesRepo;
        }

        public async void Delete(int id)
        {
            ServiceControlSubDefines entity = await subControlDefinesRepo.GetAsync(id, null);
            entity.Deletion = 1;

            subControlDefinesRepo.Update(entity);
        }

        public async Task<SubControlModel> Get(int id)
        {
            return new SubControlModel(await subControlDefinesRepo.GetAsync(id, null));
        }

        public async Task<SubControlListModel> Get(SubControlListRequestModel searchModel)
        {
            SubControlListModel listModel = new SubControlListModel();

            var list = subControlDefinesRepo.GetQueryPaged(
                    searchModel.PageNumber,
                    searchModel.PageCount,
                    searchModel.GetFilter(),
                    searchModel.GetOrderBy()
                );

            listModel.List = (await list.ToListAsync()).Select(k => new SubControlItemModel(k)).ToList();
            listModel.Count = await subControlDefinesRepo.QueryCountAsync(searchModel.GetFilter());
            listModel.CurrentPage = searchModel.PageNumber;
            listModel.PageCount = searchModel.PageCount;

            return listModel;
        }

        public async Task<SubControlListModel> Get()
        {
            return await Get(new SubControlListRequestModel());
        }

        public Task<SubControlModel> Post(SubControlModel model)
        {
            subControlDefinesRepo.Add(model.Convert());

            return Task.FromResult(model);
        }

        public Task<SubControlModel> Put(int id, SubControlModel model)
        {
            model.Id = id;

            subControlDefinesRepo.Update(model.Convert());

            return Task.FromResult(model);
        }
    }
}
