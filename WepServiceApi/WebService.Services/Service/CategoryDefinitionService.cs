﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebService.Models.Service;
using WebService.Models.Service.ItemModels;
using WebService.Models.Service.ListModels;
using WebService.Models.Service.RequestList;
using WebService.Repositories.Services;
using WebService.Services.Service.Interface;
using WepService.Data;

namespace WebService.Services.Service
{
    public class CategoryDefinitionService : ICategoryDefinitionService
    {
        private ICategoryDefinitionRepo categoryDefinitionRepo;

        public CategoryDefinitionService(
            ICategoryDefinitionRepo categoryDefinitionRepo)
        {
            this.categoryDefinitionRepo = categoryDefinitionRepo;
        }

        public async void Delete(int id)
        {
            ServicesCategoryDefinition entity = await categoryDefinitionRepo.GetAsync(id, null);
            entity.Deletion = 1;

            categoryDefinitionRepo.Update(entity);
        }

        public async Task<CategoryDefinitionModel> Get(int id)
        {
            return new CategoryDefinitionModel(await categoryDefinitionRepo.GetAsync(id, null));
        }

        public async Task<CategoryDefinitionListModel> Get(CategoryDefinitionRequestModel searchModel)
        {
            CategoryDefinitionListModel listModel = new CategoryDefinitionListModel();

            var list = categoryDefinitionRepo.GetQueryPaged(
                    searchModel.PageNumber,
                    searchModel.PageCount,
                    searchModel.GetFilter(),
                    searchModel.GetOrderBy()
                );

            listModel.List = (await list.ToListAsync()).Select(k => new CategoryDefinitionItemModel(k)).ToList();
            listModel.Count = await categoryDefinitionRepo.QueryCountAsync(searchModel.GetFilter());
            listModel.CurrentPage = searchModel.PageNumber;
            listModel.PageCount = searchModel.PageCount;

            return listModel;
        }

        public async Task<CategoryDefinitionListModel> Get()
        {
            return await Get(new CategoryDefinitionRequestModel());
        }

        public Task<CategoryDefinitionModel> Post(CategoryDefinitionModel model)
        {
            categoryDefinitionRepo.Add(model.Convert());

            return Task.FromResult(model);
        }

        public Task<CategoryDefinitionModel> Put(int id, CategoryDefinitionModel model)
        {
            model.Id = id;

            categoryDefinitionRepo.Update(model.Convert());

            return Task.FromResult(model);
        }
    }
}