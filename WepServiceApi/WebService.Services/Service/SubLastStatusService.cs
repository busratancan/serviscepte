﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebService.Models.Service;
using WebService.Models.Service.ItemModels;
using WebService.Models.Service.ListModels;
using WebService.Models.Service.RequestList;
using WebService.Repositories.Services;
using WebService.Services.Service.Interface;
using WepService.Data;

namespace WebService.Services.Service
{
    public class SubLastStatusService : ISubLastStatusService
    {
        private ISubLastStatusRepo subLastStatusRepo;

        public SubLastStatusService(
            ISubLastStatusRepo subLastStatusRepo)
        {
            this.subLastStatusRepo = subLastStatusRepo;
        }

        public async void Delete(int id)
        {
            ServicesLastStatusDefinationInfs entity = await subLastStatusRepo.GetAsync(id, null);
            entity.Deletion = 1;

            subLastStatusRepo.Update(entity);
        }

        public async Task<SubLastStatusModel> Get(int id)
        {
            return new SubLastStatusModel(await subLastStatusRepo.GetAsync(id, null));
        }

        public async Task<SubLastStatusIListModel> Get(SubLastStatusIRequestListModel searchModel)
        {
            SubLastStatusIListModel listModel = new SubLastStatusIListModel();

            var list = subLastStatusRepo.GetQueryPaged(
                    searchModel.PageNumber,
                    searchModel.PageCount,
                    searchModel.GetFilter(),
                    searchModel.GetOrderBy()
                );

            listModel.List = (await list.ToListAsync()).Select(k => new SubLastStatusItemModel(k)).ToList();
            listModel.Count = await subLastStatusRepo.QueryCountAsync(searchModel.GetFilter());
            listModel.CurrentPage = searchModel.PageNumber;
            listModel.PageCount = searchModel.PageCount;

            return listModel;
        }

        public async Task<SubLastStatusIListModel> Get()
        {
            return await Get(new SubLastStatusIRequestListModel());
        }

        public Task<SubLastStatusModel> Post(SubLastStatusModel model)
        {
            subLastStatusRepo.Add(model.Convert());

            return Task.FromResult(model);
        }

        public Task<SubLastStatusModel> Put(int id, SubLastStatusModel model)
        {
            model.Id = id;

            subLastStatusRepo.Update(model.Convert());

            return Task.FromResult(model);
        }
    }
}
