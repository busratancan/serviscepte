﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebService.Models.Service;
using WebService.Models.Service.ItemModels;
using WebService.Models.Service.ListModels;
using WebService.Models.Service.RequestList;
using WebService.Repositories.Services;
using WebService.Services.Service.Interface;
using WepService.Data;

namespace WebService.Services.Service
{
    public class ModuleDefineService : IModuleDefineService
    {
        private ModuleDefineRepo moduleDefineRepo;

        public ModuleDefineService(
            ModuleDefineRepo moduleDefineRepo)
        {
            this.moduleDefineRepo = moduleDefineRepo;
        }

        public async void Delete(int id)
        {
            ServicesCategorySubDefinition entity = await moduleDefineRepo.GetAsync(id, null);
            entity.Deletion = 1;

            moduleDefineRepo.Update(entity);
        }

        public async Task<ModuleDefineModel> Get(int id)
        {
            return new ModuleDefineModel(await moduleDefineRepo.GetAsync(id, null));
        }

        public async Task<ModuleDefineListModel> Get(ModuleDefineRequestListModel searchModel)
        {
            ModuleDefineListModel listModel = new ModuleDefineListModel();

            var list = moduleDefineRepo.GetQueryPaged(
                    searchModel.PageNumber,
                    searchModel.PageCount,
                    searchModel.GetFilter(),
                    searchModel.GetOrderBy()
                );

            listModel.List = (await list.ToListAsync()).Select(k => new ModuleDefineItemModel(k)).ToList();
            listModel.Count = await moduleDefineRepo.QueryCountAsync(searchModel.GetFilter());
            listModel.CurrentPage = searchModel.PageNumber;
            listModel.PageCount = searchModel.PageCount;

            return listModel;
        }

        public async Task<ModuleDefineListModel> Get()
        {
            return await Get(new ModuleDefineRequestListModel());
        }

        public Task<ModuleDefineModel> Post(ModuleDefineModel model)
        {
            moduleDefineRepo.Add(model.Convert());

            return Task.FromResult(model);
        }

        public Task<ModuleDefineModel> Put(int id, ModuleDefineModel model)
        {
            model.Id = id;

            moduleDefineRepo.Update(model.Convert());

            return Task.FromResult(model);
        }
    }
}
