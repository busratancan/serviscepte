﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebService.Models.Service;
using WebService.Models.Service.RequestList;
using WebService.Repositories.Services.InterfaceRepo;
using WebService.Services.Service.Interface;
using WepService.Data;

namespace WebService.Services.Service
{
    public class RequestDefinationService : IRequestDefinationService
    {
        private IRequestDefinationRepo requestDefinationRepo;

        public RequestDefinationService(
            IRequestDefinationRepo requestDefinationRepo)
        {
            this.requestDefinationRepo = requestDefinationRepo;
        }

        public async void Delete(int id)
        {
            ServicesRequestDefination entity = await requestDefinationRepo.GetAsync(id, null);
            entity.Deletion = 1;

            requestDefinationRepo.Update(entity);
        }

        public async Task<RequestDefinationModel> Get(int id)
        {
            return new RequestDefinationModel(await requestDefinationRepo.GetAsync(id, null));
        }

        public async Task<RequestDefinationListModel> Get(RequestDefinationRequestListModel searchModel)
        {
            RequestDefinationListModel listModel = new RequestDefinationListModel();

            var list = requestDefinationRepo.GetQueryPaged(
                    searchModel.PageNumber,
                    searchModel.PageCount,
                    searchModel.GetFilter(),
                    searchModel.GetOrderBy()
                );

            listModel.List = (await list.ToListAsync()).Select(k => new RequestDefinationItemModel(k)).ToList();
            listModel.Count = await requestDefinationRepo.QueryCountAsync(searchModel.GetFilter());
            listModel.CurrentPage = searchModel.PageNumber;
            listModel.PageCount = searchModel.PageCount;

            return listModel;
        }

        public async Task<RequestDefinationListModel> Get()
        {
            return await Get(new RequestDefinationRequestListModel());
        }

        public Task<RequestDefinationModel> Post(RequestDefinationModel model)
        {
            requestDefinationRepo.Add(model.Convert());

            return Task.FromResult(model);
        }

        public Task<RequestDefinationModel> Put(int id, RequestDefinationModel model)
        {
            model.Id = id;

            requestDefinationRepo.Update(model.Convert());

            return Task.FromResult(model);
        }
    }
}