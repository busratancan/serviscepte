﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ElevatorSubControlHistory
    {
        public int Id { get; set; }
        public int? ElevatorControlHistoryId { get; set; }
        public DateTime? ContralDate { get; set; }
        public string Notes { get; set; }
        public int? UserId { get; set; }
        public DateTime? InsDateTime { get; set; }
        public sbyte? Deletion { get; set; }
        public sbyte? ControlResult { get; set; }
    }
}
