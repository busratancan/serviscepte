﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ServicesLastStatusDefination
    {
        public ServicesLastStatusDefination()
        {
            ServicesLastStatusDefinationInfs = new HashSet<ServicesLastStatusDefinationInfs>();
            ServicesOpenClose = new HashSet<ServicesOpenClose>();
        }

        public int Id { get; set; }
        public string Defination { get; set; }
        public string Code { get; set; }
        public string Color { get; set; }
        public int? Order { get; set; }
        public string Exp { get; set; }
        public int? SevicesRequestDefinationId { get; set; }
        public string SevicesRequestDefinationDefination { get; set; }
        public int? ServicesCategoryDefinitionId { get; set; }
        public string ServicesCategoryName { get; set; }
        public sbyte? OpenNewService { get; set; }
        public sbyte? ServiceImpact { get; set; }
        public string OpenNewServiceRequestDefinationId { get; set; }
        public string OpenNewServiceRequestDefinationDefination { get; set; }
        public string ColorCode { get; set; }
        public sbyte? Deletion { get; set; }
        public DateTime? DeletionDatetime { get; set; }
        public int? DeletionUserId { get; set; }
        public int? SmsThemesId { get; set; }
        public string SmsThemesName { get; set; }

        public ServicesCategoryDefinition ServicesCategoryDefinition { get; set; }
        public ServicesRequestDefination SevicesRequestDefination { get; set; }
        public SmsThemes SmsThemes { get; set; }
        public ICollection<ServicesLastStatusDefinationInfs> ServicesLastStatusDefinationInfs { get; set; }
        public ICollection<ServicesOpenClose> ServicesOpenClose { get; set; }
    }
}
