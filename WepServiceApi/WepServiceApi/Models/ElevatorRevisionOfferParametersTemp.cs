﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ElevatorRevisionOfferParametersTemp
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Defination { get; set; }
        public string OfferCaption { get; set; }
        public decimal? Fee { get; set; }
        public decimal? MaxDiscount { get; set; }
        public int? Basket { get; set; }
        public sbyte? ElevatorOrder { get; set; }
        public int? ElevatorRevisionParametersId { get; set; }
        public sbyte? OfferVisible { get; set; }
        public string Unit { get; set; }
        public decimal? Qty { get; set; }

        public ElevatorRevisionParameters ElevatorRevisionParameters { get; set; }
    }
}
