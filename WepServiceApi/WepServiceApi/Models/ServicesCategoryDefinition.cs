﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ServicesCategoryDefinition
    {
        public ServicesCategoryDefinition()
        {
            ServicesCategorySubDefinition = new HashSet<ServicesCategorySubDefinition>();
            ServicesContract = new HashSet<ServicesContract>();
            ServicesLastStatusDefination = new HashSet<ServicesLastStatusDefination>();
            ServicesPointsBaseInformation = new HashSet<ServicesPointsBaseInformation>();
            ServicesRequestDefination = new HashSet<ServicesRequestDefination>();
        }

        public int Id { get; set; }
        public string Defination { get; set; }
        public string Code { get; set; }
        public string Color { get; set; }
        public int? Order { get; set; }
        public string Exp { get; set; }
        public string ColorCode { get; set; }

        public ICollection<ServicesCategorySubDefinition> ServicesCategorySubDefinition { get; set; }
        public ICollection<ServicesContract> ServicesContract { get; set; }
        public ICollection<ServicesLastStatusDefination> ServicesLastStatusDefination { get; set; }
        public ICollection<ServicesPointsBaseInformation> ServicesPointsBaseInformation { get; set; }
        public ICollection<ServicesRequestDefination> ServicesRequestDefination { get; set; }
    }
}
