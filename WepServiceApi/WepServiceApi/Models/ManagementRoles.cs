﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ManagementRoles
    {
        public ManagementRoles()
        {
            ManagementRolesAuthorization = new HashSet<ManagementRolesAuthorization>();
        }

        public int Id { get; set; }
        public string RoleCode { get; set; }
        public string RoleDefination { get; set; }
        public string Exp1 { get; set; }
        public string Exp2 { get; set; }
        public sbyte? Deletion { get; set; }
        public int? DeletionUserId { get; set; }

        public ICollection<ManagementRolesAuthorization> ManagementRolesAuthorization { get; set; }
    }
}
