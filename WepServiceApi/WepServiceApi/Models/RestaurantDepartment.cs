﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class RestaurantDepartment
    {
        public RestaurantDepartment()
        {
            RestaurantPrinterParameters = new HashSet<RestaurantPrinterParameters>();
        }

        public int Id { get; set; }
        public string DepartmentName { get; set; }
        public sbyte? Visible { get; set; }

        public ICollection<RestaurantPrinterParameters> RestaurantPrinterParameters { get; set; }
    }
}
