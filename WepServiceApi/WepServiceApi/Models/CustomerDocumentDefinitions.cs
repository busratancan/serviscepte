﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class CustomerDocumentDefinitions
    {
        public int Id { get; set; }
        public DateTime? Datetime { get; set; }
        public string Defination { get; set; }
        public int? CustomerId { get; set; }
        public string DocGroup { get; set; }
        public sbyte? Deletion { get; set; }
        public DateTime? InsDatetime { get; set; }
        public int? TotalPage { get; set; }
    }
}
