﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class CommercialPaperTemp
    {
        public int Id { get; set; }
        public decimal? Currency { get; set; }
        public DateTime? PayDate { get; set; }
        public string NotaryWarning { get; set; }
        public int? DocumentNumber { get; set; }
        public string Exp { get; set; }
        public int? CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string OpType { get; set; }
    }
}
