﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class RestaurantSpeedGroupButtons
    {
        public int Id { get; set; }
        public string ButtonName { get; set; }
        public int? Grup2Id { get; set; }
        public string ButtonCaption { get; set; }
        public int? UserId { get; set; }
        public int? Type { get; set; }
    }
}
