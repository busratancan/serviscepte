﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ProductionRecipeDefines
    {
        public ProductionRecipeDefines()
        {
            ProductionOrdersTemp = new HashSet<ProductionOrdersTemp>();
            ProductionRecipeFormula = new HashSet<ProductionRecipeFormula>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Define { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public string Exp { get; set; }
        public string Exp2 { get; set; }
        public int? ProductionUnit { get; set; }
        public string ProductionUnitDefine { get; set; }

        public Product Product { get; set; }
        public ICollection<ProductionOrdersTemp> ProductionOrdersTemp { get; set; }
        public ICollection<ProductionRecipeFormula> ProductionRecipeFormula { get; set; }
    }
}
