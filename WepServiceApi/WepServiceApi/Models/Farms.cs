﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class Farms
    {
        public Farms()
        {
            FarmPaddock = new HashSet<FarmPaddock>();
        }

        public int Id { get; set; }
        public string FarmCode { get; set; }
        public string FarmDefination { get; set; }
        public string Exp { get; set; }
        public string Exp2 { get; set; }
        public decimal? M2 { get; set; }

        public ICollection<FarmPaddock> FarmPaddock { get; set; }
    }
}
