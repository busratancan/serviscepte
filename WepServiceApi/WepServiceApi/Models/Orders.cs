﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class Orders
    {
        public int Id { get; set; }
        public int? Type { get; set; }
        public string OrdersProductsBasket { get; set; }
        public DateTime? InsDateTime { get; set; }
        public DateTime? OrderDateTime { get; set; }
        public int? UserId { get; set; }
        public int? CustomerId { get; set; }
        public string Barcode { get; set; }
        public decimal? OrderAmount { get; set; }
        public string OrderSerial { get; set; }
        public string OrderNumber { get; set; }
        public sbyte? Stats { get; set; }
        public DateTime? InsDatetime1 { get; set; }

        public Customer Customer { get; set; }
    }
}
