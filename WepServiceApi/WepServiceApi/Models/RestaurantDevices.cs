﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class RestaurantDevices
    {
        public int Id { get; set; }
        public DateTime? AuthorityDate { get; set; }
        public string MacAdress { get; set; }
        public int? UserId { get; set; }
        public string Exp { get; set; }
        public string Exp2 { get; set; }
    }
}
