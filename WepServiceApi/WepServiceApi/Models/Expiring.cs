﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class Expiring
    {
        public int Id { get; set; }
        public string ExpiryCode { get; set; }
        public string ExpiryDescription { get; set; }
        public int? DayOption { get; set; }
        public decimal? MonthInterest { get; set; }
        public string Exp1 { get; set; }
        public string Exp2 { get; set; }
        public string Exp3 { get; set; }
        public int? ExpiringDays { get; set; }
        public int? PricingId { get; set; }
        public string PricingName { get; set; }
    }
}
