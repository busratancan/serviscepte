﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class Currency
    {
        public Currency()
        {
            CurrencyRatesOfExchange = new HashSet<CurrencyRatesOfExchange>();
        }

        public int Id { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencySymbol { get; set; }
        public string TcmbFindText { get; set; }
        public string CurrencyName { get; set; }
        public string Exp1 { get; set; }
        public string Exp2 { get; set; }
        public string Exp3 { get; set; }

        public ICollection<CurrencyRatesOfExchange> CurrencyRatesOfExchange { get; set; }
    }
}
