﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class EmailParameters
    {
        public int Id { get; set; }
        public string ServerName { get; set; }
        public int? Port { get; set; }
        public string UserName { get; set; }
        public sbyte? ConnectionMethod { get; set; }
        public string Password { get; set; }
        public string FromExp { get; set; }
    }
}
