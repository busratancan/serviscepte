﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class Branch
    {
        public Branch()
        {
            PersonelCards = new HashSet<PersonelCards>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Defination { get; set; }
        public int? AuthorizedPersonelId { get; set; }
        public string AuthorizedPersonelName { get; set; }
        public string BrachAdress { get; set; }
        public string Exp1 { get; set; }
        public string Exp2 { get; set; }
        public sbyte? Deletion { get; set; }
        public int? DeletionUserId { get; set; }
        public DateTime? DeletionDateTime { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string MailAdress { get; set; }

        public PersonelCards AuthorizedPersonel { get; set; }
        public Management DeletionUser { get; set; }
        public ICollection<PersonelCards> PersonelCards { get; set; }
    }
}
