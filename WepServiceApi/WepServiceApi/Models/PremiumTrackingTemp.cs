﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class PremiumTrackingTemp
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public DateTime? ScanTime { get; set; }
        public int? ScanUser { get; set; }
    }
}
