﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ManagementRolesServiceRequestAuthorization
    {
        public int Id { get; set; }
        public int? ServiceRequestId { get; set; }
        public string ServiceRequestDefination { get; set; }
        public int? UserId { get; set; }
        public DateTime? InsDatetime { get; set; }
        public sbyte? Deletion { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int? DeletionUserId { get; set; }
        public int? ManagementRolesAuthorizationId { get; set; }

        public ManagementRolesAuthorization ManagementRolesAuthorization { get; set; }
    }
}
