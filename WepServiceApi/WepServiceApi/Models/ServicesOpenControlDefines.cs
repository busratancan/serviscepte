﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ServicesOpenControlDefines
    {
        public int Id { get; set; }
        public int? ServiceControlDefinesId { get; set; }
        public int? ServicesOpenId { get; set; }

        public ServiceControlDefines ServiceControlDefines { get; set; }
        public ServicesOpen ServicesOpen { get; set; }
    }
}
