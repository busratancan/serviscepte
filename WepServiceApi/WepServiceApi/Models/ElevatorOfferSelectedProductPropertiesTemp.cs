﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ElevatorOfferSelectedProductPropertiesTemp
    {
        public int Id { get; set; }
        public int? PropertyId { get; set; }
        public int? SubPropertyId { get; set; }
        public string PropertyName { get; set; }
        public string SubPropertyName { get; set; }
        public int? Basket { get; set; }
        public int? OfferGroupOrderId { get; set; }
        public sbyte? Deletion { get; set; }
        public int? TabOrder { get; set; }
        public sbyte? PropertiesType { get; set; }
    }
}
