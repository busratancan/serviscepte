﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class CustomerPersonelActivityTemp
    {
        public int Id { get; set; }
        public int? CustomerPersonelId { get; set; }
        public int? CustomerId { get; set; }
        public DateTime? Dateofentry { get; set; }
        public DateTime? Dateofdeparture { get; set; }
        public int? UserId { get; set; }
        public decimal? DailyAmount { get; set; }
        public decimal? ShiftAmount { get; set; }
    }
}
