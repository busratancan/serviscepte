﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class CurrencyOut
    {
        public int Id { get; set; }
        public int? CurrencyId { get; set; }
    }
}
