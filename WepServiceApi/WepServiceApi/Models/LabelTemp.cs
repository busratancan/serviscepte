﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class LabelTemp
    {
        public int Id { get; set; }
        public string Barcode { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public int? ShelfQty { get; set; }
        public int? TicketQty { get; set; }
        public int? Basket { get; set; }
        public int? ColorId { get; set; }
        public decimal? Price { get; set; }
        public sbyte? Ean { get; set; }
        public string CurrencySymbol { get; set; }
        public decimal? Weight { get; set; }
        public decimal? DiscountPrice { get; set; }
    }
}
