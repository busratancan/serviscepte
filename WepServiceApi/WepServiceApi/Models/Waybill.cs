﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class Waybill
    {
        public int Id { get; set; }
        public int? Type { get; set; }
        public string WaybillProductsBasket { get; set; }
        public DateTime? InsDateTime { get; set; }
        public DateTime? WaybillDateTime { get; set; }
        public int? UserId { get; set; }
        public int? CustomerId { get; set; }
        public string Barcode { get; set; }
        public decimal? WaybillAmount { get; set; }
        public string WaybillSerial { get; set; }
        public string WaybillNumber { get; set; }
        public sbyte? Stats { get; set; }
        public int? OrdersId { get; set; }
        public int? Deletion { get; set; }
    }
}
