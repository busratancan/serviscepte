﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class CustomerBank
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string AccountHolder { get; set; }
        public int BankId { get; set; }
        public string BankDepartment { get; set; }
        public string AccountNumber { get; set; }
        public string AdAccountNumber { get; set; }
        public string Iban { get; set; }
        public int? CustomerNumber { get; set; }
        public string DepartmentTel { get; set; }
        public string DepartmentContactPerson { get; set; }
        public string Exp { get; set; }
        public string Exp2 { get; set; }
        public string DepartmentFax { get; set; }
        public string DepartmentAdress { get; set; }
        public string DepartmentMail { get; set; }
    }
}
