﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class CustomerOffset
    {
        public int Id { get; set; }
        public int? CurrentInputId { get; set; }
        public int? CurrentOutputId { get; set; }
        public int? CustomerId { get; set; }

        public CustomerCurrentInput CurrentInput { get; set; }
        public CustomerCurrentOutput CurrentOutput { get; set; }
        public Customer Customer { get; set; }
    }
}
