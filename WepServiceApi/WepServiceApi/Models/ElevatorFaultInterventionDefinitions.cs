﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ElevatorFaultInterventionDefinitions
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Define { get; set; }
        public string Exp1 { get; set; }
        public string Exp2 { get; set; }
    }
}
