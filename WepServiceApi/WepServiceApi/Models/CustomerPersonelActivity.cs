﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class CustomerPersonelActivity
    {
        public int Id { get; set; }
        public int? CustomerPersonelId { get; set; }
        public int? CustomerId { get; set; }
        public DateTime? Dateofentry { get; set; }
        public DateTime? Dateofdeparture { get; set; }
        public int? UserId { get; set; }
        public decimal? DailyAmount { get; set; }
        public decimal? ShiftAmount { get; set; }
        public string HoursOfOvertime { get; set; }
        public int? DailyWorkingHours { get; set; }
        public int? DailyWorkingMinutes { get; set; }
        public int? DailyWorkingSecond { get; set; }
    }
}
