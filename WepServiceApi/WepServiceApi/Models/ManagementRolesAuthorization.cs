﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ManagementRolesAuthorization
    {
        public ManagementRolesAuthorization()
        {
            ManagementRolesServiceRequestAuthorization = new HashSet<ManagementRolesServiceRequestAuthorization>();
        }

        public int Id { get; set; }
        public int ManagementRolesId { get; set; }
        public sbyte? WinAuthority { get; set; }
        public sbyte? WinCustomerAuthority { get; set; }
        public string WinCustomerAccountType { get; set; }
        public sbyte? WinCustomerPersonelAuthority { get; set; }
        public sbyte? WinStockAuthority { get; set; }
        public sbyte? WinInstallmentAuthority { get; set; }
        public sbyte? WinOfferAuthority { get; set; }
        public sbyte? WinOrderAuthority { get; set; }
        public sbyte? WinWaybillAuthority { get; set; }
        public sbyte? WinInvoiceAuthority { get; set; }
        public sbyte? WinInvoiceAccessOnlySpeedSales { get; set; }
        public sbyte? WinSafeAuthority { get; set; }
        public sbyte? WinPosAuthority { get; set; }
        public sbyte? WinBankAuthority { get; set; }
        public sbyte? WinCheckCommercialPaperAuthority { get; set; }
        public sbyte? WinDocumentAuthority { get; set; }
        public sbyte? WinDepartmentAuthority { get; set; }
        public sbyte? WinTargetingAuthority { get; set; }
        public sbyte? WinCreditcardAuthority { get; set; }
        public sbyte? WinSmsAuthority { get; set; }
        public sbyte? WinIncomeAndExpenseAuthority { get; set; }
        public sbyte? WinCurrencyAuthority { get; set; }
        public sbyte? WinVehicleAuthority { get; set; }
        public sbyte? WinProducerReceiptAuthority { get; set; }
        public sbyte? WinServiceAuthority { get; set; }
        public sbyte? WinProductionAuthority { get; set; }
        public sbyte? WinFarmManagementAuthority { get; set; }
        public sbyte? WinAgriculturalAuthority { get; set; }
        public sbyte? WinEInvoiceAuthority { get; set; }
        public sbyte? WinRestaurantAuthority { get; set; }
        public sbyte? WinSettings { get; set; }
        public sbyte? ScAuthority { get; set; }
        public sbyte? ScAccessAllRequestList { get; set; }
        public sbyte? ScAccessCustomer { get; set; }
        public sbyte? ScAccessAccountExtract { get; set; }
        public sbyte? ScAccessCollection { get; set; }
        public sbyte? WinCustomerPersonelDebit { get; set; }
        public sbyte? RcAuthority { get; set; }
        public sbyte? RcTakeAccount { get; set; }
        public sbyte? RcCollection { get; set; }

        public ManagementRoles ManagementRoles { get; set; }
        public ICollection<ManagementRolesServiceRequestAuthorization> ManagementRolesServiceRequestAuthorization { get; set; }
    }
}
