﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class CurrencyRatesOfExchangeTemp
    {
        public int Id { get; set; }
        public int? CurrencyId { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencyCode { get; set; }
        public DateTime? ExDate { get; set; }
        public decimal? CurrencyBuying { get; set; }
        public decimal? CurrencySelling { get; set; }
        public int? Basket { get; set; }
        public decimal? CurrencyBuyingAmount { get; set; }
        public decimal? ReceivedCurrency { get; set; }
    }
}
