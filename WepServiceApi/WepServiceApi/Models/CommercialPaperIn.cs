﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class CommercialPaperIn
    {
        public int Id { get; set; }
        public decimal? Currency { get; set; }
        public DateTime? PayDate { get; set; }
        public sbyte? NotaryWarning { get; set; }
        public int? DocumentNumber { get; set; }
        public string Exp { get; set; }
        public int? CustomerId { get; set; }
        public string Type { get; set; }
        public sbyte? Status { get; set; }
        public DateTime? DeletionDateTime { get; set; }
        public int? DeletionUserId { get; set; }
        public sbyte? Deletion { get; set; }
        public DateTime? InsDate { get; set; }
        public DateTime? InsDateTime { get; set; }
        public sbyte? Reminder { get; set; }
        public int? ReminderUser { get; set; }
        public int? ExitCustomerId { get; set; }

        public Management ReminderUserNavigation { get; set; }
    }
}
