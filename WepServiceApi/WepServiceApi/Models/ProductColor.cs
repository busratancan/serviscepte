﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ProductColor
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public string ColorName { get; set; }
        public string Barcode { get; set; }
        public string Color1 { get; set; }
        public string Color2 { get; set; }
        public string Color3 { get; set; }
        public string Color4 { get; set; }
        public string Color5 { get; set; }
        public int? MainProductColorId { get; set; }
        public int? MainProductId { get; set; }
    }
}
