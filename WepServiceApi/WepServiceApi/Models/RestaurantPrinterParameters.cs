﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class RestaurantPrinterParameters
    {
        public int Id { get; set; }
        public int? ProductGroup3Id { get; set; }
        public string ProductGroup3Name { get; set; }
        public string PrinterName { get; set; }
        public int? DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public sbyte? Active { get; set; }
        public string PrinterIp { get; set; }
        public int? PrinterTestTimeOut { get; set; }
        public string AlternativePrinter { get; set; }

        public RestaurantDepartment Department { get; set; }
        public ProductGroups ProductGroup3 { get; set; }
    }
}
