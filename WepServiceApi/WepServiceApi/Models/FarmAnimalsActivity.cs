﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class FarmAnimalsActivity
    {
        public int Id { get; set; }
        public int? FarmAnimalsId { get; set; }
        public sbyte? Type { get; set; }
        public string Exp { get; set; }
        public DateTime? DateTime { get; set; }
        public decimal? Kg { get; set; }
        public decimal? Cm { get; set; }
        public string Exp2 { get; set; }
        public int? FarmVeterinaryApplicationsId { get; set; }
        public int? IfType3AnimalId { get; set; }
        public decimal? VaccineDose { get; set; }

        public FarmAnimals FarmAnimals { get; set; }
    }
}
