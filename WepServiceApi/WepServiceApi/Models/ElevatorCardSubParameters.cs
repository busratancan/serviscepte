﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ElevatorCardSubParameters
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Defination { get; set; }
        public string Exp { get; set; }
        public string Exp2 { get; set; }
        public int? ElevatorCardParametersId { get; set; }
        public int? ElevatorOrder { get; set; }
        public int? Type { get; set; }
        public int? TopDetailId { get; set; }
        public sbyte? Photo { get; set; }
        public sbyte? CustomText { get; set; }

        public ElevatorCardParameters ElevatorCardParameters { get; set; }
    }
}
