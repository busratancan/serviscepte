﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class Agricultural
    {
        public Agricultural()
        {
            AgriculturalCustomer = new HashSet<AgriculturalCustomer>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Defination { get; set; }
        public string Exp1 { get; set; }
        public string Exp2 { get; set; }
        public string Unit { get; set; }

        public ICollection<AgriculturalCustomer> AgriculturalCustomer { get; set; }
    }
}
