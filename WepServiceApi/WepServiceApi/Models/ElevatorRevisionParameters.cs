﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ElevatorRevisionParameters
    {
        public ElevatorRevisionParameters()
        {
            ElevatorRevisionOfferParametersTemp = new HashSet<ElevatorRevisionOfferParametersTemp>();
            ElevatorRevisionParametersProduct = new HashSet<ElevatorRevisionParametersProduct>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Defination { get; set; }
        public string OfferCaption { get; set; }
        public decimal? Fee { get; set; }
        public decimal? MaxDiscount { get; set; }
        public sbyte? OfferVisible { get; set; }
        public decimal? Cost { get; set; }
        public decimal? Workmanship { get; set; }

        public ICollection<ElevatorRevisionOfferParametersTemp> ElevatorRevisionOfferParametersTemp { get; set; }
        public ICollection<ElevatorRevisionParametersProduct> ElevatorRevisionParametersProduct { get; set; }
    }
}
