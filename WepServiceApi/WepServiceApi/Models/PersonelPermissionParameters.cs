﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class PersonelPermissionParameters
    {
        public int Id { get; set; }
        public sbyte? Year { get; set; }
        public sbyte? Day { get; set; }
        public sbyte? Deletion { get; set; }
    }
}
