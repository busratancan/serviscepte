﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class FormButtonDefines
    {
        public int Id { get; set; }
        public string FormName { get; set; }
        public string ButtonName { get; set; }
        public string ButtonCaption { get; set; }
        public string ShortCut { get; set; }
        public int? UserId { get; set; }
    }
}
