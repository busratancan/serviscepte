﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class FarmAnimals
    {
        public FarmAnimals()
        {
            FarmAnimalsActivity = new HashSet<FarmAnimalsActivity>();
        }

        public int Id { get; set; }
        public int? FarmsId { get; set; }
        public string FarmsName { get; set; }
        public int? FarmPaddockId { get; set; }
        public string FarmPaddockDefination { get; set; }
        public string Code { get; set; }
        public string SpecialCode { get; set; }
        public int? Mother { get; set; }
        public int? Father { get; set; }
        public string Exp { get; set; }
        public string Exp2 { get; set; }
        public string Type { get; set; }
        public DateTime? BirthDate { get; set; }
        public sbyte? Gender { get; set; }
        public sbyte? LiveStatus { get; set; }

        public ICollection<FarmAnimalsActivity> FarmAnimalsActivity { get; set; }
    }
}
