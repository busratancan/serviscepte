﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class Customer
    {
        public Customer()
        {
            AgriculturalCustomer = new HashSet<AgriculturalCustomer>();
            CustomerAdress = new HashSet<CustomerAdress>();
            CustomerCurrentInput = new HashSet<CustomerCurrentInput>();
            CustomerCurrentOutput = new HashSet<CustomerCurrentOutput>();
            CustomerOffset = new HashSet<CustomerOffset>();
            CustomerProducerReceiptTaxDefines = new HashSet<CustomerProducerReceiptTaxDefines>();
            CustomerSubscription = new HashSet<CustomerSubscription>();
            CustomerSubscriptionCurrent = new HashSet<CustomerSubscriptionCurrent>();
            Maintenance = new HashSet<Maintenance>();
            MobileScaleProductPurchase = new HashSet<MobileScaleProductPurchase>();
            Orders = new HashSet<Orders>();
            ServicesContract = new HashSet<ServicesContract>();
            ServicesOpen = new HashSet<ServicesOpen>();
            ServicesPointsBaseInformation = new HashSet<ServicesPointsBaseInformation>();
            SmsTaskList = new HashSet<SmsTaskList>();
            VehicleFuelExpense = new HashSet<VehicleFuelExpense>();
        }

        public int Id { get; set; }
        public int? MainCustomerId { get; set; }
        public int? MainCustomerIdTemp { get; set; }
        public string AccountTitle { get; set; }
        public string TaxOffice { get; set; }
        public string TaxNumber { get; set; }
        public string MobilePhone { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Adress { get; set; }
        public string Explanation { get; set; }
        public string AccountGrup { get; set; }
        public string AccountGroup2 { get; set; }
        public string AccountType { get; set; }
        public string CustomerCode { get; set; }
        public string ContactPerson { get; set; }
        public string Task { get; set; }
        public string Phone2 { get; set; }
        public string Adress2 { get; set; }
        public decimal? Risklimite { get; set; }
        public string PriceGroup { get; set; }
        public string Explanation2 { get; set; }
        public string Explanation3 { get; set; }
        public string CustomerPic { get; set; }
        public string Sector { get; set; }
        public string Region { get; set; }
        public byte[] CompanyLogo { get; set; }
        public sbyte? Ispersonel { get; set; }
        public string PrivateCode { get; set; }
        public string PrivateCode2 { get; set; }
        public string PrivateCode3 { get; set; }
        public string PrivateCode4 { get; set; }
        public string PrivateCode5 { get; set; }
        public string PrivateCode6 { get; set; }
        public string PrivateCode7 { get; set; }
        public string PrivateCode8 { get; set; }
        public string PrivateCode9 { get; set; }
        public string PrivateCode10 { get; set; }
        public string PrivateCode11 { get; set; }
        public string PrivateCode12 { get; set; }
        public string PrivateCode13 { get; set; }
        public string PrivateCode14 { get; set; }
        public string PrivateCode15 { get; set; }
        public string PrivateCode16 { get; set; }
        public string PrivateCode17 { get; set; }
        public string PrivateCode18 { get; set; }
        public string PrivateCode19 { get; set; }
        public string PrivateCode20 { get; set; }
        public int? PricingId { get; set; }
        public string PricingName { get; set; }
        public sbyte? PricePrivileged { get; set; }
        public string InformationNotes { get; set; }
        public byte[] Picture { get; set; }
        public string SalesBlock { get; set; }
        public int? RouteId { get; set; }
        public int? RouteOrder { get; set; }
        public sbyte? Visible { get; set; }
        public int? CurrentInputId { get; set; }
        public int? CurrentOutputId { get; set; }
        public decimal? FixDiscount { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string SalesInvoiceGrossPrice { get; set; }
        public string SalesInvoiceRetailPrice { get; set; }
        public sbyte? ElevatorFault { get; set; }
        public int? WithholdingRate { get; set; }
        public string MailAdress { get; set; }

        public PersonelCards PersonelCards { get; set; }
        public TempBasket TempBasket { get; set; }
        public ICollection<AgriculturalCustomer> AgriculturalCustomer { get; set; }
        public ICollection<CustomerAdress> CustomerAdress { get; set; }
        public ICollection<CustomerCurrentInput> CustomerCurrentInput { get; set; }
        public ICollection<CustomerCurrentOutput> CustomerCurrentOutput { get; set; }
        public ICollection<CustomerOffset> CustomerOffset { get; set; }
        public ICollection<CustomerProducerReceiptTaxDefines> CustomerProducerReceiptTaxDefines { get; set; }
        public ICollection<CustomerSubscription> CustomerSubscription { get; set; }
        public ICollection<CustomerSubscriptionCurrent> CustomerSubscriptionCurrent { get; set; }
        public ICollection<Maintenance> Maintenance { get; set; }
        public ICollection<MobileScaleProductPurchase> MobileScaleProductPurchase { get; set; }
        public ICollection<Orders> Orders { get; set; }
        public ICollection<ServicesContract> ServicesContract { get; set; }
        public ICollection<ServicesOpen> ServicesOpen { get; set; }
        public ICollection<ServicesPointsBaseInformation> ServicesPointsBaseInformation { get; set; }
        public ICollection<SmsTaskList> SmsTaskList { get; set; }
        public ICollection<VehicleFuelExpense> VehicleFuelExpense { get; set; }
    }
}
