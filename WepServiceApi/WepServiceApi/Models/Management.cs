﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WepServiceApi.Models
{
    public partial class Management
    {
        public Management()
        { 
            Branch = new HashSet<Branch>();
            CheckIn = new HashSet<CheckIn>();
            CheckOutput = new HashSet<CheckOutput>();
            CommercialPaperIn = new HashSet<CommercialPaperIn>();
            CommercialPaperOut = new HashSet<CommercialPaperOut>();
            ElevatorRevisionOfferParameters = new HashSet<ElevatorRevisionOfferParameters>();
            Parameters = new HashSet<Parameters>();
            PersonelCardsStartingEnding = new HashSet<PersonelCardsStartingEnding>();
            PersonelPermissionCurrent = new HashSet<PersonelPermissionCurrent>();
            RestaurantOrderList = new HashSet<RestaurantOrderList>();
            ServicesOpenActivity = new HashSet<ServicesOpenActivity>();
            ServicesOpenClose = new HashSet<ServicesOpenClose>();
            ServicesSpeedButtons = new HashSet<ServicesSpeedButtons>();
            Vehicle = new HashSet<Vehicle>();
            VehicleProductExpense = new HashSet<VehicleProductExpense>();
            VehicleTireExpense = new HashSet<VehicleTireExpense>();
        }

        internal static DateTime GetOrdinal(string v)
        {
            throw new NotImplementedException();
        }
        //[BindProperty]
        public int Id { get; set; }
        [Column("user_name")]
        public string UserName { get; set; }
        public string Mail { get; set; }
        [Column("password")]
        public string Password { get; set; }
        public int? UserType { get; set; }
        public int? DefaultSafe { get; set; }
        public string CurrencyType { get; set; }
        public string SafeName { get; set; }
        public string Pass2 { get; set; }
        public string Skin { get; set; }
        public string InvoiceSerial { get; set; }
        public string TaxInvoiceCheckBox { get; set; }    
        public sbyte? DefaultStorage { get; set; }
        public string DefaultStorageName { get; set; }     
        public sbyte? ElevatorAllPrivileges { get; set; }
        public string CustomerVisible { get; set; }
        public string Veterinary { get; set; }  
        public sbyte? NoCustomerCurrent { get; set; }
        public sbyte? NoCustomerCards { get; set; }
        public int? InvoiceSerialNumber { get; set; }
        public sbyte? Active { get; set; }
        public int ManagementRolesId { get; set; }
        public string ManagementRolesRoleDefination { get; set; }
        public int? UserRole { get; set; }
        public int? DefaultPos { get; set; }
        public string DefaultPosName { get; set; }

        public Pos DefaultPosNavigation { get; set; }
        public Safe DefaultSafeNavigation { get; set; }
        public TempBasket TempBasket { get; set; }
        public ICollection<Branch> Branch { get; set; }
        public ICollection<CheckIn> CheckIn { get; set; }
        public ICollection<CheckOutput> CheckOutput { get; set; }
        public ICollection<CommercialPaperIn> CommercialPaperIn { get; set; }
        public ICollection<CommercialPaperOut> CommercialPaperOut { get; set; }
        public ICollection<ElevatorRevisionOfferParameters> ElevatorRevisionOfferParameters { get; set; }
        public ICollection<Parameters> Parameters { get; set; }
        public ICollection<PersonelCardsStartingEnding> PersonelCardsStartingEnding { get; set; }
        public ICollection<PersonelPermissionCurrent> PersonelPermissionCurrent { get; set; }
        public ICollection<RestaurantOrderList> RestaurantOrderList { get; set; }
        public ICollection<ServicesOpenActivity> ServicesOpenActivity { get; set; }
        public ICollection<ServicesOpenClose> ServicesOpenClose { get; set; }
        public ICollection<ServicesSpeedButtons> ServicesSpeedButtons { get; set; }
        public ICollection<Vehicle> Vehicle { get; set; }
        public ICollection<VehicleProductExpense> VehicleProductExpense { get; set; }
        public ICollection<VehicleTireExpense> VehicleTireExpense { get; set; }
    }
}
