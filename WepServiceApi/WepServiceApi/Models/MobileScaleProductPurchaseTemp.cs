﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class MobileScaleProductPurchaseTemp
    {
        public int Id { get; set; }
        public int? MobileScaleCustomerId { get; set; }
        public int? CustomerId { get; set; }
        public decimal? Kg { get; set; }
        public decimal? Litre { get; set; }
        public decimal? Densities { get; set; }
        public int? Basket { get; set; }
        public DateTime? MilkingTime { get; set; }
        public DateTime? PickupTime { get; set; }
        public int? MobileScaleUserId { get; set; }
        public int? UserId { get; set; }
        public int? MobileScaleProductPurchaseId { get; set; }
        public int? Stats { get; set; }
        public int? DeviceId { get; set; }
        public int? VerificationNumber { get; set; }
    }
}
