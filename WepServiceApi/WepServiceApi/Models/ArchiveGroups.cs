﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ArchiveGroups
    {
        public int Id { get; set; }
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        public string GroupExplanation { get; set; }
        public string ForCustomer { get; set; }
        public string ForPersonel { get; set; }
        public string ForVehicle { get; set; }
    }
}
