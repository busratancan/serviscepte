﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class LoadDocumentDefinitions
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Defination { get; set; }
        public string Color { get; set; }
        public string Exp { get; set; }
        public sbyte? Deletion { get; set; }
    }
}
