﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class InvoiceShipmentTraceTemp
    {
        public int Id { get; set; }
        public int? InvoiceId { get; set; }
        public int? ProductId { get; set; }
        public int? StorageId { get; set; }
        public int? Basket { get; set; }
        public string ProductName { get; set; }
        public decimal? Qty { get; set; }
        public string StorageName { get; set; }
    }
}
