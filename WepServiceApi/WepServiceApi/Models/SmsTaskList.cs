﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class SmsTaskList
    {
        public SmsTaskList()
        {
            SmsBalance = new HashSet<SmsBalance>();
        }

        public int Id { get; set; }
        public string Message { get; set; }
        public string Number { get; set; }
        public int? CustomerId { get; set; }
        public DateTime? SendDate { get; set; }
        public sbyte? Status { get; set; }
        public int? BasketId { get; set; }
        public sbyte? Type { get; set; }
        public string Referance { get; set; }
        public string KcReferance { get; set; }

        public Customer Customer { get; set; }
        public ICollection<SmsBalance> SmsBalance { get; set; }
    }
}
