﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ElevatorRevisionParametersProduct
    {
        public int Id { get; set; }
        public int? ElevatorRevisionParametersId { get; set; }
        public int? ProductId { get; set; }
        public int? ProductQty { get; set; }

        public ElevatorRevisionParameters ElevatorRevisionParameters { get; set; }
    }
}
