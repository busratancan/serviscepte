﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class PersonelMeterialDebit
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public int? PersonelCardsId { get; set; }
        public int? Qty { get; set; }
        public string Explanation { get; set; }
        public sbyte? Deletion { get; set; }
        public DateTime? InsDateTime { get; set; }

        public PersonelCards PersonelCards { get; set; }
        public Product Product { get; set; }
    }
}
