﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class CustomerSubscriptionDetail
    {
        public int Id { get; set; }
        public int? CustomerSubscriptionId { get; set; }
        public DateTime? ContractDate { get; set; }
        public DateTime? SubscriptionStartDate { get; set; }
        public sbyte? PayDay { get; set; }
        public sbyte? OptionDay { get; set; }
        public string Exp { get; set; }
        public string Exp2 { get; set; }
        public string Exp3 { get; set; }

        public CustomerSubscription CustomerSubscription { get; set; }
    }
}
