﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class CustomerCorrespondenceReportFiles
    {
        public int Id { get; set; }
        public string Exp { get; set; }
        public string FileName { get; set; }
        public sbyte? Deletion { get; set; }
    }
}
