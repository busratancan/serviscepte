﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class WaybillProductsSub
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public decimal? Qty { get; set; }
        public int? Basket { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string Barcode { get; set; }
        public string UnitName { get; set; }
        public decimal? GroupQty { get; set; }
        public int? WaybillProductsBasketId { get; set; }
        public int? ProductType { get; set; }
    }
}
