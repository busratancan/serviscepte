﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class FarmFeedActivity
    {
        public int Id { get; set; }
        public int? PaddockId { get; set; }
        public string PaddockName { get; set; }
        public DateTime? DateTime { get; set; }
        public int? FarmFeedMixtureId { get; set; }
        public string FarmFeedMixtureName { get; set; }
    }
}
