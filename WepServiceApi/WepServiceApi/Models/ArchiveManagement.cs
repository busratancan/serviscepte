﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ArchiveManagement
    {
        public int Id { get; set; }
        public byte[] Document { get; set; }
        public sbyte? DocType { get; set; }
        public int? ArcihiveGroupsId { get; set; }
        public int? CustomerId { get; set; }
        public string DocExplanation { get; set; }
        public DateTime? InsertDatetime { get; set; }
        public int? UserId { get; set; }
        public sbyte? ArciveType { get; set; }
        public sbyte? Deletion { get; set; }
        public string DocSerial { get; set; }
        public int? DocNumner { get; set; }
    }
}
