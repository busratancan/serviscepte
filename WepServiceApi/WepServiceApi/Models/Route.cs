﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class Route
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Definition { get; set; }
        public string Responsible { get; set; }
        public int? ResponsibleId { get; set; }
        public string Exp { get; set; }
        public string Exp2 { get; set; }
        public string RouteDay { get; set; }
    }
}
