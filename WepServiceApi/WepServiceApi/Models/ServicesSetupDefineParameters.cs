﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ServicesSetupDefineParameters
    {
        public int Id { get; set; }
        public int? ServicesSetupDefinesId { get; set; }
        public string Code { get; set; }
        public string Defination { get; set; }
        public int? IntegerIn { get; set; }
        public int? DecimalIn { get; set; }
        public int? TextIn { get; set; }
        public sbyte? Deletion { get; set; }

        public ServicesSetupDefines ServicesSetupDefines { get; set; }
    }
}
