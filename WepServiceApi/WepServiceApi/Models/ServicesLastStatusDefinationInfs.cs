﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ServicesLastStatusDefinationInfs
    {
        public ServicesLastStatusDefinationInfs()
        {
            ServicesOpenCloseParameters = new HashSet<ServicesOpenCloseParameters>();
        }

        public int Id { get; set; }
        public string Caption { get; set; }
        public sbyte? TextIn { get; set; }
        public sbyte? NumericIn { get; set; }
        public sbyte? IntegerIn { get; set; }
        public sbyte? Deletion { get; set; }
        public int? ServicesLastStatusDefinationId { get; set; }
        public sbyte? PersonelIn { get; set; }
        public sbyte? DateIn { get; set; }
        public sbyte? ForceEntery { get; set; }
        public sbyte? OrderNumber { get; set; }

        public ServicesLastStatusDefination ServicesLastStatusDefination { get; set; }
        public ICollection<ServicesOpenCloseParameters> ServicesOpenCloseParameters { get; set; }
    }
}
