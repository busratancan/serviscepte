﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class CustomerSubscription
    {
        public CustomerSubscription()
        {
            CustomerSubscriptionDetail = new HashSet<CustomerSubscriptionDetail>();
        }

        public int Id { get; set; }
        public int? SubscriptionId { get; set; }
        public int? CustomerId { get; set; }
        public sbyte? Stats { get; set; }
        public decimal? CustomerPrice { get; set; }

        public Customer Customer { get; set; }
        public SubscriptionGroups Subscription { get; set; }
        public ICollection<CustomerSubscriptionDetail> CustomerSubscriptionDetail { get; set; }
    }
}
