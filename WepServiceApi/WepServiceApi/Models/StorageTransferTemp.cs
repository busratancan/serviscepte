﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class StorageTransferTemp
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal? Qty { get; set; }
        public int? Basket { get; set; }
        public int? UserId { get; set; }
        public DateTime? DateTime { get; set; }
        public string Barcode { get; set; }
        public int? ProductType { get; set; }
        public int? SetQty { get; set; }
        public int? MainProductId { get; set; }
    }
}
