﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ServicesSetupDefinesFunctions
    {
        public int Id { get; set; }
        public string GroupName { get; set; }
        public string FunctionName { get; set; }
        public string FunctionExp { get; set; }
        public string Syntax { get; set; }
        public sbyte? Deletion { get; set; }
    }
}
