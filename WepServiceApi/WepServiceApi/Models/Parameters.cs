﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class Parameters
    {
        public int Id { get; set; }
        public int? CustomerCode { get; set; }
        public string ScaleUseOnFrmSpeedRetailSales { get; set; }
        public int? UserId { get; set; }
        public string ChaneUseOnFrmSpeedRetailSales { get; set; }
        public string VisibleProductGridOnFrmSpeedRetailSales { get; set; }
        public string UpdatePricesAlwaysOnFrmSpeedRetailSales { get; set; }
        public string FrmInvoiceCustomerList { get; set; }
        public string FrmInvoiceProductList { get; set; }
        public string AutoPrintTicketOnFrmSpeedRetailSales { get; set; }
        public string FrmProductStoreModule { get; set; }
        public string FrmProductStationeryModule { get; set; }
        public string FrmProductMainStockModule { get; set; }
        public string FrmMainBackgroundImage { get; set; }
        public string FrmCustomerCurrentSendSmsOnCurrIn { get; set; }
        public string FrmCustomerCurrentSendSmsOnCurrOut { get; set; }
        public string FrmInvoicePreviewInvoice { get; set; }
        public string FrmProductEan13 { get; set; }
        public string FrmInvoiceCheckedReceipt { get; set; }
        public string FrmSpeedRetailSalesPrintDialog { get; set; }
        public string FrmInvoiceUnitPriceIsRetailPrice { get; set; }
        public string FrmCheckCpEntryPayWarningOption { get; set; }
        public string FrmCheckCpEntryOptionTime { get; set; }
        public string FrmInvoiceQtyDialog { get; set; }
        public string FrmInvoicePriceDiaolog { get; set; }
        public string FrmStorafeTransferPrintDialog { get; set; }
        public string FrmCustomerCardBL11 { get; set; }
        public string FrmCustomerCardBL12 { get; set; }
        public string FrmCustomerCardBL21 { get; set; }
        public string FrmCustomerCardBL22 { get; set; }
        public string FrmCustomerCardBL31 { get; set; }
        public string FrmCustomerCardBL32 { get; set; }
        public string FrmCustomerCardBL1C { get; set; }
        public string FrmCustomerCardBL2C { get; set; }
        public string FrmCustomerCardBL3C { get; set; }
        public string FrmCustomerCardAL11 { get; set; }
        public string FrmCustomerCardAL12 { get; set; }
        public string FrmCustomerCardAL21 { get; set; }
        public string FrmCustomerCardAL22 { get; set; }
        public string FrmCustomerCardAL31 { get; set; }
        public string FrmCustomerCardAL32 { get; set; }
        public string FrmCustomerCardAL1C { get; set; }
        public string FrmCustomerCardAL2C { get; set; }
        public string FrmCustomerCardAL3C { get; set; }
        public string FrmInvPreviewCP { get; set; }
        public string FrmInvoiceShippingDocument { get; set; }
        public decimal? FrmInvoiceCargoPayLimit { get; set; }
        public string FrmProductCardOrderCode { get; set; }
        public string FrmInvoiceSelectPayMethod { get; set; }
        public string FrmProductCardPieceTrace { get; set; }
        public string FrmCustomerCardOrderByCode { get; set; }
        public string FrmInvoiceTraceShipment { get; set; }
        public string FrmInvoiceTraceStockCountingViaShipment { get; set; }
        public string FrmInvoiceProductGridAutoProductInsert { get; set; }
        public int? FrmInvoiceDefaultTaxRate { get; set; }
        public int? FrmInvoiceInvoiceRowCount { get; set; }
        public string FrmProductShelfTrack { get; set; }
        public string FrmProductCardOrderByProductName { get; set; }
        public string ElevatorMobileSendSms { get; set; }
        public string ElevatorSenderTitle { get; set; }
        public string FrmInvoiceSpeedInsert { get; set; }
        public string FrmInvoiceUpdatePurchasePrice { get; set; }
        public string FrmInvoiceUpdatePersonelPrice { get; set; }
        public decimal? FrmInvoicePersonelPriceDiscountRate { get; set; }
        public sbyte? FrmFarmDayPregnant { get; set; }
        public string FrmProductCardSerialsMonitor { get; set; }
        public string FrmSpeedRetailSalesAskProductPriceChange { get; set; }
        public string FocusColor { get; set; }
        public string FrmSpeedSalesIfBarcodeNotOnlySound { get; set; }
        public string FrmProductAutoCalcRetailPrice { get; set; }

        public Management User { get; set; }
    }
}
