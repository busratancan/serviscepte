﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class AgriculturalCustomer
    {
        public int Id { get; set; }
        public DateTime? AppDate { get; set; }
        public int? CustomerId { get; set; }
        public string AgriculturalName { get; set; }
        public int? AgriculturalId { get; set; }
        public decimal? UnitFactor { get; set; }

        public Agricultural Agricultural { get; set; }
        public Customer Customer { get; set; }
    }
}
