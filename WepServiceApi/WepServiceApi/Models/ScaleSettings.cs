﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ScaleSettings
    {
        public int Id { get; set; }
        public int? BarcodePrefix { get; set; }
        public int? BarcodeLength { get; set; }
        public string AmountLength { get; set; }
    }
}
