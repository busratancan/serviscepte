﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class PosOut
    {
        public int Id { get; set; }
        public int? PosId { get; set; }
        public int? CustomerId { get; set; }
        public string Exp { get; set; }
        public decimal? Currency { get; set; }
        public DateTime? Datetime { get; set; }
        public int? UserId { get; set; }
        public sbyte? Type { get; set; }
        public sbyte? Deletion { get; set; }
        public DateTime? DeletionDateTime { get; set; }
        public int? DeletionUserId { get; set; }
        public string Type1 { get; set; }

        public Pos Pos { get; set; }
    }
}
