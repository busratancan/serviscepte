﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class Basket
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public int? CustomerId { get; set; }
        public int? BasketId { get; set; }
        public decimal? UnitPrice { get; set; }
    }
}
