﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class CheckTemp
    {
        public int Id { get; set; }
        public int? BankId { get; set; }
        public decimal? Currency { get; set; }
        public int? CheckNumber { get; set; }
        public int? CheckBankAccount { get; set; }
        public string Exp { get; set; }
        public DateTime? PayDate { get; set; }
        public int? UserId { get; set; }
        public string BankName { get; set; }
        public int? CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string OpType { get; set; }
        public int? CheckBankAccountId { get; set; }
    }
}
