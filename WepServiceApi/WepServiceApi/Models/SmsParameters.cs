﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class SmsParameters
    {
        public int Id { get; set; }
        public string SmsTitle { get; set; }
        public int? DefaultMaintenanceSmsTemplateId { get; set; }
        public string AfterMaintenanceSendSms { get; set; }
        public string ShUserName { get; set; }
        public string ShPassword { get; set; }
    }
}
