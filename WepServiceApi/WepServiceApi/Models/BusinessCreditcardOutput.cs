﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class BusinessCreditcardOutput
    {
        public int Id { get; set; }
        public int? BusinessCreditcardId { get; set; }
        public string Exp { get; set; }
    }
}
