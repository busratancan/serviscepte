﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class FarmFeedMixture
    {
        public FarmFeedMixture()
        {
            FarmFeedMixtureProducts = new HashSet<FarmFeedMixtureProducts>();
        }

        public int Id { get; set; }
        public string MixtureCode { get; set; }
        public string MixtureName { get; set; }
        public int? FarmPaddockId { get; set; }
        public string FarmPaddockName { get; set; }
        public string Exp { get; set; }
        public string Exp2 { get; set; }
        public sbyte? FeedType { get; set; }

        public ICollection<FarmFeedMixtureProducts> FarmFeedMixtureProducts { get; set; }
    }
}
