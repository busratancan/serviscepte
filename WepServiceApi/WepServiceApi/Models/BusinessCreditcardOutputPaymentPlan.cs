﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class BusinessCreditcardOutputPaymentPlan
    {
        public int Id { get; set; }
        public int? NumberOfInstallments { get; set; }
        public string Exp { get; set; }
        public int? BusinessCreditcardId { get; set; }
        public DateTime? TheLastPaymentDate { get; set; }
        public int? BasketId { get; set; }
        public decimal? InstallmentsCurrency { get; set; }
    }
}
