﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class StockActivities
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public sbyte? Type { get; set; }
        public decimal? Qty { get; set; }
        public int? DetailId { get; set; }
        public int? UserId { get; set; }
        public DateTime? Datetime { get; set; }
        public int? Storage { get; set; }
        public sbyte? Deletion { get; set; }
        public DateTime? DeletionDateTime { get; set; }
        public int? DeletionUserId { get; set; }
        public string StorageTransferBasketId { get; set; }
        public string DocSeri { get; set; }
        public string DocNum { get; set; }
        public int? ColorId { get; set; }

        public Product Product { get; set; }
        public Storage StorageNavigation { get; set; }
    }
}
