﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ProductNutritionalValues
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public decimal? DryMatter { get; set; }
        public decimal? MetabolicEnergy { get; set; }
        public decimal? CrudeProtein { get; set; }
        public decimal? CrudeFiber { get; set; }
        public decimal? Ash { get; set; }
        public decimal? Hcl { get; set; }
        public decimal? CrudeOil { get; set; }

        public Product Product { get; set; }
    }
}
