﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class StockCountingSubMain
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public decimal? Deficieny { get; set; }
        public decimal? Excess { get; set; }
        public decimal? Equality { get; set; }
        public int? StockCountingBasket { get; set; }
        public int? UserId { get; set; }
        public DateTime? Datetime { get; set; }
        public int? StorageId { get; set; }
        public sbyte? ProductType { get; set; }
    }
}
