﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ServicesOpenControlDefinesTemp
    {
        public int Id { get; set; }
        public int? BasketId { get; set; }
        public int? ServiceControlDefinesId { get; set; }
        public string ControlDefine { get; set; }
        public string ModuleDefination { get; set; }

        public ServiceControlDefines ServiceControlDefines { get; set; }
    }
}
