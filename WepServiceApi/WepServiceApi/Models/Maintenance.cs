﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class Maintenance
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public decimal? Fee { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? FinishDate { get; set; }
        public DateTime? InsDate { get; set; }
        public int? ElevatorCount { get; set; }

        public Customer Customer { get; set; }
    }
}
