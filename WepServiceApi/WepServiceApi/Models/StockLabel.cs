﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class StockLabel
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public string Barcode { get; set; }
        public string ProductName { get; set; }
    }
}
