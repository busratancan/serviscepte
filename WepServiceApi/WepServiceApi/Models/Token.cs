﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class Token
    {
        public int Id { get; set; }
        public string AccesKey { get; set; }
        public string Value { get; set; }
    }
}
