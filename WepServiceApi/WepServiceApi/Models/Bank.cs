﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class Bank
    {
        public Bank()
        {
            BankAccount = new HashSet<BankAccount>();
        }

        public int Id { get; set; }
        public string BankName { get; set; }
        public string BankExplain { get; set; }

        public ICollection<BankAccount> BankAccount { get; set; }
    }
}
