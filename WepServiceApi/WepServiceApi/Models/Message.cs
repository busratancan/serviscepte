﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class Message
    {
        public int Id { get; set; }
        public int? SenderId { get; set; }
        public int? Receiver { get; set; }
        public sbyte? Status { get; set; }
        public string Message1 { get; set; }
        public int? SupportId { get; set; }
        public DateTime? DateTime { get; set; }
    }
}
