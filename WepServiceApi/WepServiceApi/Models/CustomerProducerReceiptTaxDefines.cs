﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class CustomerProducerReceiptTaxDefines
    {
        public int Id { get; set; }
        public int? ProducerReceiptTaxDefinesId { get; set; }
        public int? CustomerId { get; set; }
        public string TaxName { get; set; }
        public sbyte? Exempt { get; set; }
        public sbyte? Discount { get; set; }

        public Customer Customer { get; set; }
    }
}
