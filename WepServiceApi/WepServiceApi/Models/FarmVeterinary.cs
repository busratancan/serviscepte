﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class FarmVeterinary
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public string AccountTitle { get; set; }
        public string VeterinaryCode { get; set; }
        public string Exp1 { get; set; }
        public string Exp2 { get; set; }
    }
}
