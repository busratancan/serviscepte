﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ElevatorInstallationList
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public int? OfferTempId { get; set; }
        public string ContractDate { get; set; }
        public string StatsType { get; set; }
        public int? ResponsibleUserId { get; set; }
        public int? ContractorUserId { get; set; }
        public string Notes { get; set; }
    }
}
