﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class SpeedSalesButtons
    {
        public int Id { get; set; }
        public string ButtonName { get; set; }
        public int? ButtonTag { get; set; }
        public string ButtonCaption { get; set; }
        public string Img { get; set; }
        public int? UserId { get; set; }
        public string ProductCaption { get; set; }
        public sbyte? UseJpeg { get; set; }
    }
}
