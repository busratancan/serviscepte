﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ServicesCategorySubDefinition
    {
        public ServicesCategorySubDefinition()
        {
            ServiceControlDefines = new HashSet<ServiceControlDefines>();
        }

        public int Id { get; set; }
        public string Defination { get; set; }
        public string Code { get; set; }
        public string Color { get; set; }
        public int? Order { get; set; }
        public string Exp { get; set; }
        public int? ServicesCategoryDefinitionId { get; set; }
        public sbyte? Active { get; set; }
        public sbyte? ForceQrCode { get; set; }
        public sbyte? ServiceTimeOutMinutes { get; set; }
        public sbyte? ForceAskPieceChange { get; set; }

        public ServicesCategoryDefinition ServicesCategoryDefinition { get; set; }
        public ICollection<ServiceControlDefines> ServiceControlDefines { get; set; }
    }
}
