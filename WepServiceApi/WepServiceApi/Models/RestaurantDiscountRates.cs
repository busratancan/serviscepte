﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class RestaurantDiscountRates
    {
        public int Id { get; set; }
        public string Exp { get; set; }
        public decimal? Rate { get; set; }
        public int? UserId { get; set; }
    }
}
