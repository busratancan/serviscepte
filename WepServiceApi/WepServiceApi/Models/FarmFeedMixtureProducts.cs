﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class FarmFeedMixtureProducts
    {
        public int Id { get; set; }
        public decimal? Qty { get; set; }
        public decimal? QtyOfMeal { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public int? FarmFeedMixtureId { get; set; }

        public FarmFeedMixture FarmFeedMixture { get; set; }
    }
}
