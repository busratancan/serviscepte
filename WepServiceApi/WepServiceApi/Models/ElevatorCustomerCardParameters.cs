﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ElevatorCustomerCardParameters
    {
        public int Id { get; set; }
        public int? ElevatorCardParametersId { get; set; }
        public int? ElevatorCardSubParametersId { get; set; }
        public sbyte? Type { get; set; }
        public int? CustomerId { get; set; }
        public int? ElevatorCardParametersIdForType2 { get; set; }
        public DateTime? InsTime { get; set; }
        public int? UserId { get; set; }
        public int? ElevatorOrder { get; set; }
        public byte[] Picture { get; set; }
        public string OtherDefinition { get; set; }
        public int? ServicePointsBaseInformationId { get; set; }
        public sbyte? Deletion { get; set; }
        public int? DeletionUserId { get; set; }
        public DateTime? DeletionDateTime { get; set; }

        public ServicesPointsBaseInformation ServicePointsBaseInformation { get; set; }
    }
}
