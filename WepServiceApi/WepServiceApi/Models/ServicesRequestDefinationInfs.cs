﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ServicesRequestDefinationInfs
    {
        public ServicesRequestDefinationInfs()
        {
            ServicesOpenParameters = new HashSet<ServicesOpenParameters>();
        }

        public int Id { get; set; }
        public string Caption { get; set; }
        public sbyte? TextIn { get; set; }
        public sbyte? NumericIn { get; set; }
        public sbyte? IntegerIn { get; set; }
        public sbyte? Deletion { get; set; }
        public int? ServicesRequestDefinationId { get; set; }
        public sbyte? PersonelIn { get; set; }
        public sbyte? DateIn { get; set; }
        public sbyte? ForceEntery { get; set; }
        public sbyte? OrderNumber { get; set; }

        public ServicesRequestDefination ServicesRequestDefination { get; set; }
        public ICollection<ServicesOpenParameters> ServicesOpenParameters { get; set; }
    }
}
