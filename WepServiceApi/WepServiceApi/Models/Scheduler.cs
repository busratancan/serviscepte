﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class Scheduler
    {
        public int Id { get; set; }
        public string Caption { get; set; }
        public int? EventType { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? Finish { get; set; }
        public string Location { get; set; }
        public string Message { get; set; }
        public int? Options { get; set; }
        public int? CustomerId { get; set; }
        public int? LabelColour { get; set; }
    }
}
