﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class PosDetail
    {
        public int Id { get; set; }
        public int? NumberOfInstallments { get; set; }
        public string Exp { get; set; }
        public decimal? BankCommission { get; set; }
        public decimal? CustomerCommission { get; set; }
        public int? PosId { get; set; }

        public Pos Pos { get; set; }
    }
}
