﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class SubscriptionGroups
    {
        public SubscriptionGroups()
        {
            CustomerSubscription = new HashSet<CustomerSubscription>();
        }

        public int SubscriptionId { get; set; }
        public string SubscriptionCode { get; set; }
        public string SubscriptionDefinition { get; set; }
        public string SubscriptionGroup { get; set; }
        public decimal? SubscriptionFee { get; set; }
        public string Exp { get; set; }
        public string Exp2 { get; set; }
        public string Exp3 { get; set; }
        public sbyte? Repetition { get; set; }

        public ICollection<CustomerSubscription> CustomerSubscription { get; set; }
    }
}
