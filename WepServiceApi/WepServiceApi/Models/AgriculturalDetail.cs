﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class AgriculturalDetail
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public decimal? FormulaQty { get; set; }
        public string ProductName { get; set; }
        public int? AgriculturalId { get; set; }
        public sbyte? Days { get; set; }
    }
}
