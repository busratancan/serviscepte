﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class CustomerCreditcard
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int? BankId { get; set; }
        public string CardNo { get; set; }
        public string ExpDate { get; set; }
        public string SecurityCode { get; set; }
        public string CardHolder { get; set; }
        public sbyte? CardType { get; set; }
        public decimal? CardFee { get; set; }
        public string Exp { get; set; }
        public string Exp2 { get; set; }
        public string CampaignNotes { get; set; }
        public string InstallmentNotes { get; set; }
        public decimal? MinPayment { get; set; }
        public decimal? InterestRate { get; set; }
    }
}
