﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class RestaurantPremiumCalc
    {
        public int Id { get; set; }
        public int? RestaurantPremiumCalcTempBasketId { get; set; }
        public DateTime? InsDate { get; set; }
        public string Exp { get; set; }
    }
}
