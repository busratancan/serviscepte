﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class Product2
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string ProductType { get; set; }
        public string Mark { get; set; }
        public string Unit { get; set; }
        public string MaxDiscount { get; set; }
        public string CriticalLevel { get; set; }
        public decimal? Price { get; set; }
        public decimal? PriceTaxInc { get; set; }
        public string PackagingStatus { get; set; }
        public string MaxLevel { get; set; }
        public string Barcode { get; set; }
        public string ProductCategory { get; set; }
        public string ProductSubCategories { get; set; }
        public string Explanation { get; set; }

        public TempBasket TempBasket { get; set; }
    }
}
