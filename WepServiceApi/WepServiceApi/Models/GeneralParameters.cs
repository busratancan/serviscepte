﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class GeneralParameters
    {
        public int Id { get; set; }
        public sbyte? AskPasswordOnAccesPersonelCard { get; set; }
        public string AccessPersonelCardPassword { get; set; }
        public byte[] ServisCepteLogoBase64 { get; set; }
        public string PushCompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string FuelPurchaseReasons { get; set; }
        public string MpTop1 { get; set; }
        public string MpTop2 { get; set; }
        public string MpBottom1 { get; set; }
        public string MpBottom2 { get; set; }
        public string ServiceCustomerCurrentExplanation { get; set; }
    }
}
