﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class TempBasket
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int CustomerId { get; set; }
        public int? BasketGroup { get; set; }
        public decimal? UnitPrice { get; set; }
        public int? OperationType { get; set; }
        public int UserId { get; set; }
        public DateTime? DateTi { get; set; }
        public int? Qty { get; set; }

        public Management Id1 { get; set; }
        public Product2 Id2 { get; set; }
        public Customer IdNavigation { get; set; }
    }
}
