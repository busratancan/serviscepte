﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ServiceControlSubDefines
    {
        public int Id { get; set; }
        public int? OrderNumber { get; set; }
        public sbyte? IsImportant { get; set; }
        public string ControlDefine { get; set; }
        public sbyte Active { get; set; }
        public int? ServiceControlDefinesId { get; set; }
        public sbyte? PhotoIn { get; set; }
        public sbyte? CheckboxIn { get; set; }
        public sbyte? TextIn { get; set; }
        public sbyte? IntegerIn { get; set; }
        public sbyte? DecimalIn { get; set; }
        public sbyte? TickIn { get; set; }
        public sbyte? Force { get; set; }

        public ServiceControlDefines ServiceControlDefines { get; set; }
    }
}
