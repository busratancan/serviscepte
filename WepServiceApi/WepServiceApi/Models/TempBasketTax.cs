﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class TempBasketTax
    {
        public int Id { get; set; }
        public int TempBasketId { get; set; }
        public int? ProductTaxListId { get; set; }
        public string TaxName { get; set; }
        public decimal? TaxRate { get; set; }

        public ProductTaxList2 IdNavigation { get; set; }
    }
}
