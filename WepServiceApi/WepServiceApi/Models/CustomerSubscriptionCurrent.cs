﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class CustomerSubscriptionCurrent
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public decimal? Currency { get; set; }
        public DateTime? InsDate { get; set; }
        public sbyte? Deletion { get; set; }
        public DateTime? LastPayDate { get; set; }
        public DateTime? PayDate { get; set; }
        public sbyte? Type { get; set; }

        public Customer Customer { get; set; }
    }
}
