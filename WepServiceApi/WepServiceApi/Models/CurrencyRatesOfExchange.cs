﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class CurrencyRatesOfExchange
    {
        public int Id { get; set; }
        public int? CurrencyId { get; set; }
        public DateTime? ExDate { get; set; }
        public decimal? CurrencyBuying { get; set; }
        public decimal? CurrencySelling { get; set; }

        public Currency Currency { get; set; }
    }
}
