﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class PersonelCardsFeeInformation
    {
        public int Id { get; set; }
        public decimal? MonthlyAmount { get; set; }
        public decimal? WeeklyAmount { get; set; }
        public decimal? DailyAmount { get; set; }
        public decimal? HoursAmount { get; set; }
        public decimal? MinuteAmount { get; set; }
        public decimal? SecondAmount { get; set; }
        public int? PaymentMethod { get; set; }
        public int? PaymentPeriod { get; set; }
        public int? DefaultCustomerBankId { get; set; }
        public string PaymentExp { get; set; }
        public string CardBarcode { get; set; }
        public string CardNameSurname { get; set; }
        public string CardExp { get; set; }
        public DateTime? SinceWork { get; set; }
        public DateTime? DateOfDeparture { get; set; }
        public string AgreementExp { get; set; }
        public decimal? DailyWorkingTime { get; set; }
        public int? CustomerId { get; set; }
        public decimal? HoursOfOvertime { get; set; }
        public string PersonelTask { get; set; }
        public int? RefarenceCustomerId { get; set; }
        public string RefaranceNameSurname { get; set; }
        public string PersonelAccountDay { get; set; }
        public string AddAutoPersonelFee { get; set; }
        public int? PersonelCardsId { get; set; }

        public PersonelCards PersonelCards { get; set; }
    }
}
