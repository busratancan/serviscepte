﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class CustomerAdress
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public string AdressType { get; set; }
        public string Adress { get; set; }
        public string Exp { get; set; }
        public string Province { get; set; }
        public string Town { get; set; }
        public string Country { get; set; }
        public sbyte? Deletion { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string MobilePhone { get; set; }

        public Customer Customer { get; set; }
    }
}
