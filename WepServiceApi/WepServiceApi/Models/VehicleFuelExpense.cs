﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class VehicleFuelExpense
    {
        public int Id { get; set; }
        public int? IncomeAndExpenseTypesId { get; set; }
        public int? VehicleId { get; set; }
        public int? StorageId { get; set; }
        public int? FuelCompanyCustomerId { get; set; }
        public int? FuelProductId { get; set; }
        public int? PersonelCardsId { get; set; }
        public decimal? Liter { get; set; }
        public decimal? UnitPrice { get; set; }
        public string DocumentNo { get; set; }
        public sbyte? SupplyType { get; set; }
        public sbyte? Deletion { get; set; }
        public int? UserId { get; set; }
        public string PurchaseReasons { get; set; }
        public DateTime? DateTime { get; set; }
        public DateTime? InsDateTime { get; set; }

        public Customer FuelCompanyCustomer { get; set; }
        public Product FuelProduct { get; set; }
        public IncomeAndExpenseTypes IncomeAndExpenseTypes { get; set; }
        public PersonelCards PersonelCards { get; set; }
        public Storage Storage { get; set; }
        public Vehicle Vehicle { get; set; }
    }
}
