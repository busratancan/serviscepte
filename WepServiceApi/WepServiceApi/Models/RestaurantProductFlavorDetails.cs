﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class RestaurantProductFlavorDetails
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public string FlavorDefine { get; set; }
        public decimal? Price { get; set; }
        public sbyte? Visible { get; set; }

        public Product Product { get; set; }
    }
}
