﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class CustomerCurrentOutput
    {
        public CustomerCurrentOutput()
        {
            CustomerCurrentOutputServiceNotes = new HashSet<CustomerCurrentOutputServiceNotes>();
            CustomerOffset = new HashSet<CustomerOffset>();
        }

        public int Id { get; set; }
        public sbyte? Type { get; set; }
        public int? DetailId { get; set; }
        public decimal? Currency { get; set; }
        public int? CustomerId { get; set; }
        public DateTime? Datetime { get; set; }
        public string Exp { get; set; }
        public int? UserId { get; set; }
        public string Type1 { get; set; }
        public sbyte? Deletion { get; set; }
        public DateTime? DeletionDateTime { get; set; }
        public int? DeletionUserId { get; set; }
        public DateTime? InsDatetime { get; set; }
        public string DocNumber { get; set; }
        public sbyte? ElevatorInvoiceStats { get; set; }

        public Customer Customer { get; set; }
        public ICollection<CustomerCurrentOutputServiceNotes> CustomerCurrentOutputServiceNotes { get; set; }
        public ICollection<CustomerOffset> CustomerOffset { get; set; }
    }
}
