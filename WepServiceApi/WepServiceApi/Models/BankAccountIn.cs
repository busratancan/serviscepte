﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class BankAccountIn
    {
        public int Id { get; set; }
        public string BankAccountId { get; set; }
        public decimal? Currency { get; set; }
        public int? UserId { get; set; }
        public string Exp { get; set; }
        public int? CustomerId { get; set; }
        public DateTime? DeletionDateTime { get; set; }
        public int? DeletionUserId { get; set; }
        public int? Deletion { get; set; }
        public int? Type { get; set; }
        public string DetailId { get; set; }
        public DateTime? DateTime { get; set; }
    }
}
