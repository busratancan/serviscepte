﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ServicesOpenCloseControlDefines
    {
        public int Id { get; set; }
        public int? ServiceControlDefinesId { get; set; }
        public int? ServicesOpenId { get; set; }
        public string Result1 { get; set; }
        public string Result2 { get; set; }
        public string Result3 { get; set; }
        public DateTime? StartTime { get; set; }
        public sbyte? Type { get; set; }
        public int? ServiceControlDefinesSubId { get; set; }
        public string Photo { get; set; }
        public sbyte? Deletion { get; set; }

        public ServiceControlDefines ServiceControlDefines { get; set; }
        public ServicesOpen ServicesOpen { get; set; }
    }
}
