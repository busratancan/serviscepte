﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class SafeOutput
    {
        public int Id { get; set; }
        public int? SafeId { get; set; }
        public int? CustomerId { get; set; }
        public string Exp { get; set; }
        public decimal? Currency { get; set; }
        public DateTime? Datetime { get; set; }
        public int? UserId { get; set; }
        public sbyte? Type { get; set; }
        public string Type1 { get; set; }
        public sbyte? Deletion { get; set; }
        public DateTime? DeletionDateTime { get; set; }
        public int? DeletionUserId { get; set; }
        public int? DetailId { get; set; }
        public string PrivaiteCode { get; set; }

        public Safe Safe { get; set; }
    }
}
