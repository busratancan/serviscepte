﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class Storage
    {
        public Storage()
        {
            InvoiceShipmentTrace = new HashSet<InvoiceShipmentTrace>();
            StockActivities = new HashSet<StockActivities>();
            VehicleFuelExpense = new HashSet<VehicleFuelExpense>();
            VehicleProductExpense = new HashSet<VehicleProductExpense>();
            VehicleTireExpense = new HashSet<VehicleTireExpense>();
        }

        public int Id { get; set; }
        public string StorageName { get; set; }
        public int? StorageManagerId { get; set; }
        public string StorageManager { get; set; }
        public sbyte? Deletion { get; set; }
        public int? BranchId { get; set; }
        public string BranchDefinition { get; set; }
        public int? ForliftOperatorId { get; set; }
        public string ForklifOperator { get; set; }
        public int? ResponsibleForWarehouseAccountingId { get; set; }
        public string ResponsibleForWarehouseAccountingDefination { get; set; }
        public int? WarehouseFormenId { get; set; }
        public string WarehouseFormenDefination { get; set; }
        public int? LoaderId { get; set; }
        public string LoaderDefination { get; set; }
        public int? SecurityId { get; set; }
        public string SecurityDefination { get; set; }
        public string WarehouseAdress { get; set; }
        public string Telephone { get; set; }
        public int? PhoneInternal { get; set; }
        public string Fax { get; set; }
        public string AuthorizedGsm { get; set; }
        public string AuthorizedGsm2 { get; set; }
        public string Explanation { get; set; }

        public ICollection<InvoiceShipmentTrace> InvoiceShipmentTrace { get; set; }
        public ICollection<StockActivities> StockActivities { get; set; }
        public ICollection<VehicleFuelExpense> VehicleFuelExpense { get; set; }
        public ICollection<VehicleProductExpense> VehicleProductExpense { get; set; }
        public ICollection<VehicleTireExpense> VehicleTireExpense { get; set; }
    }
}
