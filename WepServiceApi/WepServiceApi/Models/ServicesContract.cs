﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ServicesContract
    {
        public ServicesContract()
        {
            ServicesPointsBaseInformation = new HashSet<ServicesPointsBaseInformation>();
        }

        public int Id { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? CustomerId { get; set; }
        public int? ServiceDay { get; set; }
        public decimal? ContractFee { get; set; }
        public string Notes { get; set; }
        public int? ServicesCount { get; set; }
        public sbyte? Deletion { get; set; }
        public sbyte? Active { get; set; }
        public int? ServicesCategoryDefinitionId { get; set; }
        public int? ServicesRequestDefinationId { get; set; }
        public int? DeletionUserId { get; set; }
        public sbyte? Tax { get; set; }

        public Customer Customer { get; set; }
        public ServicesCategoryDefinition ServicesCategoryDefinition { get; set; }
        public ServicesRequestDefination ServicesRequestDefination { get; set; }
        public ICollection<ServicesPointsBaseInformation> ServicesPointsBaseInformation { get; set; }
    }
}
