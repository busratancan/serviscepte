﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ServicesSetupDefines
    {
        public ServicesSetupDefines()
        {
            ServicesSetupDefineParameters = new HashSet<ServicesSetupDefineParameters>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Defination { get; set; }
        public int? ServicesCategoryDefinitionId { get; set; }
        public string ServicesCategoryName { get; set; }
        public string SetupGroup { get; set; }
        public string Exp { get; set; }
        public string Exp2 { get; set; }
        public sbyte? Deletion { get; set; }
        public DateTime? DeletionTime { get; set; }
        public int? DeletionUserId { get; set; }
        public sbyte? ForceTrackSerialNumber { get; set; }
        public sbyte? ForceTrackWarranty { get; set; }

        public ICollection<ServicesSetupDefineParameters> ServicesSetupDefineParameters { get; set; }
    }
}
