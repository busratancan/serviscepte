﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class ProductPricingChangeHistory
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public decimal? RaiseRate { get; set; }
        public decimal? DiscountRate { get; set; }
        public int? UserId { get; set; }
        public DateTime? DateTime { get; set; }
        public decimal? PurchasePrice { get; set; }
        public decimal? GrossSalesPrice { get; set; }
        public decimal? RetailPrice { get; set; }
        public decimal? RetailPriceTax { get; set; }
    }
}
