﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class Settings
    {
        public int Id { get; set; }
        public int? Thema { get; set; }
    }
}
