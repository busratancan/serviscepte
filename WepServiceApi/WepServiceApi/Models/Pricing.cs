﻿using System;
using System.Collections.Generic;

namespace WepServiceApi.Models
{
    public partial class Pricing
    {
        public int Id { get; set; }
        public string PriceCode { get; set; }
        public string PriceDescription { get; set; }
        public decimal? Increment { get; set; }
        public decimal? Discount { get; set; }
        public string Exp1 { get; set; }
        public string Exp2 { get; set; }
        public string Exp3 { get; set; }
    }
}
