﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WepService.Data;
using WepServiceApi.Models;


namespace WepServiceApi.Attributes
{
    public class BasicAuthorizeFilter : IAuthorizationFilter
    {
        private readonly string realm;
        private crocodileContext Cntx { get; set; }
        //List<Login> list = new List<Login>();

        public BasicAuthorizeFilter(crocodileContext Context, string realm = null)
        {
            this.Cntx = Context;
            this.realm = realm;
        }
        //public Login login;

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            string autHeader = context.HttpContext.Request.Headers["Authorization"];

            if (autHeader != null && autHeader.StartsWith("Basic "))
            {
                var encodeUsernamePassword = autHeader.Split(' ', 2, StringSplitOptions.RemoveEmptyEntries)[1]?.Trim();
                var decodeUserNamePassword = Encoding.UTF8.GetString(Convert.FromBase64String(encodeUsernamePassword));

                var username = decodeUserNamePassword.Split(':', 2)[0];
                var password = decodeUserNamePassword.Split(':', 2)[1];

                WepService.Data.Management mng = IsAuthorized(username, password);
                if (mng != null)
                {
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, mng.Id.ToString()),
                    };
                    ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims);
                    context.HttpContext.User.AddIdentity(claimsIdentity);
                    return;
                }

            }
            context.HttpContext.Response.Headers["WWW-Authenticate"] = "Basic";

            if (!string.IsNullOrWhiteSpace(realm))
            {
                context.HttpContext.Response.Headers["WWW-Authenticate"] += $" realm=\"{realm}\"";
            }

            context.Result = new UnauthorizedResult();

        }
        public WepService.Data.Management IsAuthorized(string username, string password)
        {
            var list = Cntx.Management.ToList();
            return list.Where(k => k.UserName.Equals(username) && k.Password.Equals(password)).SingleOrDefault();

        }
    }
}
