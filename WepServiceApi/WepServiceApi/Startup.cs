﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using WebService.Models.Base;
using WebService.Repositories.Base;
using WebService.Repositories.Customer;
using WebService.Services.Base;
using WepService.Data;

namespace WepServiceApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            var sqlConnectionString = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<crocodileContext>(options =>
             options.UseMySql(sqlConnectionString));

            RepositoryProvider.SetRepositories(services);
            WebService.Services.Base.ServiceProvider.SetServices(services);
            
        }

        public void Configure(
                   IApplicationBuilder app,
                   IHostingEnvironment env,
                   
                   crocodileContext dbContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseExceptionHandler(appBuilder => {
                appBuilder.Use(async (context, next) =>
                {
                    await next();
                });
            });
            
            app.UseCors("CorsPolicy");

            app.UseStaticFiles();
            app.UseDefaultFiles(new DefaultFilesOptions
            {
                DefaultFileNames = new List<string> { "index.html" }
            });
            app.UseStaticFiles();

            app.UseMvc();

        }
    }
}
