﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WepServiceApi.Helpers
{
    public static class ResponseResult
    {
        public static string Success { get { return "success"; } }
        public static string Fail { get { return "fail"; } }
    }
}
