﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebService.Models.Base;

namespace WepServiceApi.Helpers
{
    public static class ResponseCreator<T>
    {
        public static BaseResponseModel<T> CreateResponse(T model, string result, string errorMessage)
        {
            return new BaseResponseModel<T>()
            {
                Result = result,
                ErrorMessage = errorMessage,
                Body = model
            };
        }

        internal static BaseResponseModel<SubLastStatusModel> CreateResponse(object p, string success, string v)
        {
            throw new NotImplementedException();
        }
    }
}

