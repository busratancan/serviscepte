﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Models.Base;
using WebService.Models.Customer;
using WebService.Services.Customer;
using WepServiceApi.Helpers;

namespace WepServiceApi.ServisCepteControllers
{
    //[Authorize]
    [Route("api/[controller]")]
    public class CustomersController : BaseController
    {
        private readonly ICustomerService customerService;

        public CustomersController(
            ICustomerService customerService
            )
        {
            this.customerService = customerService;
        }

        [HttpGet]
        [Route("")]
        public async Task<BaseResponseModel<CustomerListModel>> Get(
            [ModelBinder(BinderType = typeof(CustomerListRequestModel))]
                CustomerListRequestModel customerListRequest)
        {
            return ResponseCreator<CustomerListModel>
                .CreateResponse(
                    await customerService.Get(customerListRequest),
                    ResponseResult.Success,
                    "");
        }

        [HttpGet("{id}")]
        public async Task<BaseResponseModel<CustomerModel>> Get(int id)
        {
            return ResponseCreator<CustomerModel>
                .CreateResponse(
                    await customerService.Get(id),
                    ResponseResult.Success,
                    "");
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {

        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

