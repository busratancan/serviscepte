﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Models.Base;
using WebService.Models.Service;
using WebService.Services.Service;
using WepServiceApi.Helpers;

namespace WepServiceApi.Controllers.Service
{
    [Route("api/[controller]")]
    [ApiController]
    public class ControlDefineController : ControllerBase
    {
        private readonly IControlDefineService controlDefineService;


        public ControlDefineController(
            IControlDefineService controlDefineService
            )
        {  
            this.controlDefineService = controlDefineService;
        }

        [HttpGet]
        [Route("/service_control_defines")]
        public async Task<BaseResponseModel<ControlDefineList>> Get(
            [ModelBinder(BinderType = typeof(ControlDefineRequestModel))]
                ControlDefineRequestModel controlListRequest)
        {
            return ResponseCreator<ControlDefineList>
                .CreateResponse(
                    await controlDefineService.Get(controlListRequest),
                    ResponseResult.Success,
                    "");
        }

        [HttpGet("{id}")]
        public async Task<BaseResponseModel<ControlDefineModel>> Get(int id)
        {
            return ResponseCreator<ControlDefineModel>
                .CreateResponse(
                    await controlDefineService.Get(id),
                    ResponseResult.Success,
                    "");
        }
    }
}