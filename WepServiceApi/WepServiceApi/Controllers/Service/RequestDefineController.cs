﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Models.Base;
using WebService.Models.Service;
using WebService.Models.Service.RequestList;
using WebService.Services.Service.Interface;
using WepServiceApi.Helpers;

namespace WepServiceApi.Controllers.Service
{
    [Route("api/[controller]")]
    [ApiController]
    public class RequestDefineController : ControllerBase
    {
        private readonly IRequestDefinationService requestDefinationService;

        public RequestDefineController(
            IRequestDefinationService requestDefinationService
            )
        {
            this.requestDefinationService = requestDefinationService;
        }

        [HttpGet]
        [Route("/service_request_list")]
        public async Task<BaseResponseModel<RequestDefinationListModel>> Get(
            [ModelBinder(BinderType = typeof(RequestDefinationRequestListModel))]
                RequestDefinationRequestListModel requestDefinationRequestListModel)
        {
            return ResponseCreator<RequestDefinationListModel>
                .CreateResponse(
                    await requestDefinationService.Get(requestDefinationRequestListModel),
                    ResponseResult.Success,
                    "");
        }

        [HttpGet("{id}")]
        public async Task<BaseResponseModel<RequestDefinationModel>> Get(int id)
        {
            return ResponseCreator<RequestDefinationModel>
                .CreateResponse(
                    await requestDefinationService.Get(id),
                    ResponseResult.Success,
                    "");
        }
    }
}