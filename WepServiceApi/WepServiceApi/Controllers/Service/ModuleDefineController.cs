﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Models.Base;
using WebService.Models.Service;
using WebService.Models.Service.ListModels;
using WebService.Models.Service.RequestList;
using WebService.Services.Service.Interface;
using WepServiceApi.Helpers;

namespace WepServiceApi.Controllers.Service
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModuleDefineController : ControllerBase
    {
        private readonly IModuleDefineService moduleDefineService;
        public ModuleDefineController(
            IModuleDefineService moduleDefineService
            )
        {
            this.moduleDefineService = moduleDefineService;
        }

        [HttpGet]
        [Route("/service_module_defines")]
        public async Task<BaseResponseModel<ModuleDefineListModel>> Get(
            [ModelBinder(BinderType = typeof(ModuleDefineRequestListModel))]
                ModuleDefineRequestListModel moduleDefineRequestListModel)
        {
            return ResponseCreator<ModuleDefineListModel>
                .CreateResponse(
                    await moduleDefineService.Get(moduleDefineRequestListModel),
                    ResponseResult.Success,
                    "");
        }

        [HttpGet("{id}")]
        public async Task<BaseResponseModel<ModuleDefineModel>> Get(int id)
        {
            return ResponseCreator<ModuleDefineModel>
                .CreateResponse(
                    await moduleDefineService.Get(id),
                    ResponseResult.Success,
                    "");
        }
    }
}