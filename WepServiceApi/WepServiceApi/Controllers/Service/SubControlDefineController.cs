﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Models.Base;
using WebService.Models.Service;
using WebService.Models.Service.ListModels;
using WebService.Models.Service.RequestList;
using WebService.Services.Service.Interface;
using WepServiceApi.Helpers;

namespace WepServiceApi.Controllers.Service
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubControlDefineController : ControllerBase
    {
        private readonly ISubLastStatusService subLastStatusService;

        public SubControlDefineController(
            ISubLastStatusService subLastStatusService
            )
        {
            this.subLastStatusService = subLastStatusService;
        }

        [HttpGet]
        [Route("/service_sub_control_defines")]
        public async Task<BaseResponseModel<SubLastStatusIListModel>> Get(
           [ModelBinder(BinderType = typeof(SubLastStatusIRequestListModel))]
                SubLastStatusIRequestListModel subLastStatusIRequestList)
        {
            return ResponseCreator<SubLastStatusIListModel>
                .CreateResponse(
                    await subLastStatusService.Get(subLastStatusIRequestList),
                    ResponseResult.Success,
                    "");
        }

        [HttpGet("{id}")]
        public async Task<BaseResponseModel<SubLastStatusModel>> Get(int id)
        {
            return ResponseCreator<SubLastStatusModel>
                .CreateResponse(
                    await subLastStatusService.Get(id),
                    ResponseResult.Success,
                    "");
        }
    }
}
