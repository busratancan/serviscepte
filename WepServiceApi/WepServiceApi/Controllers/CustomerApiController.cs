﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Models.Base;
using WebService.Models.Customer;
using WebService.Services.Customer;
using WepServiceApi.Helpers;

namespace WepServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerApiController : ControllerBase
    {
        private readonly ICustomerService customerService;

        public CustomerApiController(
            ICustomerService customerService
            )
        {
            this.customerService = customerService;
        }
        [HttpGet]
        [Route("/deneme")]
        public async Task<BaseResponseModel<CustomerListModel>> Get(
            [ModelBinder(BinderType = typeof(CustomerListRequestModel))]
                CustomerListRequestModel customerListRequest)
        {
            return ResponseCreator<CustomerListModel>
                .CreateResponse(
                    await customerService.Get(customerListRequest),
                    ResponseResult.Success,
                    "");
        }

        [HttpGet("{id}")]
        public async Task<BaseResponseModel<CustomerModel>> Get(int id)
        {
            return ResponseCreator<CustomerModel>
                .CreateResponse(
                    await customerService.Get(id),
                    ResponseResult.Success,
                    "");
        }
    }
}