﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Models.Base;
using WebService.Models.Service.ItemModels;
using WepService.Data;

namespace WebService.Models.Service
{
    public class LastStatusModel : BaseModel, IConvertible<ServicesLastStatusDefination>
    {
        public LastStatusModel()
        {
        }

        public int Id { get; set; }
        public string Defination { get; set; }
        public string Code { get; set; }
        public string Color { get; set; }
        public int? Order { get; set; }
        public string Exp { get; set; }
        public int? SevicesRequestDefinationId { get; set; }
        public string SevicesRequestDefinationDefination { get; set; }
        public int? ServicesCategoryDefinitionId { get; set; }
        public string ServicesCategoryName { get; set; }
        public sbyte? OpenNewService { get; set; }
        public sbyte? ServiceImpact { get; set; }
        public string OpenNewServiceRequestDefinationId { get; set; }
        public string OpenNewServiceRequestDefinationDefination { get; set; }
        public string ColorCode { get; set; }
        public sbyte? Deletion { get; set; }
        public DateTime? DeletionDatetime { get; set; }
        public int? DeletionUserId { get; set; }
        public int? SmsThemesId { get; set; }
        public string SmsThemesName { get; set; }

        public LastStatusModel(ServicesLastStatusDefination slsd)
        {
            Id = slsd.Id;
            Defination = slsd.Defination;
            Code = slsd.Code;
            Color = slsd.Color;
            Order = slsd.Order;
            Exp = slsd.Exp;
            SevicesRequestDefinationId = slsd.SevicesRequestDefinationId;
            SevicesRequestDefinationDefination = slsd.SevicesRequestDefinationDefination;
            ServicesCategoryDefinitionId = slsd.ServicesCategoryDefinitionId;
            ServicesCategoryName = slsd.ServicesCategoryName;
            OpenNewService = slsd.OpenNewService;
            ServiceImpact = slsd.ServiceImpact;
            OpenNewServiceRequestDefinationId = slsd.OpenNewServiceRequestDefinationId;
            OpenNewServiceRequestDefinationDefination = slsd.OpenNewServiceRequestDefinationDefination;
            ColorCode = slsd.ColorCode;
            Deletion = slsd.Deletion;
            DeletionDatetime = slsd.DeletionDatetime;
            DeletionUserId = slsd.DeletionUserId;
            SmsThemesId = slsd.SmsThemesId;
            SmsThemesName = slsd.SmsThemesName;

        }

        public List<SubControlItemModel> SubControlItemModel { get; set; }

        public ServicesLastStatusDefination Convert()
        {
        throw new NotImplementedException();
        }

    }
}
