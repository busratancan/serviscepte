﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Models.Base;
using WepService.Data;

namespace WebService.Models.Service
{
    public class ServiceModel : BaseModel, IConvertible<ServicesOpen, ServicesCategoryDefinition, ServicesRequestDefination, CustomerEntity, ServicesPointsBaseInformation>
    {
        public ServiceModel()
        {

        }

        public int Id { get; set; }//oper_id //so
        public int? ServicesPointsBaseInformationId { get; set; }//so
        public sbyte? FaultStats { get; set; }//so 
        public int? ServiceOrder { get; set; }//spb
        public string IdentificationNumber { get; set; }//spb
        public string ServiceCode { get; set; }//spb
        public string ServiceName { get; set; }//spb
        public string Direction { get; set; }//spb
        public string CategoryColor { get; set; }//scd
        public string CategoryCode { get; set; }//scd
        public string CategoryDefination { get; set; }//scd
        public string RequestDefination { get; set; }//srd
        public string RequestColor { get; set; }//srd
        public string RequestCode { get; set; }//srd
        public string AccountTitle { get; set; }//c
        public string AccountGrup { get; set; }//c
        public string AccountType { get; set; }//c
        public string CustomerCode { get; set; }//c
        public int? CustomerId { get; set; }//c
        public int? ServicesCategoryDefinitionId { get; set; }//srd
        public DateTime? InsertTime { get; set; }//so
        public DateTime? StartDateTime { get; set; }//so
        public int? OpenedUserId { get; set; }//so
        public int? ResponsibleUserId { get; set; }//so
        public DateTime? LastValidDateTime { get; set; }//so
        public sbyte? ServicesOpenType { get; set; }//so
        public sbyte? ServicesOpenTunnel { get; set; }//so
        public sbyte? Status { get; set; }//so 

        public ServiceModel(ServicesOpen so, ServicesCategoryDefinition scd, ServicesRequestDefination srd, CustomerEntity c, ServicesPointsBaseInformation spb)
        {
            Id = so.Id;
            ServicesPointsBaseInformationId = so.ServicesPointsBaseInformationId;
            FaultStats = so.FaultStats;
            InsertTime = so.InsertTime;
            StartDateTime = so.StartDateTime;
            OpenedUserId = so.OpenedUserId;
            ResponsibleUserId = so.ResponsibleUserId;
            LastValidDateTime = so.LastValidDateTime;
            ServicesOpenType = so.ServicesOpenType;
            ServicesOpenTunnel = so.ServicesOpenTunnel;
            Status = so.Status;
            CategoryColor = scd.Color;
            CategoryCode = scd.Code;
            CategoryDefination = scd.Defination;
            RequestDefination = srd.Defination;
            RequestColor = srd.Color;
            RequestCode = srd.Code;
            ServicesCategoryDefinitionId = srd.ServicesCategoryDefinitionId;
            AccountTitle = c.AccountTitle;
            AccountGrup = c.AccountGrup;
            AccountType = c.AccountType;
            CustomerCode = c.CustomerCode;
            CustomerId = c.Id;
            ServiceOrder = spb.ServiceOrder;
            IdentificationNumber = spb.IdentificationNumber;
            ServiceCode = spb.ServiceCode;
            ServiceName = spb.ServiceName;
        }

        public List<ServiceItemModel> ServiceItemModel { get; set; }

    }

}

