﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Models.Base;
using WebService.Models.Service.ItemModels;
using WepService.Data;

namespace WebService.Models.Service
{
    public class ModuleDefineModel : BaseModel, IConvertible<ServicesCategorySubDefinition>
    {
        public ModuleDefineModel()
        {
        }

        public int Id { get; set; }
        public string Defination { get; set; }
        public string Code { get; set; }
        public string Color { get; set; }
        public int? Order { get; set; }
        public string Exp { get; set; }
        public int? ServicesCategoryDefinitionId { get; set; }
        public sbyte? Active { get; set; }
        public sbyte? ForceQrCode { get; set; }
        public sbyte? ServiceTimeOutMinutes { get; set; }
        public sbyte? ForceAskPieceChange { get; set; }

        public ModuleDefineModel(ServicesCategorySubDefinition scsd)
        {
            Id = scsd.Id;
            Defination = scsd.Defination;
            Code = scsd.Code;
            Color = scsd.Color;
            Order = scsd.Order;
            Exp = scsd.Exp;
            ServicesCategoryDefinitionId = scsd.ServicesCategoryDefinitionId;
            Active = scsd.Active;
            ForceQrCode = scsd.ForceQrCode;
            ServiceTimeOutMinutes = scsd.ServiceTimeOutMinutes;
            ForceAskPieceChange = scsd.ForceAskPieceChange;

        }

        public List<ModuleDefineItemModel> ModuleDefineItemModel { get; set; }

        public ServicesCategorySubDefinition Convert()
        {
            return new ServicesCategorySubDefinition()
            {
                Id = Id,
                Defination = Defination,
                Code = Code,
                Color = Color,
                Order = Order,
                Exp = Exp,
                ServicesCategoryDefinitionId = ServicesCategoryDefinitionId,
                Active = Active,
                ForceQrCode = ForceQrCode,
                ServiceTimeOutMinutes = ServiceTimeOutMinutes,
                ForceAskPieceChange = ForceAskPieceChange,
            };
        }
    }
}