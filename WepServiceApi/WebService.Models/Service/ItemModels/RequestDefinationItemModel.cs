﻿using System;
using System.Collections.Generic;
using System.Text;
using WepService.Data;

namespace WebService.Models.Service
{
    public class RequestDefinationItemModel
    {
        public int Id { get; set; }
        public string Defination { get; set; }
        public string Code { get; set; }
        public string Color { get; set; }
        public string HexColor { get; set; }
        public string ColorCode { get; set; }
        public int? Order { get; set; }
        public string Exp { get; set; }
        public int? ServicesCategoryDefinitionId { get; set; }
        public string ServicesCategoryName { get; set; }
        public sbyte? Period { get; set; }
        public sbyte? ComponentDialog { get; set; }
        public sbyte? MobileVisible { get; set; }
        public sbyte? OpenWithoutPersonel { get; set; }
        public sbyte? CustomizableSubModule { get; set; }
        public sbyte? AllowNotAccept { get; set; }
        public int? TaskAcceptTimeout { get; set; }
        public sbyte? AutomaticCharging { get; set; }
        public sbyte? ForcedContract { get; set; }

        public RequestDefinationItemModel(ServicesRequestDefination srd)
        {
            Id = srd.Id;
            Defination = srd.Defination;
            Code = srd.Code;
            Color = srd.Color;
            HexColor = srd.HexColor;
            ColorCode = srd.ColorCode;
            Order = srd.Order;
            Exp = srd.Exp;
            ServicesCategoryDefinitionId = srd.ServicesCategoryDefinitionId;
            ServicesCategoryName = srd.ServicesCategoryName;
            Period = srd.Period;
            ComponentDialog = srd.ComponentDialog;
            MobileVisible = srd.MobileVisible;
            OpenWithoutPersonel = srd.OpenWithoutPersonel;
            CustomizableSubModule = srd.CustomizableSubModule;
            AllowNotAccept = srd.AllowNotAccept;
            TaskAcceptTimeout = srd.TaskAcceptTimeout;
            AutomaticCharging = srd.AutomaticCharging;
            ForcedContract = srd.ForcedContract;
        }
    }

}