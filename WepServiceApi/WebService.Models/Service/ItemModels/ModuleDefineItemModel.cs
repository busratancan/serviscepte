﻿using System;
using System.Collections.Generic;
using System.Text;
using WepService.Data;

namespace WebService.Models.Service.ItemModels
{
    public class ModuleDefineItemModel
    {
        public int Id { get; set; }
        public string Defination { get; set; }
        public string Code { get; set; }
        public string Color { get; set; }
        public int? Order { get; set; }
        public string Exp { get; set; }
        public int? ServicesCategoryDefinitionId { get; set; }
        public sbyte? Active { get; set; }
        public sbyte? ForceQrCode { get; set; }
        public sbyte? ServiceTimeOutMinutes { get; set; }
        public sbyte? ForceAskPieceChange { get; set; }

        public ModuleDefineItemModel(ServicesCategorySubDefinition scsd)
        {
            Id = scsd.Id;
            Defination = scsd.Defination;
            Code = scsd.Code;
            Color = scsd.Color;
            Order = scsd.Order;
            Exp = scsd.Exp;
            ServicesCategoryDefinitionId = scsd.ServicesCategoryDefinitionId;
            Active = scsd.Active;
            ForceQrCode = scsd.ForceQrCode;
            ServiceTimeOutMinutes = scsd.ServiceTimeOutMinutes;
            ForceAskPieceChange = scsd.ForceAskPieceChange;

        }
    }
}