﻿using System;
using System.Collections.Generic;
using System.Text;
using WepService.Data;

namespace WebService.Models.Service.ItemModels
{
    public class CategoryDefinitionItemModel
    {
        public CategoryDefinitionItemModel()
        {

        }

        public CategoryDefinitionItemModel(ServicesCategoryDefinition scd)
        {
            Id = scd.Id;
            Defination = scd.Defination;
            Code = scd.Code;
            Color = scd.Color;
            Order = scd.Order;
            Exp = scd.Exp;
            ColorCode = scd.ColorCode;

        }

        public int Id { get; set; }
        public string Defination { get; set; }
        public string Code { get; set; }
        public string Color { get; set; }
        public int? Order { get; set; }
        public string Exp { get; set; }
        public string ColorCode { get; set; }

        //public List<CategoryDefinitionModel> CategoryDefinitionModel { get; set; }
    }
}
