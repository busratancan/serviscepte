﻿using System;
using System.Collections.Generic;
using System.Text;
using WepService.Data;

namespace WebService.Models.Service.ItemModels
{
    public class SubLastStatusItemModel
    {
        public int Id { get; set; }
        public string Caption { get; set; }
        public sbyte? TextIn { get; set; }
        public sbyte? NumericIn { get; set; }
        public sbyte? IntegerIn { get; set; }
        public sbyte? Deletion { get; set; }
        public int? ServicesLastStatusDefinationId { get; set; }
        public sbyte? PersonelIn { get; set; }
        public sbyte? DateIn { get; set; }
        public sbyte? ForceEntery { get; set; }
        public sbyte? OrderNumber { get; set; }

        public SubLastStatusItemModel(ServicesLastStatusDefinationInfs slsd)
        {
            Id = slsd.Id;
            Caption = slsd.Caption;
            TextIn = slsd.TextIn;
            NumericIn = slsd.NumericIn;
            IntegerIn = slsd.IntegerIn;
            Deletion = slsd.Deletion;
            ServicesLastStatusDefinationId = slsd.ServicesLastStatusDefinationId;
            PersonelIn = slsd.PersonelIn;
            DateIn = slsd.DateIn;
            ForceEntery = slsd.ForceEntery;
            OrderNumber = slsd.OrderNumber;
        }
    }
}

   