﻿using System;
using System.Collections.Generic;
using System.Text;
using WepService.Data;

namespace WebService.Models.Service
{
    public class SubControlItemModel
    {
        public int Id { get; set; }
        public int? OrderNumber { get; set; }
        public sbyte? IsImportant { get; set; }
        public string ControlDefine { get; set; }
        public sbyte Active { get; set; }
        public int? ServiceControlDefinesId { get; set; }
        public sbyte? Force { get; set; }

        public SubControlItemModel(ServiceControlSubDefines scsd)
        {
            Id = scsd.Id;
            OrderNumber = scsd.OrderNumber;
            IsImportant = scsd.IsImportant;
            ControlDefine = scsd.ControlDefine;
            Active = scsd.Active;
            ServiceControlDefinesId = scsd.ServiceControlDefinesId;
            Force = scsd.Force;

        }
    }
}
