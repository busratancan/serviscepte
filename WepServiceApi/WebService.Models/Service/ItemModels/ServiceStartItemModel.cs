﻿using System;
using System.Collections.Generic;
using System.Text;
using WepService.Data;

namespace WebService.Models.Service.ItemModels
{
    public class ServiceStartItemModel
    {
        public string result;
        public int services_open_id { get; set; }
        public DateTime? date_time { get; set; }

        public ServiceStartItemModel(ServicesOpen so)
        {
            services_open_id = so.Id;
            date_time = so.StartDateTime;
        }
    }
}
