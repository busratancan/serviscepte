﻿using System;
using System.Collections.Generic;
using System.Text;
using WepService.Data;

namespace WebService.Models.Service
{
    public class ControlDefineItemModel
    {
        public ControlDefineItemModel()
        {

        }

        public ControlDefineItemModel(ServiceControlDefines scd)
        {
            Id = scd.Id;
            OrderNumber = scd.OrderNumber;
            IsImportant = scd.IsImportant;
            ControlDefine = scd.ControlDefine;
            Active = scd.Active;
            Code = scd.Code;
            Color = scd.Color;
            ServicesRequestDefinationId = scd.ServicesRequestDefinationId;
            ServicesCategorySubDefinitionId = scd.ServicesCategorySubDefinitionId;
            ForceSelect = scd.ForceSelect;
        }

        public int Id { get; set; }
        public int? OrderNumber { get; set; }
        public sbyte? IsImportant { get; set; }
        public string ControlDefine { get; set; }
        public sbyte Active { get; set; }
        public string Code { get; set; }
        public sbyte? Color { get; set; }
        public int? ServicesRequestDefinationId { get; set; }
        public int? ServicesCategorySubDefinitionId { get; set; }
        public sbyte? ForceSelect { get; set; }

        //public List<ControlDefineModel> ControlDefineModel { get; set; }
    }
}
