﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WebService.Models.Base.Search;
using WepService.Data;

namespace WebService.Models.Service.RequestList
{
    public class CategoryDefinitionRequestModel : ListRequestModel, ISearchable<ServicesCategoryDefinition>
    {
        public CategoryDefinitionRequestModel()
            : base(1, 25)
        {
            Query = "";
        }

        public string Query { get; set; }

        public override Task BindModelAsync(ModelBindingContext bindingContext)
        {
            base.BindModelAsync(bindingContext);

            var queryProvider = bindingContext.ValueProvider.GetValue("query");
            if (queryProvider == ValueProviderResult.None)
                return Task.CompletedTask;

            Query = queryProvider.FirstValue;

            return Task.CompletedTask;
        }

        public Expression<Func<ServicesCategoryDefinition, bool>> GetFilter()
        {
            Expression<Func<ServicesCategoryDefinition, bool>> filter = (c => true);
            if (!string.IsNullOrEmpty(Query))
                filter = (c => c.Code.Contains(Query));

            return filter;
        }

        public Func<IQueryable<ServicesCategoryDefinition>, IOrderedQueryable<ServicesCategoryDefinition>> GetOrderBy()
        {
            return (a => a.OrderByDescending(b => b.Id));
        }
    }
}