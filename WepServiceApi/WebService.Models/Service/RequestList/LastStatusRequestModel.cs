﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WebService.Models.Base.Search;
using WepService.Data;

namespace WebService.Models.Service.RequestList
{
    public class LastStatusRequestModel : ListRequestModel, ISearchable<ServicesLastStatusDefination>
    {
        public LastStatusRequestModel()
            : base(1, 25)
        {
            Query = "";
        }

        public string Query { get; set; }

        public override Task BindModelAsync(ModelBindingContext bindingContext)
        {
            base.BindModelAsync(bindingContext);

            var queryProvider = bindingContext.ValueProvider.GetValue("query");
            if (queryProvider == ValueProviderResult.None)
                return Task.CompletedTask;

            Query = queryProvider.FirstValue;

            return Task.CompletedTask;
        }

        public Expression<Func<ServicesLastStatusDefination, bool>> GetFilter()
        {
            Expression<Func<ServicesLastStatusDefination, bool>> filter = (c => true);
            if (!string.IsNullOrEmpty(Query))
                filter = (c => c.Id.ToString().Contains(Query));

            return filter;
        }

        public Func<IQueryable<ServicesLastStatusDefination>, IOrderedQueryable<ServicesLastStatusDefination>> GetOrderBy()
        {
            return (a => a.OrderByDescending(b => b.Id));
        }
    }
}
