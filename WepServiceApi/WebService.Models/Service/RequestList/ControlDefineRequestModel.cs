﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WebService.Models.Base.Search;
using WepService.Data;

namespace WebService.Models.Service
{
    public class ControlDefineRequestModel : ListRequestModel, ISearchable<ServiceControlDefines>
    {
        public ControlDefineRequestModel()
            : base(1, 25)
        {
            Query = "";
        }

        public string Query { get; set; }

        public override Task BindModelAsync(ModelBindingContext bindingContext)
        {
            base.BindModelAsync(bindingContext);

            var queryProvider = bindingContext.ValueProvider.GetValue("query");
            if (queryProvider == ValueProviderResult.None)
                return Task.CompletedTask;

            Query = queryProvider.FirstValue;

            return Task.CompletedTask;
        }

        public Expression<Func<ServiceControlDefines, bool>> GetFilter()
        {
            Expression<Func<ServiceControlDefines, bool>> filter = (c => true);
            if (!string.IsNullOrEmpty(Query))
                filter = (c => c.Code.Contains(Query));

            return filter;
        }

        public Func<IQueryable<ServiceControlDefines>, IOrderedQueryable<ServiceControlDefines>> GetOrderBy()
        {
            return (a => a.OrderByDescending(b => b.Id));
        } 
    }
}
