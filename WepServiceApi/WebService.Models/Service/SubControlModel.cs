﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Models.Base;
using WepService.Data;

namespace WebService.Models.Service
{
    public class SubControlModel : BaseModel, IConvertible<ServiceControlSubDefines>
    {
        public SubControlModel()
        {
        }
    
        public int Id { get; set; }
        public int? OrderNumber { get; set; }
        public sbyte? IsImportant { get; set; }
        public string ControlDefine { get; set; }
        public sbyte Active { get; set; }
        public int? ServiceControlDefinesId { get; set; }
        public sbyte? Force { get; set; }

        public SubControlModel(ServiceControlSubDefines scsd)
        {
            Id = scsd.Id;
            OrderNumber = scsd.OrderNumber;
            IsImportant = scsd.IsImportant;
            ControlDefine = scsd.ControlDefine;
            Active = scsd.Active;
            ServiceControlDefinesId = scsd.ServiceControlDefinesId;
            Force = scsd.Force;

        }
        public List<SubControlItemModel> SubControlItemModel { get; set; }

        public ServiceControlSubDefines Convert()
        {
            return new ServiceControlSubDefines()
            {
                Id = Id,
                OrderNumber = OrderNumber,
                IsImportant = IsImportant,
                ControlDefine = ControlDefine,
                Active = Active,
                ServiceControlDefinesId = ServiceControlDefinesId,
                Force = Force,
            };
        }
    }
}

    
