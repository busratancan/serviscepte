﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Models.Base;
using WebService.Models.Service.ItemModels;
using WepService.Data;

namespace WebService.Models.Service
{
    public class ServiceStartModel : BaseModel, IConvertible<ServicesOpen>
    {
        public ServiceStartModel()
        {

        }

        public string result;
        public int services_open_id { get; set; }
        public DateTime? date_time { get; set; }

        public ServiceStartModel(ServicesOpen so)
        {
            services_open_id = so.Id;
            date_time = so.StartDateTime;
        }

        public List<ServiceStartItemModel> ServiceStartItemModel { get; set; }

        public ServicesOpen Convert()
        {
            return new ServicesOpen()
            {
                Id = services_open_id,
                StartDateTime = date_time,
            };
        }
    }
}