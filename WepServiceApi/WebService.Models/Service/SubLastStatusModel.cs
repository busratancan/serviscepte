﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Models.Base;
using WebService.Models.Service.ItemModels;
using WepService.Data;

namespace WebService.Models.Service
{
    public class SubLastStatusModel : BaseModel, IConvertible<ServicesLastStatusDefinationInfs>
    {
        public SubLastStatusModel()
        {
        }

        public int Id { get; set; }
        public string Caption { get; set; }
        public sbyte? TextIn { get; set; }
        public sbyte? NumericIn { get; set; }
        public sbyte? IntegerIn { get; set; }
        public sbyte? Deletion { get; set; }
        public int? ServicesLastStatusDefinationId { get; set; }
        public sbyte? PersonelIn { get; set; }
        public sbyte? DateIn { get; set; }
        public sbyte? ForceEntery { get; set; }
        public sbyte? OrderNumber { get; set; }

        public SubLastStatusModel(ServicesLastStatusDefinationInfs slsd)
        {
            Id = slsd.Id;
            Caption = slsd.Caption;
            TextIn = slsd.TextIn;
            NumericIn = slsd.NumericIn;
            IntegerIn = slsd.IntegerIn;
            Deletion = slsd.Deletion;
            ServicesLastStatusDefinationId = slsd.ServicesLastStatusDefinationId;
            PersonelIn = slsd.PersonelIn;
            DateIn = slsd.DateIn;
            ForceEntery = slsd.ForceEntery;
            OrderNumber = slsd.OrderNumber;
        }

        public List<SubLastStatusItemModel> SubLastStatusItemModel { get; set; }

        public ServicesLastStatusDefinationInfs Convert()
        {
            return new ServicesLastStatusDefinationInfs()
            {
                Id = Id,
                Caption = Caption,
                TextIn = TextIn,
                NumericIn = NumericIn,
                IntegerIn = IntegerIn,
                Deletion = Deletion,
                ServicesLastStatusDefinationId = ServicesLastStatusDefinationId,
                PersonelIn = PersonelIn,
                DateIn = DateIn,
                ForceEntery = ForceEntery,
                OrderNumber = OrderNumber,
            };
        }
    }
}
   