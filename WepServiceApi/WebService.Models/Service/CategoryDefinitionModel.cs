﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WebService.Models.Base;
using WebService.Models.Base.Search;
using WebService.Models.Service.ItemModels;
using WepService.Data;

namespace WebService.Models.Service
{
    public class CategoryDefinitionModel : BaseModel, IConvertible<ServicesCategoryDefinition>
    {
        public CategoryDefinitionModel()
        {

        }

        public CategoryDefinitionModel(ServicesCategoryDefinition scd)
        {
            Id = scd.Id;
            Defination = scd.Defination;
            Code = scd.Code;
            Color = scd.Color;
            Order = scd.Order;
            Exp = scd.Exp;
            ColorCode = scd.ColorCode;

        }

        public int Id { get; set; }
        public string Defination { get; set; }
        public string Code { get; set; }
        public string Color { get; set; }
        public int? Order { get; set; }
        public string Exp { get; set; }
        public string ColorCode { get; set; }

        public List<CategoryDefinitionItemModel> CategoryDefinitionItemModel { get; set; }

        public ServicesCategoryDefinition Convert()
        {
            return new ServicesCategoryDefinition()
            {
                Id = Id,
                Defination = Defination,
                Code = Code,
                Color = Color,
                Order = Order,
                Exp = Exp,
                ColorCode = ColorCode,
        };
        }
    }
}