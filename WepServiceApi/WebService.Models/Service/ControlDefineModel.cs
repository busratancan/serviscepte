﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Models.Base;
using WepService.Data;

namespace WebService.Models.Service
{
    public class ControlDefineModel : BaseModel, IConvertible<ServiceControlDefines>
    {
        public ControlDefineModel()
        {

        }

        public ControlDefineModel(ServiceControlDefines scd)
        {
            Id = scd.Id;
            OrderNumber = scd.OrderNumber;
            IsImportant = scd.IsImportant;
            ControlDefine = scd.ControlDefine;
            Active = scd.Active;
            Code = scd.Code;
            Color = scd.Color;
            ServicesRequestDefinationId = scd.ServicesRequestDefinationId;
            ServicesCategorySubDefinitionId = scd.ServicesCategorySubDefinitionId;
            ForceSelect = scd.ForceSelect;
        }

        public int Id { get; set; }
        public int? OrderNumber { get; set; }
        public sbyte? IsImportant { get; set; }
        public string ControlDefine { get; set; }
        public sbyte Active { get; set; }
        public string Code { get; set; }
        public sbyte? Color { get; set; }
        public int? ServicesRequestDefinationId { get; set; }
        public int? ServicesCategorySubDefinitionId { get; set; }
        public sbyte? ForceSelect { get; set; }

        public List<ControlDefineItemModel> ControlDefineItemModel { get; set; }

        public ServiceControlDefines Convert()
        {
            return new ServiceControlDefines()
            {
                Id = Id,
                OrderNumber = OrderNumber,
                IsImportant = IsImportant,
                ControlDefine = ControlDefine,
                Active = Active,
                Code = Code,
                Color = Color,
                ServicesRequestDefinationId = ServicesRequestDefinationId,
                ServicesCategorySubDefinitionId = ServicesCategorySubDefinitionId,
                ForceSelect = ForceSelect,
        };
        }
    }
}
