﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebService.Models.Base
{
    public abstract class ListBaseModel<TModel>
    {
        public List<TModel> List { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPage
        {
            get
            {
                return Count / PageCount;
            }
        }
        public int Count { get; set; }
        public int PageCount { get; set; }
    }

    public class ListBaseModel<T1, T2, T3, T4, T5>
    {
        public List<T1, T2, T3, T4, T5> List { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPage
        {
            get
            {
                return Count / PageCount;
            }
        }
        public int Count { get; set; }
        public int PageCount { get; set; }
    }
    public class ListBaseModel<T1, T2, T3, T4>
    {
        public List<T1, T2, T3, T4> List { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPage
        {
            get
            {
                return Count / PageCount;
            }
        }
        public int Count { get; set; }
        public int PageCount { get; set; }
    }

    public class List<T1, T2, T3, T4>
    {
    }

    public class List<T1, T2, T3, T4, T5>
    {
    }
}
