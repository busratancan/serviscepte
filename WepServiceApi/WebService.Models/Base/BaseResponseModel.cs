﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebService.Models.Base
{
    public class BaseResponseModel<T> : BaseModel
    {
        public string Version
        {
            get
            {
                return Microsoft.Extensions.PlatformAbstractions.PlatformServices.Default.Application.ApplicationVersion;
            }
        }
        public string Result { get; set; }
        public string ErrorMessage { get; set; }
        public T Body { get; set; }
    }
}
