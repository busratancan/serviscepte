﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using WepService.Data;

namespace WebService.Models.Base.Search
{
    interface ISearchable<TEntity>
        where TEntity : BaseEntity
    {
        Expression<Func<TEntity, bool>> GetFilter();
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> GetOrderBy();
    }
}
