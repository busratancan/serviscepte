﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace WebService.Models.Base.Search
{
    public abstract class ListRequestModel : BaseRequestModel
    {
        public ListRequestModel(
            int pageNumber,
            int pageCount)
        {
            this.PageNumber = pageNumber;
            this.PageCount = pageCount;
        }

        public override Task BindModelAsync(ModelBindingContext bindingContext)
        {
            base.BindModelAsync(bindingContext);

            var pageCountProvider = bindingContext.ValueProvider.GetValue("pageCount");
            var pageNumberProvider = bindingContext.ValueProvider.GetValue("pageNumber");

            if (pageCountProvider == ValueProviderResult.None ||
                pageNumberProvider == ValueProviderResult.None)
                return Task.CompletedTask;

            int pageCountValue = 25;
            int pageNumberValue = 1;

            Int32.TryParse(pageCountProvider.FirstValue, out pageCountValue);
            Int32.TryParse(pageNumberProvider.FirstValue, out pageNumberValue);

            PageCount = pageCountValue > 200 ? 200 : pageCountValue;
            PageNumber = pageNumberValue;

            return Task.CompletedTask;
        }

        public int PageNumber { get; set; } = 1;
        public int PageCount { get; set; } = 25;

        public int GetStartRow()
        {
            return (PageNumber - 1) * PageCount;
        }
    }
}