﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
namespace WebService.Models.Base
{
    public abstract class BaseRequestModel : IModelBinder
    {
        public string Version { get; set; }
        public string Lang { get; set; }

        public virtual Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
            {
                throw new ArgumentNullException(nameof(bindingContext));
            }

            bindingContext.Result = ModelBindingResult.Success(this);

            return Task.CompletedTask;
        }
    }
}