﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebService.Models.Base
{
    public class BaseNoteItem
    {
        public int NoteId { get; set; }
        public int BaseId { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Note { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
