﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebService.Models.Base
{
    public interface IConvertible<TEntity>
    {
        TEntity Convert();
    }
    internal interface IConvertible<T1, T2, T3, T4, T5>
    {

    }

    internal interface IConvertible<T1, T2, T3, T4>
    {
    }
}
