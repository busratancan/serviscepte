﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Models.Base;
using WepService.Data;

namespace WebService.Models.Service
{
    public class ServicePointModel : BaseModel, IConvertible<ServicesPointsBaseInformation, CustomerEntity, ServicesContract, ServicesCategoryDefinition>
    {
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string customer_code { get; set; } //customer
        public string account_grup { get; set; } //c
        public string account_title { get; set; } //c
        public int sozlesmekalangun { get; set; }
        public DateTime? start_date { get; set; } //services contract
        public DateTime? end_date { get; set; } //sc
        public string color { get; set; } //services category def
        public int Id { get; set; }
        public DateTime? ttance_date { get; set; }
        public sbyte? manufacturer { get; set; }
        public sbyte? warranty { get; set; }
        public DateTime? due_date { get; set; }
        public DateTime? installation_date { get; set; }
        public int? customer_id { get; set; }
        public int? rrvice_order { get; set; }
        public string identification_number { get; set; }
        public string service_code { get; set; }
        public string service_name { get; set; }
        public string direction { get; set; }
        public int? services_category_definition_id { get; set; }
        public string services_category_name { get; set; }
        public sbyte? deletion { get; set; }
        public int? services_contract_id { get; set; }
        public sbyte? active { get; set; }

        public ServicePointModel(ServicesPointsBaseInformation sp, CustomerEntity c, ServicesContract sc, ServicesCategoryDefinition scd)
        {
            latitude = sp.Latitude;
            longitude = sp.Longitude;
            customer_code = c.CustomerCode;
            account_grup = c.AccountGrup;
            account_title = c.AccountTitle;
            start_date = sc.StartDate;
            end_date = sc.EndDate;
            color = scd.Color;
            Id = sp.Id;
            ttance_date = sp.AcceptanceDate;
            manufacturer = sp.Manufacturer;
            warranty = sp.Warranty;
            due_date = sp.DueDate;
            installation_date = sp.InstallationDate;
            customer_id = sp.CustomerId;
            rrvice_order = sp.ServiceOrder;
            identification_number = sp.IdentificationNumber;
            service_code = sp.ServiceCode;
            service_name = sp.ServiceName;
            direction = sp.Direction;
            services_category_definition_id = sp.ServicesCategoryDefinitionId;
            services_category_name = sp.ServicesCategoryName;
            deletion = sp.Deletion;
            services_contract_id = sp.ServicesContractId;
            active = sp.Active;
        }
    }
}
