﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Models.Base;
using WepService.Data;

namespace WebService.Models.Customer
{
    public class UserPictureModel : BaseModel, IConvertible<CustomerEntity>
    {
        public UserPictureModel()
        {
        }
        public byte[] Picture { get; set; }

        public UserPictureModel(CustomerEntity c)
        {
            Picture = c.Picture;
        }

        public List<UserPictureItemModel> CustomerItemModel { get; set; }

        public CustomerEntity Convert()
        {
            return new CustomerEntity()
            {
                Picture = Picture,
            };
        }
    }
}

    
