﻿using System;
using System.Collections.Generic;
using System.Text;
using WepService.Data;

namespace WebService.Models.Customer
{
    public class UserPictureItemModel
    {
        public byte[] Picture { get; set; }

        public UserPictureItemModel(CustomerEntity c)
        {
            Picture = c.Picture;
        }
    }
}
