﻿using System;
using System.Collections.Generic;
using System.Text;
using WepService.Data;

namespace WebService.Models.Customer
{
    public class AccountActivitiesItemModel
    {
        public int farkgun { get; set; }
        public string doc_number { get; set; }
        public string alacaktutari { get; set; }
        public string borctutari { get; set; }
        public int Id { get; set; }
        public int? detail_id { get; set; }
        public DateTime? datetime { get; set; }
        public sbyte? type { get; set; }
        public decimal? currency { get; set; }
        public decimal? tutar { get; set; }
        public string exp { get; set; }
        public decimal? bakiye { get; set; }
        public string stats { get; set; }
        public string activity_type { get; set; }

        public AccountActivitiesItemModel(CustomerCurrentInput cc)
        {
            doc_number = cc.DocNumber;
            Id = cc.Id;
            detail_id = cc.DetailId;
            datetime = cc.Datetime;
            type = cc.Type;
            currency = cc.Currency;
            exp = cc.Exp;
        }
    }
}