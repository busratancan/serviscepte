﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Models.Base;
using WepService.Data;

namespace WebService.Models.Customer
{
    public class CustomerModel : BaseModel, IConvertible<CustomerEntity>
    {
        public CustomerModel()
        {
        }
        public sbyte? Visible { get; set; }
        public string AccountGrup { get; set; }
        public int? RouteOrder { get; set; }
        public string SalesBlock { get; set; }
        public string AccountType { get; set; }
        public string ContactPerson { get; set; }
        public string Task { get; set; }
        public string CustomerCode { get; set; }
        public int Id { get; set; }
        public string AccountTitle { get; set; }
        public string TaxOffice { get; set; }
        public string TaxNumber { get; set; }
        public string Adress2 { get; set; }
        public string Adress { get; set; }
        public sbyte? PricePrivileged { get; set; }
        public int? PricingId { get; set; }
        public string MobilePhone { get; set; }

        public CustomerModel(CustomerEntity c)
        {
            Id = c.Id;
            Visible = c.Visible;
            AccountGrup = c.AccountGrup;
            RouteOrder = c.RouteOrder;
            SalesBlock = c.SalesBlock;
            AccountType = c.AccountType;
            ContactPerson = c.ContactPerson;
            Task = c.Task;
            CustomerCode = c.CustomerCode;
            AccountTitle = c.AccountTitle;
            TaxOffice = c.TaxOffice;
            TaxNumber = c.TaxNumber;
            Adress2 = c.Adress2;
            Adress = c.Adress;
            PricePrivileged = c.PricePrivileged;
            PricingId = c.PricingId;
            MobilePhone = c.MobilePhone;

        }
        public List<CustomerItemModel> CustomerItemModel { get; set; }

        public CustomerEntity Convert()
        {
            return new CustomerEntity()
            {
                Id = Id,
                CustomerCode = CustomerCode,
                MobilePhone = MobilePhone,            
            };
        }
    }
}
