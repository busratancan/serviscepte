﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class StockActivitiesSub
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public decimal? Qty { get; set; }
        public int? UserId { get; set; }
        public DateTime? Datetime { get; set; }
        public sbyte? Storage { get; set; }
        public int? InvoiceProductSubId { get; set; }
        public int? ColorId { get; set; }
        public int? Type { get; set; }
        public int? StockCountingSubId { get; set; }
    }
}
