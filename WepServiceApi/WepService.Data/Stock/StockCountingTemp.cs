﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class StockCountingTemp
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public int? ProductId { get; set; }
        public decimal? Deficieny { get; set; }
        public decimal? Excess { get; set; }
        public string Barcode { get; set; }
        public decimal? Equality { get; set; }
        public int? Basket { get; set; }
        public sbyte? ProductType { get; set; }
        public sbyte? ForceSerialTrack { get; set; }
    }
}
