﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class InvoiceProduct
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? Qty { get; set; }
        public sbyte? TaxRate { get; set; }
        public decimal? WithholdingRate { get; set; }
        public decimal? DiscountRate1 { get; set; }
        public decimal? DiscountRate2 { get; set; }
        public decimal? DiscountRate3 { get; set; }
        public decimal? DiscountRate4 { get; set; }
        public decimal? DiscountRate5 { get; set; }
        public int? Basket { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string Barcode { get; set; }
        public int? StorageId { get; set; }
        public sbyte? Type { get; set; }
        public string UnitName { get; set; }
        public int? ColorId { get; set; }
        public sbyte? Deletion { get; set; }
        public sbyte? ProductType { get; set; }
        public int? DeletionUserId { get; set; }
        public int? DetailId { get; set; }
        public string Size { get; set; }

        public Product Product { get; set; }
    }
}
