﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class InvoiceShipmentTrace
    {
        public int Id { get; set; }
        public int? InvoiceId { get; set; }
        public int? ProductId { get; set; }
        public int? StorageId { get; set; }
        public int? Basket { get; set; }
        public int? InvoiceProductBasket { get; set; }
        public DateTime? ShippingDatetime { get; set; }
        public string DriverName { get; set; }
        public string Plate { get; set; }
        public int? Deletion { get; set; }

        public Invoice Invoice { get; set; }
        public Product Product { get; set; }
        public Storage Storage { get; set; }
    }
}
