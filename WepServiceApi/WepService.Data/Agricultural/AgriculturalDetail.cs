﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class AgriculturalDetail : BaseEntity
    {
        public override int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int? ProductId { get; set; }
        public decimal? FormulaQty { get; set; }
        public string ProductName { get; set; }
        public int? AgriculturalId { get; set; }
        public sbyte? Days { get; set; }
        public override sbyte? Deletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}
