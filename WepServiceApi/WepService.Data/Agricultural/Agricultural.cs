﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class Agricultural : BaseEntity
    {
        public Agricultural()
        {
            AgriculturalCustomer = new HashSet<AgriculturalCustomer>();
        }

        public override int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string Code { get; set; }
        public string Defination { get; set; }
        public string Exp1 { get; set; }
        public string Exp2 { get; set; }
        public string Unit { get; set; }

        public ICollection<AgriculturalCustomer> AgriculturalCustomer { get; set; }
        public override sbyte? Deletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}
