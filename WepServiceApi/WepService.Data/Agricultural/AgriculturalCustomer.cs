﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class AgriculturalCustomer : BaseEntity
    {
        public override int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public DateTime? AppDate { get; set; }
        public int? CustomerId { get; set; }
        public string AgriculturalName { get; set; }
        public int? AgriculturalId { get; set; }
        public decimal? UnitFactor { get; set; }

        // public override DateTime CreatedDate { get; set; }

        public Agricultural Agricultural { get; set; }
        public CustomerEntity Customer { get; set; }
        public override sbyte? Deletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}