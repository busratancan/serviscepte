﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class BankDepartment
    {
        public int Id { get; set; }
        public int? BankId { get; set; }
        public string BankDepartment1 { get; set; }
        public string DepartmentTel { get; set; }
        public string DepartmentContactPerson { get; set; }
        public string Exp { get; set; }
        public string Exp2 { get; set; }
        public string DepartmentFax { get; set; }
        public string DepartmentAdress { get; set; }
        public string DepartmentMail { get; set; }
        public string Country { get; set; }
        public string Town { get; set; }
        public string BankName { get; set; }
    }
}
