﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class BankAccount
    {
        public BankAccount()
        {
            Pos = new HashSet<Pos>();
        }

        public int Id { get; set; }
        public string AccountHolder { get; set; }
        public int? BankId { get; set; }
        public string BankDepartment { get; set; }
        public string AccountNumber { get; set; }
        public string AdAccountNumber { get; set; }
        public string Iban { get; set; }
        public string DepartmentTel { get; set; }
        public string DepartmentContactPerson { get; set; }
        public string Exp { get; set; }
        public string Exp2 { get; set; }
        public string DepartmentFax { get; set; }
        public string DepartmentAdress { get; set; }
        public string DepartmentMail { get; set; }
        public string BankName { get; set; }
        public string DepartmantName { get; set; }
        public int? DetailId { get; set; }

        public Bank Bank { get; set; }
        public ICollection<Pos> Pos { get; set; }
    }
}
