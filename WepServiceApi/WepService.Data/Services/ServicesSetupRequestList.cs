﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ServicesSetupRequestList
    {
        public int Id { get; set; }
        public int? ServicesSetupDefinesId { get; set; }
        public int? ServicesRequestDefinationId { get; set; }
        public string RequestDefination { get; set; }
        public int? Order { get; set; }
        public int? MinuteDifference { get; set; }
        public int? HourDifference { get; set; }
        public int? DayDifference { get; set; }
        public sbyte? Deletion { get; set; }
        public int? DeletionUserId { get; set; }
        public DateTime? DeletionDateTime { get; set; }

        public ServicesRequestDefination ServicesRequestDefination { get; set; }
    }
}
