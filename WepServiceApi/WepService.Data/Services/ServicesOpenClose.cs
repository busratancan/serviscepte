﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ServicesOpenClose
    {
        public int Id { get; set; }
        public int? ServicesOpenId { get; set; }
        public int? ClosedUserId { get; set; }
        public sbyte? ServicesClosedTunnel { get; set; }
        public DateTime? InsTime { get; set; }
        public DateTime? ClosedDateTime { get; set; }
        public int ServicesLastStatusDefinationId { get; set; }

        public Management ClosedUser { get; set; }
        public ServicesLastStatusDefination ServicesLastStatusDefination { get; set; }
        public ServicesOpen ServicesOpen { get; set; }
    }
}
