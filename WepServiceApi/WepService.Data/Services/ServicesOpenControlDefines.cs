﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ServicesOpenControlDefines : BaseEntity
    {
        public override int Id { get; set; }
        public int? ServiceControlDefinesId { get; set; }
        public int? ServicesOpenId { get; set; }

        public ServiceControlDefines ServiceControlDefines { get; set; }
        public ServicesOpen ServicesOpen { get; set; }
        public override sbyte? Deletion { get; set; }
    }
}
