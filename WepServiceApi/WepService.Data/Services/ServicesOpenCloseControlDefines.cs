﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ServicesOpenCloseControlDefines : BaseEntity
    {
        public override int Id { get; set; }
        public int? ServiceControlDefinesId { get; set; }
        public int? ServicesOpenId { get; set; }
        public string Result1 { get; set; }
        public string Result2 { get; set; }
        public string Result3 { get; set; }
        public DateTime? StartTime { get; set; }
        public sbyte? Type { get; set; }
        public int? ServiceControlDefinesSubId { get; set; }
        public string Photo { get; set; }
        public override sbyte? Deletion { get; set; }
        public ServiceControlDefines ServiceControlDefines { get; set; }
        public ServicesOpen ServicesOpen { get; set; }
    }
}
