﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ServicesOpenCloseParameters
    {
        public int Id { get; set; }
        public int? ServicesOpenId { get; set; }
        public int? ServicesLastStatusDefinationInfsId { get; set; }
        public string Field1 { get; set; }
        public string Field2 { get; set; }

        public ServicesLastStatusDefinationInfs ServicesLastStatusDefinationInfs { get; set; }
        public ServicesOpen ServicesOpen { get; set; }
    }
}
