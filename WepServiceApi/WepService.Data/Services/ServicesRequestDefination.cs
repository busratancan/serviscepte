﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ServicesRequestDefination : BaseEntity
    {
        public ServicesRequestDefination()
        {
            ServiceControlDefines = new HashSet<ServiceControlDefines>();
            ServicesContract = new HashSet<ServicesContract>();
            ServicesLastStatusDefination = new HashSet<ServicesLastStatusDefination>();
            ServicesOpen = new HashSet<ServicesOpen>();
            ServicesRequestDefinationInfs = new HashSet<ServicesRequestDefinationInfs>();
            ServicesSetupRequestList = new HashSet<ServicesSetupRequestList>();
        }

        public override int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string Defination { get; set; }
        public string Code { get; set; }
        public string Color { get; set; }
        public string HexColor { get; set; }
        public string ColorCode { get; set; }
        public int? Order { get; set; }
        public string Exp { get; set; }
        public int? ServicesCategoryDefinitionId { get; set; }
        public string ServicesCategoryName { get; set; }
        public sbyte? Period { get; set; }
        public sbyte? ForcedContract { get; set; }
        public sbyte? ComponentDialog { get; set; }
        public sbyte? MobileVisible { get; set; }
        public sbyte? OpenWithoutPersonel { get; set; }
        public sbyte? CustomizableSubModule { get; set; }
        public sbyte? AllowNotAccept { get; set; }
        public override sbyte? Deletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public DateTime? DeletionDateTime { get; set; }
        public int? DeletionUserId { get; set; }
        public sbyte? AutoInsertServiceInContract { get; set; }
        public int? SmsThemesId { get; set; }
        public string SmsThemesName { get; set; }
        public string ContractReportFile { get; set; }
        public string ServiceReportFile { get; set; }
        public sbyte? ServiceReportFileActive { get; set; }
        public string ServiceReportExcelFile { get; set; }
        public sbyte? ServiceReportExcelFileActive { get; set; }
        public int? TaskAcceptTimeout { get; set; }
        public int? SmsStartThemesId { get; set; }
        public string SmsStartThemesName { get; set; }
        public sbyte? AutomaticCharging { get; set; }
        public int? OperationTime { get; set; }
        public sbyte? PieceBillsAreMergedAutomatically { get; set; }
        public string RequestGroup { get; set; }
        public sbyte? OpenServiceWithoutPoint { get; set; }
        public string OfferExcelTemplate { get; set; }
        public sbyte? OfferExcelProductsPageNo { get; set; }
        public string OfferExcelProductsStartColumn { get; set; }
        public sbyte? OfferExcelProductsStartRow { get; set; }
        public string OfferExcelProductsPrintPages { get; set; }
        public sbyte? OfferExcelControlsPageNo { get; set; }
        public string OfferExcelControlsStartColumn { get; set; }
        public sbyte? OfferExcelControlsStartRow { get; set; }
        public string OfferExcelPrintPages { get; set; }

        public ServicesCategoryDefinition ServicesCategoryDefinition { get; set; }
        public SmsThemes SmsThemes { get; set; }
        public ICollection<ServiceControlDefines> ServiceControlDefines { get; set; }
        public ICollection<ServicesContract> ServicesContract { get; set; }
        public ICollection<ServicesLastStatusDefination> ServicesLastStatusDefination { get; set; }
        public ICollection<ServicesOpen> ServicesOpen { get; set; }
        public ICollection<ServicesRequestDefinationInfs> ServicesRequestDefinationInfs { get; set; }
        public ICollection<ServicesSetupRequestList> ServicesSetupRequestList { get; set; }
    }
}
