﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ServicesSpeedButtons
    {
        public int Id { get; set; }
        public int? RequestId { get; set; }
        public string Defination { get; set; }
        public sbyte? Active { get; set; }
        public byte[] Glyph { get; set; }
        public int? UserId { get; set; }
        public sbyte? ButtonType { get; set; }

        public Management User { get; set; }
    }
}
