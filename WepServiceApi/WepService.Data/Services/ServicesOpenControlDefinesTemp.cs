﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ServicesOpenControlDefinesTemp : BaseEntity
    {
        public override int Id { get; set; }
        public int? BasketId { get; set; }
        public int? ServiceControlDefinesId { get; set; }
        public string ControlDefine { get; set; }
        public string ModuleDefination { get; set; }

        public ServiceControlDefines ServiceControlDefines { get; set; }
        public override sbyte? Deletion { get; set; }
    }
}
