﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ServicesDirectionList
    {
        public int Id { get; set; }
        public string DirectionName { get; set; }
    }
}
