﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ServicesOpenParameters
    {
        public int Id { get; set; }
        public int? ServicesOpenId { get; set; }
        public int? ServicesRequestDefinationInfsId { get; set; }
        public string Field1 { get; set; }
        public string Field2 { get; set; }

        public ServicesOpen ServicesOpen { get; set; }
        public ServicesRequestDefinationInfs ServicesRequestDefinationInfs { get; set; }
    }
}
