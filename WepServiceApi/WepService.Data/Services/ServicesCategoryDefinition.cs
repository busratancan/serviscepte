﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ServicesCategoryDefinition : ServiceBaseEntity
    {
        public ServicesCategoryDefinition()
        {
            ServicesCategorySubDefinition = new HashSet<ServicesCategorySubDefinition>();
            ServicesContract = new HashSet<ServicesContract>();
            ServicesLastStatusDefination = new HashSet<ServicesLastStatusDefination>();
            ServicesPointsBaseInformation = new HashSet<ServicesPointsBaseInformation>();
            ServicesRequestDefination = new HashSet<ServicesRequestDefination>();
        }

        public override int Id { get; set; }
        public override string Defination { get; set; }
        public override string Code { get; set; }
        public override string Color { get; set; }
        public override int? Order { get; set; }
        public override string Exp { get; set; }
        public string ColorCode { get; set; }
    

        public ICollection<ServicesCategorySubDefinition> ServicesCategorySubDefinition { get; set; }
        public ICollection<ServicesContract> ServicesContract { get; set; }
        public ICollection<ServicesLastStatusDefination> ServicesLastStatusDefination { get; set; }
        public ICollection<ServicesPointsBaseInformation> ServicesPointsBaseInformation { get; set; }
        public ICollection<ServicesRequestDefination> ServicesRequestDefination { get; set; }
        public override sbyte? Deletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}
