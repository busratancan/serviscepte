﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ServicesLastStatusDefinationInfs : BaseEntity
    {
        public ServicesLastStatusDefinationInfs()
        {
            ServicesOpenCloseParameters = new HashSet<ServicesOpenCloseParameters>();
        }

        public override int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string Caption { get; set; }
        public sbyte? TextIn { get; set; }
        public sbyte? NumericIn { get; set; }
        public sbyte? IntegerIn { get; set; }
        public override sbyte? Deletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int? ServicesLastStatusDefinationId { get; set; }
        public sbyte? PersonelIn { get; set; }
        public sbyte? DateIn { get; set; }
        public sbyte? ForceEntery { get; set; }
        public sbyte? OrderNumber { get; set; }

        public ServicesLastStatusDefination ServicesLastStatusDefination { get; set; }
        public ICollection<ServicesOpenCloseParameters> ServicesOpenCloseParameters { get; set; }

        //public static implicit operator ServicesLastStatusDefinationInfs(ServicesLastStatusDefination v)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
