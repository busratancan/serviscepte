﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ServicesOpen : BaseEntity
    {
        public object services_open_id;

        public ServicesOpen()
        {
            InverseMasterServicesOpen = new HashSet<ServicesOpen>();
            ServicesOpenActivity = new HashSet<ServicesOpenActivity>();
            ServicesOpenCloseControlDefines = new HashSet<ServicesOpenCloseControlDefines>();
            ServicesOpenCloseParameters = new HashSet<ServicesOpenCloseParameters>();
            ServicesOpenControlDefines = new HashSet<ServicesOpenControlDefines>();
            ServicesOpenParameters = new HashSet<ServicesOpenParameters>();
        }

        public override int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int? CustomerId { get; set; }
        public int? ServicesRequestDefinatinId { get; set; }
        public DateTime? InsertTime { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? PlanFinishDateTime { get; set; }
        public DateTime? FinishDateTime { get; set; }
        public int? OpenedUserId { get; set; }
        public int? ResponsibleUserId { get; set; }
        public DateTime? LastValidDateTime { get; set; }
        public sbyte? Status { get; set; }
        public sbyte? ServicesOpenType { get; set; }
        public sbyte? ServicesOpenTunnel { get; set; }
        public override sbyte? Deletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public DateTime? DeletionDatetime { get; set; }
        public int? DeletionUserId { get; set; }
        public int? ServicesPointsBaseInformationId { get; set; }
        public decimal? ServiceFee { get; set; }
        public int? OpensId { get; set; }
        public sbyte? FaultStats { get; set; }
        public int? InvoiceId { get; set; }
        public int? Options { get; set; }
        public int? EventType { get; set; }
        public int? MasterServicesOpenId { get; set; }

        public CustomerEntity Customer { get; set; }
        public Invoice Invoice { get; set; }
        public ServicesOpen MasterServicesOpen { get; set; }
        public ServicesPointsBaseInformation ServicesPointsBaseInformation { get; set; }
        public ServicesRequestDefination ServicesRequestDefinatin { get; set; }
        public ServicesOpenClose ServicesOpenClose { get; set; }
        public ICollection<ServicesOpen> InverseMasterServicesOpen { get; set; }
        public ICollection<ServicesOpenActivity> ServicesOpenActivity { get; set; }
        public ICollection<ServicesOpenCloseControlDefines> ServicesOpenCloseControlDefines { get; set; }
        public ICollection<ServicesOpenCloseParameters> ServicesOpenCloseParameters { get; set; }
        public ICollection<ServicesOpenControlDefines> ServicesOpenControlDefines { get; set; }
        public ICollection<ServicesOpenParameters> ServicesOpenParameters { get; set; }
       
    }
}
