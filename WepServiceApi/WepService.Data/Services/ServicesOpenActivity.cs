﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ServicesOpenActivity
    {
        public int Id { get; set; }
        public int? ServicesOpenId { get; set; }
        public sbyte? Type { get; set; }
        public int? UserId { get; set; }

        public ServicesOpen ServicesOpen { get; set; }
        public Management User { get; set; }
    }
}
