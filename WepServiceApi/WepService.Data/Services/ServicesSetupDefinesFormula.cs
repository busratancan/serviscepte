﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ServicesSetupDefinesFormula
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal? Qty { get; set; }
        public string Formula { get; set; }
        public int? ServicesSetupDefinesId { get; set; }
        public sbyte Deletion { get; set; }
        public sbyte DeletionUserId { get; set; }
        public DateTime DeletionDateTime { get; set; }
    }
}
