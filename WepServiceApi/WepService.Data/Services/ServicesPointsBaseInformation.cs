﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ServicesPointsBaseInformation : BaseEntity
    {
        public ServicesPointsBaseInformation()
        {
            ElevatorCustomerCardParameters = new HashSet<ElevatorCustomerCardParameters>();
            ServicesOpen = new HashSet<ServicesOpen>();
            ServicesProductChangeList = new HashSet<ServicesProductChangeList>();
            ServicesProductChangeListTemp = new HashSet<ServicesProductChangeListTemp>();
        }

        public override int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public DateTime? AcceptanceDate { get; set; }
        public sbyte? Manufacturer { get; set; }
        public sbyte? Warranty { get; set; }
        public DateTime? WarrantyEndDate { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? InstallationDate { get; set; }
        public int? CustomerId { get; set; }
        public int? ServiceOrder { get; set; }
        public string IdentificationNumber { get; set; }
        public string ServiceCode { get; set; }
        public string ServiceName { get; set; }
        public string Direction { get; set; }
        public int? ServicesCategoryDefinitionId { get; set; }
        public string ServicesCategoryName { get; set; }
        public int? ServicesContractId { get; set; }
        public sbyte? Active { get; set; }
        public int? DeletionUserId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public CustomerEntity Customer { get; set; }
        public ServicesCategoryDefinition ServicesCategoryDefinition { get; set; }
        public ServicesContract ServicesContract { get; set; }
        public ICollection<ElevatorCustomerCardParameters> ElevatorCustomerCardParameters { get; set; }
        public ICollection<ServicesOpen> ServicesOpen { get; set; }
        public ICollection<ServicesProductChangeList> ServicesProductChangeList { get; set; }
        public ICollection<ServicesProductChangeListTemp> ServicesProductChangeListTemp { get; set; }
        public override sbyte? Deletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}
