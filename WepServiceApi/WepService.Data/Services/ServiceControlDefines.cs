﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{ 
    public partial class ServiceControlDefines : BaseEntity
    {
        public ServiceControlDefines()
        {
            ServiceControlSubDefines = new HashSet<ServiceControlSubDefines>();
            ServicesOpenCloseControlDefines = new HashSet<ServicesOpenCloseControlDefines>();
            ServicesOpenControlDefines = new HashSet<ServicesOpenControlDefines>();
            ServicesOpenControlDefinesTemp = new HashSet<ServicesOpenControlDefinesTemp>();
        }
       
        public override int Id { get; set; }
        public int? OrderNumber { get; set; }
        public sbyte? IsImportant { get; set; }
        public string ControlDefine { get; set; }
        public sbyte Active { get; set; }
        public string Code { get; set; }
        public sbyte? Color { get; set; }
        public int? ServicesRequestDefinationId { get; set; }
        public int? ServicesCategorySubDefinitionId { get; set; }
        public sbyte? PhotoIn { get; set; }
        public sbyte? CheckboxIn { get; set; }
        public sbyte? TextIn { get; set; }
        public sbyte? IntegerIn { get; set; }
        public sbyte? DecimalIn { get; set; }
        public sbyte? SubIn { get; set; }
        public sbyte? ForceSelect { get; set; }


        public ServicesCategorySubDefinition ServicesCategorySubDefinition { get; set; }
        public ServicesRequestDefination ServicesRequestDefination { get; set; }
        public ICollection<ServiceControlSubDefines> ServiceControlSubDefines { get; set; }
        public ICollection<ServicesOpenCloseControlDefines> ServicesOpenCloseControlDefines { get; set; }
        public ICollection<ServicesOpenControlDefines> ServicesOpenControlDefines { get; set; }
        public ICollection<ServicesOpenControlDefinesTemp> ServicesOpenControlDefinesTemp { get; set; }
        public override sbyte? Deletion { get; set; }
    }
}
