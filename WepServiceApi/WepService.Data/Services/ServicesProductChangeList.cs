﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ServicesProductChangeList
    {
        public int Id { get; set; }
        public string FirstPhoto { get; set; }
        public string LastPhoto { get; set; }
        public sbyte? InvoiceStats { get; set; }
        public int? InvoiceId { get; set; }
        public string Note { get; set; }
        public int? ProductId { get; set; }
        public int? Qty { get; set; }
        public sbyte? Type { get; set; }
        public DateTime? InsDateTime { get; set; }
        public int? ServicesPointsBaseInformationId { get; set; }
        public int? Basket { get; set; }
        public int? UserId { get; set; }
        public DateTime? DateTime { get; set; }
        public int? ServicesOpenId { get; set; }
        public sbyte? Deletion { get; set; }
        public int? DetailId { get; set; }
        public decimal? UnitPriceTax { get; set; }

        public Invoice Invoice { get; set; }
        public Product Product { get; set; }
        public ServicesPointsBaseInformation ServicesPointsBaseInformation { get; set; }
    }
}
