﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class MobileScaleDevice
    {
        public MobileScaleDevice()
        {
            MobileScaleSync = new HashSet<MobileScaleSync>();
        }

        public int Id { get; set; }
        public int? VehicleId { get; set; }
        public int? DeviceId { get; set; }
        public string Name { get; set; }
        public string DeviceSerial { get; set; }
        public string DeviceKey { get; set; }
        public string LicancePlate { get; set; }

        public Vehicle Vehicle { get; set; }
        public ICollection<MobileScaleSync> MobileScaleSync { get; set; }
    }
}
