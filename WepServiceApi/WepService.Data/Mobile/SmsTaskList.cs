﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class SmsTaskList : BaseEntity
    {
        public SmsTaskList()
        {
            SmsBalance = new HashSet<SmsBalance>();
        }

        public override int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string Message { get; set; }
        public string Number { get; set; }
        public int? CustomerId { get; set; }
        public DateTime? SendDate { get; set; }
        public sbyte? Status { get; set; }
        public int? BasketId { get; set; }
        public sbyte? Type { get; set; }
        public string Referance { get; set; }
        public string KcReferance { get; set; }

        public CustomerEntity Customer { get; set; }
        public ICollection<SmsBalance> SmsBalance { get; set; }
        public override sbyte? Deletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}
