﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class SmsBalance
    {
        public int Id { get; set; }
        public sbyte? Type { get; set; }
        public int? SmsTaskListId { get; set; }
        public int? Qty { get; set; }
        public DateTime? DateTime { get; set; }
        public string Exp { get; set; }

        public SmsTaskList SmsTaskList { get; set; }
    }
}
