﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class MobileScaleSync
    {
        public int Id { get; set; }
        public sbyte? SyncType { get; set; }
        public int? LastSyncId { get; set; }
        public int? MobileScaleDeviceId { get; set; }
        public DateTime? Date { get; set; }
        public sbyte? Progress { get; set; }

        public MobileScaleDevice MobileScaleDevice { get; set; }
    }
}
