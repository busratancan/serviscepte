﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class SmsTask
    {
        public int Id { get; set; }
        public string TaskName { get; set; }
        public int? SmsTaskListBasketId { get; set; }
        public DateTime? DateTime { get; set; }
    }
}
