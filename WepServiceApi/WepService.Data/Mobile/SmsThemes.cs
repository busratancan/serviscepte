﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class SmsThemes
    {
        public SmsThemes()
        {
            ServicesLastStatusDefination = new HashSet<ServicesLastStatusDefination>();
            ServicesRequestDefination = new HashSet<ServicesRequestDefination>();
        }

        public int Id { get; set; }
        public string ThemeName { get; set; }
        public string Theme { get; set; }

        public ICollection<ServicesLastStatusDefination> ServicesLastStatusDefination { get; set; }
        public ICollection<ServicesRequestDefination> ServicesRequestDefination { get; set; }
    }
}
