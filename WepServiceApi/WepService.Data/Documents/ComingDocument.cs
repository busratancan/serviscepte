﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ComingDocument
    {
        public int Id { get; set; }
        public DateTime InputDate { get; set; }
        public DateTime DocumentDate { get; set; }
        public DateTime SaveDate { get; set; }
        public int UserId { get; set; }
        public string DocumentRegisterNumber { get; set; }
        public string DocumentSource { get; set; }
        public string SubjectCode { get; set; }
        public string Subject { get; set; }
        public string Attachments { get; set; }
        public string FilePath { get; set; }
    }
}
