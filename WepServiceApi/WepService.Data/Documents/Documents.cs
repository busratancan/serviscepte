﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class Documents
    {
        public int Id { get; set; }
        public int? TotalPage { get; set; }
        public int? Page { get; set; }
        public byte[] Document { get; set; }
        public int? CustomerDocumentDefinitionsId { get; set; }
        public sbyte? Deletion { get; set; }
        public int? Basket { get; set; }
    }
}
