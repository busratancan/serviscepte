﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class Token
    {
        public int Id { get; set; }
        public string AccesKey { get; set; }
        public string Value { get; set; }
    }
}
