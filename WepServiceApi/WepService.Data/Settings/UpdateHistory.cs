﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class UpdateHistory
    {
        public int Id { get; set; }
        public string VersionNumber { get; set; }
        public DateTime? UpdateTime { get; set; }
        public int? EmergencyRate { get; set; }
        public string VersionUrl { get; set; }
        public string Comments { get; set; }
        public string UpdateState { get; set; }
        public DateTime? InsertTime { get; set; }
        public string UploadPath { get; set; }
        public sbyte? IsServer { get; set; }
    }
}
