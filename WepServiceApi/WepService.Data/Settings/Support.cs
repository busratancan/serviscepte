﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class Support
    {
        public int Id { get; set; }
        public string ReceiverUnit { get; set; }
        public string Request { get; set; }
        public sbyte FeedBack { get; set; }
        public int UserId { get; set; }
        public sbyte Status { get; set; }
    }
}
