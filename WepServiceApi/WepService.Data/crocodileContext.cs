﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WepService.Data
{
    public partial class crocodileContext : DbContext
    {
        public crocodileContext()
        {
        }

        public crocodileContext(DbContextOptions<crocodileContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Agricultural> Agricultural { get; set; }
        public virtual DbSet<AgriculturalCustomer> AgriculturalCustomer { get; set; }
        public virtual DbSet<AgriculturalDetail> AgriculturalDetail { get; set; }
        public virtual DbSet<ArchiveGroups> ArchiveGroups { get; set; }
        public virtual DbSet<ArchiveManagement> ArchiveManagement { get; set; }
        public virtual DbSet<Bank> Bank { get; set; }
        public virtual DbSet<BankAccount> BankAccount { get; set; }
        public virtual DbSet<BankAccountIn> BankAccountIn { get; set; }
        public virtual DbSet<BankAccountOut> BankAccountOut { get; set; }
        public virtual DbSet<BankDepartment> BankDepartment { get; set; }
        public virtual DbSet<Basket> Basket { get; set; }
        public virtual DbSet<Branch> Branch { get; set; }
        public virtual DbSet<BusinessCreditcard> BusinessCreditcard { get; set; }
        public virtual DbSet<BusinessCreditcardOutput> BusinessCreditcardOutput { get; set; }
        public virtual DbSet<BusinessCreditcardOutputPaymentPlan> BusinessCreditcardOutputPaymentPlan { get; set; }
        public virtual DbSet<BusinessCreditcardOutputPaymentPlanTemp> BusinessCreditcardOutputPaymentPlanTemp { get; set; }
        public virtual DbSet<CallerIdLog> CallerIdLog { get; set; }
        public virtual DbSet<CargoFeeDefines> CargoFeeDefines { get; set; }
        public virtual DbSet<CheckIn> CheckIn { get; set; }
        public virtual DbSet<CheckOutput> CheckOutput { get; set; }
        public virtual DbSet<CheckTemp> CheckTemp { get; set; }
        public virtual DbSet<ComingDocument> ComingDocument { get; set; }
        public virtual DbSet<CommercialPaperIn> CommercialPaperIn { get; set; }
        public virtual DbSet<CommercialPaperOut> CommercialPaperOut { get; set; }
        public virtual DbSet<CommercialPaperTemp> CommercialPaperTemp { get; set; }
        public virtual DbSet<Currency> Currency { get; set; }
        public virtual DbSet<CurrencyIn> CurrencyIn { get; set; }
        public virtual DbSet<CurrencyOut> CurrencyOut { get; set; }
        public virtual DbSet<CurrencyRatesOfExchange> CurrencyRatesOfExchange { get; set; }
        public virtual DbSet<CurrencyRatesOfExchangeTemp> CurrencyRatesOfExchangeTemp { get; set; }
        public virtual DbSet<CustomerEntity> Customer { get; set; }
        public virtual DbSet<CustomerAdress> CustomerAdress { get; set; }
        public virtual DbSet<CustomerBank> CustomerBank { get; set; }
        public virtual DbSet<CustomerCorrespondenceReportFiles> CustomerCorrespondenceReportFiles { get; set; }
        public virtual DbSet<CustomerCreditcard> CustomerCreditcard { get; set; }
        public virtual DbSet<CustomerCurrentInput> CustomerCurrentInput { get; set; }
        public virtual DbSet<CustomerCurrentOutput> CustomerCurrentOutput { get; set; }
        public virtual DbSet<CustomerCurrentOutputServiceNotes> CustomerCurrentOutputServiceNotes { get; set; }
        public virtual DbSet<CustomerDocumentDefinitions> CustomerDocumentDefinitions { get; set; }
        public virtual DbSet<CustomerOffset> CustomerOffset { get; set; }
        public virtual DbSet<CustomerPersonelActivity> CustomerPersonelActivity { get; set; }
        public virtual DbSet<CustomerPersonelActivityTemp> CustomerPersonelActivityTemp { get; set; }
        public virtual DbSet<CustomerProducerReceiptTaxDefines> CustomerProducerReceiptTaxDefines { get; set; }
        public virtual DbSet<CustomerSubscription> CustomerSubscription { get; set; }
        public virtual DbSet<CustomerSubscriptionCurrent> CustomerSubscriptionCurrent { get; set; }
        public virtual DbSet<CustomerSubscriptionDetail> CustomerSubscriptionDetail { get; set; }
        public virtual DbSet<DeviceLocation> DeviceLocation { get; set; }
        public virtual DbSet<Documents> Documents { get; set; }
        public virtual DbSet<ElevatorCardParameters> ElevatorCardParameters { get; set; }
        public virtual DbSet<ElevatorCardSubParameters> ElevatorCardSubParameters { get; set; }
        public virtual DbSet<ElevatorCustomerCardParameters> ElevatorCustomerCardParameters { get; set; }
        public virtual DbSet<ElevatorFaultInterventionDefinitions> ElevatorFaultInterventionDefinitions { get; set; }
        public virtual DbSet<ElevatorInstallationList> ElevatorInstallationList { get; set; }
        public virtual DbSet<ElevatorOfferSelectedProductPropertiesTemp> ElevatorOfferSelectedProductPropertiesTemp { get; set; }
        public virtual DbSet<ElevatorOfferSelectedProductSizes> ElevatorOfferSelectedProductSizes { get; set; }
        public virtual DbSet<ElevatorOfferTempProductList> ElevatorOfferTempProductList { get; set; }
        public virtual DbSet<ElevatorRevisionOffer> ElevatorRevisionOffer { get; set; }
        public virtual DbSet<ElevatorRevisionOfferParameters> ElevatorRevisionOfferParameters { get; set; }
        public virtual DbSet<ElevatorRevisionOfferParametersTemp> ElevatorRevisionOfferParametersTemp { get; set; }
        public virtual DbSet<ElevatorRevisionParameters> ElevatorRevisionParameters { get; set; }
        public virtual DbSet<ElevatorRevisionParametersProduct> ElevatorRevisionParametersProduct { get; set; }
        public virtual DbSet<ElevatorSubcontractorDefines> ElevatorSubcontractorDefines { get; set; }
        public virtual DbSet<ElevatorSubControlHistory> ElevatorSubControlHistory { get; set; }
        public virtual DbSet<EmailParameters> EmailParameters { get; set; }
        public virtual DbSet<Endofthedayreport> Endofthedayreport { get; set; }
        public virtual DbSet<Expense> Expense { get; set; }
        public virtual DbSet<Expiring> Expiring { get; set; }
        public virtual DbSet<FarmAnimals> FarmAnimals { get; set; }
        public virtual DbSet<FarmAnimalsActivity> FarmAnimalsActivity { get; set; }
        public virtual DbSet<FarmFeedActivity> FarmFeedActivity { get; set; }
        public virtual DbSet<FarmFeedMixture> FarmFeedMixture { get; set; }
        public virtual DbSet<FarmFeedMixtureProducts> FarmFeedMixtureProducts { get; set; }
        public virtual DbSet<FarmPaddock> FarmPaddock { get; set; }
        public virtual DbSet<Farms> Farms { get; set; }
        public virtual DbSet<FarmVeterinary> FarmVeterinary { get; set; }
        public virtual DbSet<FarmVeterinaryApplications> FarmVeterinaryApplications { get; set; }
        public virtual DbSet<FormButtonDefines> FormButtonDefines { get; set; }
        public virtual DbSet<GeneralParameters> GeneralParameters { get; set; }
        public virtual DbSet<Income> Income { get; set; }
        public virtual DbSet<IncomeAndExpenseTypes> IncomeAndExpenseTypes { get; set; }
        public virtual DbSet<Installments> Installments { get; set; }
        public virtual DbSet<InstallmentsPlan> InstallmentsPlan { get; set; }
        public virtual DbSet<InstallmentsPlanTemp> InstallmentsPlanTemp { get; set; }
        public virtual DbSet<Invoice> Invoice { get; set; }
        public virtual DbSet<InvoiceProduct> InvoiceProduct { get; set; }
        public virtual DbSet<InvoiceShipmentTrace> InvoiceShipmentTrace { get; set; }
        public virtual DbSet<InvoiceShipmentTraceTemp> InvoiceShipmentTraceTemp { get; set; }
        public virtual DbSet<LabelTemp> LabelTemp { get; set; }
        public virtual DbSet<LicanceInfo> LicanceInfo { get; set; }
        public virtual DbSet<LoadActivity> LoadActivity { get; set; }
        public virtual DbSet<LoadActivityList> LoadActivityList { get; set; }
        public virtual DbSet<LoadActivityTemp> LoadActivityTemp { get; set; }
        public virtual DbSet<LoadDocumentDefinitions> LoadDocumentDefinitions { get; set; }
        public virtual DbSet<LoadLocationDefinations> LoadLocationDefinations { get; set; }
        public virtual DbSet<LoadPackageDefinitions> LoadPackageDefinitions { get; set; }
        public virtual DbSet<LoadStatusDefinitions> LoadStatusDefinitions { get; set; }
        public virtual DbSet<LoadTypeDefinition> LoadTypeDefinition { get; set; }
        public virtual DbSet<LoadUnits> LoadUnits { get; set; }
        public virtual DbSet<Maintenance> Maintenance { get; set; }
        public virtual DbSet<Management> Management { get; set; }
        public virtual DbSet<ManagementRoles> ManagementRoles { get; set; }
        public virtual DbSet<ManagementRolesAuthorization> ManagementRolesAuthorization { get; set; }
        public virtual DbSet<ManagementRolesServiceRequestAuthorization> ManagementRolesServiceRequestAuthorization { get; set; }
        public virtual DbSet<Message> Message { get; set; }
        public virtual DbSet<MobileScaleDevice> MobileScaleDevice { get; set; }
        public virtual DbSet<MobileScaleProductPurchase> MobileScaleProductPurchase { get; set; }
        public virtual DbSet<MobileScaleProductPurchaseTemp> MobileScaleProductPurchaseTemp { get; set; }
        public virtual DbSet<MobileScaleSync> MobileScaleSync { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }
        public virtual DbSet<OrdersProducts> OrdersProducts { get; set; }
        public virtual DbSet<OrdersProductsSub> OrdersProductsSub { get; set; }
        public virtual DbSet<OrdersProductsSubMain> OrdersProductsSubMain { get; set; }
        public virtual DbSet<Parameters> Parameters { get; set; }
        public virtual DbSet<PersonelCards> PersonelCards { get; set; }
        public virtual DbSet<PersonelCardsFeeInformation> PersonelCardsFeeInformation { get; set; }
        public virtual DbSet<PersonelCardsIdentityInformation> PersonelCardsIdentityInformation { get; set; }
        public virtual DbSet<PersonelCardsStartingEnding> PersonelCardsStartingEnding { get; set; }
        public virtual DbSet<PersonelHolidayParameters> PersonelHolidayParameters { get; set; }
        public virtual DbSet<PersonelMeterialDebit> PersonelMeterialDebit { get; set; }
        public virtual DbSet<PersonelPermissionCurrent> PersonelPermissionCurrent { get; set; }
        public virtual DbSet<PersonelPermissionDayParameters> PersonelPermissionDayParameters { get; set; }
        public virtual DbSet<PersonelPermissionParameters> PersonelPermissionParameters { get; set; }
        public virtual DbSet<Pos> Pos { get; set; }
        public virtual DbSet<PosDetail> PosDetail { get; set; }
        public virtual DbSet<PosIn> PosIn { get; set; }
        public virtual DbSet<PosOut> PosOut { get; set; }
        public virtual DbSet<PremiumTracking> PremiumTracking { get; set; }
        public virtual DbSet<PremiumTrackingTemp> PremiumTrackingTemp { get; set; }
        public virtual DbSet<Pricing> Pricing { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<Product2> Product2 { get; set; }
        public virtual DbSet<ProductColor> ProductColor { get; set; }
        public virtual DbSet<ProductGroups> ProductGroups { get; set; }
        public virtual DbSet<ProductionHistory> ProductionHistory { get; set; }
        public virtual DbSet<ProductionOrdersTemp> ProductionOrdersTemp { get; set; }
        public virtual DbSet<ProductionRecipeDefines> ProductionRecipeDefines { get; set; }
        public virtual DbSet<ProductionRecipeFormula> ProductionRecipeFormula { get; set; }
        public virtual DbSet<ProductNutritionalValues> ProductNutritionalValues { get; set; }
        public virtual DbSet<ProductPricingChangeHistory> ProductPricingChangeHistory { get; set; }
        public virtual DbSet<ProductSerials> ProductSerials { get; set; }
        public virtual DbSet<ProductSerialsTemp> ProductSerialsTemp { get; set; }
        public virtual DbSet<ProductSizes> ProductSizes { get; set; }
        public virtual DbSet<ProductTaxList2> ProductTaxList2 { get; set; }
        public virtual DbSet<ProductUnit> ProductUnit { get; set; }
        public virtual DbSet<ReportDesigns> ReportDesigns { get; set; }
        public virtual DbSet<RestaurantDepartment> RestaurantDepartment { get; set; }
        public virtual DbSet<RestaurantDevices> RestaurantDevices { get; set; }
        public virtual DbSet<RestaurantDiscountRates> RestaurantDiscountRates { get; set; }
        public virtual DbSet<RestaurantOrder> RestaurantOrder { get; set; }
        public virtual DbSet<RestaurantOrderList> RestaurantOrderList { get; set; }
        public virtual DbSet<RestaurantOrderListTempDeletes> RestaurantOrderListTempDeletes { get; set; }
        public virtual DbSet<RestaurantParameters> RestaurantParameters { get; set; }
        public virtual DbSet<RestaurantPremiumCalc> RestaurantPremiumCalc { get; set; }
        public virtual DbSet<RestaurantPremiumCalcTemp> RestaurantPremiumCalcTemp { get; set; }
        public virtual DbSet<RestaurantPremiumDefines> RestaurantPremiumDefines { get; set; }
        public virtual DbSet<RestaurantPremiumDefinesConditions> RestaurantPremiumDefinesConditions { get; set; }
        public virtual DbSet<RestaurantPremiumDefinesNonConditions> RestaurantPremiumDefinesNonConditions { get; set; }
        public virtual DbSet<RestaurantPremiumDefinesSubConditions> RestaurantPremiumDefinesSubConditions { get; set; }
        public virtual DbSet<RestaurantPrinterParameters> RestaurantPrinterParameters { get; set; }
        public virtual DbSet<RestaurantProductFlavorDetails> RestaurantProductFlavorDetails { get; set; }
        public virtual DbSet<RestaurantSpeedGroupButtons> RestaurantSpeedGroupButtons { get; set; }
        public virtual DbSet<RestaurantTables> RestaurantTables { get; set; }
        public virtual DbSet<RestaurantUser> RestaurantUser { get; set; }
        public virtual DbSet<Route> Route { get; set; }
        public virtual DbSet<RouteParameters> RouteParameters { get; set; }
        public virtual DbSet<Safe> Safe { get; set; }
        public virtual DbSet<SafeInput> SafeInput { get; set; }
        public virtual DbSet<SafeOutput> SafeOutput { get; set; }
        public virtual DbSet<ScaleSettings> ScaleSettings { get; set; }
        public virtual DbSet<Scheduler> Scheduler { get; set; }
       
        public virtual DbSet<ServiceControlDefines> ServiceControlDefines { get; set; }
        public virtual DbSet<ServiceControlSubDefines> ServiceControlSubDefines { get; set; }
        public virtual DbSet<ServicesCategoryDefinition> ServicesCategoryDefinition { get; set; }
        public virtual DbSet<ServicesCategorySubDefinition> ServicesCategorySubDefinition { get; set; }
        public virtual DbSet<ServicesContract> ServicesContract { get; set; }
        public virtual DbSet<ServicesDirectionList> ServicesDirectionList { get; set; }
        public virtual DbSet<ServicesLastStatusDefination> ServicesLastStatusDefination { get; set; }
        public virtual DbSet<ServicesLastStatusDefinationInfs> ServicesLastStatusDefinationInfs { get; set; }
        public virtual DbSet<ServicesOpen> ServicesOpen { get; set; }
        public virtual DbSet<ServicesOpenActivity> ServicesOpenActivity { get; set; }
        public virtual DbSet<ServicesOpenClose> ServicesOpenClose { get; set; }
        public virtual DbSet<ServicesOpenCloseControlDefines> ServicesOpenCloseControlDefines { get; set; }
        public virtual DbSet<ServicesOpenCloseParameters> ServicesOpenCloseParameters { get; set; }
        public virtual DbSet<ServicesOpenControlDefines> ServicesOpenControlDefines { get; set; }
        public virtual DbSet<ServicesOpenControlDefinesTemp> ServicesOpenControlDefinesTemp { get; set; }
        public virtual DbSet<ServicesOpenParameters> ServicesOpenParameters { get; set; }
        public virtual DbSet<ServicesPointsBaseInformation> ServicesPointsBaseInformation { get; set; }
        public virtual DbSet<ServicesProductChangeList> ServicesProductChangeList { get; set; }
        public virtual DbSet<ServicesProductChangeListTemp> ServicesProductChangeListTemp { get; set; }
        public virtual DbSet<ServicesRequestDefination> ServicesRequestDefination { get; set; }
        public virtual DbSet<ServicesRequestDefinationInfs> ServicesRequestDefinationInfs { get; set; }
        public virtual DbSet<ServicesSetupDefineParameters> ServicesSetupDefineParameters { get; set; }
        public virtual DbSet<ServicesSetupDefines> ServicesSetupDefines { get; set; }
        public virtual DbSet<ServicesSetupDefinesFormula> ServicesSetupDefinesFormula { get; set; }
        public virtual DbSet<ServicesSetupDefinesFunctions> ServicesSetupDefinesFunctions { get; set; }
        public virtual DbSet<ServicesSetupRequestList> ServicesSetupRequestList { get; set; }
        public virtual DbSet<ServicesSpeedButtons> ServicesSpeedButtons { get; set; }
        public virtual DbSet<Settings> Settings { get; set; }
        public virtual DbSet<SmsBalance> SmsBalance { get; set; }
        public virtual DbSet<SmsParameters> SmsParameters { get; set; }
        public virtual DbSet<SmsTask> SmsTask { get; set; }
        public virtual DbSet<SmsTaskList> SmsTaskList { get; set; }
        public virtual DbSet<SmsThemes> SmsThemes { get; set; }
        public virtual DbSet<SpeedSalesButtons> SpeedSalesButtons { get; set; }
        public virtual DbSet<SpeedSalesTabsheetCaptions> SpeedSalesTabsheetCaptions { get; set; }
        public virtual DbSet<StockActivities> StockActivities { get; set; }
        public virtual DbSet<StockActivitiesSub> StockActivitiesSub { get; set; }
        public virtual DbSet<StockActivitiesSubMain> StockActivitiesSubMain { get; set; }
        public virtual DbSet<StockCounting> StockCounting { get; set; }
        public virtual DbSet<StockCountingSub> StockCountingSub { get; set; }
        public virtual DbSet<StockCountingSubMain> StockCountingSubMain { get; set; }
        public virtual DbSet<StockCountingTemp> StockCountingTemp { get; set; }
        public virtual DbSet<StockLabel> StockLabel { get; set; }
        public virtual DbSet<Storage> Storage { get; set; }
        public virtual DbSet<StorageTransfer> StorageTransfer { get; set; }
        public virtual DbSet<StorageTransferTemp> StorageTransferTemp { get; set; }
        public virtual DbSet<SubscriptionGroups> SubscriptionGroups { get; set; }
        public virtual DbSet<Support> Support { get; set; }
        public virtual DbSet<TempBasket> TempBasket { get; set; }
        public virtual DbSet<TempBasketTax> TempBasketTax { get; set; }
        public virtual DbSet<Token> Token { get; set; }
        public virtual DbSet<UpdateHistory> UpdateHistory { get; set; }
        public virtual DbSet<Vehicle> Vehicle { get; set; }
        public virtual DbSet<VehicleDebit> VehicleDebit { get; set; }
        public virtual DbSet<VehicleFuelExpense> VehicleFuelExpense { get; set; }
        public virtual DbSet<VehicleKilometer> VehicleKilometer { get; set; }
        public virtual DbSet<VehicleProductExpense> VehicleProductExpense { get; set; }
        public virtual DbSet<VehicleTireExpense> VehicleTireExpense { get; set; }
        public virtual DbSet<Waybill> Waybill { get; set; }
        public virtual DbSet<WaybillProducts> WaybillProducts { get; set; }
        public virtual DbSet<WaybillProductsSub> WaybillProductsSub { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("server=localhost;port=3306;user=root;password=pdaodqq;database=crocodile2;TreatTinyAsBoolean=false;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Agricultural>(entity =>
            {
                entity.ToTable("agricultural");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.Exp1)
                    .HasColumnName("exp1")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Unit)
                    .HasColumnName("unit")
                    .HasColumnType("varchar(10)");
            });

            modelBuilder.Entity<AgriculturalCustomer>(entity =>
            {
                entity.ToTable("agricultural_customer");

                entity.HasIndex(e => e.AgriculturalId)
                    .HasName("agricultural_id");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AgriculturalId)
                    .HasColumnName("agricultural_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.AgriculturalName)
                    .HasColumnName("agricultural_name")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.AppDate)
                    .HasColumnName("app_date")
                    .HasColumnType("date");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UnitFactor)
                    .HasColumnName("unit_factor")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'1.00'");

                entity.HasOne(d => d.Agricultural)
                    .WithMany(p => p.AgriculturalCustomer)
                    .HasForeignKey(d => d.AgriculturalId)
                    .HasConstraintName("agricultural_customer_ibfk_2");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.AgriculturalCustomer)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("agricultural_customer_ibfk_1");
            });

            modelBuilder.Entity<AgriculturalDetail>(entity =>
            {
                entity.ToTable("agricultural_detail");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AgriculturalId)
                    .HasColumnName("agricultural_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Days)
                    .HasColumnName("days")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.FormulaQty)
                    .HasColumnName("formula_qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(150)");
            });

            modelBuilder.Entity<ArchiveGroups>(entity =>
            {
                entity.ToTable("archive_groups");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ForCustomer)
                    .HasColumnName("for_customer")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.ForPersonel)
                    .HasColumnName("for_personel")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.ForVehicle)
                    .HasColumnName("for_vehicle")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.GroupCode)
                    .HasColumnName("group_code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.GroupExplanation)
                    .HasColumnName("group_explanation")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.GroupName)
                    .HasColumnName("group_name")
                    .HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<ArchiveManagement>(entity =>
            {
                entity.ToTable("archive_management");

                entity.HasIndex(e => e.ArcihiveGroupsId)
                    .HasName("arcihive_groups_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ArcihiveGroupsId)
                    .HasColumnName("arcihive_groups_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ArciveType)
                    .HasColumnName("arcive_type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DocExplanation)
                    .HasColumnName("doc_explanation")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.DocNumner)
                    .HasColumnName("doc_numner")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DocSerial)
                    .HasColumnName("doc_serial")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.DocType)
                    .HasColumnName("doc_Type")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Document).HasColumnName("document");

                entity.Property(e => e.InsertDatetime)
                    .HasColumnName("insert_datetime")
                    .HasColumnType("timestamp");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Bank>(entity =>
            {
                entity.ToTable("bank");

                entity.HasIndex(e => e.BankName)
                    .HasName("bank_name");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BankExplain)
                    .HasColumnName("bank_explain")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.BankName)
                    .HasColumnName("bank_name")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<BankAccount>(entity =>
            {
                entity.ToTable("bank_account");

                entity.HasIndex(e => e.BankId)
                    .HasName("bank_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AccountHolder)
                    .HasColumnName("account_holder")
                    .HasColumnType("varchar(300)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.AccountNumber)
                    .HasColumnName("account_number")
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.AdAccountNumber)
                    .HasColumnName("ad_account_number")
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.BankDepartment)
                    .HasColumnName("bank_department")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.BankId)
                    .HasColumnName("bank_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.BankName)
                    .HasColumnName("bank_name")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.DepartmantName)
                    .HasColumnName("departmant_name")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.DepartmentAdress)
                    .HasColumnName("department_adress")
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.DepartmentContactPerson)
                    .HasColumnName("department_contact_person")
                    .HasColumnType("varchar(60)");

                entity.Property(e => e.DepartmentFax)
                    .HasColumnName("department_fax")
                    .HasColumnType("varchar(16)");

                entity.Property(e => e.DepartmentMail)
                    .HasColumnName("department_mail")
                    .HasColumnType("varchar(140)");

                entity.Property(e => e.DepartmentTel)
                    .HasColumnName("department_tel")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.DetailId)
                    .HasColumnName("detail_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(260)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(260)");

                entity.Property(e => e.Iban)
                    .HasColumnName("iban")
                    .HasColumnType("varchar(30)");

                entity.HasOne(d => d.Bank)
                    .WithMany(p => p.BankAccount)
                    .HasForeignKey(d => d.BankId)
                    .HasConstraintName("bank_account_ibfk_1");
            });

            modelBuilder.Entity<BankAccountIn>(entity =>
            {
                entity.ToTable("bank_account_in");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BankAccountId)
                    .HasColumnName("bank_account_id")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DetailId)
                    .HasColumnName("detail_id")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<BankAccountOut>(entity =>
            {
                entity.ToTable("bank_account_out");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BankAccountId)
                    .HasColumnName("bank_account_id")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DetailId)
                    .HasColumnName("detail_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("varchar(255)")
                    .HasDefaultValueSql("'3'");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<BankDepartment>(entity =>
            {
                entity.ToTable("bank_department");

                entity.HasIndex(e => e.BankName)
                    .HasName("bank_name");

                entity.HasIndex(e => e.Country)
                    .HasName("country");

                entity.HasIndex(e => e.Town)
                    .HasName("town");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BankDepartment1)
                    .HasColumnName("bank_department")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.BankId)
                    .HasColumnName("bank_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.BankName)
                    .HasColumnName("bank_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Country)
                    .HasColumnName("country")
                    .HasColumnType("varchar(60)");

                entity.Property(e => e.DepartmentAdress)
                    .HasColumnName("department_adress")
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.DepartmentContactPerson)
                    .HasColumnName("department_contact_person")
                    .HasColumnType("varchar(60)");

                entity.Property(e => e.DepartmentFax)
                    .HasColumnName("department_fax")
                    .HasColumnType("varchar(16)");

                entity.Property(e => e.DepartmentMail)
                    .HasColumnName("department_mail")
                    .HasColumnType("varchar(140)");

                entity.Property(e => e.DepartmentTel)
                    .HasColumnName("department_tel")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(260)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(260)");

                entity.Property(e => e.Town)
                    .HasColumnName("town")
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<Basket>(entity =>
            {
                entity.ToTable("basket");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BasketId)
                    .HasColumnName("basket_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UnitPrice)
                    .HasColumnName("unit_price")
                    .HasColumnType("decimal(12,2)");
            });

            modelBuilder.Entity<Branch>(entity =>
            {
                entity.ToTable("branch");

                entity.HasIndex(e => e.AuthorizedPersonelId)
                    .HasName("authorized_personel_id");

                entity.HasIndex(e => e.DeletionUserId)
                    .HasName("deletion_user_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AuthorizedPersonelId)
                    .HasColumnName("authorized_personel_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.AuthorizedPersonelName)
                    .HasColumnName("authorized_personel_name")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.BrachAdress)
                    .HasColumnName("brach_adress")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp")
                    ;

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp1)
                    .HasColumnName("exp1")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FaxNumber)
                    .HasColumnName("fax_number")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.MailAdress)
                    .HasColumnName("mail_adress")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.PhoneNumber)
                    .HasColumnName("phone_number")
                    .HasColumnType("varchar(15)");

                entity.HasOne(d => d.AuthorizedPersonel)
                    .WithMany(p => p.Branch)
                    .HasForeignKey(d => d.AuthorizedPersonelId)
                    .HasConstraintName("branch_ibfk_3");

                entity.HasOne(d => d.DeletionUser)
                    .WithMany(p => p.Branch)
                    .HasForeignKey(d => d.DeletionUserId)
                    .HasConstraintName("branch_ibfk_2");
            });

            modelBuilder.Entity<BusinessCreditcard>(entity =>
            {
                entity.ToTable("business_creditcard");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BankId)
                    .HasColumnName("bank_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CampaignNotes)
                    .HasColumnName("campaign_notes")
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.CardFee)
                    .HasColumnName("card_fee")
                    .HasColumnType("decimal(10,8)");

                entity.Property(e => e.CardHolder)
                    .HasColumnName("card_holder")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.CardNo)
                    .HasColumnName("card_no")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.CardType)
                    .HasColumnName("card_type")
                    .HasColumnType("tinyint(2)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(250)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(250)");

                entity.Property(e => e.ExpDate)
                    .HasColumnName("exp_date")
                    .HasColumnType("varchar(6)");

                entity.Property(e => e.InstallmentNotes)
                    .HasColumnName("installment_notes")
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.InterestRate)
                    .HasColumnName("interest_rate")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.MinPayment)
                    .HasColumnName("min_payment")
                    .HasColumnType("decimal(10,8)");

                entity.Property(e => e.SecurityCode)
                    .HasColumnName("security_code")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.TheLastPaymentDate)
                    .HasColumnName("the_last_payment_date")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<BusinessCreditcardOutput>(entity =>
            {
                entity.ToTable("business_creditcard_output");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BusinessCreditcardId)
                    .HasColumnName("business_creditcard_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(250)");
            });

            modelBuilder.Entity<BusinessCreditcardOutputPaymentPlan>(entity =>
            {
                entity.ToTable("business_creditcard_output_payment_plan");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BasketId)
                    .HasColumnName("basket_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.BusinessCreditcardId)
                    .HasColumnName("business_creditcard_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.InstallmentsCurrency)
                    .HasColumnName("installments_currency")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.NumberOfInstallments)
                    .HasColumnName("number_of_installments")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TheLastPaymentDate)
                    .HasColumnName("the_last_payment_date")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<BusinessCreditcardOutputPaymentPlanTemp>(entity =>
            {
                entity.ToTable("business_creditcard_output_payment_plan_temp");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BasketId)
                    .HasColumnName("basket_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.BusinessCreditcardId)
                    .HasColumnName("business_creditcard_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.InstallmentsCurrency)
                    .HasColumnName("installments_currency")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.NumberOfInstallments)
                    .HasColumnName("number_of_installments")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TheLastPaymentDate)
                    .HasColumnName("the_last_payment_date")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<CallerIdLog>(entity =>
            {
                entity.ToTable("caller_id_log");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.LineNumber)
                    .HasColumnName("line_number")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Number)
                    .HasColumnName("number")
                    .HasColumnType("varchar(12)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<CargoFeeDefines>(entity =>
            {
                entity.ToTable("cargo_fee_defines");

                entity.HasIndex(e => e.CargoProductId)
                    .HasName("cargo_fee_defines_ibfk_1");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CargoProductId)
                    .HasColumnName("cargo_product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Currency1)
                    .HasColumnName("currency1")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Currency2)
                    .HasColumnName("currency2")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Currency3)
                    .HasColumnName("currency3")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Currency4)
                    .HasColumnName("currency4")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Currency5)
                    .HasColumnName("currency5")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Currency6)
                    .HasColumnName("currency6")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Currency7)
                    .HasColumnName("currency7")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Currency8)
                    .HasColumnName("currency8")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Ds11)
                    .HasColumnName("ds1_1")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Ds12)
                    .HasColumnName("ds1_2")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Ds21)
                    .HasColumnName("ds2_1")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Ds22)
                    .HasColumnName("ds2-2")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Ds31)
                    .HasColumnName("ds3_1")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Ds32)
                    .HasColumnName("ds3_2")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Ds41)
                    .HasColumnName("ds4_1")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Ds42)
                    .HasColumnName("ds4_2")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Ds51)
                    .HasColumnName("ds5_1")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Ds52)
                    .HasColumnName("ds5_2")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Ds61)
                    .HasColumnName("ds6_1")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Ds62)
                    .HasColumnName("ds6_2")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Ds71)
                    .HasColumnName("ds7_1")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Ds72)
                    .HasColumnName("ds7_2")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Ds81)
                    .HasColumnName("ds8_1")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ReceiveCargoDifference)
                    .HasColumnName("Receive_Cargo_Difference")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.UseCargoFeeDefines)
                    .HasColumnName("Use_cargo_fee_defines")
                    .HasColumnType("varchar(20)");

                entity.HasOne(d => d.CargoProduct)
                    .WithMany(p => p.CargoFeeDefines)
                    .HasForeignKey(d => d.CargoProductId)
                    .HasConstraintName("cargo_fee_defines_ibfk_1");
            });

            modelBuilder.Entity<CheckIn>(entity =>
            {
                entity.ToTable("check_in");

                entity.HasIndex(e => e.BankId)
                    .HasName("bank_id");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.HasIndex(e => e.Deletion)
                    .HasName("deletion");

                entity.HasIndex(e => e.ReminderUser)
                    .HasName("reminder_user");

                entity.HasIndex(e => e.Type)
                    .HasName("type_");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BankId)
                    .HasColumnName("bank_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CheckBankAccount)
                    .HasColumnName("check_bank_account")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.CheckNumber)
                    .HasColumnName("check_number")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasColumnType("decimal(18,5)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ExitCustomerId)
                    .HasColumnName("exit_customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(400)");

                entity.Property(e => e.InsDateTime)
                    .HasColumnName("ins_date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PayDate)
                    .HasColumnName("pay_date")
                    .HasColumnType("date");

                entity.Property(e => e.Reminder)
                    .HasColumnName("reminder")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ReminderUser)
                    .HasColumnName("reminder_user")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Type)
                    .HasColumnName("type_")
                    .HasColumnType("varchar(3)")
                    .HasDefaultValueSql("'in'");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.ReminderUserNavigation)
                    .WithMany(p => p.CheckIn)
                    .HasForeignKey(d => d.ReminderUser)
                    .HasConstraintName("check_in_ibfk_1");
            });

            modelBuilder.Entity<CheckOutput>(entity =>
            {
                entity.ToTable("check_output");

                entity.HasIndex(e => e.BankId)
                    .HasName("bank_id");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.HasIndex(e => e.Deletion)
                    .HasName("deletion");

                entity.HasIndex(e => e.ReminderUser)
                    .HasName("reminder_user");

                entity.HasIndex(e => e.Type)
                    .HasName("type_");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BankId)
                    .HasColumnName("bank_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CheckBankAccount)
                    .HasColumnName("check_bank_account")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.CheckBankAccountId)
                    .HasColumnName("check_bank_account_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CheckNumber)
                    .HasColumnName("check_number")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasColumnType("decimal(18,5)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(400)");

                entity.Property(e => e.InsDateTime)
                    .HasColumnName("ins_date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PayDate)
                    .HasColumnName("pay_date")
                    .HasColumnType("date");

                entity.Property(e => e.Reminder)
                    .HasColumnName("reminder")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ReminderUser)
                    .HasColumnName("reminder_user")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Type)
                    .HasColumnName("type_")
                    .HasColumnType("varchar(3)")
                    .HasDefaultValueSql("'out'");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.ReminderUserNavigation)
                    .WithMany(p => p.CheckOutput)
                    .HasForeignKey(d => d.ReminderUser)
                    .HasConstraintName("check_output_ibfk_1");
            });

            modelBuilder.Entity<CheckTemp>(entity =>
            {
                entity.ToTable("check_temp");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BankId)
                    .HasColumnName("bank_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.BankName)
                    .HasColumnName("bank_name")
                    .HasColumnType("varchar(60)");

                entity.Property(e => e.CheckBankAccount)
                    .HasColumnName("check_bank_account")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CheckBankAccountId)
                    .HasColumnName("check_bank_account_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.CheckNumber)
                    .HasColumnName("check_number")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CustomerName)
                    .HasColumnName("customer_name")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(400)");

                entity.Property(e => e.OpType)
                    .HasColumnName("op_Type")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PayDate)
                    .HasColumnName("pay_date")
                    .HasColumnType("date");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<ComingDocument>(entity =>
            {
                entity.ToTable("coming_document");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Attachments)
                    .IsRequired()
                    .HasColumnName("attachments")
                    .HasColumnType("varchar(500)");

                entity.Property(e => e.DocumentDate)
                    .HasColumnName("document_date")
                    .HasColumnType("date");

                entity.Property(e => e.DocumentRegisterNumber)
                    .IsRequired()
                    .HasColumnName("document_register_number")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.DocumentSource)
                    .IsRequired()
                    .HasColumnName("document_source")
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.FilePath)
                    .IsRequired()
                    .HasColumnName("file_path")
                    .HasColumnType("varchar(2000)");

                entity.Property(e => e.InputDate)
                    .HasColumnName("input_date")
                    .HasColumnType("date");

                entity.Property(e => e.SaveDate)
                    .HasColumnName("save_date")
                    .HasColumnType("date");

                entity.Property(e => e.Subject)
                    .IsRequired()
                    .HasColumnName("subject")
                    .HasColumnType("varchar(500)");

                entity.Property(e => e.SubjectCode)
                    .IsRequired()
                    .HasColumnName("subject_code")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(3)");
            });

            modelBuilder.Entity<CommercialPaperIn>(entity =>
            {
                entity.ToTable("commercial_paper_in");

                entity.HasIndex(e => e.ReminderUser)
                    .HasName("reminder_user");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasColumnType("decimal(18,5)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DocumentNumber)
                    .HasColumnName("document_number")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ExitCustomerId)
                    .HasColumnName("exit_customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.InsDate)
                    .HasColumnName("ins_date")
                    .HasColumnType("timestamp(3)");

                entity.Property(e => e.InsDateTime)
                    .HasColumnName("ins_date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.NotaryWarning)
                    .HasColumnName("notary_warning")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.PayDate)
                    .HasColumnName("pay_date")
                    .HasColumnType("date");

                entity.Property(e => e.Reminder)
                    .HasColumnName("reminder")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ReminderUser)
                    .HasColumnName("reminder_user")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Type)
                    .HasColumnName("type_")
                    .HasColumnType("varchar(5)")
                    .HasDefaultValueSql("'in'");

                entity.HasOne(d => d.ReminderUserNavigation)
                    .WithMany(p => p.CommercialPaperIn)
                    .HasForeignKey(d => d.ReminderUser)
                    .HasConstraintName("commercial_paper_in_ibfk_1");
            });

            modelBuilder.Entity<CommercialPaperOut>(entity =>
            {
                entity.ToTable("commercial_paper_out");

                entity.HasIndex(e => e.ReminderUser)
                    .HasName("reminder_user");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasColumnType("decimal(18,5)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DocumentNumber)
                    .HasColumnName("document_number")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.InsDate)
                    .HasColumnName("ins_date")
                    .HasColumnType("timestamp(3)");

                entity.Property(e => e.InsDateTime)
                    .HasColumnName("ins_date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.NotaryWarning)
                    .HasColumnName("notary_warning")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.PayDate)
                    .HasColumnName("pay_date")
                    .HasColumnType("date");

                entity.Property(e => e.Reminder)
                    .HasColumnName("reminder")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ReminderUser)
                    .HasColumnName("reminder_user")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Type)
                    .HasColumnName("type_")
                    .HasColumnType("varchar(5)")
                    .HasDefaultValueSql("'out'");

                entity.HasOne(d => d.ReminderUserNavigation)
                    .WithMany(p => p.CommercialPaperOut)
                    .HasForeignKey(d => d.ReminderUser)
                    .HasConstraintName("commercial_paper_out_ibfk_1");
            });

            modelBuilder.Entity<CommercialPaperTemp>(entity =>
            {
                entity.ToTable("commercial_paper_temp");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasColumnType("decimal(18,5)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CustomerName)
                    .HasColumnName("customer_name")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.DocumentNumber)
                    .HasColumnName("document_number")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.NotaryWarning)
                    .HasColumnName("notary_warning")
                    .HasColumnType("varchar(10)")
                    .HasDefaultValueSql("'False'");

                entity.Property(e => e.OpType)
                    .HasColumnName("op_type")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.PayDate)
                    .HasColumnName("pay_date")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<Currency>(entity =>
            {
                entity.ToTable("currency");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CurrencyCode)
                    .HasColumnName("Currency_Code")
                    .HasColumnType("varchar(25)");

                entity.Property(e => e.CurrencyName)
                    .HasColumnName("Currency_Name")
                    .HasColumnType("varchar(85)");

                entity.Property(e => e.CurrencySymbol)
                    .HasColumnName("Currency_Symbol")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.Exp1).HasColumnType("varchar(120)");

                entity.Property(e => e.Exp2).HasColumnType("varchar(120)");

                entity.Property(e => e.Exp3).HasColumnType("varchar(120)");

                entity.Property(e => e.TcmbFindText)
                    .HasColumnName("TCMB_Find_Text")
                    .HasColumnType("varchar(90)");
            });

            modelBuilder.Entity<CurrencyIn>(entity =>
            {
                entity.ToTable("currency_in");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CurrencyId)
                    .HasColumnName("Currency_Id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<CurrencyOut>(entity =>
            {
                entity.ToTable("currency_out");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CurrencyId)
                    .HasColumnName("Currency_Id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<CurrencyRatesOfExchange>(entity =>
            {
                entity.ToTable("currency_rates_of_exchange");

                entity.HasIndex(e => e.CurrencyId)
                    .HasName("Currency_Id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CurrencyBuying)
                    .HasColumnName("Currency_Buying")
                    .HasColumnType("decimal(10,5)");

                entity.Property(e => e.CurrencyId)
                    .HasColumnName("Currency_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CurrencySelling)
                    .HasColumnName("Currency_Selling")
                    .HasColumnType("decimal(10,5)");

                entity.Property(e => e.ExDate)
                    .HasColumnName("Ex_Date")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.Currency)
                    .WithMany(p => p.CurrencyRatesOfExchange)
                    .HasForeignKey(d => d.CurrencyId)
                    .HasConstraintName("currency_rates_of_exchange_ibfk_1");
            });

            modelBuilder.Entity<CurrencyRatesOfExchangeTemp>(entity =>
            {
                entity.ToTable("currency_rates_of_exchange_temp");

                entity.HasIndex(e => e.Basket)
                    .HasName("Basket");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Basket).HasColumnType("int(11)");

                entity.Property(e => e.CurrencyBuying)
                    .HasColumnName("Currency_Buying")
                    .HasColumnType("decimal(10,5)");

                entity.Property(e => e.CurrencyBuyingAmount)
                    .HasColumnName("Currency_Buying_Amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CurrencyCode)
                    .HasColumnName("Currency_Code")
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.CurrencyId)
                    .HasColumnName("Currency_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CurrencyName)
                    .HasColumnName("Currency_Name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.CurrencySelling)
                    .HasColumnName("Currency_Selling")
                    .HasColumnType("decimal(10,5)");

                entity.Property(e => e.ExDate)
                    .HasColumnName("Ex_Date")
                    .HasColumnType("date");

                entity.Property(e => e.ReceivedCurrency)
                    .HasColumnName("Received_Currency")
                    .HasColumnType("decimal(10,2)");
            });

            modelBuilder.Entity<CustomerEntity>(entity =>
            {
                entity.ToTable("customer");

                entity.HasIndex(e => e.AccountGrup)
                    .HasName("customer_index3");

                entity.HasIndex(e => e.AccountTitle)
                    .HasName("customer_index2");

                entity.HasIndex(e => e.AccountType)
                    .HasName("customer_index4");

                entity.HasIndex(e => e.ContactPerson)
                    .HasName("contact person");

                entity.HasIndex(e => e.Ispersonel)
                    .HasName("ispersonel");

                entity.HasIndex(e => e.MainCustomerId)
                    .HasName("ndx_customer_main_customer_id");

                entity.HasIndex(e => e.RouteId)
                    .HasName("route_id");

                entity.HasIndex(e => e.Visible)
                    .HasName("visible");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AccountGroup2)
                    .HasColumnName("account_group2")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.AccountGrup)
                    .HasColumnName("account_grup")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.AccountTitle)
                    .HasColumnName("account_title")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.AccountType)
                    .HasColumnName("account_type")
                    .HasColumnType("varchar(60)");

                entity.Property(e => e.Adress)
                    .HasColumnName("adress")
                    .HasColumnType("varchar(1000)");

                entity.Property(e => e.Adress2)
                    .HasColumnName("adress2")
                    .HasColumnType("varchar(1000)");

                entity.Property(e => e.CompanyLogo).HasColumnName("company_logo");

                entity.Property(e => e.ContactPerson)
                    .HasColumnName("contact_person")
                    .HasColumnType("varchar(400)");

                entity.Property(e => e.CurrentInputId)
                    .HasColumnName("current_input_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.CurrentOutputId)
                    .HasColumnName("current_output_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.CustomerCode)
                    .HasColumnName("customer_code")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.CustomerPic)
                    .HasColumnName("customer_pic")
                    .HasColumnType("varchar(1500)");

                entity.Property(e => e.ElevatorFault)
                    .HasColumnName("elevator_fault")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Explanation)
                    .HasColumnName("explanation")
                    .HasColumnType("varchar(1000)");

                entity.Property(e => e.Explanation2)
                    .HasColumnName("explanation2")
                    .HasColumnType("varchar(1000)");

                entity.Property(e => e.Explanation3)
                    .HasColumnName("explanation3")
                    .HasColumnType("varchar(1000)");

                entity.Property(e => e.Fax)
                    .HasColumnName("fax")
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.FixDiscount)
                    .HasColumnName("fix_discount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.InformationNotes)
                    .HasColumnName("information_notes")
                    .HasColumnType("text");

                entity.Property(e => e.Ispersonel)
                    .HasColumnName("ispersonel")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Latitude)
                    .HasColumnName("latitude")
                    .HasColumnType("varchar(110)");

                entity.Property(e => e.Longitude)
                    .HasColumnName("longitude")
                    .HasColumnType("varchar(110)");

                entity.Property(e => e.MailAdress)
                    .HasColumnName("mail_adress")
                    .HasColumnType("varchar(120)");

                entity.Property(e => e.MainCustomerId)
                    .HasColumnName("main_customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MainCustomerIdTemp)
                    .HasColumnName("main_customer_id_temp")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MobilePhone)
                    .HasColumnName("mobile_phone")
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.Phone2)
                    .HasColumnName("phone2")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.Picture).HasColumnName("picture");

                entity.Property(e => e.PriceGroup)
                    .HasColumnName("price_group")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PricePrivileged)
                    .HasColumnName("price_privileged")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PricingId)
                    .HasColumnName("pricing_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PricingName)
                    .HasColumnName("pricing_name")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.PrivateCode)
                    .HasColumnName("private_code")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode10)
                    .HasColumnName("private_code10")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode11)
                    .HasColumnName("private_code11")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode12)
                    .HasColumnName("private_code12")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode13)
                    .HasColumnName("private_code13")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode14)
                    .HasColumnName("private_code14")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode15)
                    .HasColumnName("private_code15")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode16)
                    .HasColumnName("private_code16")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode17)
                    .HasColumnName("private_code17")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode18)
                    .HasColumnName("private_code18")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode19)
                    .HasColumnName("private_code19")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode2)
                    .HasColumnName("private_code2")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode20)
                    .HasColumnName("private_code20")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode3)
                    .HasColumnName("private_code3")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode4)
                    .HasColumnName("private_code4")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode5)
                    .HasColumnName("private_code5")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode6)
                    .HasColumnName("private_code6")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode7)
                    .HasColumnName("private_code7")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode8)
                    .HasColumnName("private_code8")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode9)
                    .HasColumnName("private_code9")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.Region)
                    .HasColumnName("region")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Risklimite)
                    .HasColumnName("risklimite")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.RouteId)
                    .HasColumnName("route_id")
                    .HasColumnType("int(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.RouteOrder)
                    .HasColumnName("route_order")
                    .HasColumnType("int(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.SalesBlock)
                    .HasColumnName("Sales_Block")
                    .HasColumnType("varchar(11)");

                entity.Property(e => e.SalesInvoiceGrossPrice)
                    .HasColumnName("sales_invoice_gross_price")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.SalesInvoiceRetailPrice)
                    .HasColumnName("sales_invoice_retail_price")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Sector)
                    .HasColumnName("sector")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Task)
                    .HasColumnName("task")
                    .HasColumnType("varchar(60)");

                entity.Property(e => e.TaxNumber)
                    .HasColumnName("tax_number")
                    .HasColumnType("varchar(11)");

                entity.Property(e => e.TaxOffice)
                    .HasColumnName("tax_office")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Visible)
                    .HasColumnName("visible")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WithholdingRate)
                    .HasColumnName("withholding_rate")
                    .HasColumnType("int(1)")
                    .HasDefaultValueSql("'0'");
            });

            modelBuilder.Entity<CustomerAdress>(entity =>
            {
                entity.ToTable("customer_adress");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Adress)
                    .HasColumnName("adress")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.AdressType)
                    .HasColumnName("adress_type")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Country)
                    .HasColumnName("country")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Fax)
                    .HasColumnName("fax")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.MobilePhone)
                    .HasColumnName("mobile_phone")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.Province)
                    .HasColumnName("province")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Town)
                    .HasColumnName("town")
                    .HasColumnType("varchar(150)");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerAdress)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("customer_adress_ibfk_1");
            });

            modelBuilder.Entity<CustomerBank>(entity =>
            {
                entity.ToTable("customer_bank");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AccountHolder)
                    .IsRequired()
                    .HasColumnName("account_holder")
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.AccountNumber)
                    .HasColumnName("account_number")
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.AdAccountNumber)
                    .HasColumnName("ad_account_number")
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.BankDepartment)
                    .HasColumnName("bank_department")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.BankId)
                    .HasColumnName("bank_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CustomerNumber)
                    .HasColumnName("customer_number")
                    .HasColumnType("int(30)");

                entity.Property(e => e.DepartmentAdress)
                    .HasColumnName("department_adress")
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.DepartmentContactPerson)
                    .HasColumnName("department_contact_person")
                    .HasColumnType("varchar(60)");

                entity.Property(e => e.DepartmentFax)
                    .HasColumnName("department_fax")
                    .HasColumnType("varchar(16)");

                entity.Property(e => e.DepartmentMail)
                    .HasColumnName("department_mail")
                    .HasColumnType("varchar(140)");

                entity.Property(e => e.DepartmentTel)
                    .HasColumnName("department_tel")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(260)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(260)");

                entity.Property(e => e.Iban)
                    .HasColumnName("iban")
                    .HasColumnType("varchar(30)");
            });

            modelBuilder.Entity<CustomerCorrespondenceReportFiles>(entity =>
            {
                entity.ToTable("customer_correspondence_report_files");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.FileName)
                    .HasColumnName("file_name")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<CustomerCreditcard>(entity =>
            {
                entity.ToTable("customer_creditcard");

                entity.HasIndex(e => e.BankId)
                    .HasName("bank_id");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BankId)
                    .HasColumnName("bank_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CampaignNotes)
                    .HasColumnName("campaign_notes")
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.CardFee)
                    .HasColumnName("card_fee")
                    .HasColumnType("decimal(10,8)");

                entity.Property(e => e.CardHolder)
                    .HasColumnName("card_holder")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.CardNo)
                    .HasColumnName("card_no")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.CardType)
                    .HasColumnName("card_type")
                    .HasColumnType("tinyint(2)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(250)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(250)");

                entity.Property(e => e.ExpDate)
                    .HasColumnName("exp_date")
                    .HasColumnType("varchar(6)");

                entity.Property(e => e.InstallmentNotes)
                    .HasColumnName("installment_notes")
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.InterestRate)
                    .HasColumnName("interest_rate")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.MinPayment)
                    .HasColumnName("min_payment")
                    .HasColumnType("decimal(10,8)");

                entity.Property(e => e.SecurityCode)
                    .HasColumnName("security_code")
                    .HasColumnType("varchar(10)");
            });

            modelBuilder.Entity<CustomerCurrentInput>(entity =>
            {
                entity.ToTable("customer_current_input");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.HasIndex(e => e.Deletion)
                    .HasName("deletion");

                entity.HasIndex(e => e.Type)
                    .HasName("ndx_input_type");

                entity.HasIndex(e => new { e.Type, e.CustomerId })
                    .HasName("ndx_input_type_2");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasColumnType("decimal(18,2)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Datetime)
                    .HasColumnName("datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DetailId)
                    .HasColumnName("detail_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'-1'");

                entity.Property(e => e.DocNumber)
                    .HasColumnName("doc_number")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(500)");

                entity.Property(e => e.InsDatetime)
                    .HasColumnName("ins_datetime")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Type1)
                    .HasColumnName("type_")
                    .HasColumnType("varchar(3)")
                    .HasDefaultValueSql("'in'");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerCurrentInput)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("customer_id");
            });

            modelBuilder.Entity<CustomerCurrentOutput>(entity =>
            {
                entity.ToTable("customer_current_output");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.HasIndex(e => e.Deletion)
                    .HasName("deletion");

                entity.HasIndex(e => e.Type)
                    .HasName("ndx_output_type");

                entity.HasIndex(e => new { e.Type, e.CustomerId })
                    .HasName("ndx_output_type_customer_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasColumnType("decimal(18,2)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Datetime)
                    .HasColumnName("datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DetailId)
                    .HasColumnName("detail_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'-1'");

                entity.Property(e => e.DocNumber)
                    .HasColumnName("doc_number")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.ElevatorInvoiceStats)
                    .HasColumnName("elevator_invoice_stats")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'2'");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(500)");

                entity.Property(e => e.InsDatetime)
                    .HasColumnName("ins_datetime")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Type1)
                    .HasColumnName("type_")
                    .HasColumnType("varchar(3)")
                    .HasDefaultValueSql("'out'");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerCurrentOutput)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("customer_current_output_ibfk_1");
            });

            modelBuilder.Entity<CustomerCurrentOutputServiceNotes>(entity =>
            {
                entity.ToTable("customer_current_output_service_notes");

                entity.HasIndex(e => e.CustomerCurrentOutputId)
                    .HasName("customer_current_output_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CustomerCurrentOutputId)
                    .HasColumnName("customer_current_output_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DocNumber)
                    .HasColumnName("doc_number")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.InsTime)
                    .HasColumnName("ins_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.HasOne(d => d.CustomerCurrentOutput)
                    .WithMany(p => p.CustomerCurrentOutputServiceNotes)
                    .HasForeignKey(d => d.CustomerCurrentOutputId)
                    .HasConstraintName("customer_current_output_service_notes_ibfk_1");
            });

            modelBuilder.Entity<CustomerDocumentDefinitions>(entity =>
            {
                entity.ToTable("customer_document_definitions");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Datetime)
                    .HasColumnName("datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DocGroup)
                    .HasColumnName("doc_group")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.InsDatetime)
                    .HasColumnName("ins_datetime")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.TotalPage)
                    .HasColumnName("total_page")
                    .HasColumnType("int(3)");
            });

            modelBuilder.Entity<CustomerOffset>(entity =>
            {
                entity.ToTable("customer_offset");

                entity.HasIndex(e => e.CurrentInputId)
                    .HasName("current_input_id");

                entity.HasIndex(e => e.CurrentOutputId)
                    .HasName("current_output_id");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CurrentInputId)
                    .HasColumnName("current_input_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CurrentOutputId)
                    .HasColumnName("current_output_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.CurrentInput)
                    .WithMany(p => p.CustomerOffset)
                    .HasForeignKey(d => d.CurrentInputId)
                    .HasConstraintName("customer_offset_ibfk_2");

                entity.HasOne(d => d.CurrentOutput)
                    .WithMany(p => p.CustomerOffset)
                    .HasForeignKey(d => d.CurrentOutputId)
                    .HasConstraintName("customer_offset_ibfk_3");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerOffset)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("customer_offset_ibfk_1");
            });

            modelBuilder.Entity<CustomerPersonelActivity>(entity =>
            {
                entity.ToTable("customer_personel_activity");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CustomerPersonelId)
                    .HasColumnName("customer_personel_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DailyAmount)
                    .HasColumnName("daily_amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.DailyWorkingHours)
                    .HasColumnName("daily_working_hours")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DailyWorkingMinutes)
                    .HasColumnName("daily_working_minutes")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DailyWorkingSecond)
                    .HasColumnName("daily_working_second")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Dateofdeparture)
                    .HasColumnName("dateofdeparture")
                    .HasColumnType("datetime");

                entity.Property(e => e.Dateofentry)
                    .HasColumnName("dateofentry")
                    .HasColumnType("datetime");

                entity.Property(e => e.HoursOfOvertime)
                    .HasColumnName("hours_of_overtime")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ShiftAmount)
                    .HasColumnName("shift_amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<CustomerPersonelActivityTemp>(entity =>
            {
                entity.ToTable("customer_personel_activity_temp");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CustomerPersonelId)
                    .HasColumnName("customer_personel_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DailyAmount)
                    .HasColumnName("daily_amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Dateofdeparture)
                    .HasColumnName("dateofdeparture")
                    .HasColumnType("datetime");

                entity.Property(e => e.Dateofentry)
                    .HasColumnName("dateofentry")
                    .HasColumnType("datetime");

                entity.Property(e => e.ShiftAmount)
                    .HasColumnName("shift_amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<CustomerProducerReceiptTaxDefines>(entity =>
            {
                entity.ToTable("customer_producer_receipt_tax_defines");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.HasIndex(e => new { e.ProducerReceiptTaxDefinesId, e.CustomerId })
                    .HasName("producer_receipt_tax_defines_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Discount)
                    .HasColumnName("discount")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Exempt)
                    .HasColumnName("exempt")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.ProducerReceiptTaxDefinesId)
                    .HasColumnName("producer_receipt_tax_defines_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TaxName)
                    .HasColumnName("tax_name")
                    .HasColumnType("varchar(50)");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerProducerReceiptTaxDefines)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("customer_producer_receipt_tax_defines_ibfk_1");
            });

            modelBuilder.Entity<CustomerSubscription>(entity =>
            {
                entity.ToTable("customer_subscription");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.HasIndex(e => e.SubscriptionId)
                    .HasName("subscription_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CustomerPrice)
                    .HasColumnName("Customer_Price")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Stats)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.SubscriptionId)
                    .HasColumnName("subscription_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerSubscription)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("customer_subscription_ibfk_2");

                entity.HasOne(d => d.Subscription)
                    .WithMany(p => p.CustomerSubscription)
                    .HasForeignKey(d => d.SubscriptionId)
                    .HasConstraintName("customer_subscription_ibfk_1");
            });

            modelBuilder.Entity<CustomerSubscriptionCurrent>(entity =>
            {
                entity.ToTable("customer_subscription_current");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.InsDate)
                    .HasColumnName("ins_date")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.LastPayDate)
                    .HasColumnName("last_pay_date")
                    .HasColumnType("timestamp");

                entity.Property(e => e.PayDate)
                    .HasColumnName("pay_date")
                    .HasColumnType("timestamp");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'1'");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerSubscriptionCurrent)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("customer_subscription_current_ibfk_1");
            });

            modelBuilder.Entity<CustomerSubscriptionDetail>(entity =>
            {
                entity.ToTable("customer_subscription_detail");

                entity.HasIndex(e => e.CustomerSubscriptionId)
                    .HasName("customer_subscription_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ContractDate)
                    .HasColumnName("contract_date")
                    .HasColumnType("timestamp");

                entity.Property(e => e.CustomerSubscriptionId)
                    .HasColumnName("customer_subscription_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Exp3)
                    .HasColumnName("exp3")
                    .HasColumnType("varchar(155)");

                entity.Property(e => e.OptionDay)
                    .HasColumnName("option_day")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.PayDay)
                    .HasColumnName("pay_day")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.SubscriptionStartDate)
                    .HasColumnName("subscription_start_date")
                    .HasColumnType("timestamp");

                entity.HasOne(d => d.CustomerSubscription)
                    .WithMany(p => p.CustomerSubscriptionDetail)
                    .HasForeignKey(d => d.CustomerSubscriptionId)
                    .HasConstraintName("customer_subscription_detail_ibfk_1");
            });

            modelBuilder.Entity<DeviceLocation>(entity =>
            {
                entity.ToTable("device_location");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.HasIndex(e => e.DeviceImei)
                    .HasName("device_imei");

                entity.HasIndex(e => e.InsDateTime)
                    .HasName("ins_date_time");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DeviceImei)
                    .HasColumnName("device_imei")
                    .HasColumnType("varchar(17)");

                entity.Property(e => e.InsDateTime)
                    .HasColumnName("ins_date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.Latitude)
                    .HasColumnName("latitude")
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.Longitude)
                    .HasColumnName("longitude")
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Documents>(entity =>
            {
                entity.ToTable("documents");

                entity.HasIndex(e => e.CustomerDocumentDefinitionsId)
                    .HasName("customer_document_definitions_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CustomerDocumentDefinitionsId)
                    .HasColumnName("customer_document_definitions_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Document).HasColumnName("document");

                entity.Property(e => e.Page)
                    .HasColumnName("page")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TotalPage)
                    .HasColumnName("total_page")
                    .HasColumnType("int(10)");
            });

            modelBuilder.Entity<ElevatorCardParameters>(entity =>
            {
                entity.ToTable("elevator_card_parameters");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.ControlType)
                    .HasColumnName("control_type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.ElevatorOrder)
                    .HasColumnName("elevator_order")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<ElevatorCardSubParameters>(entity =>
            {
                entity.ToTable("elevator_card_sub_parameters");

                entity.HasIndex(e => e.ElevatorCardParametersId)
                    .HasName("elevator_card_parameters_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.CustomText)
                    .HasColumnName("custom_text")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.ElevatorCardParametersId)
                    .HasColumnName("elevator_card_parameters_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ElevatorOrder)
                    .HasColumnName("elevator_order")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Photo)
                    .HasColumnName("photo")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.TopDetailId)
                    .HasColumnName("top_detail_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.HasOne(d => d.ElevatorCardParameters)
                    .WithMany(p => p.ElevatorCardSubParameters)
                    .HasForeignKey(d => d.ElevatorCardParametersId)
                    .HasConstraintName("elevator_card_sub_parameters_ibfk_1");
            });

            modelBuilder.Entity<ElevatorCustomerCardParameters>(entity =>
            {
                entity.ToTable("elevator_customer_card_parameters");

                entity.HasIndex(e => e.ServicePointsBaseInformationId)
                    .HasName("service_points_base_information_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp")
                    ;

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ElevatorCardParametersId)
                    .HasColumnName("elevator_card_parameters_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ElevatorCardParametersIdForType2)
                    .HasColumnName("elevator_card_parameters_id_for_type_2")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ElevatorCardSubParametersId)
                    .HasColumnName("elevator_card_sub_parameters_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ElevatorOrder)
                    .HasColumnName("elevator_order")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InsTime)
                    .HasColumnName("ins_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.OtherDefinition)
                    .HasColumnName("other_definition")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Picture).HasColumnName("picture");

                entity.Property(e => e.ServicePointsBaseInformationId)
                    .HasColumnName("service_points_base_information_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.ServicePointsBaseInformation)
                    .WithMany(p => p.ElevatorCustomerCardParameters)
                    .HasForeignKey(d => d.ServicePointsBaseInformationId)
                    .HasConstraintName("elevator_customer_card_parameters_ibfk_1");
            });

            modelBuilder.Entity<ElevatorFaultInterventionDefinitions>(entity =>
            {
                entity.ToTable("elevator_fault_intervention_definitions");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Define)
                    .HasColumnName("define")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.Exp1)
                    .HasColumnName("exp1")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<ElevatorInstallationList>(entity =>
            {
                entity.ToTable("elevator_installation_list");

                entity.HasIndex(e => e.OfferTempId)
                    .HasName("offer_temp_id")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ContractDate)
                    .HasColumnName("contract_date")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ContractorUserId)
                    .HasColumnName("Contractor_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Notes)
                    .HasColumnName("notes")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.OfferTempId)
                    .HasColumnName("offer_temp_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ResponsibleUserId)
                    .HasColumnName("responsible_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.StatsType)
                    .HasColumnName("stats_type")
                    .HasColumnType("varchar(255)")
                    .HasDefaultValueSql("'0'");
            });

            modelBuilder.Entity<ElevatorOfferSelectedProductPropertiesTemp>(entity =>
            {
                entity.ToTable("elevator_offer_selected_product_properties_temp");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.OfferGroupOrderId)
                    .HasColumnName("offer_group_order_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PropertiesType)
                    .HasColumnName("properties_type")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PropertyId)
                    .HasColumnName("property_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PropertyName)
                    .HasColumnName("property_name")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.SubPropertyId)
                    .HasColumnName("sub_property_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SubPropertyName)
                    .HasColumnName("sub_property_name")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.TabOrder)
                    .HasColumnName("tab_order")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<ElevatorOfferSelectedProductSizes>(entity =>
            {
                entity.ToTable("elevator_offer_selected_product_sizes");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.OfferGroupOrderId)
                    .HasColumnName("offer_group_order_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Size)
                    .HasColumnName("size")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.TabOrder)
                    .HasColumnName("tab_order")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<ElevatorOfferTempProductList>(entity =>
            {
                entity.ToTable("elevator_offer_temp_product_list");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ApplicationPart)
                    .HasColumnName("application_part")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.GroupName)
                    .HasColumnName("group_name")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.OfferGroup)
                    .HasColumnName("offer_group")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.SelectedMark)
                    .HasColumnName("selected_mark")
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.Size)
                    .HasColumnName("size")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");
            });

            modelBuilder.Entity<ElevatorRevisionOffer>(entity =>
            {
                entity.ToTable("elevator_revision_offer");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ElevatorControlHistoryId)
                    .HasColumnName("elevator_control_history_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ElevatorRevisionOfferParametersBasket)
                    .HasColumnName("elevator_revision_offer_parameters_basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.LaborCosts)
                    .HasColumnName("labor_costs")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.OfferDate)
                    .HasColumnName("offer_date")
                    .HasColumnType("date");

                entity.Property(e => e.Profit)
                    .HasColumnName("profit")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Stats)
                    .HasColumnName("stats")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.StatusColor)
                    .HasColumnName("status_color")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.TargetColor)
                    .HasColumnName("target_color")
                    .HasColumnType("tinyint(3)");
            });

            modelBuilder.Entity<ElevatorRevisionOfferParameters>(entity =>
            {
                entity.ToTable("elevator_revision_offer_parameters");

                entity.HasIndex(e => e.StatsChangeUserId)
                    .HasName("stats_change_user_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.ElevatorOrder)
                    .HasColumnName("elevator_order")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.ElevatorRevisionParametersId)
                    .HasColumnName("elevator_revision_parameters_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Fee)
                    .HasColumnName("fee")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.MaxDiscount)
                    .HasColumnName("max_discount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.OfferCaption)
                    .HasColumnName("offer_caption")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.OfferVisible)
                    .HasColumnName("offer_visible")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Stats)
                    .HasColumnName("stats")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.StatsChangeDatetime)
                    .HasColumnName("stats_change_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.StatsChangeUserId)
                    .HasColumnName("stats_change_user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.StatsChangeUser)
                    .WithMany(p => p.ElevatorRevisionOfferParameters)
                    .HasForeignKey(d => d.StatsChangeUserId)
                    .HasConstraintName("elevator_revision_offer_parameters_ibfk_1");
            });

            modelBuilder.Entity<ElevatorRevisionOfferParametersTemp>(entity =>
            {
                entity.ToTable("elevator_revision_offer_parameters_temp");

                entity.HasIndex(e => e.ElevatorRevisionParametersId)
                    .HasName("elevator_revision_parameters_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.ElevatorOrder)
                    .HasColumnName("elevator_order")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.ElevatorRevisionParametersId)
                    .HasColumnName("elevator_revision_parameters_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Fee)
                    .HasColumnName("fee")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.MaxDiscount)
                    .HasColumnName("max_discount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.OfferCaption)
                    .HasColumnName("offer_caption")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.OfferVisible)
                    .HasColumnName("offer_visible")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Unit)
                    .HasColumnName("unit")
                    .HasColumnType("varchar(20)")
                    .HasDefaultValueSql("'Adet'");

                entity.HasOne(d => d.ElevatorRevisionParameters)
                    .WithMany(p => p.ElevatorRevisionOfferParametersTemp)
                    .HasForeignKey(d => d.ElevatorRevisionParametersId)
                    .HasConstraintName("elevator_revision_offer_parameters_temp_ibfk_1");
            });

            modelBuilder.Entity<ElevatorRevisionParameters>(entity =>
            {
                entity.ToTable("elevator_revision_parameters");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Cost)
                    .HasColumnName("cost")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.Fee)
                    .HasColumnName("fee")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.MaxDiscount)
                    .HasColumnName("max_discount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.OfferCaption)
                    .HasColumnName("offer_caption")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.OfferVisible)
                    .HasColumnName("offer_visible")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Workmanship)
                    .HasColumnName("workmanship")
                    .HasColumnType("decimal(10,2)");
            });

            modelBuilder.Entity<ElevatorRevisionParametersProduct>(entity =>
            {
                entity.ToTable("elevator_revision_parameters_product");

                entity.HasIndex(e => e.ElevatorRevisionParametersId)
                    .HasName("elevator_revision_parameters_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ElevatorRevisionParametersId)
                    .HasColumnName("elevator_revision_parameters_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductQty)
                    .HasColumnName("product_qty")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.ElevatorRevisionParameters)
                    .WithMany(p => p.ElevatorRevisionParametersProduct)
                    .HasForeignKey(d => d.ElevatorRevisionParametersId)
                    .HasConstraintName("elevator_revision_parameters_product_ibfk_1");
            });

            modelBuilder.Entity<ElevatorSubcontractorDefines>(entity =>
            {
                entity.ToTable("elevator_subcontractor_defines");

                entity.Property(e => e.Id).HasColumnType("int(11)");
            });

            modelBuilder.Entity<ElevatorSubControlHistory>(entity =>
            {
                entity.ToTable("elevator_sub_control_history");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ContralDate)
                    .HasColumnName("contral_date")
                    .HasColumnType("date");

                entity.Property(e => e.ControlResult)
                    .HasColumnName("control_result")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ElevatorControlHistoryId)
                    .HasColumnName("elevator_control_history_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InsDateTime)
                    .HasColumnName("ins_date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Notes)
                    .HasColumnName("notes")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<EmailParameters>(entity =>
            {
                entity.ToTable("email_parameters");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ConnectionMethod)
                    .HasColumnName("connection_method")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.FromExp)
                    .HasColumnName("from_exp")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Port)
                    .HasColumnName("port")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServerName)
                    .HasColumnName("server_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.UserName)
                    .HasColumnName("user_name")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<Endofthedayreport>(entity =>
            {
                entity.ToTable("endofthedayreport");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.SubType)
                    .HasColumnName("sub_type")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("varchar(15)");
            });

            modelBuilder.Entity<Expense>(entity =>
            {
                entity.ToTable("expense");

                entity.HasIndex(e => e.InExTypesId)
                    .HasName("in_ex_types_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DetailId)
                    .HasColumnName("detail_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DocNo)
                    .HasColumnName("doc_no")
                    .HasColumnType("varchar(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.InExTypesId)
                    .HasColumnName("in_ex_types_id")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.InsDateTime)
                    .HasColumnName("ins_date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.PayDetailId)
                    .HasColumnName("pay_detail_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PayType)
                    .HasColumnName("pay_type")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Expiring>(entity =>
            {
                entity.ToTable("expiring");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DayOption)
                    .HasColumnName("day_option")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp1)
                    .HasColumnName("exp1")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Exp3)
                    .HasColumnName("exp3")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.ExpiringDays)
                    .HasColumnName("expiring_days")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ExpiryCode)
                    .HasColumnName("expiry_code")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ExpiryDescription)
                    .HasColumnName("expiry_description")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.MonthInterest)
                    .HasColumnName("month_interest")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.PricingId)
                    .HasColumnName("pricing_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PricingName)
                    .HasColumnName("pricing_name")
                    .HasColumnType("varchar(30)");
            });

            modelBuilder.Entity<FarmAnimals>(entity =>
            {
                entity.ToTable("farm_animals");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BirthDate)
                    .HasColumnName("birth_date")
                    .HasColumnType("date");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FarmPaddockDefination)
                    .HasColumnName("farm_paddock_defination")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.FarmPaddockId)
                    .HasColumnName("farm_paddock_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FarmsId)
                    .HasColumnName("farms_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FarmsName)
                    .HasColumnName("farms_name")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Father)
                    .HasColumnName("father")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'-1'");

                entity.Property(e => e.Gender)
                    .HasColumnName("gender")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.LiveStatus)
                    .HasColumnName("live_status")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Mother)
                    .HasColumnName("mother")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'-1'");

                entity.Property(e => e.SpecialCode)
                    .HasColumnName("special_code")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("varchar(30)");
            });

            modelBuilder.Entity<FarmAnimalsActivity>(entity =>
            {
                entity.ToTable("farm_animals_activity");

                entity.HasIndex(e => e.FarmAnimalsId)
                    .HasName("farm_animals_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Cm)
                    .HasColumnName("cm")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FarmAnimalsId)
                    .HasColumnName("farm_animals_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FarmVeterinaryApplicationsId)
                    .HasColumnName("farm_veterinary_applications_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IfType3AnimalId)
                    .HasColumnName("if_type_3_animal_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Kg)
                    .HasColumnName("kg")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.VaccineDose)
                    .HasColumnName("vaccine_dose")
                    .HasColumnType("decimal(10,2)");

                entity.HasOne(d => d.FarmAnimals)
                    .WithMany(p => p.FarmAnimalsActivity)
                    .HasForeignKey(d => d.FarmAnimalsId)
                    .HasConstraintName("farm_animals_activity_ibfk_1");
            });

            modelBuilder.Entity<FarmFeedActivity>(entity =>
            {
                entity.ToTable("farm_feed_activity");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.FarmFeedMixtureId)
                    .HasColumnName("farm_feed_mixture_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FarmFeedMixtureName)
                    .HasColumnName("farm_feed_mixture_name")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.PaddockId)
                    .HasColumnName("paddock_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PaddockName)
                    .HasColumnName("paddock_name")
                    .HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<FarmFeedMixture>(entity =>
            {
                entity.ToTable("farm_feed_mixture");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FarmPaddockId)
                    .HasColumnName("farm_paddock_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FarmPaddockName)
                    .HasColumnName("farm_paddock_name")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.FeedType)
                    .HasColumnName("feed_type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.MixtureCode)
                    .HasColumnName("mixture_code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.MixtureName)
                    .HasColumnName("mixture_name")
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<FarmFeedMixtureProducts>(entity =>
            {
                entity.ToTable("farm_feed_mixture_products");

                entity.HasIndex(e => e.FarmFeedMixtureId)
                    .HasName("farm_feed_mixture_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.FarmFeedMixtureId)
                    .HasColumnName("farm_feed_mixture_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(120)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.QtyOfMeal)
                    .HasColumnName("qty_of_meal")
                    .HasColumnType("decimal(10,2)");

                entity.HasOne(d => d.FarmFeedMixture)
                    .WithMany(p => p.FarmFeedMixtureProducts)
                    .HasForeignKey(d => d.FarmFeedMixtureId)
                    .HasConstraintName("farm_feed_mixture_products_ibfk_1");
            });

            modelBuilder.Entity<FarmPaddock>(entity =>
            {
                entity.ToTable("farm_paddock");

                entity.HasIndex(e => e.FarmsId)
                    .HasName("farms_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FarmsId)
                    .HasColumnName("farms_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PaddockCode)
                    .HasColumnName("paddock_code")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.PaddockDefination)
                    .HasColumnName("paddock_defination")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Rfid)
                    .HasColumnName("rfid")
                    .HasColumnType("varchar(50)");

                entity.HasOne(d => d.Farms)
                    .WithMany(p => p.FarmPaddock)
                    .HasForeignKey(d => d.FarmsId)
                    .HasConstraintName("farm_paddock_ibfk_1");
            });

            modelBuilder.Entity<Farms>(entity =>
            {
                entity.ToTable("farms");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FarmCode)
                    .HasColumnName("farm_code")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.FarmDefination)
                    .HasColumnName("farm_defination")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.M2)
                    .HasColumnName("m2")
                    .HasColumnType("decimal(10,2)");
            });

            modelBuilder.Entity<FarmVeterinary>(entity =>
            {
                entity.ToTable("farm_veterinary");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AccountTitle)
                    .HasColumnName("account_title")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp1)
                    .HasColumnName("exp1")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.VeterinaryCode)
                    .HasColumnName("veterinary_code")
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<FarmVeterinaryApplications>(entity =>
            {
                entity.ToTable("farm_veterinary_applications");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AppCode)
                    .HasColumnName("app_code")
                    .HasColumnType("varchar(75)");

                entity.Property(e => e.AppDefination)
                    .HasColumnName("app_defination")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.AppType)
                    .HasColumnName("app_type")
                    .HasColumnType("varchar(75)");

                entity.Property(e => e.AppTypeCode)
                    .HasColumnName("app_type_code")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Exp1)
                    .HasColumnName("exp1")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<FormButtonDefines>(entity =>
            {
                entity.ToTable("form_button_defines");

                entity.HasIndex(e => e.FormName)
                    .HasName("Form_Name");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ButtonCaption)
                    .HasColumnName("Button_Caption")
                    .HasColumnType("varchar(110)");

                entity.Property(e => e.ButtonName)
                    .HasColumnName("Button_Name")
                    .HasColumnType("varchar(65)");

                entity.Property(e => e.FormName)
                    .HasColumnName("Form_Name")
                    .HasColumnType("varchar(60)");

                entity.Property(e => e.ShortCut).HasColumnType("varchar(15)");

                entity.Property(e => e.UserId)
                    .HasColumnName("User_Id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");
            });

            modelBuilder.Entity<GeneralParameters>(entity =>
            {
                entity.ToTable("general_parameters");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AccessPersonelCardPassword)
                    .HasColumnName("access_personel_card_password")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.AskPasswordOnAccesPersonelCard)
                    .HasColumnName("ask_password_on_acces_personel_card")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.CompanyName)
                    .HasColumnName("company_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FuelPurchaseReasons)
                    .HasColumnName("fuel_purchase_reasons")
                    .HasColumnType("text");

                entity.Property(e => e.MpBottom1)
                    .HasColumnName("mp_bottom1")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.MpBottom2)
                    .HasColumnName("mp_bottom2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.MpTop1)
                    .HasColumnName("mp_top1")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.MpTop2)
                    .HasColumnName("mp_top2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.PushCompanyCode)
                    .HasColumnName("push_company_code")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ServiceCustomerCurrentExplanation)
                    .HasColumnName("service_customer_current_explanation")
                    .HasColumnType("varchar(255)")
                    .HasDefaultValueSql("'{services_open_id} Numaralı {year}-{month} Ayı {service_point_name} Servis Tutarı'");

                entity.Property(e => e.ServisCepteLogoBase64).HasColumnName("servis_cepte_logo_base_64");
            });

            modelBuilder.Entity<Income>(entity =>
            {
                entity.ToTable("income");

                entity.HasIndex(e => e.InExTypesId)
                    .HasName("in_ex_types_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DetailId)
                    .HasColumnName("detail_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DocNo)
                    .HasColumnName("doc_no")
                    .HasColumnType("varchar(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.InExTypesId)
                    .HasColumnName("in_ex_types_id")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.InsDateTime)
                    .HasColumnName("ins_date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.PayDetailId)
                    .HasColumnName("pay_detail_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PayType)
                    .HasColumnName("pay_type")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<IncomeAndExpenseTypes>(entity =>
            {
                entity.ToTable("income_and_expense_types");

                entity.HasIndex(e => e.VehicleId)
                    .HasName("vehicle_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(60)");

                entity.Property(e => e.Definition)
                    .HasColumnName("definition")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp3)
                    .HasColumnName("exp3")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FixedExpenses)
                    .HasColumnName("fixed_expenses")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.FuelType)
                    .HasColumnName("fuel_type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Group)
                    .HasColumnName("group_")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.IsVehicleAccount)
                    .HasColumnName("is_vehicle_account")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.RangeAgainDay)
                    .HasColumnName("range_again_day")
                    .HasColumnType("int(11)");

                entity.Property(e => e.RangeAgainMonth)
                    .HasColumnName("range_again_month")
                    .HasColumnType("int(11)");

                entity.Property(e => e.RangeAgainYear)
                    .HasColumnName("range_again_year")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServiceType)
                    .HasColumnName("service_type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.TireType)
                    .HasColumnName("tire_type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.TypeExpense)
                    .HasColumnName("type_expense")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.TypeIncome)
                    .HasColumnName("type_income")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.VariableExpenses)
                    .HasColumnName("variable_expenses")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.VehicleId)
                    .HasColumnName("vehicle_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Vehicle)
                    .WithMany(p => p.IncomeAndExpenseTypes)
                    .HasForeignKey(d => d.VehicleId)
                    .HasConstraintName("income_and_expense_types_ibfk_1");
            });

            modelBuilder.Entity<Installments>(entity =>
            {
                entity.ToTable("installments");

                entity.HasIndex(e => e.InstallmentsPlanBasketId)
                    .HasName("installments_plan_basket_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DownCurrency)
                    .HasColumnName("down_currency")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.DownDetailId)
                    .HasColumnName("down_detail_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DownPaymentType)
                    .HasColumnName("down_payment_type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.InstallmentAssurance)
                    .HasColumnName("installment_assurance")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.InstallmentAssuranceId)
                    .HasColumnName("installment_assurance_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InstallmentsPlanBasketId)
                    .HasColumnName("installments_plan_basket_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InstallmentsSource)
                    .HasColumnName("installments_source")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.InstallmentsSourceId)
                    .HasColumnName("installments_source_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InstallmentsType)
                    .HasColumnName("installments_type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.InvoiceType)
                    .HasColumnName("invoice_type")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.TotalCurrency)
                    .HasColumnName("total_currency")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.TotalInterest)
                    .HasColumnName("total_interest")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.TotalInterestAmount)
                    .HasColumnName("total_interest_amount")
                    .HasColumnType("decimal(10,2)");
            });

            modelBuilder.Entity<InstallmentsPlan>(entity =>
            {
                entity.ToTable("installments_plan");

                entity.HasIndex(e => e.BasketId)
                    .HasName("ndx_installments_plan_basket_id");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("ndx_installments_plan_customer_id");

                entity.HasIndex(e => e.PayDate)
                    .HasName("ndx_installments_plan_pay_date");

                entity.HasIndex(e => e.UserId)
                    .HasName("ndx_installments_plan_user_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AmountOfInterest)
                    .HasColumnName("amount_of_interest")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.BasketId)
                    .HasColumnName("basket_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InsDate)
                    .HasColumnName("ins_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.InstallmentsCurrency)
                    .HasColumnName("installments_currency")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.InstallmentsCurrencyInterest)
                    .HasColumnName("installments_currency_interest")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.InterestRate)
                    .HasColumnName("interest_rate")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.NumberOfInstallments)
                    .HasColumnName("number_of_installments")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PayDate)
                    .HasColumnName("pay_date")
                    .HasColumnType("date");

                entity.Property(e => e.TheDifferenceInDays)
                    .HasColumnName("the_difference_in_days")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<InstallmentsPlanTemp>(entity =>
            {
                entity.ToTable("installments_plan_temp");

                entity.HasIndex(e => e.BasketId)
                    .HasName("ndx_installments_plan_temp_basket_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AmountOfInterest)
                    .HasColumnName("amount_of_interest")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.BasketId)
                    .HasColumnName("basket_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InstallmentsCurrency)
                    .HasColumnName("installments_currency")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.InstallmentsCurrencyInterest)
                    .HasColumnName("installments_currency_interest")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.InterestRate)
                    .HasColumnName("interest_rate")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.NumberOfInstallments)
                    .HasColumnName("number_of_installments")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PayDate)
                    .HasColumnName("pay_date")
                    .HasColumnType("date");

                entity.Property(e => e.TheDifferenceInDays)
                    .HasColumnName("the_difference_in_days")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Invoice>(entity =>
            {
                entity.ToTable("invoice");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("nx_invoice_customer_id");

                entity.HasIndex(e => e.Deletion)
                    .HasName("deletion");

                entity.HasIndex(e => e.IfEditNewInvoiceId)
                    .HasName("if_edit_new_invoice_id");

                entity.HasIndex(e => e.InvoiceProductBasket)
                    .HasName("ndx_invoice_invoice_product_basket");

                entity.HasIndex(e => e.Type)
                    .HasName("ndx_invoice_type");

                entity.HasIndex(e => e.UserId)
                    .HasName("ndx_invoice_user_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.CustomerCurrentStats)
                    .HasColumnName("Customer_Current_Stats")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DetailId)
                    .HasColumnName("detail_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ExpiringId)
                    .HasColumnName("expiring_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IfEditNewInvoiceId)
                    .HasColumnName("if_edit_new_invoice_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InvoiceDateTime)
                    .HasColumnName("invoice_date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.InvoiceNumber)
                    .HasColumnName("invoice_number")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.InvoiceProductBasket)
                    .HasColumnName("invoice_product_basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InvoiceSerial)
                    .HasColumnName("invoice_serial")
                    .HasColumnType("varchar(5)");

                entity.Property(e => e.PayDetailId)
                    .HasColumnName("pay_detail_id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.PayDoor)
                    .HasColumnName("pay_door")
                    .HasColumnType("varchar(25)")
                    .HasDefaultValueSql("'Hayir'");

                entity.Property(e => e.PayMethod)
                    .HasColumnName("pay_method")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.PayType)
                    .HasColumnName("pay_type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.PricingId)
                    .HasColumnName("pricing_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServiceInvoice)
                    .HasColumnName("service_invoice")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ShipmentStats)
                    .HasColumnName("Shipment_Stats")
                    .HasColumnType("int(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Stats)
                    .HasColumnName("stats")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.SubCustomerDistrubite)
                    .HasColumnName("sub_customer_distrubite")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<InvoiceProduct>(entity =>
            {
                entity.ToTable("invoice_product");

                entity.HasIndex(e => e.Basket)
                    .HasName("basket");

                entity.HasIndex(e => e.Deletion)
                    .HasName("deletion");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ColorId)
                    .HasColumnName("color_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DetailId)
                    .HasColumnName("detail_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DiscountRate1)
                    .HasColumnName("discount_rate1")
                    .HasColumnType("decimal(10,5)")
                    .HasDefaultValueSql("'0.00000'");

                entity.Property(e => e.DiscountRate2)
                    .HasColumnName("discount_rate2")
                    .HasColumnType("decimal(10,5)")
                    .HasDefaultValueSql("'0.00000'");

                entity.Property(e => e.DiscountRate3)
                    .HasColumnName("discount_rate3")
                    .HasColumnType("decimal(10,5)")
                    .HasDefaultValueSql("'0.00000'");

                entity.Property(e => e.DiscountRate4)
                    .HasColumnName("discount_rate4")
                    .HasColumnType("decimal(10,5)")
                    .HasDefaultValueSql("'0.00000'");

                entity.Property(e => e.DiscountRate5)
                    .HasColumnName("discount_rate5")
                    .HasColumnType("decimal(10,5)")
                    .HasDefaultValueSql("'0.00000'");

                entity.Property(e => e.ProductCode)
                    .HasColumnName("product_code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(140)");

                entity.Property(e => e.ProductType)
                    .HasColumnName("product_type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Size)
                    .HasColumnName("size")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.StorageId)
                    .HasColumnName("storage_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TaxRate)
                    .HasColumnName("tax_rate")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.UnitName)
                    .HasColumnName("unit_name")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.UnitPrice)
                    .HasColumnName("unit_price")
                    .HasColumnType("decimal(18,5)");

                entity.Property(e => e.WithholdingRate)
                    .HasColumnName("withholding_rate")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.InvoiceProduct)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("invoice_product_ibfk_1");
            });

            modelBuilder.Entity<InvoiceShipmentTrace>(entity =>
            {
                entity.ToTable("invoice_shipment_trace");

                entity.HasIndex(e => e.InvoiceId)
                    .HasName("invoice_id");

                entity.HasIndex(e => e.InvoiceProductBasket)
                    .HasName("invoice_product_basket");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.HasIndex(e => e.StorageId)
                    .HasName("storage_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DriverName)
                    .HasColumnName("driver_name")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.InvoiceId)
                    .HasColumnName("invoice_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InvoiceProductBasket)
                    .HasColumnName("invoice_product_basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Plate)
                    .HasColumnName("plate")
                    .HasColumnType("varchar(60)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ShippingDatetime)
                    .HasColumnName("shipping_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.StorageId)
                    .HasColumnName("storage_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Invoice)
                    .WithMany(p => p.InvoiceShipmentTrace)
                    .HasForeignKey(d => d.InvoiceId)
                    .HasConstraintName("invoice_shipment_trace_ibfk_1");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.InvoiceShipmentTrace)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("invoice_shipment_trace_ibfk_2");

                entity.HasOne(d => d.Storage)
                    .WithMany(p => p.InvoiceShipmentTrace)
                    .HasForeignKey(d => d.StorageId)
                    .HasConstraintName("invoice_shipment_trace_ibfk_3");
            });

            modelBuilder.Entity<InvoiceShipmentTraceTemp>(entity =>
            {
                entity.ToTable("invoice_shipment_trace_temp");

                entity.HasIndex(e => e.InvoiceId)
                    .HasName("invoice_id");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.HasIndex(e => e.StorageId)
                    .HasName("storage_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InvoiceId)
                    .HasColumnName("invoice_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.StorageId)
                    .HasColumnName("storage_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.StorageName)
                    .HasColumnName("storage_name")
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<LabelTemp>(entity =>
            {
                entity.ToTable("label_temp");

                entity.HasIndex(e => e.Basket)
                    .HasName("basket");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ColorId)
                    .HasColumnName("color_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CurrencySymbol)
                    .HasColumnName("Currency_Symbol")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.DiscountPrice)
                    .HasColumnName("discount_price")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Ean)
                    .HasColumnName("ean")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.ShelfQty)
                    .HasColumnName("shelf_qty")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TicketQty)
                    .HasColumnName("ticket_qty")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Weight)
                    .HasColumnName("weight")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");
            });

            modelBuilder.Entity<LicanceInfo>(entity =>
            {
                entity.ToTable("licance_info");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Ks)
                    .HasColumnName("ks")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Message)
                    .HasColumnName("message")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Vs)
                    .HasColumnName("vs")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<LoadActivity>(entity =>
            {
                entity.ToTable("load_activity");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp")
                    ;

                entity.Property(e => e.DocNumber)
                    .HasColumnName("doc_number")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.InsDateTime)
                    .HasColumnName("ins_date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ListBasketId)
                    .HasColumnName("list_basket_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PersonelId)
                    .HasColumnName("personel_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.VehicleId)
                    .HasColumnName("vehicle_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<LoadActivityList>(entity =>
            {
                entity.ToTable("load_activity_list");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Km)
                    .HasColumnName("km")
                    .HasColumnType("int(11)");

                entity.Property(e => e.LoadTypeDefination)
                    .HasColumnName("load_type_defination")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.LoadTypeDefinitionId)
                    .HasColumnName("load_type_definition_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PriceType)
                    .HasColumnName("price_type")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PriceTypeExp)
                    .HasColumnName("price_type_exp")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ReceiverLoadLocationDefinations)
                    .HasColumnName("receiver_load_location_definations")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.ReceiverLoadLocationDefinationsId)
                    .HasColumnName("receiver_load_location_definations_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.RentUnitPrice)
                    .HasColumnName("rent_unit_price")
                    .HasColumnType("decimal(10,5)");

                entity.Property(e => e.SenderLoadLocationDefinations)
                    .HasColumnName("sender_load_location_definations")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.SenderLoadLocationDefinationsId)
                    .HasColumnName("sender_load_location_definations_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UnitPrice)
                    .HasColumnName("unit_price")
                    .HasColumnType("decimal(10,5)");
            });

            modelBuilder.Entity<LoadActivityTemp>(entity =>
            {
                entity.ToTable("load_activity_temp");

                entity.HasIndex(e => e.LoadTypeDefinitionId)
                    .HasName("load_type_definition_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Km)
                    .HasColumnName("km")
                    .HasColumnType("int(11)");

                entity.Property(e => e.LoadTypeDefination)
                    .HasColumnName("load_type_defination")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.LoadTypeDefinitionId)
                    .HasColumnName("load_type_definition_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PriceType)
                    .HasColumnName("price_type")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PriceTypeExp)
                    .HasColumnName("price_type_exp")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ReceiverLoadLocationDefinations)
                    .HasColumnName("receiver_load_location_definations")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.ReceiverLoadLocationDefinationsId)
                    .HasColumnName("receiver_load_location_definations_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.RentUnitPrice)
                    .HasColumnName("rent_unit_price")
                    .HasColumnType("decimal(10,5)");

                entity.Property(e => e.SenderLoadLocationDefinations)
                    .HasColumnName("sender_load_location_definations")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.SenderLoadLocationDefinationsId)
                    .HasColumnName("sender_load_location_definations_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UnitPrice)
                    .HasColumnName("unit_price")
                    .HasColumnType("decimal(10,5)");

                entity.HasOne(d => d.LoadTypeDefinition)
                    .WithMany(p => p.LoadActivityTemp)
                    .HasForeignKey(d => d.LoadTypeDefinitionId)
                    .HasConstraintName("load_activity_temp_ibfk_1");
            });

            modelBuilder.Entity<LoadDocumentDefinitions>(entity =>
            {
                entity.ToTable("load_document_definitions");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Color)
                    .HasColumnName("color")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(200)");
            });

            modelBuilder.Entity<LoadLocationDefinations>(entity =>
            {
                entity.ToTable("load_location_definations");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Color)
                    .HasColumnName("color")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(200)");
            });

            modelBuilder.Entity<LoadPackageDefinitions>(entity =>
            {
                entity.ToTable("load_package_definitions");

                entity.HasIndex(e => e.Defination)
                    .HasName("defination");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Color)
                    .HasColumnName("color")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(200)");
            });

            modelBuilder.Entity<LoadStatusDefinitions>(entity =>
            {
                entity.ToTable("load_status_definitions");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Color)
                    .HasColumnName("color")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(200)");
            });

            modelBuilder.Entity<LoadTypeDefinition>(entity =>
            {
                entity.ToTable("load_type_definition");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Color)
                    .HasColumnName("color")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(200)");
            });

            modelBuilder.Entity<LoadUnits>(entity =>
            {
                entity.ToTable("load_units");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Color)
                    .HasColumnName("color")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(200)");
            });

            modelBuilder.Entity<Maintenance>(entity =>
            {
                entity.ToTable("maintenance");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ElevatorCount)
                    .HasColumnName("elevator_count")
                    .HasColumnType("int(10)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Fee)
                    .HasColumnName("fee")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.FinishDate)
                    .HasColumnName("finish_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.InsDate)
                    .HasColumnName("ins_date")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Maintenance)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("maintenance_ibfk_1");
            });

            modelBuilder.Entity<Management>(entity =>
            {
                entity.ToTable("management");

                entity.HasIndex(e => e.DefaultPos)
                    .HasName("default_pos");

                entity.HasIndex(e => e.DefaultSafe)
                    .HasName("default_safe");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.CurrencyType)
                    .HasColumnName("currency_type")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.CustomerVisible)
                    .HasColumnName("customer_visible")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.DefaultPos)
                    .HasColumnName("default_pos")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DefaultPosName)
                    .HasColumnName("default_pos_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.DefaultSafe)
                    .HasColumnName("default_safe")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DefaultStorage)
                    .HasColumnName("default_storage")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.DefaultStorageName)
                    .HasColumnName("default_storage_name")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ElevatorAllPrivileges)
                    .HasColumnName("elevator_all_privileges")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.InvoiceSerial)
                    .HasColumnName("invoice_serial")
                    .HasColumnType("varchar(5)");

                entity.Property(e => e.InvoiceSerialNumber)
                    .HasColumnName("invoice_serial_number")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Mail)
                    .HasColumnName("mail")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.ManagementRolesId)
                    .HasColumnName("management_roles_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ManagementRolesRoleDefination)
                    .HasColumnName("management_roles_role_defination")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.NoCustomerCards)
                    .HasColumnName("no_customer_cards")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.NoCustomerCurrent)
                    .HasColumnName("no_customer_current")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Pass2)
                    .HasColumnName("pass2")
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.SafeName)
                    .HasColumnName("safe_name")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Skin)
                    .HasColumnName("skin")
                    .HasColumnType("varchar(60)");

                entity.Property(e => e.TaxInvoiceCheckBox)
                    .HasColumnName("tax_invoice_check_box")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.UserName)
                    .HasColumnName("user_name")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.UserRole)
                    .HasColumnName("user_role")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserType)
                    .HasColumnName("user_type")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Veterinary)
                    .HasColumnName("veterinary")
                    .HasColumnType("varchar(20)");

                entity.HasOne(d => d.DefaultPosNavigation)
                    .WithMany(p => p.Management)
                    .HasForeignKey(d => d.DefaultPos)
                    .HasConstraintName("management_ibfk_2");

                entity.HasOne(d => d.DefaultSafeNavigation)
                    .WithMany(p => p.Management)
                    .HasForeignKey(d => d.DefaultSafe)
                    .HasConstraintName("management_ibfk_1");
            });

            modelBuilder.Entity<ManagementRoles>(entity =>
            {
                entity.ToTable("management_roles");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp1)
                    .HasColumnName("exp1")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.RoleCode)
                    .HasColumnName("role_code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.RoleDefination)
                    .HasColumnName("role_defination")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<ManagementRolesAuthorization>(entity =>
            {
                entity.ToTable("management_roles_authorization");

                entity.HasIndex(e => e.ManagementRolesId)
                    .HasName("management_roles_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ManagementRolesId)
                    .HasColumnName("management_roles_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.RcAuthority)
                    .HasColumnName("rc_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.RcCollection)
                    .HasColumnName("rc_collection")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.RcTakeAccount)
                    .HasColumnName("rc_take_account")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ScAccessAccountExtract)
                    .HasColumnName("sc_access_account_extract")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ScAccessAllRequestList)
                    .HasColumnName("sc_access_all_request_list")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ScAccessCollection)
                    .HasColumnName("sc_access_collection")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ScAccessCustomer)
                    .HasColumnName("sc_access_customer")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ScAuthority)
                    .HasColumnName("sc_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinAgriculturalAuthority)
                    .HasColumnName("win_agricultural_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinAuthority)
                    .HasColumnName("win_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinBankAuthority)
                    .HasColumnName("win_bank_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinCheckCommercialPaperAuthority)
                    .HasColumnName("win_check_commercial_paper_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinCreditcardAuthority)
                    .HasColumnName("win_creditcard_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinCurrencyAuthority)
                    .HasColumnName("win_currency_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinCustomerAccountType)
                    .HasColumnName("win_customer_account_type")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.WinCustomerAuthority)
                    .HasColumnName("win_customer_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinCustomerPersonelAuthority)
                    .HasColumnName("win_customer_personel_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinCustomerPersonelDebit)
                    .HasColumnName("win_customer_personel_debit")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinDepartmentAuthority)
                    .HasColumnName("win_department_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinDocumentAuthority)
                    .HasColumnName("win_document_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinEInvoiceAuthority)
                    .HasColumnName("win_e-invoice_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinFarmManagementAuthority)
                    .HasColumnName("win_farm_management_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinIncomeAndExpenseAuthority)
                    .HasColumnName("win_income_and_expense_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinInstallmentAuthority)
                    .HasColumnName("win_installment_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinInvoiceAccessOnlySpeedSales)
                    .HasColumnName("win_invoice_access_only_speed_sales")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinInvoiceAuthority)
                    .HasColumnName("win_invoice_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinOfferAuthority)
                    .HasColumnName("win_offer_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinOrderAuthority)
                    .HasColumnName("win_order_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinPosAuthority)
                    .HasColumnName("win_pos_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinProducerReceiptAuthority)
                    .HasColumnName("win_producer_Receipt_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinProductionAuthority)
                    .HasColumnName("win_Production_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinRestaurantAuthority)
                    .HasColumnName("win_restaurant_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinSafeAuthority)
                    .HasColumnName("win_safe_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinServiceAuthority)
                    .HasColumnName("win_service_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinSettings)
                    .HasColumnName("win_settings")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.WinSmsAuthority)
                    .HasColumnName("win_sms_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinStockAuthority)
                    .HasColumnName("win_stock_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinTargetingAuthority)
                    .HasColumnName("win_targeting_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinVehicleAuthority)
                    .HasColumnName("win_vehicle_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WinWaybillAuthority)
                    .HasColumnName("win_waybill_authority")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.HasOne(d => d.ManagementRoles)
                    .WithMany(p => p.ManagementRolesAuthorization)
                    .HasForeignKey(d => d.ManagementRolesId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("management_roles_authorization_ibfk_1");
            });

            modelBuilder.Entity<ManagementRolesServiceRequestAuthorization>(entity =>
            {
                entity.ToTable("management_roles_service_request_authorization");

                entity.HasIndex(e => e.ManagementRolesAuthorizationId)
                    .HasName("management_roles_authorization_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionTime)
                    .HasColumnName("deletion_time")
                    .HasColumnType("timestamp")
                    ;

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InsDatetime)
                    .HasColumnName("ins_datetime")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ManagementRolesAuthorizationId)
                    .HasColumnName("management_roles_authorization_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServiceRequestDefination)
                    .HasColumnName("service_request_defination")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ServiceRequestId)
                    .HasColumnName("service_request_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.ManagementRolesAuthorization)
                    .WithMany(p => p.ManagementRolesServiceRequestAuthorization)
                    .HasForeignKey(d => d.ManagementRolesAuthorizationId)
                    .HasConstraintName("management_roles_service_request_authorization_ibfk_1");
            });

            modelBuilder.Entity<Message>(entity =>
            {
                entity.ToTable("message");

                entity.HasIndex(e => e.Receiver)
                    .HasName("ndx_message_3");

                entity.HasIndex(e => e.SenderId)
                    .HasName("ndx_message_2");

                entity.HasIndex(e => e.Status)
                    .HasName("ndx_message_1");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Message1)
                    .HasColumnName("message")
                    .HasColumnType("varchar(1200)");

                entity.Property(e => e.Receiver)
                    .HasColumnName("receiver")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SenderId)
                    .HasColumnName("sender_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.SupportId)
                    .HasColumnName("support_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");
            });

            modelBuilder.Entity<MobileScaleDevice>(entity =>
            {
                entity.ToTable("mobile_scale_device");

                entity.HasIndex(e => e.VehicleId)
                    .HasName("vehicle_foreign");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DeviceId)
                    .HasColumnName("Device_ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DeviceKey)
                    .HasColumnName("Device_Key")
                    .HasColumnType("varchar(35)");

                entity.Property(e => e.DeviceSerial)
                    .HasColumnName("Device_Serial")
                    .HasColumnType("varchar(35)");

                entity.Property(e => e.LicancePlate)
                    .HasColumnName("Licance_Plate")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Name).HasColumnType("varchar(35)");

                entity.Property(e => e.VehicleId)
                    .HasColumnName("Vehicle_Id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Vehicle)
                    .WithMany(p => p.MobileScaleDevice)
                    .HasForeignKey(d => d.VehicleId)
                    .HasConstraintName("vehicle_foreign");
            });

            modelBuilder.Entity<MobileScaleProductPurchase>(entity =>
            {
                entity.ToTable("mobile_scale_product_purchase");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("Customer_id");

                entity.HasIndex(e => e.MobileScaleCustomerId)
                    .HasName("Mobile_Scale_Customer_Id");

                entity.HasIndex(e => e.MobileScaleProductPurchaseId)
                    .HasName("mobile_scale_product_purchase_id");

                entity.HasIndex(e => e.PickupTime)
                    .HasName("Pickup_Time");

                entity.HasIndex(e => e.Stats)
                    .HasName("stats");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Basket).HasColumnType("int(11)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("Customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Densities).HasColumnType("decimal(10,2)");

                entity.Property(e => e.DeviceId)
                    .HasColumnName("Device_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Kg).HasColumnType("decimal(10,2)");

                entity.Property(e => e.Litre).HasColumnType("decimal(10,2)");

                entity.Property(e => e.MilkingTime)
                    .HasColumnName("Milking_Time")
                    .HasColumnType("timestamp");

                entity.Property(e => e.MobileScaleCustomerId)
                    .HasColumnName("Mobile_Scale_Customer_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MobileScaleProductPurchaseId)
                    .HasColumnName("mobile_scale_product_purchase_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MobileScaleUserId)
                    .HasColumnName("mobile_scale_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PickupTime)
                    .HasColumnName("Pickup_Time")
                    .HasColumnType("timestamp");

                entity.Property(e => e.Stats)
                    .HasColumnName("stats")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.VerificationNumber)
                    .HasColumnName("Verification_Number")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.MobileScaleProductPurchase)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("mobile_scale_product_purchase_ibfk_1");
            });

            modelBuilder.Entity<MobileScaleProductPurchaseTemp>(entity =>
            {
                entity.ToTable("mobile_scale_product_purchase_temp");

                entity.HasIndex(e => e.VerificationNumber)
                    .HasName("Verification_Number")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Basket).HasColumnType("int(11)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("Customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Densities).HasColumnType("decimal(10,2)");

                entity.Property(e => e.DeviceId)
                    .HasColumnName("Device_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Kg).HasColumnType("decimal(10,2)");

                entity.Property(e => e.Litre).HasColumnType("decimal(10,2)");

                entity.Property(e => e.MilkingTime)
                    .HasColumnName("Milking_Time")
                    .HasColumnType("timestamp");

                entity.Property(e => e.MobileScaleCustomerId)
                    .HasColumnName("Mobile_Scale_Customer_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MobileScaleProductPurchaseId)
                    .HasColumnName("mobile_scale_product_purchase_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MobileScaleUserId)
                    .HasColumnName("mobile_scale_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PickupTime)
                    .HasColumnName("Pickup_Time")
                    .HasColumnType("timestamp");

                entity.Property(e => e.Stats)
                    .HasColumnName("stats")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.VerificationNumber)
                    .HasColumnName("Verification_Number")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<MobileScaleSync>(entity =>
            {
                entity.ToTable("mobile_scale_sync");

                entity.HasIndex(e => e.MobileScaleDeviceId)
                    .HasName("mobile_scale_device");

                entity.HasIndex(e => e.SyncType)
                    .HasName("sync_type");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.LastSyncId)
                    .HasColumnName("last_sync_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MobileScaleDeviceId)
                    .HasColumnName("mobile_scale_device_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Progress)
                    .HasColumnName("progress")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.SyncType)
                    .HasColumnName("sync_type")
                    .HasColumnType("tinyint(3)");

                entity.HasOne(d => d.MobileScaleDevice)
                    .WithMany(p => p.MobileScaleSync)
                    .HasForeignKey(d => d.MobileScaleDeviceId)
                    .HasConstraintName("mobile_scale_sync_ibfk_1");
            });

            modelBuilder.Entity<Orders>(entity =>
            {
                entity.ToTable("orders");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("ndx_orders_customer_id");

                entity.HasIndex(e => e.Stats)
                    .HasName("ndx_orders_stats");

                entity.HasIndex(e => e.Type)
                    .HasName("ndx_orders_type");

                entity.HasIndex(e => e.UserId)
                    .HasName("ndx_orders_user_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InsDateTime)
                    .HasColumnName("ins_date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.InsDatetime1)
                    .HasColumnName("ins_datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.OrderAmount)
                    .HasColumnName("order_amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.OrderDateTime)
                    .HasColumnName("order_date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.OrderNumber)
                    .HasColumnName("order_number")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.OrderSerial)
                    .HasColumnName("order_serial")
                    .HasColumnType("varchar(5)");

                entity.Property(e => e.OrdersProductsBasket)
                    .HasColumnName("orders_products_basket")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Stats)
                    .HasColumnName("stats")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("orders_ibfk_1");
            });

            modelBuilder.Entity<OrdersProducts>(entity =>
            {
                entity.ToTable("orders_products");

                entity.HasIndex(e => e.Basket)
                    .HasName("ndx_ordeR_products_basket");

                entity.HasIndex(e => e.ProductId)
                    .HasName("ndx_order_products_product_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ColorId)
                    .HasColumnName("color_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DiscountRate1)
                    .HasColumnName("discount_rate1")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.DiscountRate2)
                    .HasColumnName("discount_rate2")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.DiscountRate3)
                    .HasColumnName("discount_rate3")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.DiscountRate4)
                    .HasColumnName("discount_rate4")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.DiscountRate5)
                    .HasColumnName("discount_rate5")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.DiscountUnitPrice)
                    .HasColumnName("discount_unit_price")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.DiscountUnitPriceAmount)
                    .HasColumnName("discount_unit_price_amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ProductCode)
                    .HasColumnName("product_code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(140)");

                entity.Property(e => e.ProductType)
                    .HasColumnName("product_type")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.StorageId)
                    .HasColumnName("storage_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TaxAmount)
                    .HasColumnName("tax_amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.TaxRate)
                    .HasColumnName("tax_rate")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.TotalDiscontAmount)
                    .HasColumnName("total_discont_amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.UnitName)
                    .HasColumnName("unit_name")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.UnitPrice)
                    .HasColumnName("unit_price")
                    .HasColumnType("decimal(10,5)");

                entity.Property(e => e.UnitPriceTax)
                    .HasColumnName("unit_price_tax")
                    .HasColumnType("decimal(10,2)");
            });

            modelBuilder.Entity<OrdersProductsSub>(entity =>
            {
                entity.ToTable("orders_products_sub");

                entity.HasIndex(e => e.Barcode)
                    .HasName("ndx_orders_products_sub_barcode");

                entity.HasIndex(e => e.OrderProductsBasketId)
                    .HasName("ndx_orders_products_sub_order_products_basket_id");

                entity.HasIndex(e => e.ProductId)
                    .HasName("ndx_orders_products_sub_product_id");

                entity.HasIndex(e => e.ProductName)
                    .HasName("ndx_orders_products_sub_product_name");

                entity.HasIndex(e => e.Qty)
                    .HasName("ndx_orders_products_sub_basket");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.GroupQty)
                    .HasColumnName("group_qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.OrderProductsBasketId)
                    .HasColumnName("order_products_basket_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductCode)
                    .HasColumnName("product_code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(140)");

                entity.Property(e => e.ProductType)
                    .HasColumnName("product_type")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.UnitName)
                    .HasColumnName("unit_name")
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<OrdersProductsSubMain>(entity =>
            {
                entity.ToTable("orders_products_sub_main");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.GroupQty)
                    .HasColumnName("group_qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ProductCode)
                    .HasColumnName("product_code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(140)");

                entity.Property(e => e.ProductType)
                    .HasColumnName("product_type")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.UnitName)
                    .HasColumnName("unit_name")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.WaybillProductsBasketId)
                    .HasColumnName("waybill_products_basket_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Parameters>(entity =>
            {
                entity.ToTable("parameters");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AutoPrintTicketOnFrmSpeedRetailSales)
                    .HasColumnName("Auto_Print_Ticket_On_Frm_Speed_Retail_Sales")
                    .HasColumnType("varchar(10)")
                    .HasDefaultValueSql("'False'");

                entity.Property(e => e.ChaneUseOnFrmSpeedRetailSales)
                    .HasColumnName("Chane_Use_On_Frm_Speed_Retail_Sales")
                    .HasColumnType("varchar(10)")
                    .HasDefaultValueSql("'False'");

                entity.Property(e => e.CustomerCode)
                    .HasColumnName("customer_code")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ElevatorMobileSendSms)
                    .HasColumnName("Elevator_Mobile_Send_Sms")
                    .HasColumnType("varchar(20)")
                    .HasDefaultValueSql("'True'");

                entity.Property(e => e.ElevatorSenderTitle)
                    .HasColumnName("Elevator_Sender_Title")
                    .HasColumnType("varchar(11)");

                entity.Property(e => e.FocusColor)
                    .HasColumnName("focus_color")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.FrmCheckCpEntryOptionTime)
                    .HasColumnName("Frm_Check_Cp_Entry_Option_Time")
                    .HasColumnType("varchar(10)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.FrmCheckCpEntryPayWarningOption)
                    .HasColumnName("Frm_Check_Cp_Entry_Pay_Warning_Option")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.FrmCustomerCardAL11)
                    .HasColumnName("Frm_Customer_Card_A_L1_1")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.FrmCustomerCardAL12)
                    .HasColumnName("Frm_Customer_Card_A_L1_2")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.FrmCustomerCardAL1C)
                    .HasColumnName("Frm_Customer_Card_A_L1_C")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FrmCustomerCardAL21)
                    .HasColumnName("Frm_Customer_Card_A_L2_1")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.FrmCustomerCardAL22)
                    .HasColumnName("Frm_Customer_Card_A_L2_2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FrmCustomerCardAL2C)
                    .HasColumnName("Frm_Customer_Card_A_L2_C")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FrmCustomerCardAL31)
                    .HasColumnName("Frm_Customer_Card_A_L3_1")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FrmCustomerCardAL32)
                    .HasColumnName("Frm_Customer_Card_A_L3_2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FrmCustomerCardAL3C)
                    .HasColumnName("Frm_Customer_Card_A_L3_C")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FrmCustomerCardBL11)
                    .HasColumnName("Frm_Customer_Card_B_L1_1")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.FrmCustomerCardBL12)
                    .HasColumnName("Frm_Customer_Card_B_L1_2")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.FrmCustomerCardBL1C)
                    .HasColumnName("Frm_Customer_Card_B_L1_C")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FrmCustomerCardBL21)
                    .HasColumnName("Frm_Customer_Card_B_L2_1")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.FrmCustomerCardBL22)
                    .HasColumnName("Frm_Customer_Card_B_L2_2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FrmCustomerCardBL2C)
                    .HasColumnName("Frm_Customer_Card_B_L2_C")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FrmCustomerCardBL31)
                    .HasColumnName("Frm_Customer_Card_B_L3_1")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FrmCustomerCardBL32)
                    .HasColumnName("Frm_Customer_Card_B_L3_2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FrmCustomerCardBL3C)
                    .HasColumnName("Frm_Customer_Card_B_L3_C")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FrmCustomerCardOrderByCode)
                    .HasColumnName("Frm_Customer_Card_Order_ByCode")
                    .HasColumnType("varchar(25)");

                entity.Property(e => e.FrmCustomerCurrentSendSmsOnCurrIn)
                    .HasColumnName("Frm_Customer_Current_Send_Sms_On_Curr_In")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.FrmCustomerCurrentSendSmsOnCurrOut)
                    .HasColumnName("Frm_Customer_Current_Send_Sms_On_Curr_Out")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.FrmFarmDayPregnant)
                    .HasColumnName("Frm_Farm_Day_Pregnant")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.FrmInvPreviewCP)
                    .HasColumnName("Frm_Inv_Preview_C_P")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.FrmInvoiceCargoPayLimit)
                    .HasColumnName("Frm_Invoice_Cargo_Pay_Limit")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.FrmInvoiceCheckedReceipt)
                    .HasColumnName("Frm_Invoice_checked_receipt")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.FrmInvoiceCustomerList)
                    .HasColumnName("Frm_Invoice_Customer_List")
                    .HasColumnType("varchar(10)")
                    .HasDefaultValueSql("'False'");

                entity.Property(e => e.FrmInvoiceDefaultTaxRate)
                    .HasColumnName("Frm_Invoice_Default_Tax_Rate")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.FrmInvoiceInvoiceRowCount)
                    .HasColumnName("Frm_Invoice_Invoice_Row_Count")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.FrmInvoicePersonelPriceDiscountRate)
                    .HasColumnName("Frm_Invoice_Personel_Price_Discount_Rate")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.FrmInvoicePreviewInvoice)
                    .HasColumnName("Frm_Invoice_Preview_Invoice")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.FrmInvoicePriceDiaolog)
                    .HasColumnName("Frm_Invoice_Price_Diaolog")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FrmInvoiceProductGridAutoProductInsert)
                    .HasColumnName("Frm_Invoice_Product_Grid_Auto_Product_Insert")
                    .HasColumnType("varchar(25)");

                entity.Property(e => e.FrmInvoiceProductList)
                    .HasColumnName("Frm_Invoice_Product_List")
                    .HasColumnType("varchar(10)")
                    .HasDefaultValueSql("'False'");

                entity.Property(e => e.FrmInvoiceQtyDialog)
                    .HasColumnName("Frm_Invoice_Qty_Dialog")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.FrmInvoiceSelectPayMethod)
                    .HasColumnName("Frm_Invoice_Select_Pay_Method")
                    .HasColumnType("varchar(25)");

                entity.Property(e => e.FrmInvoiceShippingDocument)
                    .HasColumnName("Frm_Invoice_Shipping_Document")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.FrmInvoiceSpeedInsert)
                    .HasColumnName("Frm_Invoice_Speed_Insert")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.FrmInvoiceTraceShipment)
                    .HasColumnName("Frm_Invoice_Trace_Shipment")
                    .HasColumnType("varchar(20)")
                    .HasDefaultValueSql("'False'");

                entity.Property(e => e.FrmInvoiceTraceStockCountingViaShipment)
                    .HasColumnName("Frm_Invoice_Trace_Stock_Counting_Via_Shipment")
                    .HasColumnType("varchar(25)")
                    .HasDefaultValueSql("'False'");

                entity.Property(e => e.FrmInvoiceUnitPriceIsRetailPrice)
                    .HasColumnName("Frm_Invoice_Unit_Price_Is_Retail_Price")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.FrmInvoiceUpdatePersonelPrice)
                    .HasColumnName("Frm_Invoice_Update_Personel_Price")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.FrmInvoiceUpdatePurchasePrice)
                    .HasColumnName("Frm_Invoice_Update_Purchase_Price")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.FrmMainBackgroundImage)
                    .HasColumnName("Frm_Main_Background_Image")
                    .HasColumnType("text");

                entity.Property(e => e.FrmProductAutoCalcRetailPrice)
                    .HasColumnName("Frm_Product_Auto_Calc_Retail_price")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.FrmProductCardOrderByProductName)
                    .HasColumnName("Frm_ProductCard_Order_By_Product_Name")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.FrmProductCardOrderCode)
                    .HasColumnName("Frm_ProductCard_Order_Code")
                    .HasColumnType("varchar(25)");

                entity.Property(e => e.FrmProductCardPieceTrace)
                    .HasColumnName("Frm_Product_Card_Piece_Trace")
                    .HasColumnType("varchar(25)");

                entity.Property(e => e.FrmProductCardSerialsMonitor)
                    .HasColumnName("Frm_Product_Card_Serials_Monitor")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.FrmProductEan13)
                    .HasColumnName("Frm_Product_Ean13")
                    .HasColumnType("varchar(10)")
                    .HasDefaultValueSql("'False'");

                entity.Property(e => e.FrmProductMainStockModule)
                    .HasColumnName("Frm_Product_Main_Stock_Module")
                    .HasColumnType("varchar(10)")
                    .HasDefaultValueSql("'False'");

                entity.Property(e => e.FrmProductShelfTrack)
                    .HasColumnName("Frm_Product_Shelf_Track")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.FrmProductStationeryModule)
                    .HasColumnName("Frm_Product_Stationery_Module")
                    .HasColumnType("varchar(10)")
                    .HasDefaultValueSql("'False'");

                entity.Property(e => e.FrmProductStoreModule)
                    .HasColumnName("Frm_Product_Store_Module")
                    .HasColumnType("varchar(10)")
                    .HasDefaultValueSql("'False'");

                entity.Property(e => e.FrmSpeedRetailSalesAskProductPriceChange)
                    .HasColumnName("Frm_Speed_Retail_Sales_Ask_Product_Price_Change")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.FrmSpeedRetailSalesPrintDialog)
                    .HasColumnName("Frm_Speed_Retail_Sales_Print_Dialog")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.FrmSpeedSalesIfBarcodeNotOnlySound)
                    .HasColumnName("Frm_Speed_Sales_If_Barcode_Not_Only_Sound")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.FrmStorafeTransferPrintDialog)
                    .HasColumnName("Frm_Storafe_Transfer_Print_Dialog")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.ScaleUseOnFrmSpeedRetailSales)
                    .HasColumnName("Scale_Use_On_Frm_Speed_Retail_Sales")
                    .HasColumnType("varchar(6)")
                    .HasDefaultValueSql("'False'");

                entity.Property(e => e.UpdatePricesAlwaysOnFrmSpeedRetailSales)
                    .HasColumnName("Update_Prices_Always_On_Frm_Speed_Retail_Sales")
                    .HasColumnType("varchar(10)")
                    .HasDefaultValueSql("'False'");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.VisibleProductGridOnFrmSpeedRetailSales)
                    .HasColumnName("Visible_Product_Grid_On_Frm_Speed_Retail_Sales")
                    .HasColumnType("varchar(10)")
                    .HasDefaultValueSql("'False'");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Parameters)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("parameters_ibfk_1");
            });

            modelBuilder.Entity<PersonelCards>(entity =>
            {
                entity.ToTable("personel_cards");

                entity.HasIndex(e => e.BranchId)
                    .HasName("branch_id");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.BranchDefination)
                    .HasColumnName("branch_defination")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.BranchId)
                    .HasColumnName("branch_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CloseExp)
                    .HasColumnName("close_exp")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Department)
                    .HasColumnName("department")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.DrivingLicense)
                    .HasColumnName("driving_license")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.ExitDate)
                    .HasColumnName("exit_date")
                    .HasColumnType("date");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.GroupName)
                    .HasColumnName("group_name")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.GroupOrder)
                    .HasColumnName("group_order")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ManagamentUserName)
                    .HasColumnName("managament_user_name")
                    .HasColumnType("varchar(256)");

                entity.Property(e => e.ManagementId)
                    .HasColumnName("management_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PersonelCode)
                    .HasColumnName("personel_code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Picture).HasColumnName("picture");

                entity.Property(e => e.ReferenceCustomerAccountTitle)
                    .HasColumnName("reference_customer_account_title")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ReferenceCustomerId)
                    .HasColumnName("reference_customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("date");

                entity.Property(e => e.Task)
                    .HasColumnName("task")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Visible)
                    .HasColumnName("visible")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.HasOne(d => d.BranchNavigation)
                    .WithMany(p => p.PersonelCards)
                    .HasForeignKey(d => d.BranchId)
                    .HasConstraintName("personel_cards_ibfk_2");

                entity.HasOne(d => d.Customer)
                    .WithOne(p => p.PersonelCards)
                    .HasForeignKey<PersonelCards>(d => d.CustomerId)
                    .HasConstraintName("personel_cards_ibfk_1");
            });

            modelBuilder.Entity<PersonelCardsFeeInformation>(entity =>
            {
                entity.ToTable("personel_cards_fee_information");

                entity.HasIndex(e => e.PersonelCardsId)
                    .HasName("personel_cards_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AddAutoPersonelFee)
                    .HasColumnName("add_auto_personel_fee")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.AgreementExp)
                    .HasColumnName("agreement_exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.CardBarcode)
                    .HasColumnName("card_barcode")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.CardExp)
                    .HasColumnName("card_exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.CardNameSurname)
                    .HasColumnName("card_name_surname")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DailyAmount)
                    .HasColumnName("daily_amount")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.DailyWorkingTime)
                    .HasColumnName("daily_working_time")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'8.00'");

                entity.Property(e => e.DateOfDeparture)
                    .HasColumnName("date_of_departure")
                    .HasColumnType("date");

                entity.Property(e => e.DefaultCustomerBankId)
                    .HasColumnName("default_customer_bank_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.HoursAmount)
                    .HasColumnName("hours_amount")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.HoursOfOvertime)
                    .HasColumnName("hours_of_overtime")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.MinuteAmount)
                    .HasColumnName("minute_amount")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.MonthlyAmount)
                    .HasColumnName("monthly_amount")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.PaymentExp)
                    .HasColumnName("payment_exp")
                    .HasColumnType("varchar(500)");

                entity.Property(e => e.PaymentMethod)
                    .HasColumnName("payment_method")
                    .HasColumnType("int(10)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PaymentPeriod)
                    .HasColumnName("payment_period")
                    .HasColumnType("int(10)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PersonelAccountDay)
                    .HasColumnName("personel_account_day")
                    .HasColumnType("varchar(40)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PersonelCardsId)
                    .HasColumnName("personel_cards_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PersonelTask)
                    .HasColumnName("personel_task")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.RefaranceNameSurname)
                    .HasColumnName("refarance_name_surname")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.RefarenceCustomerId)
                    .HasColumnName("refarence_customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SecondAmount)
                    .HasColumnName("second_amount")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.SinceWork)
                    .HasColumnName("since_work")
                    .HasColumnType("date");

                entity.Property(e => e.WeeklyAmount)
                    .HasColumnName("weekly_amount")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.HasOne(d => d.PersonelCards)
                    .WithMany(p => p.PersonelCardsFeeInformation)
                    .HasForeignKey(d => d.PersonelCardsId)
                    .HasConstraintName("personel_cards_fee_information_ibfk_1");
            });

            modelBuilder.Entity<PersonelCardsIdentityInformation>(entity =>
            {
                entity.ToTable("personel_cards_identity_information");

                entity.HasIndex(e => e.PersonelCardsId)
                    .HasName("personel_cards_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BloodGroup)
                    .HasColumnName("blood_group")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.DateOfBirth)
                    .HasColumnName("date_of_birth")
                    .HasColumnType("date");

                entity.Property(e => e.DateOfIssue)
                    .HasColumnName("date_of_issue")
                    .HasColumnType("date");

                entity.Property(e => e.DocumentNo)
                    .HasColumnName("document_no")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.DriversLicenseClass)
                    .HasColumnName("Drivers_license_class")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.DrivingLicenseSerialAndNo)
                    .HasColumnName("Driving_license_serial_and_no")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.EducationalKnowledge)
                    .HasColumnName("Educational_knowledge")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.EnrollmentNumber)
                    .HasColumnName("Enrollment_number")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.FamilySequenceNo)
                    .HasColumnName("Family_sequence_no")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.FatherName)
                    .HasColumnName("Father_name")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.MaritalStatus)
                    .HasColumnName("Marital_status")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.MilitaryStatus)
                    .HasColumnName("Military_status")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.MotherName)
                    .HasColumnName("mother_name")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.NeighborhoodVillage)
                    .HasColumnName("Neighborhood_village")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.NumberOfChildren)
                    .HasColumnName("number_of_children")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.PersonelCardsId)
                    .HasColumnName("personel_cards_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PlaceOfBirth)
                    .HasColumnName("Place_of_birth")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.PlaceOfIssue)
                    .HasColumnName("place_of_issue")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Province)
                    .HasColumnName("province")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Religious)
                    .HasColumnName("religious")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.SequenceNo)
                    .HasColumnName("Sequence no")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SgkNumber)
                    .HasColumnName("Sgk_number")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.TcIdentificationNumber)
                    .HasColumnName("TC_Identification_number")
                    .HasColumnType("bigint(11)");

                entity.Property(e => e.TheReasonForTheIssuanceOfAWallet)
                    .HasColumnName("The_reason_for_the_issuance_of_a_wallet")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Town)
                    .HasColumnName("town")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Vol).HasColumnType("varchar(10)");

                entity.Property(e => e.WalletIssueDate)
                    .HasColumnName("Wallet_issue_date")
                    .HasColumnType("date");

                entity.Property(e => e.WalletLocation)
                    .HasColumnName("Wallet_location")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.WalletNo)
                    .HasColumnName("Wallet_no")
                    .HasColumnType("int(7)");

                entity.Property(e => e.WalletSerialNumber)
                    .HasColumnName("Wallet_serial_number")
                    .HasColumnType("varchar(10)");

                entity.HasOne(d => d.PersonelCards)
                    .WithMany(p => p.PersonelCardsIdentityInformation)
                    .HasForeignKey(d => d.PersonelCardsId)
                    .HasConstraintName("personel_cards_identity_information_ibfk_1");
            });

            modelBuilder.Entity<PersonelCardsStartingEnding>(entity =>
            {
                entity.ToTable("personel_cards_starting_ending");

                entity.HasIndex(e => e.DeletionUserId)
                    .HasName("deletion_user_id");

                entity.HasIndex(e => e.PersonelCardsId)
                    .HasName("personel_cards_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ActivityDateTime)
                    .HasColumnName("activity_date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.InsDateTime)
                    .HasColumnName("ins_date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.PersonelCardsId)
                    .HasColumnName("personel_cards_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.HasOne(d => d.DeletionUser)
                    .WithMany(p => p.PersonelCardsStartingEnding)
                    .HasForeignKey(d => d.DeletionUserId)
                    .HasConstraintName("personel_cards_starting_ending_ibfk_2");

                entity.HasOne(d => d.PersonelCards)
                    .WithMany(p => p.PersonelCardsStartingEnding)
                    .HasForeignKey(d => d.PersonelCardsId)
                    .HasConstraintName("personel_cards_starting_ending_ibfk_1");
            });

            modelBuilder.Entity<PersonelHolidayParameters>(entity =>
            {
                entity.ToTable("personel_holiday_parameters");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.InsDateTime)
                    .HasColumnName("ins_date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<PersonelMeterialDebit>(entity =>
            {
                entity.ToTable("personel_meterial_debit");

                entity.HasIndex(e => e.PersonelCardsId)
                    .HasName("personel_cards_id");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Explanation)
                    .HasColumnName("explanation")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.InsDateTime)
                    .HasColumnName("ins_date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PersonelCardsId)
                    .HasColumnName("personel_cards_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.PersonelCards)
                    .WithMany(p => p.PersonelMeterialDebit)
                    .HasForeignKey(d => d.PersonelCardsId)
                    .HasConstraintName("personel_meterial_debit_ibfk_2");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.PersonelMeterialDebit)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("personel_meterial_debit_ibfk_1");
            });

            modelBuilder.Entity<PersonelPermissionCurrent>(entity =>
            {
                entity.ToTable("personel_permission_current");

                entity.HasIndex(e => e.PersonelCardsId)
                    .HasName("personel_cards_id");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.InsTime)
                    .HasColumnName("ins_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PermissionDate)
                    .HasColumnName("permission_date")
                    .HasColumnType("date");

                entity.Property(e => e.PermissionType)
                    .HasColumnName("permission_type")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PersonelCardsId)
                    .HasColumnName("personel_cards_id")
                    .HasColumnType("int(255)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.PersonelCards)
                    .WithMany(p => p.PersonelPermissionCurrent)
                    .HasForeignKey(d => d.PersonelCardsId)
                    .HasConstraintName("personel_permission_current_ibfk_1");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.PersonelPermissionCurrent)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("personel_permission_current_ibfk_2");
            });

            modelBuilder.Entity<PersonelPermissionDayParameters>(entity =>
            {
                entity.ToTable("personel_permission_day_parameters");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Friday)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Monday)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Saturday)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Sunday)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Thursday)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Tuesday)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Wednesday)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");
            });

            modelBuilder.Entity<PersonelPermissionParameters>(entity =>
            {
                entity.ToTable("personel_permission_parameters");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Day)
                    .HasColumnName("day")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Year)
                    .HasColumnName("year")
                    .HasColumnType("tinyint(3)");
            });

            modelBuilder.Entity<Pos>(entity =>
            {
                entity.ToTable("pos");

                entity.HasIndex(e => e.BankAccountId)
                    .HasName("bank_account_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BankAccountId)
                    .HasColumnName("bank_account_id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.PosAccountHolder)
                    .HasColumnName("pos_account_holder")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.PosBankName)
                    .HasColumnName("pos_bank_name")
                    .HasColumnType("varchar(60)");

                entity.Property(e => e.PosBlockTime)
                    .HasColumnName("pos_block_time")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PosCode)
                    .HasColumnName("pos_code")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.PosDepartment)
                    .HasColumnName("pos_department")
                    .HasColumnType("varchar(60)");

                entity.Property(e => e.PosDepartmentId)
                    .HasColumnName("pos_department_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PosExp)
                    .HasColumnName("pos_exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.PosName)
                    .HasColumnName("pos_name")
                    .HasColumnType("varchar(255)");

                entity.HasOne(d => d.BankAccount)
                    .WithMany(p => p.Pos)
                    .HasForeignKey(d => d.BankAccountId)
                    .HasConstraintName("pos_ibfk_1");
            });

            modelBuilder.Entity<PosDetail>(entity =>
            {
                entity.ToTable("pos_detail");

                entity.HasIndex(e => e.PosId)
                    .HasName("pos_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BankCommission)
                    .HasColumnName("bank_commission")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CustomerCommission)
                    .HasColumnName("customer_commission")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.NumberOfInstallments)
                    .HasColumnName("number_of_installments")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PosId)
                    .HasColumnName("pos_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Pos)
                    .WithMany(p => p.PosDetail)
                    .HasForeignKey(d => d.PosId)
                    .HasConstraintName("pos_detail_ibfk_1");
            });

            modelBuilder.Entity<PosIn>(entity =>
            {
                entity.ToTable("pos_in");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.HasIndex(e => e.Deletion)
                    .HasName("deletion");

                entity.HasIndex(e => e.DetailId)
                    .HasName("detail_id");

                entity.HasIndex(e => e.PosId)
                    .HasName("pos_id");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.HasIndex(e => new { e.PosId, e.CustomerId })
                    .HasName("pos_id_2");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Datetime)
                    .HasColumnName("datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DetailId)
                    .HasColumnName("detail_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(400)");

                entity.Property(e => e.InstallmentNumber)
                    .HasColumnName("installment_number")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.NetCurrency)
                    .HasColumnName("net_currency")
                    .HasColumnType("decimal(10,4)");

                entity.Property(e => e.PosId)
                    .HasColumnName("pos_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Type1)
                    .HasColumnName("type_")
                    .HasColumnType("varchar(2)")
                    .HasDefaultValueSql("'in'");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Pos)
                    .WithMany(p => p.PosIn)
                    .HasForeignKey(d => d.PosId)
                    .HasConstraintName("pos_in_ibfk_1");
            });

            modelBuilder.Entity<PosOut>(entity =>
            {
                entity.ToTable("pos_out");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.HasIndex(e => e.Deletion)
                    .HasName("deletion");

                entity.HasIndex(e => e.PosId)
                    .HasName("pos_id");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.HasIndex(e => new { e.PosId, e.CustomerId })
                    .HasName("pos_id_2");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Datetime)
                    .HasColumnName("datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(400)");

                entity.Property(e => e.PosId)
                    .HasColumnName("pos_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Type1)
                    .HasColumnName("type_")
                    .HasColumnType("varchar(2)")
                    .HasDefaultValueSql("'ou'");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Pos)
                    .WithMany(p => p.PosOut)
                    .HasForeignKey(d => d.PosId)
                    .HasConstraintName("pos_out_ibfk_1");
            });

            modelBuilder.Entity<PremiumTracking>(entity =>
            {
                entity.ToTable("premium_tracking");

                entity.Property(e => e.Id).HasColumnType("int(11)");
            });

            modelBuilder.Entity<PremiumTrackingTemp>(entity =>
            {
                entity.ToTable("premium_tracking_temp");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ScanTime)
                    .HasColumnName("scan_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ScanUser)
                    .HasColumnName("scan_user")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Pricing>(entity =>
            {
                entity.ToTable("pricing");

                entity.HasIndex(e => e.PriceCode)
                    .HasName("price_code");

                entity.HasIndex(e => e.PriceDescription)
                    .HasName("price_description");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Discount)
                    .HasColumnName("discount")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.Exp1)
                    .HasColumnName("exp1")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Exp3)
                    .HasColumnName("exp3")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Increment)
                    .HasColumnName("increment")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.PriceCode)
                    .HasColumnName("price_code")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.PriceDescription)
                    .HasColumnName("price_description")
                    .HasColumnType("varchar(150)");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("product");

                entity.HasIndex(e => e.Barcode)
                    .HasName("barcode");

                entity.HasIndex(e => e.Code)
                    .HasName("code");

                entity.HasIndex(e => e.Group1)
                    .HasName("group1");

                entity.HasIndex(e => e.Group2)
                    .HasName("group2");

                entity.HasIndex(e => e.MainProductId)
                    .HasName("product_ndx_main_product_id");

                entity.HasIndex(e => e.Mark)
                    .HasName("mark");

                entity.HasIndex(e => e.Name)
                    .HasName("name");

                entity.HasIndex(e => e.Visible)
                    .HasName("visible");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ActiveScale)
                    .HasColumnName("active_scale")
                    .HasColumnType("varchar(7)");

                entity.Property(e => e.Author)
                    .HasColumnName("author")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.BarcodeScale)
                    .HasColumnName("barcode_scale")
                    .HasColumnType("varchar(7)");

                entity.Property(e => e.BookType)
                    .HasColumnName("book_type")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.CurrenySource)
                    .HasColumnName("curreny_source")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DoublePortion)
                    .HasColumnName("double_portion")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.DoublePortionOrder)
                    .HasColumnName("double_portion_order")
                    .HasColumnType("varchar(20)")
                    .HasDefaultValueSql("'False'");

                entity.Property(e => e.Ean).HasColumnType("tinyint(3)");

                entity.Property(e => e.FixedDiscountRate)
                    .HasColumnName("fixed_discount_rate")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ForceSerialTrack)
                    .HasColumnName("force_serial_track")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.GrossSalesPrice)
                    .HasColumnName("gross_sales_price")
                    .HasColumnType("decimal(10,5)");

                entity.Property(e => e.GrossSalesPriceTax)
                    .HasColumnName("gross_sales_price_tax")
                    .HasColumnType("decimal(10,5)");

                entity.Property(e => e.Group)
                    .HasColumnName("group")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Group1)
                    .HasColumnName("group1")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Group2)
                    .HasColumnName("group2")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Group3)
                    .HasColumnName("group3")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Group4)
                    .HasColumnName("group4")
                    .HasColumnType("int(11)");

                entity.Property(e => e.GroupQty)
                    .HasColumnName("group_qty")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.HalfPortion)
                    .HasColumnName("half_portion")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.HalfPortionOrder)
                    .HasColumnName("half_portion_order")
                    .HasColumnType("varchar(20)")
                    .HasDefaultValueSql("'False'");

                entity.Property(e => e.LaborRate)
                    .HasColumnName("labor_rate")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.MainProductId)
                    .HasColumnName("main_product_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.MainProductIdTemp)
                    .HasColumnName("main_product_id_temp")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Mark)
                    .HasColumnName("mark")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.MaxOrder)
                    .HasColumnName("max_order")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MaxStock)
                    .HasColumnName("max_stock")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.MinOrder)
                    .HasColumnName("min_order")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MinStock)
                    .HasColumnName("min_stock")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.MobileVisible)
                    .HasColumnName("mobile_visible")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.OneHalfPortion)
                    .HasColumnName("one_half_portion")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.OneHalfPortionOrder)
                    .HasColumnName("one_half_portion_order")
                    .HasColumnType("varchar(20)")
                    .HasDefaultValueSql("'False'");

                entity.Property(e => e.OnePortion)
                    .HasColumnName("one_portion")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.OnePortionOrder)
                    .HasColumnName("one_portion_order")
                    .HasColumnType("varchar(20)")
                    .HasDefaultValueSql("'False'");

                entity.Property(e => e.PersonalPrice)
                    .HasColumnName("personal_price")
                    .HasColumnType("decimal(10,5)");

                entity.Property(e => e.PersonalPriceTax)
                    .HasColumnName("personal_price_tax")
                    .HasColumnType("decimal(10,5)");

                entity.Property(e => e.Picture)
                    .HasColumnName("picture")
                    .HasColumnType("varchar(1000)");

                entity.Property(e => e.PremiumAmount)
                    .HasColumnName("premium_amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.PricePrivileged)
                    .HasColumnName("price_privileged")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PrivateCode)
                    .HasColumnName("private_code")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode10)
                    .HasColumnName("private_code10")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode2)
                    .HasColumnName("private_code2")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode3)
                    .HasColumnName("private_code3")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode4)
                    .HasColumnName("private_code4")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode5)
                    .HasColumnName("private_code5")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode6)
                    .HasColumnName("private_code6")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode7)
                    .HasColumnName("private_code7")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode8)
                    .HasColumnName("private_code8")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.PrivateCode9)
                    .HasColumnName("private_code9")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.ProducerAccountTitle)
                    .HasColumnName("producer_account_title")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.ProducerId)
                    .HasColumnName("producer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProfitRate)
                    .HasColumnName("profit_rate")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.PurchasePrice)
                    .HasColumnName("purchase_price")
                    .HasColumnType("decimal(10,5)");

                entity.Property(e => e.PurchasePriceTax)
                    .HasColumnName("purchase_price_tax")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.PurchaseTaxPercent)
                    .HasColumnName("purchase_tax_percent")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.RetailPrice)
                    .HasColumnName("retail_price")
                    .HasColumnType("decimal(10,5)");

                entity.Property(e => e.RetailPriceTax)
                    .HasColumnName("retail_price_tax")
                    .HasColumnType("decimal(10,5)");

                entity.Property(e => e.SalesTaxPercent)
                    .HasColumnName("sales_tax_percent")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.SellerCustomerId)
                    .HasColumnName("seller_customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SellerCustomerTitle)
                    .HasColumnName("seller_customer_title")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.ShelfInf)
                    .HasColumnName("shelf_inf")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Size)
                    .HasColumnName("size")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.SyncId)
                    .HasColumnName("sync_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TechnicOrder)
                    .HasColumnName("technic_order")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.TechnicOrder2)
                    .HasColumnName("technic_order_2")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.TechnicOrder3)
                    .HasColumnName("technic_order_3")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.TechnicTitle)
                    .HasColumnName("technic_title")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.TechnicTitle2)
                    .HasColumnName("technic_title_2")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.TechnicTitle3)
                    .HasColumnName("technic_title_3")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Unit)
                    .HasColumnName("unit")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.UnitFactor)
                    .HasColumnName("unit_factor")
                    .HasColumnType("decimal(10,5)")
                    .HasDefaultValueSql("'0.00100'");

                entity.Property(e => e.Variant)
                    .HasColumnName("variant")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Visible)
                    .HasColumnName("visible")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WaiterPercent)
                    .HasColumnName("waiter_percent")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.WarrantyPeriod)
                    .HasColumnName("Warranty_period")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Weight)
                    .HasColumnName("weight")
                    .HasColumnType("decimal(10,4)");

                entity.HasOne(d => d.Group1Navigation)
                    .WithMany(p => p.ProductGroup1Navigation)
                    .HasForeignKey(d => d.Group1)
                    .HasConstraintName("product_ibfk_1");

                entity.HasOne(d => d.Group2Navigation)
                    .WithMany(p => p.ProductGroup2Navigation)
                    .HasForeignKey(d => d.Group2)
                    .HasConstraintName("product_ibfk_2");
            });

            modelBuilder.Entity<Product2>(entity =>
            {
                entity.ToTable("product2");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.CriticalLevel)
                    .HasColumnName("critical_level")
                    .HasColumnType("varchar(11)");

                entity.Property(e => e.Explanation)
                    .HasColumnName("explanation")
                    .HasColumnType("varchar(1000)");

                entity.Property(e => e.Mark)
                    .HasColumnName("mark")
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.MaxDiscount)
                    .HasColumnName("max_discount")
                    .HasColumnType("varchar(5)");

                entity.Property(e => e.MaxLevel)
                    .HasColumnName("max_level")
                    .HasColumnType("varchar(11)");

                entity.Property(e => e.PackagingStatus)
                    .HasColumnName("packaging_status")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(12,2)");

                entity.Property(e => e.PriceTaxInc)
                    .HasColumnName("price_tax_inc")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ProductCategory)
                    .HasColumnName("product_category")
                    .HasColumnType("varchar(500)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(500)");

                entity.Property(e => e.ProductSubCategories)
                    .HasColumnName("product_sub_categories")
                    .HasColumnType("varchar(600)");

                entity.Property(e => e.ProductType)
                    .HasColumnName("product_type")
                    .HasColumnType("varchar(500)");

                entity.Property(e => e.Unit)
                    .HasColumnName("unit")
                    .HasColumnType("varchar(400)");
            });

            modelBuilder.Entity<ProductColor>(entity =>
            {
                entity.ToTable("product_color");

                entity.HasIndex(e => e.ProductId)
                    .HasName("ndx_product_color_product_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.Color1)
                    .HasColumnName("color1")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.Color2)
                    .HasColumnName("color2")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.Color3)
                    .HasColumnName("color3")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.Color4)
                    .HasColumnName("color4")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.Color5)
                    .HasColumnName("color5")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.ColorName)
                    .HasColumnName("color_name")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.MainProductColorId)
                    .HasColumnName("main_product_color_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MainProductId)
                    .HasColumnName("main_product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<ProductGroups>(entity =>
            {
                entity.ToTable("product_groups");

                entity.HasIndex(e => e.ParentId)
                    .HasName("parent_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.GroupName)
                    .HasColumnName("group_name")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.ParentId)
                    .HasColumnName("parent_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("product_groups_ibfk_1");
            });

            modelBuilder.Entity<ProductionHistory>(entity =>
            {
                entity.ToTable("production_history");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp")
                    ;

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InsDateTime)
                    .HasColumnName("ins_date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductionQty)
                    .HasColumnName("production_qty")
                    .HasColumnType("decimal(10,5)");

                entity.Property(e => e.ProductionRecipeDefines)
                    .HasColumnName("production_recipe_defines")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ProductionRecipeDefinesId)
                    .HasColumnName("production_recipe_defines_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Unit)
                    .HasColumnName("unit")
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<ProductionOrdersTemp>(entity =>
            {
                entity.ToTable("production_orders_temp");

                entity.HasIndex(e => e.ProductionRecipeDefinesId)
                    .HasName("production_recipe_defines_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductionQty)
                    .HasColumnName("production_qty")
                    .HasColumnType("decimal(10,5)");

                entity.Property(e => e.ProductionRecipeDefines)
                    .HasColumnName("production_recipe_defines")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ProductionRecipeDefinesId)
                    .HasColumnName("production_recipe_defines_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Unit)
                    .HasColumnName("unit")
                    .HasColumnType("varchar(20)");

                entity.HasOne(d => d.ProductionRecipeDefinesNavigation)
                    .WithMany(p => p.ProductionOrdersTemp)
                    .HasForeignKey(d => d.ProductionRecipeDefinesId)
                    .HasConstraintName("production_orders_temp_ibfk_1");
            });

            modelBuilder.Entity<ProductionRecipeDefines>(entity =>
            {
                entity.ToTable("production_recipe_defines");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Define)
                    .HasColumnName("define")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(190)");

                entity.Property(e => e.ProductionUnit)
                    .HasColumnName("production_unit")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductionUnitDefine)
                    .HasColumnName("production_unit_define")
                    .HasColumnType("varchar(255)");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductionRecipeDefines)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("production_recipe_defines_ibfk_1");
            });

            modelBuilder.Entity<ProductionRecipeFormula>(entity =>
            {
                entity.ToTable("production_recipe_formula");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.HasIndex(e => e.ProductionRecipeDefinesId)
                    .HasName("production_recipe_defines_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.FormulaQty)
                    .HasColumnName("formula_qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(255)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.ProductionRecipeDefinesId)
                    .HasColumnName("production_recipe_defines_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.WastageRate)
                    .HasColumnName("wastage_rate")
                    .HasColumnType("decimal(10,2)");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductionRecipeFormula)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("production_recipe_formula_ibfk_2");

                entity.HasOne(d => d.ProductionRecipeDefines)
                    .WithMany(p => p.ProductionRecipeFormula)
                    .HasForeignKey(d => d.ProductionRecipeDefinesId)
                    .HasConstraintName("production_recipe_formula_ibfk_3");
            });

            modelBuilder.Entity<ProductNutritionalValues>(entity =>
            {
                entity.ToTable("product_nutritional_values");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Ash)
                    .HasColumnName("ash")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CrudeFiber)
                    .HasColumnName("crude_fiber")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CrudeOil)
                    .HasColumnName("crude_oil")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CrudeProtein)
                    .HasColumnName("crude_protein")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.DryMatter)
                    .HasColumnName("dry_matter")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Hcl)
                    .HasColumnName("hcl")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.MetabolicEnergy)
                    .HasColumnName("metabolic_energy")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductNutritionalValues)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("product_nutritional_values_ibfk_1");
            });

            modelBuilder.Entity<ProductPricingChangeHistory>(entity =>
            {
                entity.ToTable("product_pricing_change_history");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DiscountRate)
                    .HasColumnName("discount_rate")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.GrossSalesPrice)
                    .HasColumnName("gross_sales_price")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PurchasePrice)
                    .HasColumnName("purchase_price")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.RaiseRate)
                    .HasColumnName("raise_rate")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.RetailPrice)
                    .HasColumnName("retail_price")
                    .HasColumnType("decimal(10,5)");

                entity.Property(e => e.RetailPriceTax)
                    .HasColumnName("retail_price_tax")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<ProductSerials>(entity =>
            {
                entity.ToTable("product_serials");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BasketId)
                    .HasColumnName("basket_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.InsDate)
                    .HasColumnName("ins_date")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SerialCode)
                    .HasColumnName("serial_code")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.StatusChangeDetailId)
                    .HasColumnName("status_change_detail_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductSerials)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("product_serials_ibfk_1");
            });

            modelBuilder.Entity<ProductSerialsTemp>(entity =>
            {
                entity.ToTable("product_serials_temp");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BasketId)
                    .HasColumnName("basket_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.InsDate)
                    .HasColumnName("ins_date")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SerialCode)
                    .HasColumnName("serial_code")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<ProductSizes>(entity =>
            {
                entity.ToTable("product_sizes");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.GroupName)
                    .HasColumnName("group_name")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.InsDatetime)
                    .HasColumnName("ins_datetime")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Size)
                    .HasColumnName("size")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<ProductTaxList2>(entity =>
            {
                entity.ToTable("product_tax_list2");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.TaxName)
                    .HasColumnName("tax_name")
                    .HasColumnType("varchar(11)");

                entity.Property(e => e.TaxRate)
                    .HasColumnName("tax_rate")
                    .HasColumnType("decimal(10,2)");
            });

            modelBuilder.Entity<ProductUnit>(entity =>
            {
                entity.ToTable("product_unit");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UnitBarcode)
                    .HasColumnName("unit_barcode")
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.UnitExp)
                    .HasColumnName("unit_exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.UnitFactor)
                    .HasColumnName("unit_factor")
                    .HasColumnType("decimal(10,5)");

                entity.Property(e => e.UnitName)
                    .HasColumnName("unit_name")
                    .HasColumnType("varchar(25)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductUnit)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("product_unit_ibfk_1");
            });

            modelBuilder.Entity<ReportDesigns>(entity =>
            {
                entity.ToTable("report_designs");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.File).HasColumnName("file");

                entity.Property(e => e.FileUrl)
                    .HasColumnName("file_url")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Isondatabase)
                    .HasColumnName("isondatabase")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<RestaurantDepartment>(entity =>
            {
                entity.ToTable("restaurant_department");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DepartmentName)
                    .HasColumnName("department_name")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Visible)
                    .HasColumnName("visible")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");
            });

            modelBuilder.Entity<RestaurantDevices>(entity =>
            {
                entity.ToTable("restaurant_devices");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AuthorityDate)
                    .HasColumnName("authority_date")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.MacAdress)
                    .HasColumnName("mac_adress")
                    .HasColumnType("varchar(120)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<RestaurantDiscountRates>(entity =>
            {
                entity.ToTable("restaurant_discount_rates");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Rate)
                    .HasColumnName("rate")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<RestaurantOrder>(entity =>
            {
                entity.ToTable("restaurant_order");

                entity.HasIndex(e => e.RestaurantOrderListBasketId)
                    .HasName("basket")
                    .IsUnique();

                entity.HasIndex(e => e.Status)
                    .HasName("status");

                entity.HasIndex(e => new { e.TableId, e.Status })
                    .HasName("table_id")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BillCollection)
                    .HasColumnName("bill_collection")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.BillUserId)
                    .HasColumnName("bill_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CloseDate)
                    .HasColumnName("close_date")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.CountOfCustomer)
                    .HasColumnName("count_of_customer")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DiscountRate)
                    .HasColumnName("discount_rate")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.IsMovedNewBasket)
                    .HasColumnName("is_moved_new_basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.OpenDate)
                    .HasColumnName("open_date")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PayType)
                    .HasColumnName("pay_type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.RestaurantOrderListBasketId)
                    .HasColumnName("restaurant_order_list_basket_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.TableId)
                    .HasColumnName("table_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Table)
                    .WithMany(p => p.RestaurantOrder)
                    .HasForeignKey(d => d.TableId)
                    .HasConstraintName("restaurant_order_ibfk_2");
            });

            modelBuilder.Entity<RestaurantOrderList>(entity =>
            {
                entity.ToTable("restaurant_order_list");

                entity.HasIndex(e => e.Basket)
                    .HasName("restaurant_order_temp_basket_id");

                entity.HasIndex(e => e.Deletion)
                    .HasName("deletion");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.FlavorId)
                    .HasColumnName("flavor_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InsDate)
                    .HasColumnName("ins_date")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Portion)
                    .HasColumnName("portion")
                    .HasColumnType("decimal(10,1)");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("decimal(12,5)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.RestaurantOrderList)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("restaurant_order_list_ibfk_2");
            });

            modelBuilder.Entity<RestaurantOrderListTempDeletes>(entity =>
            {
                entity.ToTable("restaurant_order_list_temp_deletes");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.DeletionTime)
                    .HasColumnName("deletion_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Group2Id)
                    .HasColumnName("group2_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Group3Id)
                    .HasColumnName("group3_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Portion)
                    .HasColumnName("portion")
                    .HasColumnType("decimal(10,1)");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.RestaurantOrderTempBasketId)
                    .HasColumnName("restaurant_order_temp_basket_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<RestaurantParameters>(entity =>
            {
                entity.ToTable("restaurant_parameters");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CustomerOnTrustGroup)
                    .HasColumnName("customer_on_trust_group")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.OrderProductGroupId)
                    .HasColumnName("order_product_group_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.OrderProductGroupName)
                    .HasColumnName("order_product_group_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ReceiptBottomNote1)
                    .HasColumnName("receipt_bottom_note1")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ReceiptBottomNote2)
                    .HasColumnName("receipt_bottom_note2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ReceiptTopNote1)
                    .HasColumnName("receipt_top_note1")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ReceiptTopNote2)
                    .HasColumnName("receipt_top_note2")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<RestaurantPremiumCalc>(entity =>
            {
                entity.ToTable("restaurant_premium_calc");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.InsDate)
                    .HasColumnName("ins_date")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RestaurantPremiumCalcTempBasketId)
                    .HasColumnName("restaurant_premium_calc_temp_basket_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<RestaurantPremiumCalcTemp>(entity =>
            {
                entity.ToTable("restaurant_premium_calc_temp");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ConditionType)
                    .HasColumnName("condition_type")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("int(11)");

                entity.Property(e => e.RestaurantOrderBasketId)
                    .HasColumnName("restaurant_order_basket_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.RestaurantPremiumDefinesSubConditionsId)
                    .HasColumnName("restaurant_premium_defines_sub_conditions_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TotalPremiumAmount)
                    .HasColumnName("total_premium_amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.UnitPremiumAmount)
                    .HasColumnName("unit_premium_amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<RestaurantPremiumDefines>(entity =>
            {
                entity.ToTable("restaurant_premium_defines");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ConditionBased)
                    .HasColumnName("condition_based")
                    .HasColumnType("varchar(30)")
                    .HasDefaultValueSql("'True'");

                entity.Property(e => e.Group2ConditionId)
                    .HasColumnName("group2_condition_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PreCode)
                    .HasColumnName("pre_code")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.PreDefination)
                    .HasColumnName("pre_defination")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.SalesBased)
                    .HasColumnName("sales_based")
                    .HasColumnType("varchar(30)");
            });

            modelBuilder.Entity<RestaurantPremiumDefinesConditions>(entity =>
            {
                entity.ToTable("restaurant_premium_defines_conditions");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Condition)
                    .HasColumnName("condition")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ConditionDefine)
                    .HasColumnName("condition_define")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.PremiumAmount)
                    .HasColumnName("premium_amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("int(11)");

                entity.Property(e => e.QtyCondition)
                    .HasColumnName("qty_condition")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.RestaurantPremiumDefinesId)
                    .HasColumnName("restaurant_premium_defines_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SourceId)
                    .HasColumnName("source_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SourceType)
                    .HasColumnName("source_type")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.WaiterPercent)
                    .HasColumnName("waiter_percent")
                    .HasColumnType("decimal(10,2)");
            });

            modelBuilder.Entity<RestaurantPremiumDefinesNonConditions>(entity =>
            {
                entity.ToTable("restaurant_premium_defines_non_conditions");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Condition)
                    .HasColumnName("condition")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.ConditionDefine)
                    .HasColumnName("condition_define")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.RestaurantPremiumDefinesConditionsId)
                    .HasColumnName("restaurant_premium_defines_conditions_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SourceId)
                    .HasColumnName("source_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SourceType)
                    .HasColumnName("source_type")
                    .HasColumnType("tinyint(3)");
            });

            modelBuilder.Entity<RestaurantPremiumDefinesSubConditions>(entity =>
            {
                entity.ToTable("restaurant_premium_defines_sub_conditions");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ConditionForm)
                    .HasColumnName("condition_form")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.MinConditions)
                    .HasColumnName("min_conditions")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PoolPercent)
                    .HasColumnName("pool_percent")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.PremiumAmount)
                    .HasColumnName("premium_amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.PremiumDefine)
                    .HasColumnName("premium_define")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.PremiumDefineCode)
                    .HasColumnName("premium_define_Code")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.RestaurantPremiumDefinesId)
                    .HasColumnName("restaurant_premium_defines_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.WaiterPercent)
                    .HasColumnName("waiter_percent")
                    .HasColumnType("decimal(10,2)");
            });

            modelBuilder.Entity<RestaurantPrinterParameters>(entity =>
            {
                entity.ToTable("restaurant_printer_parameters");

                entity.HasIndex(e => e.DepartmentId)
                    .HasName("department_id");

                entity.HasIndex(e => e.ProductGroup3Id)
                    .HasName("product_group3_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.AlternativePrinter)
                    .HasColumnName("alternative_printer")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.DepartmentId)
                    .HasColumnName("department_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DepartmentName)
                    .HasColumnName("department_name")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.PrinterIp)
                    .HasColumnName("printer_ip")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.PrinterName)
                    .HasColumnName("printer_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.PrinterTestTimeOut)
                    .HasColumnName("printer_test_time_out")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductGroup3Id)
                    .HasColumnName("product_group3_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductGroup3Name)
                    .HasColumnName("product_group3_name")
                    .HasColumnType("varchar(255)");

                entity.HasOne(d => d.Department)
                    .WithMany(p => p.RestaurantPrinterParameters)
                    .HasForeignKey(d => d.DepartmentId)
                    .HasConstraintName("restaurant_printer_parameters_ibfk_1");

                entity.HasOne(d => d.ProductGroup3)
                    .WithMany(p => p.RestaurantPrinterParameters)
                    .HasForeignKey(d => d.ProductGroup3Id)
                    .HasConstraintName("restaurant_printer_parameters_ibfk_2");
            });

            modelBuilder.Entity<RestaurantProductFlavorDetails>(entity =>
            {
                entity.ToTable("restaurant_product_flavor_details");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.FlavorDefine)
                    .HasColumnName("flavor_define")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Visible)
                    .HasColumnName("visible")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.RestaurantProductFlavorDetails)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("restaurant_product_flavor_details_ibfk_1");
            });

            modelBuilder.Entity<RestaurantSpeedGroupButtons>(entity =>
            {
                entity.ToTable("restaurant_speed_group_buttons");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ButtonCaption)
                    .HasColumnName("button_caption")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.ButtonName)
                    .HasColumnName("button_name")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Grup2Id)
                    .HasColumnName("grup2_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<RestaurantTables>(entity =>
            {
                entity.ToTable("restaurant_tables");

                entity.HasIndex(e => new { e.TableCode, e.TableNumber })
                    .HasName("table_code");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DepartmanId)
                    .HasColumnName("departman_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TableCapacity)
                    .HasColumnName("table_capacity")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.TableCode)
                    .HasColumnName("table_code")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.TableExample)
                    .HasColumnName("table_example")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.TableNumber)
                    .HasColumnName("table_number")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Visible)
                    .HasColumnName("visible")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");
            });

            modelBuilder.Entity<RestaurantUser>(entity =>
            {
                entity.ToTable("restaurant_user");

                entity.Property(e => e.Id).HasColumnType("int(11)");
            });

            modelBuilder.Entity<Route>(entity =>
            {
                entity.ToTable("route");

                entity.HasIndex(e => e.Definition)
                    .HasName("definition");

                entity.HasIndex(e => e.ResponsibleId)
                    .HasName("responsible_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(25)");

                entity.Property(e => e.Definition)
                    .HasColumnName("definition")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Responsible)
                    .HasColumnName("responsible")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.ResponsibleId)
                    .HasColumnName("responsible_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.RouteDay)
                    .HasColumnName("route_day")
                    .HasColumnType("varchar(15)");
            });

            modelBuilder.Entity<RouteParameters>(entity =>
            {
                entity.ToTable("route_parameters");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Friday)
                    .HasColumnName("friday")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Monday)
                    .HasColumnName("monday")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Saturday)
                    .HasColumnName("saturday")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Sunday)
                    .HasColumnName("sunday")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Thursday)
                    .HasColumnName("thursday")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Tuesday)
                    .HasColumnName("tuesday")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Wednesday)
                    .HasColumnName("wednesday")
                    .HasColumnType("tinyint(3)");
            });

            modelBuilder.Entity<Safe>(entity =>
            {
                entity.ToTable("safe");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CurrencyUnit)
                    .HasColumnName("currency_unit")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.SafeCode)
                    .HasColumnName("safe_code")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.SafeGroup)
                    .HasColumnName("safe_group")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.SafeName)
                    .HasColumnName("safe_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Visible)
                    .HasColumnName("visible")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");
            });

            modelBuilder.Entity<SafeInput>(entity =>
            {
                entity.ToTable("safe_input");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.HasIndex(e => e.Deletion)
                    .HasName("deletion");

                entity.HasIndex(e => e.SafeId)
                    .HasName("safe_id");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.HasIndex(e => new { e.SafeId, e.CustomerId })
                    .HasName("safe_id_2");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Datetime)
                    .HasColumnName("datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DetailId)
                    .HasColumnName("detail_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(400)");

                entity.Property(e => e.PrivaiteCode)
                    .HasColumnName("privaite_code")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.SafeId)
                    .HasColumnName("safe_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Type1)
                    .HasColumnName("type_")
                    .HasColumnType("varchar(2)")
                    .HasDefaultValueSql("'in'");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Safe)
                    .WithMany(p => p.SafeInput)
                    .HasForeignKey(d => d.SafeId)
                    .HasConstraintName("safe_input_ibfk_1");
            });

            modelBuilder.Entity<SafeOutput>(entity =>
            {
                entity.ToTable("safe_output");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.HasIndex(e => e.Deletion)
                    .HasName("deletion");

                entity.HasIndex(e => e.SafeId)
                    .HasName("safe_id");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.HasIndex(e => new { e.SafeId, e.CustomerId })
                    .HasName("safe_id_2");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Datetime)
                    .HasColumnName("datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DetailId)
                    .HasColumnName("detail_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(400)");

                entity.Property(e => e.PrivaiteCode)
                    .HasColumnName("privaite_code")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.SafeId)
                    .HasColumnName("safe_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Type1)
                    .HasColumnName("type_")
                    .HasColumnType("varchar(3)")
                    .HasDefaultValueSql("'out'");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Safe)
                    .WithMany(p => p.SafeOutput)
                    .HasForeignKey(d => d.SafeId)
                    .HasConstraintName("safe_output_ibfk_1");
            });

            modelBuilder.Entity<ScaleSettings>(entity =>
            {
                entity.ToTable("scale_settings");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AmountLength)
                    .HasColumnName("amount_length")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.BarcodeLength)
                    .HasColumnName("barcode_length")
                    .HasColumnType("int(11)");

                entity.Property(e => e.BarcodePrefix)
                    .HasColumnName("barcode_prefix")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Scheduler>(entity =>
            {
                entity.ToTable("scheduler");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Caption)
                    .HasColumnName("caption")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.EventType)
                    .HasColumnName("event_type")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Finish)
                    .HasColumnName("finish")
                    .HasColumnType("datetime");

                entity.Property(e => e.LabelColour)
                    .HasColumnName("label_colour")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Location)
                    .HasColumnName("location")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Message)
                    .HasColumnName("message")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Options)
                    .HasColumnName("options")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Start)
                    .HasColumnName("start")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<ServiceControlDefines>(entity =>
            {
                entity.ToTable("service_control_defines");

                entity.HasIndex(e => e.ServicesCategorySubDefinitionId)
                    .HasName("services_category_sub_definition_id");

                entity.HasIndex(e => e.ServicesRequestDefinationId)
                    .HasName("service_request_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.CheckboxIn)
                    .HasColumnName("checkbox_in")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.Color)
                    .HasColumnName("color")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.ControlDefine)
                    .IsRequired()
                    .HasColumnName("control_define")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.DecimalIn)
                    .HasColumnName("decimal_in")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.ForceSelect)
                    .HasColumnName("force_select")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.IntegerIn)
                    .HasColumnName("integer_in")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.IsImportant)
                    .HasColumnName("is_important")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.OrderNumber)
                    .HasColumnName("order_number")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PhotoIn)
                    .HasColumnName("photo_in")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.ServicesCategorySubDefinitionId)
                    .HasColumnName("services_category_sub_definition_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServicesRequestDefinationId)
                    .HasColumnName("services_request_defination_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SubIn)
                    .HasColumnName("sub_in")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.TextIn)
                    .HasColumnName("text_in")
                    .HasColumnType("tinyint(3)");

                entity.HasOne(d => d.ServicesCategorySubDefinition)
                    .WithMany(p => p.ServiceControlDefines)
                    .HasForeignKey(d => d.ServicesCategorySubDefinitionId)
                    .HasConstraintName("service_control_defines_ibfk_2");

                entity.HasOne(d => d.ServicesRequestDefination)
                    .WithMany(p => p.ServiceControlDefines)
                    .HasForeignKey(d => d.ServicesRequestDefinationId)
                    .HasConstraintName("service_control_defines_ibfk_1");
            });

            modelBuilder.Entity<ServiceControlSubDefines>(entity =>
            {
                entity.ToTable("service_control_sub_defines");

                entity.HasIndex(e => e.ServiceControlDefinesId)
                    .HasName("service_control_defines_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.CheckboxIn)
                    .HasColumnName("checkbox_in")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.ControlDefine)
                    .IsRequired()
                    .HasColumnName("control_define")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.DecimalIn)
                    .HasColumnName("decimal_in")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Force)
                    .HasColumnName("force")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.IntegerIn)
                    .HasColumnName("integer_in")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.IsImportant)
                    .HasColumnName("is_important")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.OrderNumber)
                    .HasColumnName("order_number")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PhotoIn)
                    .HasColumnName("photo_in")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.ServiceControlDefinesId)
                    .HasColumnName("service_control_defines_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TextIn)
                    .HasColumnName("text_in")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.TickIn)
                    .HasColumnName("tick_in")
                    .HasColumnType("tinyint(3)");

                entity.HasOne(d => d.ServiceControlDefines)
                    .WithMany(p => p.ServiceControlSubDefines)
                    .HasForeignKey(d => d.ServiceControlDefinesId)
                    .HasConstraintName("service_control_sub_defines_ibfk_1");
            });

            modelBuilder.Entity<ServicesCategoryDefinition>(entity =>
            {
                entity.ToTable("services_category_definition");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Color)
                    .HasColumnName("color")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.ColorCode)
                    .HasColumnName("color_code")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Order)
                    .HasColumnName("order")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<ServicesCategorySubDefinition>(entity =>
            {
                entity.ToTable("services_category_sub_definition");

                entity.HasIndex(e => e.ServicesCategoryDefinitionId)
                    .HasName("services_category_definition_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Color)
                    .HasColumnName("color")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ForceAskPieceChange)
                    .HasColumnName("force_ask_piece_change")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ForceQrCode)
                    .HasColumnName("force_qr_code")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Order)
                    .HasColumnName("order")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServiceTimeOutMinutes)
                    .HasColumnName("service_time_out_minutes")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ServicesCategoryDefinitionId)
                    .HasColumnName("services_category_definition_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.ServicesCategoryDefinition)
                    .WithMany(p => p.ServicesCategorySubDefinition)
                    .HasForeignKey(d => d.ServicesCategoryDefinitionId)
                    .HasConstraintName("services_category_sub_definition_ibfk_1");
            });

            modelBuilder.Entity<ServicesContract>(entity =>
            {
                entity.ToTable("services_contract");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.HasIndex(e => e.ServicesCategoryDefinitionId)
                    .HasName("services_category_definition_id");

                entity.HasIndex(e => e.ServicesRequestDefinationId)
                    .HasName("services_request_defination_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.ContractFee)
                    .HasColumnName("contract_fee")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.EndDate)
                    .HasColumnName("end_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Notes)
                    .HasColumnName("notes")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ServiceDay)
                    .HasColumnName("service_day")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServicesCategoryDefinitionId)
                    .HasColumnName("services_category_definition_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServicesCount)
                    .HasColumnName("services_count")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServicesRequestDefinationId)
                    .HasColumnName("services_request_defination_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Tax)
                    .HasColumnName("tax")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'1'");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.ServicesContract)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("services_contract_ibfk_1");

                entity.HasOne(d => d.ServicesCategoryDefinition)
                    .WithMany(p => p.ServicesContract)
                    .HasForeignKey(d => d.ServicesCategoryDefinitionId)
                    .HasConstraintName("services_contract_ibfk_2");

                entity.HasOne(d => d.ServicesRequestDefination)
                    .WithMany(p => p.ServicesContract)
                    .HasForeignKey(d => d.ServicesRequestDefinationId)
                    .HasConstraintName("services_contract_ibfk_3");
            });

            modelBuilder.Entity<ServicesDirectionList>(entity =>
            {
                entity.ToTable("services_direction_list");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DirectionName)
                    .HasColumnName("direction_name")
                    .HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<ServicesLastStatusDefination>(entity =>
            {
                entity.ToTable("services_last_status_defination");

                entity.HasIndex(e => e.ServicesCategoryDefinitionId)
                    .HasName("services_category_definition_id");

                entity.HasIndex(e => e.SevicesRequestDefinationId)
                    .HasName("sevices_request_defination_id");

                entity.HasIndex(e => e.SmsThemesId)
                    .HasName("sms_themes_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Color)
                    .HasColumnName("color")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.ColorCode)
                    .HasColumnName("color_code")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDatetime)
                    .HasColumnName("deletion_datetime")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.OpenNewService)
                    .HasColumnName("open_new_service")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.OpenNewServiceRequestDefinationDefination)
                    .HasColumnName("open_new_service_request_defination_defination")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.OpenNewServiceRequestDefinationId)
                    .HasColumnName("open_new_service_request_defination_id")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Order)
                    .HasColumnName("order")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServiceImpact)
                    .HasColumnName("service_impact")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ServicesCategoryDefinitionId)
                    .HasColumnName("services_category_definition_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServicesCategoryName)
                    .HasColumnName("services_category_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.SevicesRequestDefinationDefination)
                    .HasColumnName("sevices_request_defination_defination")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.SevicesRequestDefinationId)
                    .HasColumnName("sevices_request_defination_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SmsThemesId)
                    .HasColumnName("sms_themes_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SmsThemesName)
                    .HasColumnName("sms_themes_name")
                    .HasColumnType("varchar(100)");

                entity.HasOne(d => d.ServicesCategoryDefinition)
                    .WithMany(p => p.ServicesLastStatusDefination)
                    .HasForeignKey(d => d.ServicesCategoryDefinitionId)
                    .HasConstraintName("services_last_status_defination_ibfk_2");

                entity.HasOne(d => d.SevicesRequestDefination)
                    .WithMany(p => p.ServicesLastStatusDefination)
                    .HasForeignKey(d => d.SevicesRequestDefinationId)
                    .HasConstraintName("services_last_status_defination_ibfk_1");

                entity.HasOne(d => d.SmsThemes)
                    .WithMany(p => p.ServicesLastStatusDefination)
                    .HasForeignKey(d => d.SmsThemesId)
                    .HasConstraintName("services_last_status_defination_ibfk_3");
            });

            modelBuilder.Entity<ServicesLastStatusDefinationInfs>(entity =>
            {
                entity.ToTable("services_last_status_defination_infs");

                entity.HasIndex(e => e.ServicesLastStatusDefinationId)
                    .HasName("services_last_status_defination_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Caption)
                    .HasColumnName("caption")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.DateIn)
                    .HasColumnName("date_in")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ForceEntery)
                    .HasColumnName("force_entery")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.IntegerIn)
                    .HasColumnName("integer_in")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.NumericIn)
                    .HasColumnName("numeric_in")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.OrderNumber)
                    .HasColumnName("order_number")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.PersonelIn)
                    .HasColumnName("personel_in")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.ServicesLastStatusDefinationId)
                    .HasColumnName("services_last_status_defination_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TextIn)
                    .HasColumnName("text_in")
                    .HasColumnType("tinyint(3)");

                entity.HasOne(d => d.ServicesLastStatusDefination)
                    .WithMany(p => p.ServicesLastStatusDefinationInfs)
                    .HasForeignKey(d => d.ServicesLastStatusDefinationId)
                    .HasConstraintName("services_last_status_defination_infs_ibfk_1");
            });

            modelBuilder.Entity<ServicesOpen>(entity =>
            {
                entity.ToTable("services_open");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.HasIndex(e => e.InvoiceId)
                    .HasName("invoice_id");

                entity.HasIndex(e => e.MasterServicesOpenId)
                    .HasName("master_services_open_id");

                entity.HasIndex(e => e.ServicesPointsBaseInformationId)
                    .HasName("services_points_base_information_id");

                entity.HasIndex(e => e.ServicesRequestDefinatinId)
                    .HasName("services_request_definatin_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDatetime)
                    .HasColumnName("deletion_datetime")
                    .HasColumnType("timestamp")
                    ;

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.EventType)
                    .HasColumnName("event_type")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FaultStats)
                    .HasColumnName("fault_stats")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.FinishDateTime)
                    .HasColumnName("finish_date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.InsertTime)
                    .HasColumnName("insert_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.InvoiceId)
                    .HasColumnName("invoice_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.LastValidDateTime)
                    .HasColumnName("last_valid_date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.MasterServicesOpenId)
                    .HasColumnName("master_services_open_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.OpenedUserId)
                    .HasColumnName("opened_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.OpensId)
                    .HasColumnName("opens_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Options)
                    .HasColumnName("options")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PlanFinishDateTime)
                    .HasColumnName("plan_finish_date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.ResponsibleUserId)
                    .HasColumnName("responsible_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServiceFee)
                    .HasColumnName("service_fee")
                    .HasColumnType("decimal(18,5)");

                entity.Property(e => e.ServicesOpenTunnel)
                    .HasColumnName("services_open_tunnel")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ServicesOpenType)
                    .HasColumnName("services_open_type")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ServicesPointsBaseInformationId)
                    .HasColumnName("services_points_base_information_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServicesRequestDefinatinId)
                    .HasColumnName("services_request_definatin_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.StartDateTime)
                    .HasColumnName("start_date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.ServicesOpen)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("services_open_ibfk_3");

                entity.HasOne(d => d.Invoice)
                    .WithMany(p => p.ServicesOpen)
                    .HasForeignKey(d => d.InvoiceId)
                    .HasConstraintName("services_open_ibfk_5");

                entity.HasOne(d => d.MasterServicesOpen)
                    .WithMany(p => p.InverseMasterServicesOpen)
                    .HasForeignKey(d => d.MasterServicesOpenId)
                    .HasConstraintName("services_open_ibfk_4");

                entity.HasOne(d => d.ServicesPointsBaseInformation)
                    .WithMany(p => p.ServicesOpen)
                    .HasForeignKey(d => d.ServicesPointsBaseInformationId)
                    .HasConstraintName("services_open_ibfk_2");

                entity.HasOne(d => d.ServicesRequestDefinatin)
                    .WithMany(p => p.ServicesOpen)
                    .HasForeignKey(d => d.ServicesRequestDefinatinId)
                    .HasConstraintName("services_open_ibfk_1");
            });

            modelBuilder.Entity<ServicesOpenActivity>(entity =>
            {
                entity.ToTable("services_open_activity");

                entity.HasIndex(e => e.ServicesOpenId)
                    .HasName("services_open_id");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ServicesOpenId)
                    .HasColumnName("services_open_id")
                    .HasColumnType("int(255)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.ServicesOpen)
                    .WithMany(p => p.ServicesOpenActivity)
                    .HasForeignKey(d => d.ServicesOpenId)
                    .HasConstraintName("services_open_activity_ibfk_1");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.ServicesOpenActivity)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("services_open_activity_ibfk_2");
            });

            modelBuilder.Entity<ServicesOpenClose>(entity =>
            {
                entity.ToTable("services_open_close");

                entity.HasIndex(e => e.ClosedUserId)
                    .HasName("closed_user_id");

                entity.HasIndex(e => e.ServicesLastStatusDefinationId)
                    .HasName("services_last_status_defination_id");

                entity.HasIndex(e => e.ServicesOpenId)
                    .HasName("services_open_id")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ClosedDateTime)
                    .HasColumnName("closed_date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.ClosedUserId)
                    .HasColumnName("closed_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InsTime)
                    .HasColumnName("ins_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ServicesClosedTunnel)
                    .HasColumnName("services_closed_tunnel")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ServicesLastStatusDefinationId)
                    .HasColumnName("services_last_status_defination_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ServicesOpenId)
                    .HasColumnName("services_open_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.ClosedUser)
                    .WithMany(p => p.ServicesOpenClose)
                    .HasForeignKey(d => d.ClosedUserId)
                    .HasConstraintName("services_open_close_ibfk_3");

                entity.HasOne(d => d.ServicesLastStatusDefination)
                    .WithMany(p => p.ServicesOpenClose)
                    .HasForeignKey(d => d.ServicesLastStatusDefinationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("services_open_close_ibfk_1");

                entity.HasOne(d => d.ServicesOpen)
                    .WithOne(p => p.ServicesOpenClose)
                    .HasForeignKey<ServicesOpenClose>(d => d.ServicesOpenId)
                    .HasConstraintName("services_open_close_ibfk_2");
            });

            modelBuilder.Entity<ServicesOpenCloseControlDefines>(entity =>
            {
                entity.ToTable("services_open_close_control_defines");

                entity.HasIndex(e => e.ServiceControlDefinesId)
                    .HasName("service_control_defines_id");

                entity.HasIndex(e => e.ServicesOpenId)
                    .HasName("services_open_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Photo)
                    .HasColumnName("photo")
                    .HasColumnType("longtext");

                entity.Property(e => e.Result1)
                    .HasColumnName("result1")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Result2)
                    .HasColumnName("result2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Result3)
                    .HasColumnName("result3")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ServiceControlDefinesId)
                    .HasColumnName("service_control_defines_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServiceControlDefinesSubId)
                    .HasColumnName("service_control_defines_sub_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServicesOpenId)
                    .HasColumnName("services_open_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.StartTime)
                    .HasColumnName("start_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.HasOne(d => d.ServiceControlDefines)
                    .WithMany(p => p.ServicesOpenCloseControlDefines)
                    .HasForeignKey(d => d.ServiceControlDefinesId)
                    .HasConstraintName("services_open_close_control_defines_ibfk_1");

                entity.HasOne(d => d.ServicesOpen)
                    .WithMany(p => p.ServicesOpenCloseControlDefines)
                    .HasForeignKey(d => d.ServicesOpenId)
                    .HasConstraintName("services_open_close_control_defines_ibfk_2");
            });

            modelBuilder.Entity<ServicesOpenCloseParameters>(entity =>
            {
                entity.ToTable("services_open_close_parameters");

                entity.HasIndex(e => e.ServicesLastStatusDefinationInfsId)
                    .HasName("services_last_status_defination_infs_id");

                entity.HasIndex(e => e.ServicesOpenId)
                    .HasName("services_open_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Field1)
                    .HasColumnName("field1")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Field2)
                    .HasColumnName("field2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ServicesLastStatusDefinationInfsId)
                    .HasColumnName("services_last_status_defination_infs_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServicesOpenId)
                    .HasColumnName("services_open_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.ServicesLastStatusDefinationInfs)
                    .WithMany(p => p.ServicesOpenCloseParameters)
                    .HasForeignKey(d => d.ServicesLastStatusDefinationInfsId)
                    .HasConstraintName("services_open_close_parameters_ibfk_2");

                entity.HasOne(d => d.ServicesOpen)
                    .WithMany(p => p.ServicesOpenCloseParameters)
                    .HasForeignKey(d => d.ServicesOpenId)
                    .HasConstraintName("services_open_close_parameters_ibfk_1");
            });

            modelBuilder.Entity<ServicesOpenControlDefines>(entity =>
            {
                entity.ToTable("services_open_control_defines");

                entity.HasIndex(e => e.ServiceControlDefinesId)
                    .HasName("service_control_defines_id");

                entity.HasIndex(e => e.ServicesOpenId)
                    .HasName("services_open_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ServiceControlDefinesId)
                    .HasColumnName("service_control_defines_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServicesOpenId)
                    .HasColumnName("services_open_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.ServiceControlDefines)
                    .WithMany(p => p.ServicesOpenControlDefines)
                    .HasForeignKey(d => d.ServiceControlDefinesId)
                    .HasConstraintName("services_open_control_defines_ibfk_1");

                entity.HasOne(d => d.ServicesOpen)
                    .WithMany(p => p.ServicesOpenControlDefines)
                    .HasForeignKey(d => d.ServicesOpenId)
                    .HasConstraintName("services_open_control_defines_ibfk_2");
            });

            modelBuilder.Entity<ServicesOpenControlDefinesTemp>(entity =>
            {
                entity.ToTable("services_open_control_defines_temp");

                entity.HasIndex(e => e.ServiceControlDefinesId)
                    .HasName("service_control_defines_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BasketId)
                    .HasColumnName("basket_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ControlDefine)
                    .HasColumnName("control_define")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ModuleDefination)
                    .HasColumnName("module_defination")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.ServiceControlDefinesId)
                    .HasColumnName("service_control_defines_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.ServiceControlDefines)
                    .WithMany(p => p.ServicesOpenControlDefinesTemp)
                    .HasForeignKey(d => d.ServiceControlDefinesId)
                    .HasConstraintName("services_open_control_defines_temp_ibfk_1");
            });

            modelBuilder.Entity<ServicesOpenParameters>(entity =>
            {
                entity.ToTable("services_open_parameters");

                entity.HasIndex(e => e.ServicesOpenId)
                    .HasName("services_open_id");

                entity.HasIndex(e => e.ServicesRequestDefinationInfsId)
                    .HasName("services_request_defination_infs_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Field1)
                    .HasColumnName("field1")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Field2)
                    .HasColumnName("field2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ServicesOpenId)
                    .HasColumnName("services_open_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServicesRequestDefinationInfsId)
                    .HasColumnName("services_request_defination_infs_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.ServicesOpen)
                    .WithMany(p => p.ServicesOpenParameters)
                    .HasForeignKey(d => d.ServicesOpenId)
                    .HasConstraintName("services_open_parameters_ibfk_1");

                entity.HasOne(d => d.ServicesRequestDefinationInfs)
                    .WithMany(p => p.ServicesOpenParameters)
                    .HasForeignKey(d => d.ServicesRequestDefinationInfsId)
                    .HasConstraintName("services_open_parameters_ibfk_2");
            });

            modelBuilder.Entity<ServicesPointsBaseInformation>(entity =>
            {
                entity.ToTable("services_points_base_information");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.HasIndex(e => e.ServicesCategoryDefinitionId)
                    .HasName("services_category_definition_id");

                entity.HasIndex(e => e.ServicesContractId)
                    .HasName("services_contract_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AcceptanceDate)
                    .HasColumnName("acceptance_date")
                    .HasColumnType("date");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Direction)
                    .HasColumnName("direction")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.DueDate)
                    .HasColumnName("due_date")
                    .HasColumnType("date");

                entity.Property(e => e.IdentificationNumber)
                    .HasColumnName("identification_number")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.InstallationDate)
                    .HasColumnName("installation_date")
                    .HasColumnType("date");

                entity.Property(e => e.Latitude)
                    .HasColumnName("latitude")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Longitude)
                    .HasColumnName("longitude")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Manufacturer)
                    .HasColumnName("manufacturer")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.ServiceCode)
                    .HasColumnName("service_code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.ServiceName)
                    .HasColumnName("service_name")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.ServiceOrder)
                    .HasColumnName("service_order")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServicesCategoryDefinitionId)
                    .HasColumnName("services_category_definition_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServicesCategoryName)
                    .HasColumnName("services_category_name")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.ServicesContractId)
                    .HasColumnName("services_contract_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Warranty)
                    .HasColumnName("warranty")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.WarrantyEndDate)
                    .HasColumnName("warranty_end_date")
                    .HasColumnType("date");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.ServicesPointsBaseInformation)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("services_points_base_information_ibfk_4");

                entity.HasOne(d => d.ServicesCategoryDefinition)
                    .WithMany(p => p.ServicesPointsBaseInformation)
                    .HasForeignKey(d => d.ServicesCategoryDefinitionId)
                    .HasConstraintName("services_points_base_information_ibfk_3");

                entity.HasOne(d => d.ServicesContract)
                    .WithMany(p => p.ServicesPointsBaseInformation)
                    .HasForeignKey(d => d.ServicesContractId)
                    .HasConstraintName("services_points_base_information_ibfk_2");
            });

            modelBuilder.Entity<ServicesProductChangeList>(entity =>
            {
                entity.ToTable("services_product_change_list");

                entity.HasIndex(e => e.InvoiceId)
                    .HasName("invoice_id");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.HasIndex(e => e.ServicesPointsBaseInformationId)
                    .HasName("services_points_base_information_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DetailId)
                    .HasColumnName("detail_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FirstPhoto)
                    .HasColumnName("first_photo")
                    .HasColumnType("longtext");

                entity.Property(e => e.InsDateTime)
                    .HasColumnName("ins_date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.InvoiceId)
                    .HasColumnName("invoice_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InvoiceStats)
                    .HasColumnName("invoice_stats")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.LastPhoto)
                    .HasColumnName("last_photo")
                    .HasColumnType("longtext");

                entity.Property(e => e.Note)
                    .HasColumnName("note")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServicesOpenId)
                    .HasColumnName("services_open_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServicesPointsBaseInformationId)
                    .HasColumnName("services_points_base_information_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.UnitPriceTax)
                    .HasColumnName("unit_price_tax")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Invoice)
                    .WithMany(p => p.ServicesProductChangeList)
                    .HasForeignKey(d => d.InvoiceId)
                    .HasConstraintName("services_product_change_list_ibfk_3");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ServicesProductChangeList)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("services_product_change_list_ibfk_1");

                entity.HasOne(d => d.ServicesPointsBaseInformation)
                    .WithMany(p => p.ServicesProductChangeList)
                    .HasForeignKey(d => d.ServicesPointsBaseInformationId)
                    .HasConstraintName("services_product_change_list_ibfk_2");
            });

            modelBuilder.Entity<ServicesProductChangeListTemp>(entity =>
            {
                entity.ToTable("services_product_change_list_temp");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.HasIndex(e => e.ServicesPointsBaseInformationId)
                    .HasName("services_points_base_information_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DetailId)
                    .HasColumnName("detail_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FirstPhoto)
                    .HasColumnName("first_photo")
                    .HasColumnType("longtext");

                entity.Property(e => e.InsDateTime)
                    .HasColumnName("ins_date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.InvoiceId)
                    .HasColumnName("invoice_id")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.InvoiceStats)
                    .HasColumnName("invoice_stats")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.LastPhoto)
                    .HasColumnName("last_photo")
                    .HasColumnType("longtext");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Note)
                    .HasColumnName("note")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServicesOpenId)
                    .HasColumnName("services_open_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServicesPointsBaseInformationId)
                    .HasColumnName("services_points_base_information_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.UnitPriceTax)
                    .HasColumnName("unit_price_tax")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ServicesProductChangeListTemp)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("services_product_change_list_temp_ibfk_1");

                entity.HasOne(d => d.ServicesPointsBaseInformation)
                    .WithMany(p => p.ServicesProductChangeListTemp)
                    .HasForeignKey(d => d.ServicesPointsBaseInformationId)
                    .HasConstraintName("services_product_change_list_temp_ibfk_2");
            });

            modelBuilder.Entity<ServicesRequestDefination>(entity =>
            {
                entity.ToTable("services_request_defination");

                entity.HasIndex(e => e.ServicesCategoryDefinitionId)
                    .HasName("services_category_definition_id");

                entity.HasIndex(e => e.SmsThemesId)
                    .HasName("sms_theme_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AllowNotAccept)
                    .HasColumnName("allow_not_accept")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.AutoInsertServiceInContract)
                    .HasColumnName("auto_insert_service_in_contract")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.AutomaticCharging)
                    .HasColumnName("automatic_charging")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Color)
                    .HasColumnName("color")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.ColorCode)
                    .HasColumnName("color_code")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ComponentDialog)
                    .HasColumnName("component_dialog")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ContractReportFile)
                    .HasColumnName("contract_report_file")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.CustomizableSubModule)
                    .HasColumnName("Customizable_sub_module")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ForcedContract)
                    .HasColumnName("forced_contract")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.HexColor)
                    .HasColumnName("hex_color")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.MobileVisible)
                    .HasColumnName("mobile_visible")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.OfferExcelControlsPageNo)
                    .HasColumnName("offer_excel_controls_page_no")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.OfferExcelControlsStartColumn)
                    .HasColumnName("offer_excel_controls_start_column")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.OfferExcelControlsStartRow)
                    .HasColumnName("offer_excel_controls_start_row")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.OfferExcelPrintPages)
                    .HasColumnName("offer_excel_print_pages")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.OfferExcelProductsPageNo)
                    .HasColumnName("offer_excel_products_page_no")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.OfferExcelProductsPrintPages)
                    .HasColumnName("offer_excel_products_print_pages")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.OfferExcelProductsStartColumn)
                    .HasColumnName("offer_excel_products_start_column")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.OfferExcelProductsStartRow)
                    .HasColumnName("offer_excel_products_start_row")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.OfferExcelTemplate)
                    .HasColumnName("offer_excel_template")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.OpenServiceWithoutPoint)
                    .HasColumnName("open_service_without_point")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.OpenWithoutPersonel)
                    .HasColumnName("open_without_personel")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.OperationTime)
                    .HasColumnName("operation_time")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'30'");

                entity.Property(e => e.Order)
                    .HasColumnName("order")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Period)
                    .HasColumnName("period")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.PieceBillsAreMergedAutomatically)
                    .HasColumnName("piece_bills_are_merged_automatically")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'30'");

                entity.Property(e => e.RequestGroup)
                    .HasColumnName("request_group")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.ServiceReportExcelFile)
                    .HasColumnName("service_report_excel_file")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ServiceReportExcelFileActive)
                    .HasColumnName("service_report_excel_file_active")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ServiceReportFile)
                    .HasColumnName("service_report_file")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ServiceReportFileActive)
                    .HasColumnName("service_report_file_active")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.ServicesCategoryDefinitionId)
                    .HasColumnName("services_category_definition_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServicesCategoryName)
                    .HasColumnName("services_category_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.SmsStartThemesId)
                    .HasColumnName("sms_start_themes_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SmsStartThemesName)
                    .HasColumnName("sms_start_themes_name")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.SmsThemesId)
                    .HasColumnName("sms_themes_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SmsThemesName)
                    .HasColumnName("sms_themes_name")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.TaskAcceptTimeout)
                    .HasColumnName("task_accept_timeout")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.ServicesCategoryDefinition)
                    .WithMany(p => p.ServicesRequestDefination)
                    .HasForeignKey(d => d.ServicesCategoryDefinitionId)
                    .HasConstraintName("services_request_defination_ibfk_1");

                entity.HasOne(d => d.SmsThemes)
                    .WithMany(p => p.ServicesRequestDefination)
                    .HasForeignKey(d => d.SmsThemesId)
                    .HasConstraintName("services_request_defination_ibfk_2");
            });

            modelBuilder.Entity<ServicesRequestDefinationInfs>(entity =>
            {
                entity.ToTable("services_request_defination_infs");

                entity.HasIndex(e => e.ServicesRequestDefinationId)
                    .HasName("services_request_defination_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Caption)
                    .HasColumnName("caption")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.DateIn)
                    .HasColumnName("date_in")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ForceEntery)
                    .HasColumnName("force_entery")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.IntegerIn)
                    .HasColumnName("integer_in")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.NumericIn)
                    .HasColumnName("numeric_in")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.OrderNumber)
                    .HasColumnName("order_number")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.PersonelIn)
                    .HasColumnName("personel_in")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.ServicesRequestDefinationId)
                    .HasColumnName("services_request_defination_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TextIn)
                    .HasColumnName("text_in")
                    .HasColumnType("tinyint(3)");

                entity.HasOne(d => d.ServicesRequestDefination)
                    .WithMany(p => p.ServicesRequestDefinationInfs)
                    .HasForeignKey(d => d.ServicesRequestDefinationId)
                    .HasConstraintName("services_request_defination_infs_ibfk_1");
            });

            modelBuilder.Entity<ServicesSetupDefineParameters>(entity =>
            {
                entity.ToTable("services_setup_define_parameters");

                entity.HasIndex(e => e.ServicesSetupDefinesId)
                    .HasName("services_setup_defines_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.DecimalIn)
                    .HasColumnName("decimal_in")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.IntegerIn)
                    .HasColumnName("integer_in")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServicesSetupDefinesId)
                    .HasColumnName("services_setup_defines_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TextIn)
                    .HasColumnName("text_in")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.ServicesSetupDefines)
                    .WithMany(p => p.ServicesSetupDefineParameters)
                    .HasForeignKey(d => d.ServicesSetupDefinesId)
                    .HasConstraintName("services_setup_define_parameters_ibfk_1");
            });

            modelBuilder.Entity<ServicesSetupDefines>(entity =>
            {
                entity.ToTable("services_setup_defines");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionTime)
                    .HasColumnName("deletion_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ForceTrackSerialNumber)
                    .HasColumnName("force_track_serial_number")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.ForceTrackWarranty)
                    .HasColumnName("force_track_warranty")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.ServicesCategoryDefinitionId)
                    .HasColumnName("services_category_definition_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServicesCategoryName)
                    .HasColumnName("services_category_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.SetupGroup)
                    .HasColumnName("setup_group")
                    .HasColumnType("varchar(150)");
            });

            modelBuilder.Entity<ServicesSetupDefinesFormula>(entity =>
            {
                entity.ToTable("services_setup_defines_formula");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Formula)
                    .HasColumnName("formula")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductName)
                    .IsRequired()
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(200)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ServicesSetupDefinesId)
                    .HasColumnName("services_setup_defines_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<ServicesSetupDefinesFunctions>(entity =>
            {
                entity.ToTable("services_setup_defines_functions");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.FunctionExp)
                    .HasColumnName("function_exp")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FunctionName)
                    .HasColumnName("function_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.GroupName)
                    .HasColumnName("group_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Syntax)
                    .HasColumnName("syntax")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<ServicesSetupRequestList>(entity =>
            {
                entity.ToTable("services_setup_request_list");

                entity.HasIndex(e => e.ServicesRequestDefinationId)
                    .HasName("services_request_defination_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DayDifference)
                    .HasColumnName("day_difference")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp")
                 ;

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.HourDifference)
                    .HasColumnName("hour_difference")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.MinuteDifference)
                    .HasColumnName("minute_difference")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Order)
                    .HasColumnName("order")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.RequestDefination)
                    .HasColumnName("request_defination")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ServicesRequestDefinationId)
                    .HasColumnName("services_request_defination_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ServicesSetupDefinesId)
                    .HasColumnName("services_setup_defines_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.ServicesRequestDefination)
                    .WithMany(p => p.ServicesSetupRequestList)
                    .HasForeignKey(d => d.ServicesRequestDefinationId)
                    .HasConstraintName("services_setup_request_list_ibfk_1");
            });

            modelBuilder.Entity<ServicesSpeedButtons>(entity =>
            {
                entity.ToTable("services_speed_buttons");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.ButtonType)
                    .HasColumnName("button_type")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Defination)
                    .HasColumnName("defination")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Glyph).HasColumnName("glyph");

                entity.Property(e => e.RequestId)
                    .HasColumnName("request_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.ServicesSpeedButtons)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("services_speed_buttons_ibfk_1");
            });

            modelBuilder.Entity<Settings>(entity =>
            {
                entity.ToTable("settings");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Thema)
                    .HasColumnName("thema")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<SmsBalance>(entity =>
            {
                entity.ToTable("sms_balance");

                entity.HasIndex(e => e.SmsTaskListId)
                    .HasName("sms_task_list_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(1000)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SmsTaskListId)
                    .HasColumnName("sms_task_list_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");

                entity.HasOne(d => d.SmsTaskList)
                    .WithMany(p => p.SmsBalance)
                    .HasForeignKey(d => d.SmsTaskListId)
                    .HasConstraintName("sms_balance_ibfk_1");
            });

            modelBuilder.Entity<SmsParameters>(entity =>
            {
                entity.ToTable("sms_parameters");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AfterMaintenanceSendSms)
                    .HasColumnName("after_maintenance_send_sms")
                    .HasColumnType("varchar(10)")
                    .HasDefaultValueSql("'True'");

                entity.Property(e => e.DefaultMaintenanceSmsTemplateId)
                    .HasColumnName("default_maintenance_sms_template_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ShPassword)
                    .HasColumnName("sh_password")
                    .HasColumnType("varchar(50)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.ShUserName)
                    .HasColumnName("sh_user_name")
                    .HasColumnType("varchar(50)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.SmsTitle)
                    .HasColumnName("sms_title")
                    .HasColumnType("varchar(11)");
            });

            modelBuilder.Entity<SmsTask>(entity =>
            {
                entity.ToTable("sms_task");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.SmsTaskListBasketId)
                    .HasColumnName("sms_task_list_basket_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TaskName)
                    .HasColumnName("task_name")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<SmsTaskList>(entity =>
            {
                entity.ToTable("sms_task_list");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BasketId)
                    .HasColumnName("basket_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.KcReferance)
                    .HasColumnName("kc_referance")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Message)
                    .HasColumnName("message")
                    .HasColumnType("varchar(1000)");

                entity.Property(e => e.Number)
                    .HasColumnName("number")
                    .HasColumnType("varchar(12)");

                entity.Property(e => e.Referance)
                    .HasColumnName("referance")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.SendDate)
                    .HasColumnName("send_date")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'1'");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.SmsTaskList)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("sms_task_list_ibfk_1");
            });

            modelBuilder.Entity<SmsThemes>(entity =>
            {
                entity.ToTable("sms_themes");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Theme)
                    .HasColumnName("theme")
                    .HasColumnType("varchar(960)");

                entity.Property(e => e.ThemeName)
                    .HasColumnName("theme_name")
                    .HasColumnType("varchar(60)");
            });

            modelBuilder.Entity<SpeedSalesButtons>(entity =>
            {
                entity.ToTable("speed_sales_buttons");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ButtonCaption)
                    .HasColumnName("button_caption")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.ButtonName)
                    .HasColumnName("button_name")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.ButtonTag)
                    .HasColumnName("button_tag")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Img)
                    .HasColumnName("img")
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.ProductCaption)
                    .HasColumnName("product_caption")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.UseJpeg)
                    .HasColumnName("use_jpeg")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<SpeedSalesTabsheetCaptions>(entity =>
            {
                entity.ToTable("speed_sales_tabsheet_captions");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Tabsheetcaption)
                    .HasColumnName("tabsheetcaption")
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.Tabsheetname)
                    .HasColumnName("tabsheetname")
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<StockActivities>(entity =>
            {
                entity.ToTable("stock_activities");

                entity.HasIndex(e => e.Deletion)
                    .HasName("deletion");

                entity.HasIndex(e => e.DetailId)
                    .HasName("detail_id");

                entity.HasIndex(e => e.ProductId)
                    .HasName("ndx_stock_activities_product_id_2");

                entity.HasIndex(e => e.Storage)
                    .HasName("ndx_stock_activities_storage");

                entity.HasIndex(e => e.Type)
                    .HasName("ndx_stock_activities_type");

                entity.HasIndex(e => new { e.ProductId, e.Type })
                    .HasName("ndx_stock_activities_product_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ColorId)
                    .HasColumnName("color_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Datetime)
                    .HasColumnName("datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DetailId)
                    .HasColumnName("detail_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DocNum)
                    .HasColumnName("doc_num")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.DocSeri)
                    .HasColumnName("doc_seri")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Storage)
                    .HasColumnName("storage")
                    .HasColumnType("int(11)");

                entity.Property(e => e.StorageTransferBasketId)
                    .HasColumnName("storage_transfer_basket_id")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.StockActivities)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("stock_activities_ibfk_1");

                entity.HasOne(d => d.StorageNavigation)
                    .WithMany(p => p.StockActivities)
                    .HasForeignKey(d => d.Storage)
                    .HasConstraintName("stock_activities_ibfk_2");
            });

            modelBuilder.Entity<StockActivitiesSub>(entity =>
            {
                entity.ToTable("stock_activities_sub");

                entity.HasIndex(e => e.InvoiceProductSubId)
                    .HasName("invoice_product_sub_id");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.HasIndex(e => e.StockCountingSubId)
                    .HasName("stock_counting_sub_id");

                entity.HasIndex(e => e.Storage)
                    .HasName("storage");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ColorId)
                    .HasColumnName("color_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Datetime)
                    .HasColumnName("datetime")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.InvoiceProductSubId)
                    .HasColumnName("invoice_product_sub_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.StockCountingSubId)
                    .HasColumnName("stock_counting_sub_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Storage)
                    .HasColumnName("storage")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<StockActivitiesSubMain>(entity =>
            {
                entity.ToTable("stock_activities_sub_main");

                entity.HasIndex(e => e.InvoiceProductSubMainId)
                    .HasName("invoice_product_sub_id");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.HasIndex(e => e.Storage)
                    .HasName("storage");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ColorId)
                    .HasColumnName("color_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Datetime)
                    .HasColumnName("datetime")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.InvoiceProductSubMainId)
                    .HasColumnName("invoice_product_sub_main_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'-1'");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.StockCountingSubMainId)
                    .HasColumnName("stock_counting_sub_main_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'-1'");

                entity.Property(e => e.Storage)
                    .HasColumnName("storage")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<StockCounting>(entity =>
            {
                entity.ToTable("stock_counting");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Datetime)
                    .HasColumnName("datetime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deficieny)
                    .HasColumnName("deficieny")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Equality)
                    .HasColumnName("equality")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Excess)
                    .HasColumnName("excess")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductType)
                    .HasColumnName("product_type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.StorageId)
                    .HasColumnName("storage_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<StockCountingSub>(entity =>
            {
                entity.ToTable("stock_counting_sub");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Datetime)
                    .HasColumnName("datetime")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Deficieny)
                    .HasColumnName("deficieny")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Equality)
                    .HasColumnName("equality")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Excess)
                    .HasColumnName("excess")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductType)
                    .HasColumnName("product_type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.StockCountingBasket)
                    .HasColumnName("stock_counting_basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.StorageId)
                    .HasColumnName("storage_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<StockCountingSubMain>(entity =>
            {
                entity.ToTable("stock_counting_sub_main");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Datetime)
                    .HasColumnName("datetime")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Deficieny)
                    .HasColumnName("deficieny")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Equality)
                    .HasColumnName("equality")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Excess)
                    .HasColumnName("excess")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductType)
                    .HasColumnName("product_type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.StockCountingBasket)
                    .HasColumnName("stock_counting_basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.StorageId)
                    .HasColumnName("storage_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<StockCountingTemp>(entity =>
            {
                entity.ToTable("stock_counting_temp");

                entity.HasIndex(e => e.Basket)
                    .HasName("basket");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deficieny)
                    .HasColumnName("deficieny")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Equality)
                    .HasColumnName("equality")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Excess)
                    .HasColumnName("excess")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ForceSerialTrack)
                    .HasColumnName("force_serial_track")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.ProductType)
                    .HasColumnName("product_type")
                    .HasColumnType("tinyint(3)");
            });

            modelBuilder.Entity<StockLabel>(entity =>
            {
                entity.ToTable("stock_label");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(110)");
            });

            modelBuilder.Entity<Storage>(entity =>
            {
                entity.ToTable("storage");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AuthorizedGsm)
                    .HasColumnName("authorized_gsm")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.AuthorizedGsm2)
                    .HasColumnName("authorized_gsm_2")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.BranchDefinition)
                    .HasColumnName("branch_definition")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.BranchId)
                    .HasColumnName("branch_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Explanation)
                    .HasColumnName("explanation")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Fax)
                    .HasColumnName("fax")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.ForklifOperator)
                    .HasColumnName("forklif_operator")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.ForliftOperatorId)
                    .HasColumnName("forlift_operator_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.LoaderDefination)
                    .HasColumnName("loader_defination")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.LoaderId)
                    .HasColumnName("loader_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PhoneInternal)
                    .HasColumnName("phone_internal")
                    .HasColumnType("int(5)");

                entity.Property(e => e.ResponsibleForWarehouseAccountingDefination)
                    .HasColumnName("responsible_for_warehouse_accounting_defination")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.ResponsibleForWarehouseAccountingId)
                    .HasColumnName("responsible_for_warehouse_accounting_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SecurityDefination)
                    .HasColumnName("security_defination")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.SecurityId)
                    .HasColumnName("security_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.StorageManager)
                    .HasColumnName("storage_manager")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.StorageManagerId)
                    .HasColumnName("storage_manager_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.StorageName)
                    .HasColumnName("storage_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Telephone)
                    .HasColumnName("telephone")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.WarehouseAdress)
                    .HasColumnName("warehouse_adress")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.WarehouseFormenDefination)
                    .HasColumnName("warehouse_formen_defination")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.WarehouseFormenId)
                    .HasColumnName("warehouse_formen_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<StorageTransfer>(entity =>
            {
                entity.ToTable("storage_transfer");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DetailId)
                    .HasColumnName("detail_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MainProductId)
                    .HasColumnName("main_product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ProductType)
                    .HasColumnName("product_type")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ReceiverStorage)
                    .HasColumnName("receiver_storage")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SenderStorage)
                    .HasColumnName("sender_storage")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SetQty)
                    .HasColumnName("Set_Qty")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<StorageTransferTemp>(entity =>
            {
                entity.ToTable("storage_transfer_temp");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.MainProductId)
                    .HasColumnName("main_product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ProductType)
                    .HasColumnName("product_type")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.SetQty)
                    .HasColumnName("Set_Qty")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<SubscriptionGroups>(entity =>
            {
                entity.HasKey(e => e.SubscriptionId);

                entity.ToTable("subscription_groups");

                entity.Property(e => e.SubscriptionId)
                    .HasColumnName("Subscription_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(155)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(155)");

                entity.Property(e => e.Exp3)
                    .HasColumnName("exp3")
                    .HasColumnType("varchar(155)");

                entity.Property(e => e.Repetition)
                    .HasColumnName("repetition")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.SubscriptionCode)
                    .HasColumnName("Subscription_Code")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.SubscriptionDefinition)
                    .HasColumnName("Subscription_Definition")
                    .HasColumnType("varchar(120)");

                entity.Property(e => e.SubscriptionFee)
                    .HasColumnName("Subscription_Fee")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.SubscriptionGroup)
                    .HasColumnName("Subscription_Group")
                    .HasColumnType("varchar(55)");
            });

            modelBuilder.Entity<Support>(entity =>
            {
                entity.ToTable("support");

                entity.HasIndex(e => e.FeedBack)
                    .HasName("feed_back");

                entity.HasIndex(e => e.ReceiverUnit)
                    .HasName("Index_3");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.FeedBack)
                    .HasColumnName("feed_back")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.ReceiverUnit)
                    .IsRequired()
                    .HasColumnName("receiver_unit")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Request)
                    .IsRequired()
                    .HasColumnName("request")
                    .HasColumnType("varchar(1500)");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<TempBasket>(entity =>
            {
                entity.ToTable("temp_basket");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id");

                entity.HasIndex(e => e.ProductId)
                    .HasName("product_id");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.BasketGroup)
                    .HasColumnName("basket_group")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DateTi)
                    .HasColumnName("date_ti")
                    .HasColumnType("datetime");

                entity.Property(e => e.OperationType)
                    .HasColumnName("operation_type")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.UnitPrice)
                    .HasColumnName("unit_price")
                    .HasColumnType("decimal(12,2)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.TempBasket)
                    .HasForeignKey<TempBasket>(d => d.Id)
                    .HasConstraintName("temp_basket_ibfk_1");

                entity.HasOne(d => d.Id1)
                    .WithOne(p => p.TempBasket)
                    .HasForeignKey<TempBasket>(d => d.Id)
                    .HasConstraintName("temp_basket_ibfk_3");

                entity.HasOne(d => d.Id2)
                    .WithOne(p => p.TempBasket)
                    .HasForeignKey<TempBasket>(d => d.Id)
                    .HasConstraintName("temp_basket_ibfk_2");
            });

            modelBuilder.Entity<TempBasketTax>(entity =>
            {
                entity.ToTable("temp_basket_tax");

                entity.HasIndex(e => e.ProductTaxListId)
                    .HasName("product_tax_list");

                entity.HasIndex(e => e.TempBasketId)
                    .HasName("temp_basket_id");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ProductTaxListId)
                    .HasColumnName("product_tax_list_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TaxName)
                    .HasColumnName("tax_name")
                    .HasColumnType("varchar(11)");

                entity.Property(e => e.TaxRate)
                    .HasColumnName("tax_rate")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.TempBasketId)
                    .HasColumnName("temp_basket_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.TempBasketTax)
                    .HasForeignKey<TempBasketTax>(d => d.Id)
                    .HasConstraintName("temp_basket_tax_ibfk_1");
            });

            modelBuilder.Entity<Token>(entity =>
            {
                entity.ToTable("token");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AccesKey)
                    .HasColumnName("acces_key")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<UpdateHistory>(entity =>
            {
                entity.ToTable("update_history");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Comments)
                    .HasColumnName("comments")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.EmergencyRate)
                    .HasColumnName("emergency_rate")
                    .HasColumnType("int(2)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.InsertTime)
                    .HasColumnName("insert_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.IsServer)
                    .HasColumnName("is_server")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.UpdateState)
                    .HasColumnName("update_state")
                    .HasColumnType("varchar(15)")
                    .HasDefaultValueSql("'Not Updated'");

                entity.Property(e => e.UpdateTime)
                    .HasColumnName("update_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.UploadPath)
                    .HasColumnName("upload_path")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.VersionNumber)
                    .HasColumnName("version_number")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.VersionUrl)
                    .HasColumnName("version_url")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<Vehicle>(entity =>
            {
                entity.ToTable("vehicle");

                entity.HasIndex(e => e.DeletionUserId)
                    .HasName("deletion_user_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionDateTime)
                    .HasColumnName("deletion_date_time")
                    .HasColumnType("timestamp")
                    ;

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.EngineSerial)
                    .HasColumnName("engine_serial")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp1)
                    .HasColumnName("exp1")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Exp2)
                    .HasColumnName("exp2")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.FrameSerial)
                    .HasColumnName("frame_serial")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Insurance2FinishTime)
                    .HasColumnName("insurance2_finish_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Insurance2StartTime)
                    .HasColumnName("insurance2_start_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.InsuranceFinishTime)
                    .HasColumnName("insurance_finish_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.InsuranceStartTime)
                    .HasColumnName("insurance_start_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Mark)
                    .HasColumnName("mark")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Model)
                    .HasColumnName("model")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ModelYear)
                    .HasColumnName("model_year")
                    .HasColumnType("mediumint(4)");

                entity.Property(e => e.Plate).HasColumnType("varchar(35)");

                entity.Property(e => e.PurchaseAmount)
                    .HasColumnName("purchase_amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.PurchaseDate)
                    .HasColumnName("purchase_date")
                    .HasColumnType("timestamp");

                entity.Property(e => e.Species)
                    .HasColumnName("species")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.TaxDate1)
                    .HasColumnName("tax_date1")
                    .HasColumnType("date");

                entity.Property(e => e.TaxDate2)
                    .HasColumnName("tax_date2")
                    .HasColumnType("date");

                entity.Property(e => e.TrafficDocumentNumber)
                    .HasColumnName("traffic_document_number")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.VehicleGroup)
                    .HasColumnName("vehicle_group")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.VehicleOwnerAccountTitle)
                    .HasColumnName("vehicle_owner_account_title")
                    .HasColumnType("varchar(150)");

                entity.Property(e => e.VehicleOwnerCustomerId)
                    .HasColumnName("vehicle_owner_customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.VehicleOwnerType)
                    .HasColumnName("vehicle_owner_type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.VisaFinishTime)
                    .HasColumnName("visa_finish_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.VisaStartTime)
                    .HasColumnName("visa_start_time")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.DeletionUser)
                    .WithMany(p => p.Vehicle)
                    .HasForeignKey(d => d.DeletionUserId)
                    .HasConstraintName("vehicle_ibfk_1");
            });

            modelBuilder.Entity<VehicleDebit>(entity =>
            {
                entity.ToTable("vehicle_debit");

                entity.HasIndex(e => e.PersonelCardsId)
                    .HasName("personel_cards_id");

                entity.HasIndex(e => e.VehicleId)
                    .HasName("vehicle_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.InsDateTime)
                    .HasColumnName("ins_date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PersonelCardsId)
                    .HasColumnName("personel_cards_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.VehicleId)
                    .HasColumnName("vehicle_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.PersonelCards)
                    .WithMany(p => p.VehicleDebit)
                    .HasForeignKey(d => d.PersonelCardsId)
                    .HasConstraintName("vehicle_debit_ibfk_2");

                entity.HasOne(d => d.Vehicle)
                    .WithMany(p => p.VehicleDebit)
                    .HasForeignKey(d => d.VehicleId)
                    .HasConstraintName("vehicle_debit_ibfk_1");
            });

            modelBuilder.Entity<VehicleFuelExpense>(entity =>
            {
                entity.ToTable("vehicle_fuel_expense");

                entity.HasIndex(e => e.FuelCompanyCustomerId)
                    .HasName("fuel_company_customer_id");

                entity.HasIndex(e => e.FuelProductId)
                    .HasName("fuel_product_id");

                entity.HasIndex(e => e.IncomeAndExpenseTypesId)
                    .HasName("income_and_expense_types_id");

                entity.HasIndex(e => e.PersonelCardsId)
                    .HasName("personel_cards_id");

                entity.HasIndex(e => e.StorageId)
                    .HasName("storage_id");

                entity.HasIndex(e => e.VehicleId)
                    .HasName("vehicle_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DocumentNo)
                    .HasColumnName("document_no")
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.FuelCompanyCustomerId)
                    .HasColumnName("fuel_company_customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FuelProductId)
                    .HasColumnName("fuel_product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IncomeAndExpenseTypesId)
                    .HasColumnName("income_and_expense_types_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InsDateTime)
                    .HasColumnName("ins_date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Liter)
                    .HasColumnName("liter")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.PersonelCardsId)
                    .HasColumnName("personel_cards_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PurchaseReasons)
                    .HasColumnName("Purchase_Reasons")
                    .HasColumnType("varchar(255)")
                    .HasDefaultValueSql("'alım nedeni'");

                entity.Property(e => e.StorageId)
                    .HasColumnName("storage_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SupplyType)
                    .HasColumnName("supply_type")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.UnitPrice)
                    .HasColumnName("unit_price")
                    .HasColumnType("decimal(10,5)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.VehicleId)
                    .HasColumnName("vehicle_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.FuelCompanyCustomer)
                    .WithMany(p => p.VehicleFuelExpense)
                    .HasForeignKey(d => d.FuelCompanyCustomerId)
                    .HasConstraintName("vehicle_fuel_expense_ibfk_4");

                entity.HasOne(d => d.FuelProduct)
                    .WithMany(p => p.VehicleFuelExpense)
                    .HasForeignKey(d => d.FuelProductId)
                    .HasConstraintName("vehicle_fuel_expense_ibfk_5");

                entity.HasOne(d => d.IncomeAndExpenseTypes)
                    .WithMany(p => p.VehicleFuelExpense)
                    .HasForeignKey(d => d.IncomeAndExpenseTypesId)
                    .HasConstraintName("vehicle_fuel_expense_ibfk_2");

                entity.HasOne(d => d.PersonelCards)
                    .WithMany(p => p.VehicleFuelExpense)
                    .HasForeignKey(d => d.PersonelCardsId)
                    .HasConstraintName("vehicle_fuel_expense_ibfk_6");

                entity.HasOne(d => d.Storage)
                    .WithMany(p => p.VehicleFuelExpense)
                    .HasForeignKey(d => d.StorageId)
                    .HasConstraintName("vehicle_fuel_expense_ibfk_3");

                entity.HasOne(d => d.Vehicle)
                    .WithMany(p => p.VehicleFuelExpense)
                    .HasForeignKey(d => d.VehicleId)
                    .HasConstraintName("vehicle_fuel_expense_ibfk_1");
            });

            modelBuilder.Entity<VehicleKilometer>(entity =>
            {
                entity.ToTable("vehicle_kilometer");

                entity.HasIndex(e => e.VehicleId)
                    .HasName("vehicle_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DeletionUserId)
                    .HasColumnName("deletion_user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DetailId)
                    .HasColumnName("detail_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exp)
                    .HasColumnName("exp")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.InsDatetime)
                    .HasColumnName("ins_datetime")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.InsType)
                    .HasColumnName("ins_type")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.LastKm)
                    .HasColumnName("last_km")
                    .HasColumnType("int(11)");

                entity.Property(e => e.MeasurementDate)
                    .HasColumnName("measurement_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.VehicleId)
                    .HasColumnName("vehicle_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Vehicle)
                    .WithMany(p => p.VehicleKilometer)
                    .HasForeignKey(d => d.VehicleId)
                    .HasConstraintName("vehicle_kilometer_ibfk_1");
            });

            modelBuilder.Entity<VehicleProductExpense>(entity =>
            {
                entity.ToTable("vehicle_product_expense");

                entity.HasIndex(e => e.IncomeAndExpenseTypesId)
                    .HasName("income_and_expense_types_id");

                entity.HasIndex(e => e.PersonelCardsId)
                    .HasName("personel_cards_id");

                entity.HasIndex(e => e.ProductId)
                    .HasName("tire_product_id");

                entity.HasIndex(e => e.StorageId)
                    .HasName("storage_id");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.HasIndex(e => e.VehicleId)
                    .HasName("vehicle_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DocNumber)
                    .HasColumnName("doc_number")
                    .HasColumnType("varchar(16)");

                entity.Property(e => e.IncomeAndExpenseTypesId)
                    .HasColumnName("income_and_expense_types_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InsDateTime)
                    .HasColumnName("ins_date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PersonelCardsId)
                    .HasColumnName("personel_cards_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("int(11)");

                entity.Property(e => e.StorageId)
                    .HasColumnName("storage_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UnitPrice)
                    .HasColumnName("unit_price")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.VehicleId)
                    .HasColumnName("vehicle_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.IncomeAndExpenseTypes)
                    .WithMany(p => p.VehicleProductExpense)
                    .HasForeignKey(d => d.IncomeAndExpenseTypesId)
                    .HasConstraintName("vehicle_product_expense_ibfk_2");

                entity.HasOne(d => d.PersonelCards)
                    .WithMany(p => p.VehicleProductExpense)
                    .HasForeignKey(d => d.PersonelCardsId)
                    .HasConstraintName("vehicle_product_expense_ibfk_5");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.VehicleProductExpense)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("vehicle_product_expense_ibfk_4");

                entity.HasOne(d => d.Storage)
                    .WithMany(p => p.VehicleProductExpense)
                    .HasForeignKey(d => d.StorageId)
                    .HasConstraintName("vehicle_product_expense_ibfk_3");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.VehicleProductExpense)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("vehicle_product_expense_ibfk_6");

                entity.HasOne(d => d.Vehicle)
                    .WithMany(p => p.VehicleProductExpense)
                    .HasForeignKey(d => d.VehicleId)
                    .HasConstraintName("vehicle_product_expense_ibfk_1");
            });

            modelBuilder.Entity<VehicleTireExpense>(entity =>
            {
                entity.ToTable("vehicle_tire_expense");

                entity.HasIndex(e => e.IncomeAndExpenseTypesId)
                    .HasName("income_and_expense_types_id");

                entity.HasIndex(e => e.PersonelCardsId)
                    .HasName("personel_cards_id");

                entity.HasIndex(e => e.StorageId)
                    .HasName("storage_id");

                entity.HasIndex(e => e.TireProductId)
                    .HasName("tire_product_id");

                entity.HasIndex(e => e.UserId)
                    .HasName("user_id");

                entity.HasIndex(e => e.VehicleId)
                    .HasName("vehicle_id");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DateTime)
                    .HasColumnName("date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DocNumber)
                    .HasColumnName("doc_number")
                    .HasColumnType("varchar(16)");

                entity.Property(e => e.IncomeAndExpenseTypesId)
                    .HasColumnName("income_and_expense_types_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.InsDateTime)
                    .HasColumnName("ins_date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PersonelCardsId)
                    .HasColumnName("personel_cards_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("int(11)");

                entity.Property(e => e.StorageId)
                    .HasColumnName("storage_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TireLifeKm)
                    .HasColumnName("tire_life_km")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.TireProductId)
                    .HasColumnName("tire_product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UnitPrice)
                    .HasColumnName("unit_price")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.VehicleId)
                    .HasColumnName("vehicle_id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.IncomeAndExpenseTypes)
                    .WithMany(p => p.VehicleTireExpense)
                    .HasForeignKey(d => d.IncomeAndExpenseTypesId)
                    .HasConstraintName("vehicle_tire_expense_ibfk_2");

                entity.HasOne(d => d.PersonelCards)
                    .WithMany(p => p.VehicleTireExpense)
                    .HasForeignKey(d => d.PersonelCardsId)
                    .HasConstraintName("vehicle_tire_expense_ibfk_5");

                entity.HasOne(d => d.Storage)
                    .WithMany(p => p.VehicleTireExpense)
                    .HasForeignKey(d => d.StorageId)
                    .HasConstraintName("vehicle_tire_expense_ibfk_3");

                entity.HasOne(d => d.TireProduct)
                    .WithMany(p => p.VehicleTireExpense)
                    .HasForeignKey(d => d.TireProductId)
                    .HasConstraintName("vehicle_tire_expense_ibfk_4");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.VehicleTireExpense)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("vehicle_tire_expense_ibfk_6");

                entity.HasOne(d => d.Vehicle)
                    .WithMany(p => p.VehicleTireExpense)
                    .HasForeignKey(d => d.VehicleId)
                    .HasConstraintName("vehicle_tire_expense_ibfk_1");
            });

            modelBuilder.Entity<Waybill>(entity =>
            {
                entity.ToTable("waybill");

                entity.HasIndex(e => e.InsDateTime)
                    .HasName("ins_date_time");

                entity.HasIndex(e => e.Type)
                    .HasName("type");

                entity.HasIndex(e => e.WaybillProductsBasket)
                    .HasName("waybill_product_basket");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("customer_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Deletion)
                    .HasColumnName("deletion")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.InsDateTime)
                    .HasColumnName("ins_date_time")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.OrdersId)
                    .HasColumnName("orders_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Stats)
                    .HasColumnName("stats")
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.WaybillAmount)
                    .HasColumnName("waybill_amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.WaybillDateTime)
                    .HasColumnName("waybill_date_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.WaybillNumber)
                    .HasColumnName("waybill_number")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.WaybillProductsBasket)
                    .HasColumnName("waybill_products_basket")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.WaybillSerial)
                    .HasColumnName("waybill_serial")
                    .HasColumnType("varchar(5)");
            });

            modelBuilder.Entity<WaybillProducts>(entity =>
            {
                entity.ToTable("waybill_products");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ColorId)
                    .HasColumnName("color_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DiscountRate1)
                    .HasColumnName("discount_rate1")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.DiscountRate2)
                    .HasColumnName("discount_rate2")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.DiscountRate3)
                    .HasColumnName("discount_rate3")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.DiscountRate4)
                    .HasColumnName("discount_rate4")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.DiscountRate5)
                    .HasColumnName("discount_rate5")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.DiscountUnitPrice)
                    .HasColumnName("discount_unit_price")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.DiscountUnitPriceAmount)
                    .HasColumnName("discount_unit_price_amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ProductCode)
                    .HasColumnName("product_code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(140)");

                entity.Property(e => e.ProductType)
                    .HasColumnName("product_type")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.StorageId)
                    .HasColumnName("storage_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TaxAmount)
                    .HasColumnName("tax_amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.TaxRate)
                    .HasColumnName("tax_rate")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.TotalDiscontAmount)
                    .HasColumnName("total_discont_amount")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.UnitName)
                    .HasColumnName("unit_name")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.UnitPrice)
                    .HasColumnName("unit_price")
                    .HasColumnType("decimal(10,5)");

                entity.Property(e => e.UnitPriceTax)
                    .HasColumnName("unit_price_tax")
                    .HasColumnType("decimal(10,2)");
            });

            modelBuilder.Entity<WaybillProductsSub>(entity =>
            {
                entity.ToTable("waybill_products_sub");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Basket)
                    .HasColumnName("basket")
                    .HasColumnType("int(11)");

                entity.Property(e => e.GroupQty)
                    .HasColumnName("group_qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ProductCode)
                    .HasColumnName("product_code")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductName)
                    .HasColumnName("product_name")
                    .HasColumnType("varchar(140)");

                entity.Property(e => e.ProductType)
                    .HasColumnName("product_type")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Qty)
                    .HasColumnName("qty")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.UnitName)
                    .HasColumnName("unit_name")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.WaybillProductsBasketId)
                    .HasColumnName("waybill_products_basket_id")
                    .HasColumnType("int(11)");
            });
        }
    }
}
