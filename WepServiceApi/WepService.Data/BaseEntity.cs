﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WepService.Data
{
    public abstract class BaseEntity
    {
        public abstract int Id { get; set; }
        public abstract sbyte? Deletion { get; set; }

    }
}
