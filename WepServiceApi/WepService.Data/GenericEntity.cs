﻿using System;
//using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WepService.Data
{
    public abstract class GenericEntity : BaseEntity
    {
        [Required]
        [Column("Id")]
        public int CompanyId { get; set; }
        [ForeignKey("Id")]
        public CustomerEntity customer { get; set; }

        //[Required]
        //[Column("created_user_id")]
        //public int CreatedUserId { get; set; }
        //[ForeignKey("CreatedUserId")]
        //public virtual UserEntity CreatedUser { get; set; }

        [Column("lastChangedDate")]
        public DateTime LastChangedDate { get; set; }
    }
}
