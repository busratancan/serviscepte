﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class BusinessCreditcardOutput
    {
        public int Id { get; set; }
        public int? BusinessCreditcardId { get; set; }
        public string Exp { get; set; }
    }
}
