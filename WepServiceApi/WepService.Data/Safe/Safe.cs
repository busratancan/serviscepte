﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class Safe
    {
        public Safe()
        {
            Management = new HashSet<Management>();
            SafeInput = new HashSet<SafeInput>();
            SafeOutput = new HashSet<SafeOutput>();
        }

        public int Id { get; set; }
        public string SafeName { get; set; }
        public string CurrencyUnit { get; set; }
        public string Exp { get; set; }
        public string SafeCode { get; set; }
        public string SafeGroup { get; set; }
        public sbyte? Visible { get; set; }

        public ICollection<Management> Management { get; set; }
        public ICollection<SafeInput> SafeInput { get; set; }
        public ICollection<SafeOutput> SafeOutput { get; set; }
    }
}
