﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WepService.Data.Migrations
{
    public partial class Database : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "agricultural",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(type: "varchar(30)", nullable: true),
                    defination = table.Column<string>(type: "varchar(200)", nullable: true),
                    exp1 = table.Column<string>(type: "varchar(255)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    unit = table.Column<string>(type: "varchar(10)", nullable: true),
                    Deletion = table.Column<sbyte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_agricultural", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "agricultural_detail",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    formula_qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    product_name = table.Column<string>(type: "varchar(150)", nullable: true),
                    agricultural_id = table.Column<int>(type: "int(11)", nullable: true),
                    days = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    Deletion = table.Column<sbyte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_agricultural_detail", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "archive_groups",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    group_code = table.Column<string>(type: "varchar(100)", nullable: true),
                    group_name = table.Column<string>(type: "varchar(100)", nullable: true),
                    group_explanation = table.Column<string>(type: "varchar(255)", nullable: true),
                    for_customer = table.Column<string>(type: "varchar(10)", nullable: true),
                    for_personel = table.Column<string>(type: "varchar(10)", nullable: true),
                    for_vehicle = table.Column<string>(type: "varchar(10)", nullable: true),
                    Deletion = table.Column<sbyte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_archive_groups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "archive_management",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    document = table.Column<byte[]>(nullable: true),
                    doc_Type = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'1'"),
                    arcihive_groups_id = table.Column<int>(type: "int(11)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    doc_explanation = table.Column<string>(type: "varchar(200)", nullable: true),
                    insert_datetime = table.Column<DateTime>(type: "timestamp", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    arcive_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    doc_serial = table.Column<string>(type: "varchar(10)", nullable: true),
                    doc_numner = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_archive_management", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "bank",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    bank_name = table.Column<string>(type: "varchar(255)", nullable: true),
                    bank_explain = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bank", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "bank_account_in",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    bank_account_id = table.Column<string>(type: "varchar(255)", nullable: true),
                    currency = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: true),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    type = table.Column<int>(type: "int(11)", nullable: true),
                    detail_id = table.Column<string>(type: "varchar(255)", nullable: true),
                    date_time = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bank_account_in", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "bank_account_out",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    bank_account_id = table.Column<string>(type: "varchar(255)", nullable: true),
                    currency = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: true),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    detail_id = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<string>(type: "varchar(255)", nullable: true, defaultValueSql: "'3'"),
                    date_time = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bank_account_out", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "bank_department",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    bank_id = table.Column<int>(type: "int(11)", nullable: true),
                    bank_department = table.Column<string>(type: "varchar(200)", nullable: true),
                    department_tel = table.Column<string>(type: "varchar(15)", nullable: true),
                    department_contact_person = table.Column<string>(type: "varchar(60)", nullable: true),
                    exp = table.Column<string>(type: "varchar(260)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(260)", nullable: true),
                    department_fax = table.Column<string>(type: "varchar(16)", nullable: true),
                    department_adress = table.Column<string>(type: "varchar(300)", nullable: true),
                    department_mail = table.Column<string>(type: "varchar(140)", nullable: true),
                    country = table.Column<string>(type: "varchar(60)", nullable: true),
                    town = table.Column<string>(type: "varchar(50)", nullable: true),
                    bank_name = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bank_department", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "basket",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    basket_id = table.Column<int>(type: "int(11)", nullable: true),
                    unit_price = table.Column<decimal>(type: "decimal(12,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_basket", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "business_creditcard",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    bank_id = table.Column<int>(type: "int(11)", nullable: true),
                    card_no = table.Column<string>(type: "varchar(20)", nullable: true),
                    exp_date = table.Column<string>(type: "varchar(6)", nullable: true),
                    security_code = table.Column<string>(type: "varchar(10)", nullable: true),
                    card_holder = table.Column<string>(type: "varchar(200)", nullable: true),
                    card_type = table.Column<sbyte>(type: "tinyint(2)", nullable: true),
                    card_fee = table.Column<decimal>(type: "decimal(10,8)", nullable: true),
                    exp = table.Column<string>(type: "varchar(250)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(250)", nullable: true),
                    campaign_notes = table.Column<string>(type: "varchar(300)", nullable: true),
                    installment_notes = table.Column<string>(type: "varchar(300)", nullable: true),
                    min_payment = table.Column<decimal>(type: "decimal(10,8)", nullable: true),
                    interest_rate = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    the_last_payment_date = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_business_creditcard", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "business_creditcard_output",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    business_creditcard_id = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(250)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_business_creditcard_output", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "business_creditcard_output_payment_plan",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    number_of_installments = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    business_creditcard_id = table.Column<int>(type: "int(11)", nullable: true),
                    the_last_payment_date = table.Column<DateTime>(type: "date", nullable: true),
                    basket_id = table.Column<int>(type: "int(11)", nullable: true),
                    installments_currency = table.Column<decimal>(type: "decimal(10,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_business_creditcard_output_payment_plan", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "business_creditcard_output_payment_plan_temp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    number_of_installments = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    business_creditcard_id = table.Column<int>(type: "int(11)", nullable: true),
                    the_last_payment_date = table.Column<DateTime>(type: "date", nullable: true),
                    basket_id = table.Column<int>(type: "int(11)", nullable: true),
                    installments_currency = table.Column<decimal>(type: "decimal(10,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_business_creditcard_output_payment_plan_temp", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "caller_id_log",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    number = table.Column<string>(type: "varchar(12)", nullable: true),
                    line_number = table.Column<string>(type: "varchar(255)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_caller_id_log", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "check_temp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    bank_id = table.Column<int>(type: "int(11)", nullable: true),
                    currency = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    check_number = table.Column<int>(type: "int(11)", nullable: true),
                    check_bank_account = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(400)", nullable: true),
                    pay_date = table.Column<DateTime>(type: "date", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    bank_name = table.Column<string>(type: "varchar(60)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    customer_name = table.Column<string>(type: "varchar(100)", nullable: true),
                    op_Type = table.Column<string>(type: "varchar(15)", nullable: true),
                    check_bank_account_id = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_check_temp", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "coming_document",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    input_date = table.Column<DateTime>(type: "date", nullable: false),
                    document_date = table.Column<DateTime>(type: "date", nullable: false),
                    save_date = table.Column<DateTime>(type: "date", nullable: false),
                    user_id = table.Column<int>(type: "int(3)", nullable: false),
                    document_register_number = table.Column<string>(type: "varchar(100)", nullable: false),
                    document_source = table.Column<string>(type: "varchar(300)", nullable: false),
                    subject_code = table.Column<string>(type: "varchar(200)", nullable: false),
                    subject = table.Column<string>(type: "varchar(500)", nullable: false),
                    attachments = table.Column<string>(type: "varchar(500)", nullable: false),
                    file_path = table.Column<string>(type: "varchar(2000)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_coming_document", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "commercial_paper_temp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    currency = table.Column<decimal>(type: "decimal(18,5)", nullable: true),
                    pay_date = table.Column<DateTime>(type: "date", nullable: true),
                    notary_warning = table.Column<string>(type: "varchar(10)", nullable: true, defaultValueSql: "'False'"),
                    document_number = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(300)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    customer_name = table.Column<string>(type: "varchar(100)", nullable: true),
                    op_type = table.Column<string>(type: "varchar(20)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_commercial_paper_temp", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "currency",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Currency_Code = table.Column<string>(type: "varchar(25)", nullable: true),
                    Currency_Symbol = table.Column<string>(type: "varchar(10)", nullable: true),
                    TCMB_Find_Text = table.Column<string>(type: "varchar(90)", nullable: true),
                    Currency_Name = table.Column<string>(type: "varchar(85)", nullable: true),
                    Exp1 = table.Column<string>(type: "varchar(120)", nullable: true),
                    Exp2 = table.Column<string>(type: "varchar(120)", nullable: true),
                    Exp3 = table.Column<string>(type: "varchar(120)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_currency", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "currency_in",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Currency_Id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_currency_in", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "currency_out",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Currency_Id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_currency_out", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "currency_rates_of_exchange_temp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Currency_Id = table.Column<int>(type: "int(11)", nullable: true),
                    Currency_Name = table.Column<string>(type: "varchar(255)", nullable: true),
                    Currency_Code = table.Column<string>(type: "varchar(30)", nullable: true),
                    Ex_Date = table.Column<DateTime>(type: "date", nullable: true),
                    Currency_Buying = table.Column<decimal>(type: "decimal(10,5)", nullable: true),
                    Currency_Selling = table.Column<decimal>(type: "decimal(10,5)", nullable: true),
                    Basket = table.Column<int>(type: "int(11)", nullable: true),
                    Currency_Buying_Amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    Received_Currency = table.Column<decimal>(type: "decimal(10,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_currency_rates_of_exchange_temp", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "customer",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    main_customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    main_customer_id_temp = table.Column<int>(type: "int(11)", nullable: true),
                    account_title = table.Column<string>(type: "varchar(200)", nullable: true),
                    tax_office = table.Column<string>(type: "varchar(150)", nullable: true),
                    tax_number = table.Column<string>(type: "varchar(11)", nullable: true),
                    mobile_phone = table.Column<string>(type: "varchar(30)", nullable: true),
                    phone = table.Column<string>(type: "varchar(30)", nullable: true),
                    fax = table.Column<string>(type: "varchar(30)", nullable: true),
                    adress = table.Column<string>(type: "varchar(1000)", nullable: true),
                    explanation = table.Column<string>(type: "varchar(1000)", nullable: true),
                    account_grup = table.Column<string>(type: "varchar(50)", nullable: true),
                    account_group2 = table.Column<string>(type: "varchar(50)", nullable: true),
                    account_type = table.Column<string>(type: "varchar(60)", nullable: true),
                    customer_code = table.Column<string>(type: "varchar(20)", nullable: true),
                    contact_person = table.Column<string>(type: "varchar(400)", nullable: true),
                    task = table.Column<string>(type: "varchar(60)", nullable: true),
                    phone2 = table.Column<string>(type: "varchar(15)", nullable: true),
                    adress2 = table.Column<string>(type: "varchar(1000)", nullable: true),
                    risklimite = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    price_group = table.Column<string>(type: "varchar(15)", nullable: true),
                    explanation2 = table.Column<string>(type: "varchar(1000)", nullable: true),
                    explanation3 = table.Column<string>(type: "varchar(1000)", nullable: true),
                    customer_pic = table.Column<string>(type: "varchar(1500)", nullable: true),
                    sector = table.Column<string>(type: "varchar(150)", nullable: true),
                    region = table.Column<string>(type: "varchar(150)", nullable: true),
                    company_logo = table.Column<byte[]>(nullable: true),
                    ispersonel = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    private_code = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code2 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code3 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code4 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code5 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code6 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code7 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code8 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code9 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code10 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code11 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code12 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code13 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code14 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code15 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code16 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code17 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code18 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code19 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code20 = table.Column<string>(type: "varchar(15)", nullable: true),
                    pricing_id = table.Column<int>(type: "int(11)", nullable: true),
                    pricing_name = table.Column<string>(type: "varchar(50)", nullable: true),
                    price_privileged = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    information_notes = table.Column<string>(type: "text", nullable: true),
                    picture = table.Column<byte[]>(nullable: true),
                    Sales_Block = table.Column<string>(type: "varchar(11)", nullable: true),
                    route_id = table.Column<int>(type: "int(3)", nullable: true, defaultValueSql: "'0'"),
                    route_order = table.Column<int>(type: "int(3)", nullable: true, defaultValueSql: "'0'"),
                    visible = table.Column<sbyte>(type: "tinyint(1)", nullable: true, defaultValueSql: "'0'"),
                    current_input_id = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    current_output_id = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    fix_discount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    latitude = table.Column<string>(type: "varchar(110)", nullable: true),
                    longitude = table.Column<string>(type: "varchar(110)", nullable: true),
                    sales_invoice_gross_price = table.Column<string>(type: "varchar(50)", nullable: true),
                    sales_invoice_retail_price = table.Column<string>(type: "varchar(50)", nullable: true),
                    elevator_fault = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    withholding_rate = table.Column<int>(type: "int(1)", nullable: true, defaultValueSql: "'0'"),
                    mail_adress = table.Column<string>(type: "varchar(120)", nullable: true),
                    Deletion = table.Column<sbyte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "customer_bank",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    customer_id = table.Column<int>(type: "int(11)", nullable: false),
                    account_holder = table.Column<string>(type: "varchar(300)", nullable: false),
                    bank_id = table.Column<int>(type: "int(11)", nullable: false),
                    bank_department = table.Column<string>(type: "varchar(200)", nullable: true),
                    account_number = table.Column<string>(type: "varchar(30)", nullable: true),
                    ad_account_number = table.Column<string>(type: "varchar(30)", nullable: true),
                    iban = table.Column<string>(type: "varchar(30)", nullable: true),
                    customer_number = table.Column<int>(type: "int(30)", nullable: true),
                    department_tel = table.Column<string>(type: "varchar(15)", nullable: true),
                    department_contact_person = table.Column<string>(type: "varchar(60)", nullable: true),
                    exp = table.Column<string>(type: "varchar(260)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(260)", nullable: true),
                    department_fax = table.Column<string>(type: "varchar(16)", nullable: true),
                    department_adress = table.Column<string>(type: "varchar(300)", nullable: true),
                    department_mail = table.Column<string>(type: "varchar(140)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_bank", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "customer_correspondence_report_files",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    exp = table.Column<string>(type: "varchar(100)", nullable: true),
                    file_name = table.Column<string>(type: "varchar(255)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_correspondence_report_files", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "customer_creditcard",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    customer_id = table.Column<int>(type: "int(11)", nullable: false),
                    bank_id = table.Column<int>(type: "int(11)", nullable: true),
                    card_no = table.Column<string>(type: "varchar(20)", nullable: true),
                    exp_date = table.Column<string>(type: "varchar(6)", nullable: true),
                    security_code = table.Column<string>(type: "varchar(10)", nullable: true),
                    card_holder = table.Column<string>(type: "varchar(200)", nullable: true),
                    card_type = table.Column<sbyte>(type: "tinyint(2)", nullable: true),
                    card_fee = table.Column<decimal>(type: "decimal(10,8)", nullable: true),
                    exp = table.Column<string>(type: "varchar(250)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(250)", nullable: true),
                    campaign_notes = table.Column<string>(type: "varchar(300)", nullable: true),
                    installment_notes = table.Column<string>(type: "varchar(300)", nullable: true),
                    min_payment = table.Column<decimal>(type: "decimal(10,8)", nullable: true),
                    interest_rate = table.Column<decimal>(type: "decimal(10,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_creditcard", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "customer_document_definitions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    datetime = table.Column<DateTime>(type: "datetime", nullable: true),
                    defination = table.Column<string>(type: "varchar(200)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    doc_group = table.Column<string>(type: "varchar(100)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(1)", nullable: true, defaultValueSql: "'0'"),
                    ins_datetime = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    total_page = table.Column<int>(type: "int(3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_document_definitions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "customer_personel_activity",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    customer_personel_id = table.Column<int>(type: "int(11)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    dateofentry = table.Column<DateTime>(type: "datetime", nullable: true),
                    dateofdeparture = table.Column<DateTime>(type: "datetime", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    daily_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    shift_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    hours_of_overtime = table.Column<string>(type: "varchar(255)", nullable: true),
                    daily_working_hours = table.Column<int>(type: "int(11)", nullable: true),
                    daily_working_minutes = table.Column<int>(type: "int(11)", nullable: true),
                    daily_working_second = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_personel_activity", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "customer_personel_activity_temp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    customer_personel_id = table.Column<int>(type: "int(11)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    dateofentry = table.Column<DateTime>(type: "datetime", nullable: true),
                    dateofdeparture = table.Column<DateTime>(type: "datetime", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    daily_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    shift_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_personel_activity_temp", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "device_location",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    device_imei = table.Column<string>(type: "varchar(17)", nullable: true),
                    latitude = table.Column<string>(type: "varchar(30)", nullable: true),
                    longitude = table.Column<string>(type: "varchar(30)", nullable: true),
                    ins_date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_device_location", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "documents",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    total_page = table.Column<int>(type: "int(10)", nullable: true),
                    page = table.Column<int>(type: "int(11)", nullable: true),
                    document = table.Column<byte[]>(nullable: true),
                    customer_document_definitions_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(1)", nullable: true, defaultValueSql: "'0'"),
                    basket = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_documents", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "elevator_card_parameters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(type: "varchar(100)", nullable: true),
                    defination = table.Column<string>(type: "varchar(200)", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    control_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    elevator_order = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_elevator_card_parameters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "elevator_fault_intervention_definitions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(type: "varchar(100)", nullable: true),
                    define = table.Column<string>(type: "varchar(200)", nullable: true),
                    exp1 = table.Column<string>(type: "varchar(255)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_elevator_fault_intervention_definitions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "elevator_installation_list",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    offer_temp_id = table.Column<int>(type: "int(11)", nullable: true),
                    contract_date = table.Column<string>(type: "varchar(255)", nullable: true),
                    stats_type = table.Column<string>(type: "varchar(255)", nullable: true, defaultValueSql: "'0'"),
                    responsible_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    Contractor_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    notes = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_elevator_installation_list", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "elevator_offer_selected_product_properties_temp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    property_id = table.Column<int>(type: "int(11)", nullable: true),
                    sub_property_id = table.Column<int>(type: "int(11)", nullable: true),
                    property_name = table.Column<string>(type: "varchar(100)", nullable: true),
                    sub_property_name = table.Column<string>(type: "varchar(100)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    offer_group_order_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    tab_order = table.Column<int>(type: "int(11)", nullable: true),
                    properties_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_elevator_offer_selected_product_properties_temp", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "elevator_offer_selected_product_sizes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    offer_group_order_id = table.Column<int>(type: "int(11)", nullable: true),
                    tab_order = table.Column<int>(type: "int(11)", nullable: true),
                    size = table.Column<string>(type: "varchar(100)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_elevator_offer_selected_product_sizes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "elevator_offer_temp_product_list",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    group_name = table.Column<string>(type: "varchar(150)", nullable: true),
                    size = table.Column<string>(type: "varchar(100)", nullable: true),
                    qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    selected_mark = table.Column<string>(type: "varchar(30)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    offer_group = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    application_part = table.Column<sbyte>(type: "tinyint(3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_elevator_offer_temp_product_list", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "elevator_revision_offer",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    currency = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    elevator_revision_offer_parameters_basket = table.Column<int>(type: "int(11)", nullable: true),
                    stats = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    elevator_control_history_id = table.Column<int>(type: "int(11)", nullable: true),
                    offer_date = table.Column<DateTime>(type: "date", nullable: true),
                    labor_costs = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    profit = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    target_color = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    status_color = table.Column<sbyte>(type: "tinyint(3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_elevator_revision_offer", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "elevator_revision_parameters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(type: "varchar(100)", nullable: true),
                    defination = table.Column<string>(type: "varchar(200)", nullable: true),
                    offer_caption = table.Column<string>(type: "varchar(255)", nullable: true),
                    fee = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    max_discount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    offer_visible = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    cost = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    workmanship = table.Column<decimal>(type: "decimal(10,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_elevator_revision_parameters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "elevator_sub_control_history",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    elevator_control_history_id = table.Column<int>(type: "int(11)", nullable: true),
                    contral_date = table.Column<DateTime>(type: "date", nullable: true),
                    notes = table.Column<string>(type: "varchar(255)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    ins_date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    control_result = table.Column<sbyte>(type: "tinyint(3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_elevator_sub_control_history", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "elevator_subcontractor_defines",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_elevator_subcontractor_defines", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "email_parameters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    server_name = table.Column<string>(type: "varchar(255)", nullable: true),
                    port = table.Column<int>(type: "int(11)", nullable: true),
                    user_name = table.Column<string>(type: "varchar(255)", nullable: true),
                    connection_method = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    password = table.Column<string>(type: "varchar(255)", nullable: true),
                    from_exp = table.Column<string>(type: "varchar(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_email_parameters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "endofthedayreport",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    type = table.Column<string>(type: "varchar(15)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    currency = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    sub_type = table.Column<string>(type: "varchar(15)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_endofthedayreport", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "expense",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    in_ex_types_id = table.Column<string>(type: "varchar(255)", nullable: true),
                    detail_id = table.Column<int>(type: "int(11)", nullable: true),
                    currency = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    ins_date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    pay_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    pay_detail_id = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    doc_no = table.Column<string>(type: "varchar(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_expense", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "expiring",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    expiry_code = table.Column<string>(type: "varchar(50)", nullable: true),
                    expiry_description = table.Column<string>(type: "varchar(150)", nullable: true),
                    day_option = table.Column<int>(type: "int(11)", nullable: true),
                    month_interest = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    exp1 = table.Column<string>(type: "varchar(150)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(150)", nullable: true),
                    exp3 = table.Column<string>(type: "varchar(150)", nullable: true),
                    expiring_days = table.Column<int>(type: "int(11)", nullable: true),
                    pricing_id = table.Column<int>(type: "int(11)", nullable: true),
                    pricing_name = table.Column<string>(type: "varchar(30)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_expiring", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "farm_animals",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    farms_id = table.Column<int>(type: "int(11)", nullable: true),
                    farms_name = table.Column<string>(type: "varchar(100)", nullable: true),
                    farm_paddock_id = table.Column<int>(type: "int(11)", nullable: true),
                    farm_paddock_defination = table.Column<string>(type: "varchar(100)", nullable: true),
                    code = table.Column<string>(type: "varchar(50)", nullable: true),
                    special_code = table.Column<string>(type: "varchar(50)", nullable: true),
                    mother = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'-1'"),
                    father = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'-1'"),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    type = table.Column<string>(type: "varchar(30)", nullable: true),
                    birth_date = table.Column<DateTime>(type: "date", nullable: true),
                    gender = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    live_status = table.Column<sbyte>(type: "tinyint(3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_farm_animals", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "farm_feed_activity",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    paddock_id = table.Column<int>(type: "int(11)", nullable: true),
                    paddock_name = table.Column<string>(type: "varchar(100)", nullable: true),
                    date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    farm_feed_mixture_id = table.Column<int>(type: "int(11)", nullable: true),
                    farm_feed_mixture_name = table.Column<string>(type: "varchar(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_farm_feed_activity", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "farm_feed_mixture",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    mixture_code = table.Column<string>(type: "varchar(100)", nullable: true),
                    mixture_name = table.Column<string>(type: "varchar(20)", nullable: true),
                    farm_paddock_id = table.Column<int>(type: "int(11)", nullable: true),
                    farm_paddock_name = table.Column<string>(type: "varchar(150)", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    feed_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_farm_feed_mixture", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "farm_veterinary",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    account_title = table.Column<string>(type: "varchar(255)", nullable: true),
                    veterinary_code = table.Column<string>(type: "varchar(50)", nullable: true),
                    exp1 = table.Column<string>(type: "varchar(255)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_farm_veterinary", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "farm_veterinary_applications",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    app_code = table.Column<string>(type: "varchar(75)", nullable: true),
                    app_defination = table.Column<string>(type: "varchar(200)", nullable: true),
                    app_type = table.Column<string>(type: "varchar(75)", nullable: true),
                    exp1 = table.Column<string>(type: "varchar(255)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    app_type_code = table.Column<sbyte>(type: "tinyint(3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_farm_veterinary_applications", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "farms",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    farm_code = table.Column<string>(type: "varchar(20)", nullable: true),
                    farm_defination = table.Column<string>(type: "varchar(150)", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    m2 = table.Column<decimal>(type: "decimal(10,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_farms", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "form_button_defines",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Form_Name = table.Column<string>(type: "varchar(60)", nullable: true),
                    Button_Name = table.Column<string>(type: "varchar(65)", nullable: true),
                    Button_Caption = table.Column<string>(type: "varchar(110)", nullable: true),
                    ShortCut = table.Column<string>(type: "varchar(15)", nullable: true),
                    User_Id = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_form_button_defines", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "general_parameters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ask_password_on_acces_personel_card = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    access_personel_card_password = table.Column<string>(type: "varchar(100)", nullable: true),
                    servis_cepte_logo_base_64 = table.Column<byte[]>(nullable: true),
                    push_company_code = table.Column<string>(type: "varchar(50)", nullable: true),
                    company_name = table.Column<string>(type: "varchar(255)", nullable: true),
                    fuel_purchase_reasons = table.Column<string>(type: "text", nullable: true),
                    mp_top1 = table.Column<string>(type: "varchar(255)", nullable: true),
                    mp_top2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    mp_bottom1 = table.Column<string>(type: "varchar(255)", nullable: true),
                    mp_bottom2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    service_customer_current_explanation = table.Column<string>(type: "varchar(255)", nullable: true, defaultValueSql: "'{services_open_id} Numaralı {year}-{month} Ayı {service_point_name} Servis Tutarı'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_general_parameters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "income",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    in_ex_types_id = table.Column<string>(type: "varchar(255)", nullable: true),
                    detail_id = table.Column<int>(type: "int(11)", nullable: true),
                    currency = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    ins_date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    pay_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    pay_detail_id = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    doc_no = table.Column<string>(type: "varchar(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_income", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "installments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    installments_plan_basket_id = table.Column<int>(type: "int(11)", nullable: true),
                    installments_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    invoice_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    total_currency = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    total_interest = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    total_interest_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    down_payment_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    down_currency = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    down_detail_id = table.Column<int>(type: "int(11)", nullable: true),
                    installment_assurance = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    installment_assurance_id = table.Column<int>(type: "int(11)", nullable: true),
                    installments_source = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    installments_source_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_installments", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "installments_plan",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    number_of_installments = table.Column<int>(type: "int(11)", nullable: true),
                    installments_currency = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    pay_date = table.Column<DateTime>(type: "date", nullable: true),
                    basket_id = table.Column<int>(type: "int(11)", nullable: true),
                    the_difference_in_days = table.Column<int>(type: "int(11)", nullable: true),
                    interest_rate = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    amount_of_interest = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    installments_currency_interest = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    ins_date = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_installments_plan", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "installments_plan_temp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    number_of_installments = table.Column<int>(type: "int(11)", nullable: true),
                    installments_currency = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    pay_date = table.Column<DateTime>(type: "date", nullable: true),
                    basket_id = table.Column<int>(type: "int(11)", nullable: true),
                    the_difference_in_days = table.Column<int>(type: "int(11)", nullable: true),
                    interest_rate = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    amount_of_interest = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    installments_currency_interest = table.Column<decimal>(type: "decimal(10,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_installments_plan_temp", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "invoice",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    type = table.Column<int>(type: "int(11)", nullable: true),
                    invoice_product_basket = table.Column<int>(type: "int(11)", nullable: true),
                    date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    invoice_date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    barcode = table.Column<string>(type: "varchar(50)", nullable: true),
                    invoice_serial = table.Column<string>(type: "varchar(5)", nullable: true),
                    invoice_number = table.Column<string>(type: "varchar(10)", nullable: true),
                    sub_customer_distrubite = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    pay_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    pay_detail_id = table.Column<int>(type: "int(10)", nullable: true),
                    pricing_id = table.Column<int>(type: "int(11)", nullable: true),
                    expiring_id = table.Column<int>(type: "int(11)", nullable: true),
                    stats = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'1'"),
                    detail_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    if_edit_new_invoice_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: true),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    pay_door = table.Column<string>(type: "varchar(25)", nullable: true, defaultValueSql: "'Hayir'"),
                    pay_method = table.Column<string>(type: "varchar(50)", nullable: true),
                    Shipment_Stats = table.Column<int>(type: "int(3)", nullable: true, defaultValueSql: "'0'"),
                    Customer_Current_Stats = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'1'"),
                    service_invoice = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_invoice", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "invoice_shipment_trace_temp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    invoice_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    storage_id = table.Column<int>(type: "int(11)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    product_name = table.Column<string>(type: "varchar(150)", nullable: true),
                    qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    storage_name = table.Column<string>(type: "varchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_invoice_shipment_trace_temp", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "label_temp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    barcode = table.Column<string>(type: "varchar(50)", nullable: true),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_name = table.Column<string>(type: "varchar(100)", nullable: true),
                    shelf_qty = table.Column<int>(type: "int(11)", nullable: true),
                    ticket_qty = table.Column<int>(type: "int(11)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    color_id = table.Column<int>(type: "int(11)", nullable: true),
                    price = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    ean = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    Currency_Symbol = table.Column<string>(type: "varchar(10)", nullable: true),
                    weight = table.Column<decimal>(type: "decimal(10,2)", nullable: true, defaultValueSql: "'0.00'"),
                    discount_price = table.Column<decimal>(type: "decimal(10,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_label_temp", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "licance_info",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    ks = table.Column<string>(type: "varchar(255)", nullable: true),
                    vs = table.Column<string>(type: "varchar(255)", nullable: true),
                    message = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_licance_info", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "load_activity",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    doc_number = table.Column<string>(type: "varchar(20)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    vehicle_id = table.Column<int>(type: "int(11)", nullable: true),
                    personel_id = table.Column<int>(type: "int(11)", nullable: true),
                    list_basket_id = table.Column<int>(type: "int(11)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    ins_date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: true),
                    date_time = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_load_activity", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "load_activity_list",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    load_type_definition_id = table.Column<int>(type: "int(11)", nullable: true),
                    load_type_defination = table.Column<string>(type: "varchar(150)", nullable: true),
                    sender_load_location_definations_id = table.Column<int>(type: "int(11)", nullable: true),
                    sender_load_location_definations = table.Column<string>(type: "varchar(150)", nullable: true),
                    receiver_load_location_definations_id = table.Column<int>(type: "int(11)", nullable: true),
                    receiver_load_location_definations = table.Column<string>(type: "varchar(150)", nullable: true),
                    qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    km = table.Column<int>(type: "int(11)", nullable: true),
                    price_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    unit_price = table.Column<decimal>(type: "decimal(10,5)", nullable: true),
                    rent_unit_price = table.Column<decimal>(type: "decimal(10,5)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    price_type_exp = table.Column<string>(type: "varchar(100)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_load_activity_list", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "load_document_definitions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(type: "varchar(100)", nullable: true),
                    defination = table.Column<string>(type: "varchar(100)", nullable: true),
                    color = table.Column<string>(type: "varchar(100)", nullable: true),
                    exp = table.Column<string>(type: "varchar(200)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_load_document_definitions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "load_location_definations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(type: "varchar(100)", nullable: true),
                    defination = table.Column<string>(type: "varchar(100)", nullable: true),
                    color = table.Column<string>(type: "varchar(100)", nullable: true),
                    exp = table.Column<string>(type: "varchar(200)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_load_location_definations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "load_package_definitions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(type: "varchar(100)", nullable: true),
                    defination = table.Column<string>(type: "varchar(100)", nullable: true),
                    color = table.Column<string>(type: "varchar(100)", nullable: true),
                    exp = table.Column<string>(type: "varchar(200)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_load_package_definitions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "load_status_definitions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(type: "varchar(100)", nullable: true),
                    defination = table.Column<string>(type: "varchar(100)", nullable: true),
                    color = table.Column<string>(type: "varchar(100)", nullable: true),
                    exp = table.Column<string>(type: "varchar(200)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_load_status_definitions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "load_type_definition",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(type: "varchar(100)", nullable: true),
                    defination = table.Column<string>(type: "varchar(100)", nullable: true),
                    color = table.Column<string>(type: "varchar(100)", nullable: true),
                    exp = table.Column<string>(type: "varchar(200)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_load_type_definition", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "load_units",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(type: "varchar(100)", nullable: true),
                    defination = table.Column<string>(type: "varchar(100)", nullable: true),
                    color = table.Column<string>(type: "varchar(100)", nullable: true),
                    exp = table.Column<string>(type: "varchar(200)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_load_units", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "management_roles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    role_code = table.Column<string>(type: "varchar(100)", nullable: true),
                    role_defination = table.Column<string>(type: "varchar(255)", nullable: true),
                    exp1 = table.Column<string>(type: "varchar(255)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_management_roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "message",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    sender_id = table.Column<int>(type: "int(11)", nullable: true),
                    receiver = table.Column<int>(type: "int(11)", nullable: true),
                    status = table.Column<sbyte>(type: "tinyint(1)", nullable: true),
                    message = table.Column<string>(type: "varchar(1200)", nullable: true),
                    support_id = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    date_time = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_message", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "mobile_scale_product_purchase_temp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Mobile_Scale_Customer_Id = table.Column<int>(type: "int(11)", nullable: true),
                    Customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    Kg = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    Litre = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    Densities = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    Basket = table.Column<int>(type: "int(11)", nullable: true),
                    Milking_Time = table.Column<DateTime>(type: "timestamp", nullable: true),
                    Pickup_Time = table.Column<DateTime>(type: "timestamp", nullable: true),
                    mobile_scale_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    mobile_scale_product_purchase_id = table.Column<int>(type: "int(11)", nullable: true),
                    stats = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    Device_Id = table.Column<int>(type: "int(11)", nullable: true),
                    Verification_Number = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mobile_scale_product_purchase_temp", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "orders_products",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    unit_price = table.Column<decimal>(type: "decimal(10,5)", nullable: true),
                    unit_price_tax = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    product_name = table.Column<string>(type: "varchar(140)", nullable: true),
                    product_code = table.Column<string>(type: "varchar(100)", nullable: true),
                    barcode = table.Column<string>(type: "varchar(100)", nullable: true),
                    amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    tax_rate = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    tax_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    discount_rate1 = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    discount_rate2 = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    discount_rate3 = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    discount_rate4 = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    discount_rate5 = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    total_discont_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    unit_name = table.Column<string>(type: "varchar(20)", nullable: true),
                    discount_unit_price = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    discount_unit_price_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    storage_id = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    color_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_type = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_orders_products", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "orders_products_sub",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    product_name = table.Column<string>(type: "varchar(140)", nullable: true),
                    product_code = table.Column<string>(type: "varchar(100)", nullable: true),
                    barcode = table.Column<string>(type: "varchar(100)", nullable: true),
                    unit_name = table.Column<string>(type: "varchar(20)", nullable: true),
                    group_qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    order_products_basket_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_type = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_orders_products_sub", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "orders_products_sub_main",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    product_name = table.Column<string>(type: "varchar(140)", nullable: true),
                    product_code = table.Column<string>(type: "varchar(100)", nullable: true),
                    barcode = table.Column<string>(type: "varchar(100)", nullable: true),
                    unit_name = table.Column<string>(type: "varchar(20)", nullable: true),
                    group_qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    waybill_products_basket_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_type = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_orders_products_sub_main", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "personel_holiday_parameters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    date = table.Column<DateTime>(type: "date", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    ins_date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_personel_holiday_parameters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "personel_permission_day_parameters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Monday = table.Column<sbyte>(type: "tinyint(3)", nullable: false, defaultValueSql: "'0'"),
                    Tuesday = table.Column<sbyte>(type: "tinyint(3)", nullable: false, defaultValueSql: "'0'"),
                    Wednesday = table.Column<sbyte>(type: "tinyint(3)", nullable: false, defaultValueSql: "'0'"),
                    Thursday = table.Column<sbyte>(type: "tinyint(3)", nullable: false, defaultValueSql: "'0'"),
                    Friday = table.Column<sbyte>(type: "tinyint(3)", nullable: false, defaultValueSql: "'0'"),
                    Saturday = table.Column<sbyte>(type: "tinyint(3)", nullable: false, defaultValueSql: "'0'"),
                    Sunday = table.Column<sbyte>(type: "tinyint(3)", nullable: false, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_personel_permission_day_parameters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "personel_permission_parameters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    year = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    day = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_personel_permission_parameters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "premium_tracking",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_premium_tracking", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "premium_tracking_temp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    scan_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    scan_user = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_premium_tracking_temp", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "pricing",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    price_code = table.Column<string>(type: "varchar(50)", nullable: true),
                    price_description = table.Column<string>(type: "varchar(150)", nullable: true),
                    increment = table.Column<decimal>(type: "decimal(10,2)", nullable: true, defaultValueSql: "'0.00'"),
                    discount = table.Column<decimal>(type: "decimal(10,2)", nullable: true, defaultValueSql: "'0.00'"),
                    exp1 = table.Column<string>(type: "varchar(150)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(150)", nullable: true),
                    exp3 = table.Column<string>(type: "varchar(150)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_pricing", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "product_color",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    color_name = table.Column<string>(type: "varchar(50)", nullable: true),
                    barcode = table.Column<string>(type: "varchar(30)", nullable: true),
                    color1 = table.Column<string>(type: "varchar(15)", nullable: true),
                    color2 = table.Column<string>(type: "varchar(15)", nullable: true),
                    color3 = table.Column<string>(type: "varchar(15)", nullable: true),
                    color4 = table.Column<string>(type: "varchar(15)", nullable: true),
                    color5 = table.Column<string>(type: "varchar(15)", nullable: true),
                    main_product_color_id = table.Column<int>(type: "int(11)", nullable: true),
                    main_product_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_color", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "product_groups",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    group_name = table.Column<string>(type: "varchar(100)", nullable: true),
                    parent_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_groups", x => x.Id);
                    table.ForeignKey(
                        name: "product_groups_ibfk_1",
                        column: x => x.parent_id,
                        principalTable: "product_groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "product_pricing_change_history",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    raise_rate = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    discount_rate = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    purchase_price = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    gross_sales_price = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    retail_price = table.Column<decimal>(type: "decimal(10,5)", nullable: true),
                    retail_price_tax = table.Column<decimal>(type: "decimal(10,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_pricing_change_history", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "product_serials_temp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    basket_id = table.Column<int>(type: "int(11)", nullable: true),
                    serial_code = table.Column<string>(type: "varchar(50)", nullable: true),
                    ins_date = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_serials_temp", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "product_sizes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    size = table.Column<string>(type: "varchar(15)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    ins_datetime = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    group_name = table.Column<string>(type: "varchar(150)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_sizes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "product_tax_list2",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    tax_name = table.Column<string>(type: "varchar(11)", nullable: true),
                    tax_rate = table.Column<decimal>(type: "decimal(10,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_tax_list2", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "product2",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_name = table.Column<string>(type: "varchar(500)", nullable: true),
                    product_type = table.Column<string>(type: "varchar(500)", nullable: true),
                    mark = table.Column<string>(type: "varchar(300)", nullable: true),
                    unit = table.Column<string>(type: "varchar(400)", nullable: true),
                    max_discount = table.Column<string>(type: "varchar(5)", nullable: true),
                    critical_level = table.Column<string>(type: "varchar(11)", nullable: true),
                    price = table.Column<decimal>(type: "decimal(12,2)", nullable: true),
                    price_tax_inc = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    packaging_status = table.Column<string>(type: "varchar(10)", nullable: true),
                    max_level = table.Column<string>(type: "varchar(11)", nullable: true),
                    barcode = table.Column<string>(type: "varchar(200)", nullable: true),
                    product_category = table.Column<string>(type: "varchar(500)", nullable: true),
                    product_sub_categories = table.Column<string>(type: "varchar(600)", nullable: true),
                    explanation = table.Column<string>(type: "varchar(1000)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product2", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "production_history",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    production_recipe_defines_id = table.Column<int>(type: "int(11)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    production_qty = table.Column<decimal>(type: "decimal(10,5)", nullable: true),
                    production_recipe_defines = table.Column<string>(type: "varchar(255)", nullable: true),
                    unit = table.Column<string>(type: "varchar(20)", nullable: true),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    ins_date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_production_history", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "report_designs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    type = table.Column<int>(type: "int(11)", nullable: true),
                    file_url = table.Column<string>(type: "varchar(255)", nullable: true),
                    defination = table.Column<string>(type: "varchar(100)", nullable: true),
                    file = table.Column<byte[]>(nullable: true),
                    isondatabase = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_report_designs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "restaurant_department",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    department_name = table.Column<string>(type: "varchar(100)", nullable: true),
                    visible = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_restaurant_department", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "restaurant_devices",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    authority_date = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    mac_adress = table.Column<string>(type: "varchar(120)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_restaurant_devices", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "restaurant_discount_rates",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    rate = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_restaurant_discount_rates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "restaurant_order_list_temp_deletes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_name = table.Column<string>(type: "varchar(255)", nullable: true),
                    qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    restaurant_order_temp_basket_id = table.Column<int>(type: "int(11)", nullable: true),
                    status = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    portion = table.Column<decimal>(type: "decimal(10,1)", nullable: true),
                    group2_id = table.Column<int>(type: "int(11)", nullable: true),
                    group3_id = table.Column<int>(type: "int(11)", nullable: true),
                    price = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion_user_id = table.Column<int>(type: "int(10)", nullable: true),
                    deletion_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_restaurant_order_list_temp_deletes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "restaurant_parameters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    order_product_group_id = table.Column<int>(type: "int(11)", nullable: true),
                    order_product_group_name = table.Column<string>(type: "varchar(255)", nullable: true),
                    customer_on_trust_group = table.Column<string>(type: "varchar(100)", nullable: true),
                    receipt_top_note1 = table.Column<string>(type: "varchar(255)", nullable: true),
                    receipt_top_note2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    receipt_bottom_note1 = table.Column<string>(type: "varchar(255)", nullable: true),
                    receipt_bottom_note2 = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_restaurant_parameters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "restaurant_premium_calc",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    restaurant_premium_calc_temp_basket_id = table.Column<int>(type: "int(11)", nullable: true),
                    ins_date = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    exp = table.Column<string>(type: "varchar(150)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_restaurant_premium_calc", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "restaurant_premium_calc_temp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    restaurant_premium_defines_sub_conditions_id = table.Column<int>(type: "int(11)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    qty = table.Column<int>(type: "int(11)", nullable: true),
                    total_premium_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    unit_premium_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    restaurant_order_basket_id = table.Column<int>(type: "int(11)", nullable: true),
                    condition_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_restaurant_premium_calc_temp", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "restaurant_premium_defines",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    pre_code = table.Column<string>(type: "varchar(150)", nullable: true),
                    pre_defination = table.Column<string>(type: "varchar(255)", nullable: true),
                    group2_condition_id = table.Column<int>(type: "int(11)", nullable: true),
                    condition_based = table.Column<string>(type: "varchar(30)", nullable: true, defaultValueSql: "'True'"),
                    sales_based = table.Column<string>(type: "varchar(30)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_restaurant_premium_defines", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "restaurant_premium_defines_conditions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    source_id = table.Column<int>(type: "int(11)", nullable: true),
                    source_type = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    condition = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    qty = table.Column<int>(type: "int(11)", nullable: true),
                    qty_condition = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    restaurant_premium_defines_id = table.Column<int>(type: "int(11)", nullable: true),
                    condition_define = table.Column<string>(type: "varchar(255)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    premium_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    waiter_percent = table.Column<decimal>(type: "decimal(10,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_restaurant_premium_defines_conditions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "restaurant_premium_defines_non_conditions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    restaurant_premium_defines_conditions_id = table.Column<int>(type: "int(11)", nullable: true),
                    source_id = table.Column<int>(type: "int(11)", nullable: true),
                    source_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    condition = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    condition_define = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_restaurant_premium_defines_non_conditions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "restaurant_premium_defines_sub_conditions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    premium_define_Code = table.Column<string>(type: "varchar(150)", nullable: true),
                    premium_define = table.Column<string>(type: "varchar(150)", nullable: true),
                    min_conditions = table.Column<int>(type: "int(11)", nullable: true),
                    premium_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    waiter_percent = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    pool_percent = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    restaurant_premium_defines_id = table.Column<int>(type: "int(11)", nullable: true),
                    condition_form = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_restaurant_premium_defines_sub_conditions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "restaurant_speed_group_buttons",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    button_name = table.Column<string>(type: "varchar(100)", nullable: true),
                    grup2_id = table.Column<int>(type: "int(11)", nullable: true),
                    button_caption = table.Column<string>(type: "varchar(100)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_restaurant_speed_group_buttons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "restaurant_tables",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    table_code = table.Column<string>(type: "varchar(255)", nullable: true),
                    departman_id = table.Column<int>(type: "int(11)", nullable: true),
                    table_number = table.Column<string>(type: "varchar(50)", nullable: true),
                    table_example = table.Column<string>(type: "varchar(255)", nullable: true),
                    table_capacity = table.Column<string>(type: "varchar(255)", nullable: true),
                    visible = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_restaurant_tables", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "restaurant_user",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_restaurant_user", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "route",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(type: "varchar(25)", nullable: true),
                    definition = table.Column<string>(type: "varchar(100)", nullable: true),
                    responsible = table.Column<string>(type: "varchar(100)", nullable: true),
                    responsible_id = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(150)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(150)", nullable: true),
                    route_day = table.Column<string>(type: "varchar(15)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_route", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "route_parameters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    monday = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    tuesday = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    wednesday = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    thursday = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    friday = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    saturday = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    sunday = table.Column<sbyte>(type: "tinyint(3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_route_parameters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "safe",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    safe_name = table.Column<string>(type: "varchar(255)", nullable: true),
                    currency_unit = table.Column<string>(type: "varchar(20)", nullable: true),
                    exp = table.Column<string>(type: "varchar(300)", nullable: true),
                    safe_code = table.Column<string>(type: "varchar(10)", nullable: true),
                    safe_group = table.Column<string>(type: "varchar(100)", nullable: true),
                    visible = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_safe", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "scale_settings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    barcode_prefix = table.Column<int>(type: "int(11)", nullable: true),
                    barcode_length = table.Column<int>(type: "int(11)", nullable: true),
                    amount_length = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_scale_settings", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "scheduler",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    caption = table.Column<string>(type: "varchar(255)", nullable: true),
                    event_type = table.Column<int>(type: "int(11)", nullable: true),
                    start = table.Column<DateTime>(type: "datetime", nullable: true),
                    finish = table.Column<DateTime>(type: "datetime", nullable: true),
                    location = table.Column<string>(type: "varchar(255)", nullable: true),
                    message = table.Column<string>(type: "varchar(255)", nullable: true),
                    options = table.Column<int>(type: "int(11)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    label_colour = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_scheduler", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "services_category_definition",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    defination = table.Column<string>(type: "varchar(255)", nullable: true),
                    code = table.Column<string>(type: "varchar(100)", nullable: true),
                    color = table.Column<string>(type: "varchar(20)", nullable: true),
                    order = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    color_code = table.Column<string>(type: "varchar(50)", nullable: true),
                    Deletion = table.Column<sbyte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_category_definition", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "services_direction_list",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    direction_name = table.Column<string>(type: "varchar(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_direction_list", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "services_setup_defines",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(type: "varchar(150)", nullable: true),
                    defination = table.Column<string>(type: "varchar(255)", nullable: true),
                    services_category_definition_id = table.Column<int>(type: "int(11)", nullable: true),
                    services_category_name = table.Column<string>(type: "varchar(255)", nullable: true),
                    setup_group = table.Column<string>(type: "varchar(150)", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    force_track_serial_number = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    force_track_warranty = table.Column<sbyte>(type: "tinyint(3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_setup_defines", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "services_setup_defines_formula",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_name = table.Column<string>(type: "varchar(200)", nullable: false, defaultValueSql: "'0'"),
                    qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    formula = table.Column<string>(type: "varchar(150)", nullable: true),
                    services_setup_defines_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: false, defaultValueSql: "'0'"),
                    deletion_user_id = table.Column<sbyte>(type: "tinyint(3)", nullable: false, defaultValueSql: "'0'"),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: false, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_setup_defines_formula", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "services_setup_defines_functions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    group_name = table.Column<string>(type: "varchar(255)", nullable: true),
                    function_name = table.Column<string>(type: "varchar(255)", nullable: true),
                    function_exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    syntax = table.Column<string>(type: "varchar(255)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_setup_defines_functions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "settings",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    thema = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_settings", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sms_parameters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    sms_title = table.Column<string>(type: "varchar(11)", nullable: true),
                    default_maintenance_sms_template_id = table.Column<int>(type: "int(11)", nullable: true),
                    after_maintenance_send_sms = table.Column<string>(type: "varchar(10)", nullable: true, defaultValueSql: "'True'"),
                    sh_user_name = table.Column<string>(type: "varchar(50)", nullable: true, defaultValueSql: "''"),
                    sh_password = table.Column<string>(type: "varchar(50)", nullable: true, defaultValueSql: "''")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sms_parameters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sms_task",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    task_name = table.Column<string>(type: "varchar(255)", nullable: true),
                    sms_task_list_basket_id = table.Column<int>(type: "int(11)", nullable: true),
                    date_time = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sms_task", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sms_themes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    theme_name = table.Column<string>(type: "varchar(60)", nullable: true),
                    theme = table.Column<string>(type: "varchar(960)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sms_themes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "speed_sales_buttons",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    button_name = table.Column<string>(type: "varchar(15)", nullable: true),
                    button_tag = table.Column<int>(type: "int(11)", nullable: true),
                    button_caption = table.Column<string>(type: "varchar(20)", nullable: true),
                    img = table.Column<string>(type: "varchar(300)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_caption = table.Column<string>(type: "varchar(20)", nullable: true),
                    use_jpeg = table.Column<sbyte>(type: "tinyint(1)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_speed_sales_buttons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "speed_sales_tabsheet_captions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    tabsheetname = table.Column<string>(type: "varchar(30)", nullable: true),
                    tabsheetcaption = table.Column<string>(type: "varchar(30)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_speed_sales_tabsheet_captions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "stock_activities_sub",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    datetime = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    storage = table.Column<sbyte>(type: "tinyint(1)", nullable: true),
                    invoice_product_sub_id = table.Column<int>(type: "int(11)", nullable: true),
                    color_id = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<int>(type: "int(11)", nullable: true),
                    stock_counting_sub_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_stock_activities_sub", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "stock_activities_sub_main",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    datetime = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    storage = table.Column<sbyte>(type: "tinyint(1)", nullable: true),
                    invoice_product_sub_main_id = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'-1'"),
                    color_id = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<int>(type: "int(11)", nullable: true),
                    stock_counting_sub_main_id = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'-1'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_stock_activities_sub_main", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "stock_counting",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    deficieny = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    excess = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    equality = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    datetime = table.Column<DateTime>(type: "datetime", nullable: true),
                    exp = table.Column<string>(type: "varchar(300)", nullable: true),
                    storage_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_stock_counting", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "stock_counting_sub",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    deficieny = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    excess = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    equality = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    stock_counting_basket = table.Column<int>(type: "int(11)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    datetime = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    storage_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_stock_counting_sub", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "stock_counting_sub_main",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    deficieny = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    excess = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    equality = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    stock_counting_basket = table.Column<int>(type: "int(11)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    datetime = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    storage_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_stock_counting_sub_main", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "stock_counting_temp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_name = table.Column<string>(type: "varchar(150)", nullable: true),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    deficieny = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    excess = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    barcode = table.Column<string>(type: "varchar(20)", nullable: true),
                    equality = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    product_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    force_serial_track = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_stock_counting_temp", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "stock_label",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    barcode = table.Column<string>(type: "varchar(20)", nullable: true),
                    product_name = table.Column<string>(type: "varchar(110)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_stock_label", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "storage",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    storage_name = table.Column<string>(type: "varchar(255)", nullable: true),
                    storage_manager_id = table.Column<int>(type: "int(11)", nullable: true),
                    storage_manager = table.Column<string>(type: "varchar(255)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    branch_id = table.Column<int>(type: "int(11)", nullable: true),
                    branch_definition = table.Column<string>(type: "varchar(150)", nullable: true),
                    forlift_operator_id = table.Column<int>(type: "int(11)", nullable: true),
                    forklif_operator = table.Column<string>(type: "varchar(150)", nullable: true),
                    responsible_for_warehouse_accounting_id = table.Column<int>(type: "int(11)", nullable: true),
                    responsible_for_warehouse_accounting_defination = table.Column<string>(type: "varchar(150)", nullable: true),
                    warehouse_formen_id = table.Column<int>(type: "int(11)", nullable: true),
                    warehouse_formen_defination = table.Column<string>(type: "varchar(150)", nullable: true),
                    loader_id = table.Column<int>(type: "int(11)", nullable: true),
                    loader_defination = table.Column<string>(type: "varchar(150)", nullable: true),
                    security_id = table.Column<int>(type: "int(11)", nullable: true),
                    security_defination = table.Column<string>(type: "varchar(150)", nullable: true),
                    warehouse_adress = table.Column<string>(type: "varchar(255)", nullable: true),
                    telephone = table.Column<string>(type: "varchar(20)", nullable: true),
                    phone_internal = table.Column<int>(type: "int(5)", nullable: true),
                    fax = table.Column<string>(type: "varchar(20)", nullable: true),
                    authorized_gsm = table.Column<string>(type: "varchar(20)", nullable: true),
                    authorized_gsm_2 = table.Column<string>(type: "varchar(20)", nullable: true),
                    explanation = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_storage", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "storage_transfer",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_name = table.Column<string>(type: "varchar(255)", nullable: true),
                    qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    barcode = table.Column<string>(type: "varchar(30)", nullable: true),
                    product_type = table.Column<int>(type: "int(11)", nullable: true),
                    Set_Qty = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'1'"),
                    main_product_id = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    detail_id = table.Column<int>(type: "int(11)", nullable: true),
                    sender_storage = table.Column<int>(type: "int(11)", nullable: true),
                    receiver_storage = table.Column<int>(type: "int(11)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_storage_transfer", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "storage_transfer_temp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_name = table.Column<string>(type: "varchar(255)", nullable: true),
                    qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    barcode = table.Column<string>(type: "varchar(30)", nullable: true),
                    product_type = table.Column<int>(type: "int(11)", nullable: true),
                    Set_Qty = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'1'"),
                    main_product_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_storage_transfer_temp", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "subscription_groups",
                columns: table => new
                {
                    Subscription_Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Subscription_Code = table.Column<string>(type: "varchar(50)", nullable: true),
                    Subscription_Definition = table.Column<string>(type: "varchar(120)", nullable: true),
                    Subscription_Group = table.Column<string>(type: "varchar(55)", nullable: true),
                    Subscription_Fee = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    exp = table.Column<string>(type: "varchar(155)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(155)", nullable: true),
                    exp3 = table.Column<string>(type: "varchar(155)", nullable: true),
                    repetition = table.Column<sbyte>(type: "tinyint(3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_subscription_groups", x => x.Subscription_Id);
                });

            migrationBuilder.CreateTable(
                name: "support",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    receiver_unit = table.Column<string>(type: "varchar(100)", nullable: false),
                    request = table.Column<string>(type: "varchar(1500)", nullable: false),
                    feed_back = table.Column<sbyte>(type: "tinyint(1)", nullable: false),
                    user_id = table.Column<int>(type: "int(11)", nullable: false),
                    status = table.Column<sbyte>(type: "tinyint(1)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_support", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "token",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    acces_key = table.Column<string>(type: "varchar(255)", nullable: true),
                    value = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_token", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "update_history",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    version_number = table.Column<string>(type: "varchar(10)", nullable: true),
                    update_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    emergency_rate = table.Column<int>(type: "int(2)", nullable: true, defaultValueSql: "'1'"),
                    version_url = table.Column<string>(type: "varchar(255)", nullable: true),
                    comments = table.Column<string>(type: "varchar(255)", nullable: true),
                    update_state = table.Column<string>(type: "varchar(15)", nullable: true, defaultValueSql: "'Not Updated'"),
                    insert_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    upload_path = table.Column<string>(type: "varchar(255)", nullable: true),
                    is_server = table.Column<sbyte>(type: "tinyint(3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_update_history", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "waybill",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    type = table.Column<int>(type: "int(11)", nullable: true),
                    waybill_products_basket = table.Column<string>(type: "varchar(255)", nullable: true),
                    ins_date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    waybill_date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    barcode = table.Column<string>(type: "varchar(50)", nullable: true),
                    waybill_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    waybill_serial = table.Column<string>(type: "varchar(5)", nullable: true),
                    waybill_number = table.Column<string>(type: "varchar(10)", nullable: true),
                    stats = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    orders_id = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    deletion = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_waybill", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "waybill_products",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    unit_price = table.Column<decimal>(type: "decimal(10,5)", nullable: true),
                    unit_price_tax = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    product_name = table.Column<string>(type: "varchar(140)", nullable: true),
                    product_code = table.Column<string>(type: "varchar(100)", nullable: true),
                    barcode = table.Column<string>(type: "varchar(100)", nullable: true),
                    amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    tax_rate = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    tax_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    discount_rate1 = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    discount_rate2 = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    discount_rate3 = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    discount_rate4 = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    discount_rate5 = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    total_discont_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    unit_name = table.Column<string>(type: "varchar(20)", nullable: true),
                    discount_unit_price = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    discount_unit_price_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    storage_id = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    color_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_type = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_waybill_products", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "waybill_products_sub",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    product_name = table.Column<string>(type: "varchar(140)", nullable: true),
                    product_code = table.Column<string>(type: "varchar(100)", nullable: true),
                    barcode = table.Column<string>(type: "varchar(100)", nullable: true),
                    unit_name = table.Column<string>(type: "varchar(20)", nullable: true),
                    group_qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    waybill_products_basket_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_type = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_waybill_products_sub", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "bank_account",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    account_holder = table.Column<string>(type: "varchar(300)", nullable: true, defaultValueSql: "''"),
                    bank_id = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    bank_department = table.Column<string>(type: "varchar(200)", nullable: true),
                    account_number = table.Column<string>(type: "varchar(30)", nullable: true),
                    ad_account_number = table.Column<string>(type: "varchar(30)", nullable: true),
                    iban = table.Column<string>(type: "varchar(30)", nullable: true),
                    department_tel = table.Column<string>(type: "varchar(15)", nullable: true),
                    department_contact_person = table.Column<string>(type: "varchar(60)", nullable: true),
                    exp = table.Column<string>(type: "varchar(260)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(260)", nullable: true),
                    department_fax = table.Column<string>(type: "varchar(16)", nullable: true),
                    department_adress = table.Column<string>(type: "varchar(300)", nullable: true),
                    department_mail = table.Column<string>(type: "varchar(140)", nullable: true),
                    bank_name = table.Column<string>(type: "varchar(100)", nullable: true),
                    departmant_name = table.Column<string>(type: "varchar(200)", nullable: true),
                    detail_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bank_account", x => x.Id);
                    table.ForeignKey(
                        name: "bank_account_ibfk_1",
                        column: x => x.bank_id,
                        principalTable: "bank",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "currency_rates_of_exchange",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Currency_Id = table.Column<int>(type: "int(11)", nullable: true),
                    Ex_Date = table.Column<DateTime>(type: "datetime", nullable: true),
                    Currency_Buying = table.Column<decimal>(type: "decimal(10,5)", nullable: true),
                    Currency_Selling = table.Column<decimal>(type: "decimal(10,5)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_currency_rates_of_exchange", x => x.Id);
                    table.ForeignKey(
                        name: "currency_rates_of_exchange_ibfk_1",
                        column: x => x.Currency_Id,
                        principalTable: "currency",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "agricultural_customer",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    app_date = table.Column<DateTime>(type: "date", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    agricultural_name = table.Column<string>(type: "varchar(100)", nullable: true),
                    agricultural_id = table.Column<int>(type: "int(11)", nullable: true),
                    unit_factor = table.Column<decimal>(type: "decimal(10,2)", nullable: true, defaultValueSql: "'1.00'"),
                    Deletion = table.Column<sbyte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_agricultural_customer", x => x.Id);
                    table.ForeignKey(
                        name: "agricultural_customer_ibfk_2",
                        column: x => x.agricultural_id,
                        principalTable: "agricultural",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "agricultural_customer_ibfk_1",
                        column: x => x.customer_id,
                        principalTable: "customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "customer_adress",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    adress_type = table.Column<string>(type: "varchar(100)", nullable: true),
                    adress = table.Column<string>(type: "varchar(255)", nullable: true),
                    exp = table.Column<string>(type: "varchar(100)", nullable: true),
                    province = table.Column<string>(type: "varchar(150)", nullable: true),
                    town = table.Column<string>(type: "varchar(150)", nullable: true),
                    country = table.Column<string>(type: "varchar(150)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    phone = table.Column<string>(type: "varchar(15)", nullable: true),
                    fax = table.Column<string>(type: "varchar(15)", nullable: true),
                    mobile_phone = table.Column<string>(type: "varchar(15)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_adress", x => x.Id);
                    table.ForeignKey(
                        name: "customer_adress_ibfk_1",
                        column: x => x.customer_id,
                        principalTable: "customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "customer_current_input",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    detail_id = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'-1'"),
                    currency = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    datetime = table.Column<DateTime>(type: "datetime", nullable: true),
                    exp = table.Column<string>(type: "varchar(500)", nullable: true),
                    type_ = table.Column<string>(type: "varchar(3)", nullable: true, defaultValueSql: "'in'"),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: true),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    ins_datetime = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    doc_number = table.Column<string>(type: "varchar(20)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_current_input", x => x.Id);
                    table.ForeignKey(
                        name: "customer_id",
                        column: x => x.customer_id,
                        principalTable: "customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "customer_current_output",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    detail_id = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'-1'"),
                    currency = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    datetime = table.Column<DateTime>(type: "datetime", nullable: true),
                    exp = table.Column<string>(type: "varchar(500)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    type_ = table.Column<string>(type: "varchar(3)", nullable: true, defaultValueSql: "'out'"),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: true),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    ins_datetime = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    doc_number = table.Column<string>(type: "varchar(20)", nullable: true),
                    elevator_invoice_stats = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'2'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_current_output", x => x.Id);
                    table.ForeignKey(
                        name: "customer_current_output_ibfk_1",
                        column: x => x.customer_id,
                        principalTable: "customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "customer_producer_receipt_tax_defines",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    producer_receipt_tax_defines_id = table.Column<int>(type: "int(11)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    tax_name = table.Column<string>(type: "varchar(50)", nullable: true),
                    exempt = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'1'"),
                    discount = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    Deletion = table.Column<sbyte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_producer_receipt_tax_defines", x => x.Id);
                    table.ForeignKey(
                        name: "customer_producer_receipt_tax_defines_ibfk_1",
                        column: x => x.customer_id,
                        principalTable: "customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "customer_subscription_current",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    currency = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    ins_date = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    last_pay_date = table.Column<DateTime>(type: "timestamp", nullable: true),
                    pay_date = table.Column<DateTime>(type: "timestamp", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'1'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_subscription_current", x => x.Id);
                    table.ForeignKey(
                        name: "customer_subscription_current_ibfk_1",
                        column: x => x.customer_id,
                        principalTable: "customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "maintenance",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    fee = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    start_date = table.Column<DateTime>(type: "datetime", nullable: true),
                    finish_date = table.Column<DateTime>(type: "datetime", nullable: true),
                    ins_date = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    elevator_count = table.Column<int>(type: "int(10)", nullable: true, defaultValueSql: "'1'"),
                    Deletion = table.Column<sbyte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_maintenance", x => x.Id);
                    table.ForeignKey(
                        name: "maintenance_ibfk_1",
                        column: x => x.customer_id,
                        principalTable: "customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "mobile_scale_product_purchase",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Mobile_Scale_Customer_Id = table.Column<int>(type: "int(11)", nullable: true),
                    Customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    Kg = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    Litre = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    Densities = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    Basket = table.Column<int>(type: "int(11)", nullable: true),
                    Milking_Time = table.Column<DateTime>(type: "timestamp", nullable: true),
                    Pickup_Time = table.Column<DateTime>(type: "timestamp", nullable: true),
                    mobile_scale_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    mobile_scale_product_purchase_id = table.Column<int>(type: "int(11)", nullable: true),
                    stats = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    Device_Id = table.Column<int>(type: "int(11)", nullable: true),
                    Verification_Number = table.Column<int>(type: "int(11)", nullable: true),
                    Deletion = table.Column<sbyte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mobile_scale_product_purchase", x => x.Id);
                    table.ForeignKey(
                        name: "mobile_scale_product_purchase_ibfk_1",
                        column: x => x.Customer_id,
                        principalTable: "customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "orders",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    type = table.Column<int>(type: "int(11)", nullable: true),
                    orders_products_basket = table.Column<string>(type: "varchar(255)", nullable: true),
                    ins_date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    order_date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    barcode = table.Column<string>(type: "varchar(50)", nullable: true),
                    order_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    order_serial = table.Column<string>(type: "varchar(5)", nullable: true),
                    order_number = table.Column<string>(type: "varchar(10)", nullable: true),
                    stats = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    ins_datetime = table.Column<DateTime>(type: "datetime", nullable: true),
                    Deletion = table.Column<sbyte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_orders", x => x.Id);
                    table.ForeignKey(
                        name: "orders_ibfk_1",
                        column: x => x.customer_id,
                        principalTable: "customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "sms_task_list",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    message = table.Column<string>(type: "varchar(1000)", nullable: true),
                    number = table.Column<string>(type: "varchar(12)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    send_date = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    status = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    basket_id = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'1'"),
                    referance = table.Column<string>(type: "varchar(50)", nullable: true),
                    kc_referance = table.Column<string>(type: "varchar(255)", nullable: true),
                    Deletion = table.Column<sbyte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sms_task_list", x => x.Id);
                    table.ForeignKey(
                        name: "sms_task_list_ibfk_1",
                        column: x => x.customer_id,
                        principalTable: "customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "elevator_card_sub_parameters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(type: "varchar(100)", nullable: true),
                    defination = table.Column<string>(type: "varchar(200)", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    elevator_card_parameters_id = table.Column<int>(type: "int(11)", nullable: true),
                    elevator_order = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    top_detail_id = table.Column<int>(type: "int(11)", nullable: true),
                    photo = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    custom_text = table.Column<sbyte>(type: "tinyint(3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_elevator_card_sub_parameters", x => x.Id);
                    table.ForeignKey(
                        name: "elevator_card_sub_parameters_ibfk_1",
                        column: x => x.elevator_card_parameters_id,
                        principalTable: "elevator_card_parameters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "elevator_revision_offer_parameters_temp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(type: "varchar(100)", nullable: true),
                    defination = table.Column<string>(type: "varchar(200)", nullable: true),
                    offer_caption = table.Column<string>(type: "varchar(255)", nullable: true),
                    fee = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    max_discount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    elevator_order = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    elevator_revision_parameters_id = table.Column<int>(type: "int(11)", nullable: true),
                    offer_visible = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    unit = table.Column<string>(type: "varchar(20)", nullable: true, defaultValueSql: "'Adet'"),
                    qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_elevator_revision_offer_parameters_temp", x => x.Id);
                    table.ForeignKey(
                        name: "elevator_revision_offer_parameters_temp_ibfk_1",
                        column: x => x.elevator_revision_parameters_id,
                        principalTable: "elevator_revision_parameters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "elevator_revision_parameters_product",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    elevator_revision_parameters_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_qty = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_elevator_revision_parameters_product", x => x.Id);
                    table.ForeignKey(
                        name: "elevator_revision_parameters_product_ibfk_1",
                        column: x => x.elevator_revision_parameters_id,
                        principalTable: "elevator_revision_parameters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "farm_animals_activity",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    farm_animals_id = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    kg = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    cm = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    farm_veterinary_applications_id = table.Column<int>(type: "int(11)", nullable: true),
                    if_type_3_animal_id = table.Column<int>(type: "int(11)", nullable: true),
                    vaccine_dose = table.Column<decimal>(type: "decimal(10,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_farm_animals_activity", x => x.Id);
                    table.ForeignKey(
                        name: "farm_animals_activity_ibfk_1",
                        column: x => x.farm_animals_id,
                        principalTable: "farm_animals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "farm_feed_mixture_products",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    qty_of_meal = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_name = table.Column<string>(type: "varchar(120)", nullable: true),
                    farm_feed_mixture_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_farm_feed_mixture_products", x => x.Id);
                    table.ForeignKey(
                        name: "farm_feed_mixture_products_ibfk_1",
                        column: x => x.farm_feed_mixture_id,
                        principalTable: "farm_feed_mixture",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "farm_paddock",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    farms_id = table.Column<int>(type: "int(11)", nullable: true),
                    paddock_code = table.Column<string>(type: "varchar(255)", nullable: true),
                    paddock_defination = table.Column<string>(type: "varchar(150)", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    rfid = table.Column<string>(type: "varchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_farm_paddock", x => x.Id);
                    table.ForeignKey(
                        name: "farm_paddock_ibfk_1",
                        column: x => x.farms_id,
                        principalTable: "farms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "load_activity_temp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    load_type_definition_id = table.Column<int>(type: "int(11)", nullable: true),
                    load_type_defination = table.Column<string>(type: "varchar(150)", nullable: true),
                    sender_load_location_definations_id = table.Column<int>(type: "int(11)", nullable: true),
                    sender_load_location_definations = table.Column<string>(type: "varchar(150)", nullable: true),
                    receiver_load_location_definations_id = table.Column<int>(type: "int(11)", nullable: true),
                    receiver_load_location_definations = table.Column<string>(type: "varchar(150)", nullable: true),
                    qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    km = table.Column<int>(type: "int(11)", nullable: true),
                    price_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    unit_price = table.Column<decimal>(type: "decimal(10,5)", nullable: true),
                    rent_unit_price = table.Column<decimal>(type: "decimal(10,5)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    price_type_exp = table.Column<string>(type: "varchar(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_load_activity_temp", x => x.Id);
                    table.ForeignKey(
                        name: "load_activity_temp_ibfk_1",
                        column: x => x.load_type_definition_id,
                        principalTable: "load_type_definition",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "management_roles_authorization",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    management_roles_id = table.Column<int>(type: "int(11)", nullable: false, defaultValueSql: "'0'"),
                    win_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_customer_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_customer_account_type = table.Column<string>(type: "varchar(100)", nullable: true),
                    win_customer_personel_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_stock_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_installment_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_offer_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_order_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_waybill_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_invoice_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_invoice_access_only_speed_sales = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_safe_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_pos_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_bank_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_check_commercial_paper_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_document_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_department_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_targeting_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_creditcard_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_sms_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_income_and_expense_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_currency_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_vehicle_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_producer_Receipt_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_service_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_Production_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_farm_management_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_agricultural_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_einvoice_authority = table.Column<sbyte>(name: "win_e-invoice_authority", type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_restaurant_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_settings = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    sc_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    sc_access_all_request_list = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    sc_access_customer = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    sc_access_account_extract = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    sc_access_collection = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    win_customer_personel_debit = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    rc_authority = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    rc_take_account = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    rc_collection = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_management_roles_authorization", x => x.Id);
                    table.ForeignKey(
                        name: "management_roles_authorization_ibfk_1",
                        column: x => x.management_roles_id,
                        principalTable: "management_roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "product",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(type: "varchar(50)", nullable: true),
                    barcode = table.Column<string>(type: "varchar(20)", nullable: true),
                    name = table.Column<string>(type: "varchar(150)", nullable: true),
                    purchase_price = table.Column<decimal>(type: "decimal(10,5)", nullable: true),
                    gross_sales_price = table.Column<decimal>(type: "decimal(10,5)", nullable: true),
                    retail_price = table.Column<decimal>(type: "decimal(10,5)", nullable: true),
                    purchase_tax_percent = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    sales_tax_percent = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    seller_customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    seller_customer_title = table.Column<string>(type: "varchar(150)", nullable: true),
                    mark = table.Column<string>(type: "varchar(20)", nullable: true),
                    private_code = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code2 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code3 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code4 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code5 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code6 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code7 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code8 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code9 = table.Column<string>(type: "varchar(15)", nullable: true),
                    private_code10 = table.Column<string>(type: "varchar(15)", nullable: true),
                    unit = table.Column<string>(type: "varchar(20)", nullable: true),
                    producer_id = table.Column<int>(type: "int(11)", nullable: true),
                    producer_account_title = table.Column<string>(type: "varchar(150)", nullable: true),
                    max_stock = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    min_stock = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    min_order = table.Column<int>(type: "int(11)", nullable: true),
                    max_order = table.Column<int>(type: "int(11)", nullable: true),
                    picture = table.Column<string>(type: "varchar(1000)", nullable: true),
                    active_scale = table.Column<string>(type: "varchar(7)", nullable: true),
                    barcode_scale = table.Column<string>(type: "varchar(7)", nullable: true),
                    retail_price_tax = table.Column<decimal>(type: "decimal(10,5)", nullable: true),
                    author = table.Column<string>(type: "varchar(100)", nullable: true),
                    book_type = table.Column<string>(type: "varchar(50)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    main_product_id = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    main_product_id_temp = table.Column<int>(type: "int(11)", nullable: true),
                    group_qty = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'1'"),
                    price_privileged = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    purchase_price_tax = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    visible = table.Column<sbyte>(type: "tinyint(1)", nullable: true, defaultValueSql: "'0'"),
                    technic_order = table.Column<string>(type: "varchar(50)", nullable: true),
                    technic_title = table.Column<string>(type: "varchar(50)", nullable: true),
                    technic_title_2 = table.Column<string>(type: "varchar(50)", nullable: true),
                    technic_order_2 = table.Column<string>(type: "varchar(50)", nullable: true),
                    technic_title_3 = table.Column<string>(type: "varchar(50)", nullable: true),
                    technic_order_3 = table.Column<string>(type: "varchar(50)", nullable: true),
                    weight = table.Column<decimal>(type: "decimal(10,4)", nullable: true),
                    group = table.Column<string>(type: "varchar(150)", nullable: true),
                    size = table.Column<string>(type: "varchar(15)", nullable: true),
                    mobile_visible = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    shelf_inf = table.Column<string>(type: "varchar(150)", nullable: true),
                    gross_sales_price_tax = table.Column<decimal>(type: "decimal(10,5)", nullable: true),
                    unit_factor = table.Column<decimal>(type: "decimal(10,5)", nullable: true, defaultValueSql: "'0.00100'"),
                    Ean = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    personal_price = table.Column<decimal>(type: "decimal(10,5)", nullable: true),
                    personal_price_tax = table.Column<decimal>(type: "decimal(10,5)", nullable: true),
                    group1 = table.Column<int>(type: "int(11)", nullable: true),
                    group2 = table.Column<int>(type: "int(11)", nullable: true),
                    group3 = table.Column<int>(type: "int(11)", nullable: true),
                    group4 = table.Column<int>(type: "int(11)", nullable: true),
                    fixed_discount_rate = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    half_portion = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    one_portion = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    one_half_portion = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    double_portion = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    half_portion_order = table.Column<string>(type: "varchar(20)", nullable: true, defaultValueSql: "'False'"),
                    one_portion_order = table.Column<string>(type: "varchar(20)", nullable: true, defaultValueSql: "'False'"),
                    one_half_portion_order = table.Column<string>(type: "varchar(20)", nullable: true, defaultValueSql: "'False'"),
                    double_portion_order = table.Column<string>(type: "varchar(20)", nullable: true, defaultValueSql: "'False'"),
                    force_serial_track = table.Column<string>(type: "varchar(10)", nullable: true),
                    premium_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    waiter_percent = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    curreny_source = table.Column<int>(type: "int(11)", nullable: true),
                    sync_id = table.Column<int>(type: "int(11)", nullable: true),
                    Warranty_period = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    variant = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    profit_rate = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    labor_rate = table.Column<decimal>(type: "decimal(10,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product", x => x.Id);
                    table.ForeignKey(
                        name: "product_ibfk_1",
                        column: x => x.group1,
                        principalTable: "product_groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "product_ibfk_2",
                        column: x => x.group2,
                        principalTable: "product_groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "temp_basket_tax",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    temp_basket_id = table.Column<int>(type: "int(11)", nullable: false, defaultValueSql: "'0'"),
                    product_tax_list_id = table.Column<int>(type: "int(11)", nullable: true),
                    tax_name = table.Column<string>(type: "varchar(11)", nullable: true),
                    tax_rate = table.Column<decimal>(type: "decimal(10,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_temp_basket_tax", x => x.Id);
                    table.ForeignKey(
                        name: "temp_basket_tax_ibfk_1",
                        column: x => x.Id,
                        principalTable: "product_tax_list2",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "restaurant_printer_parameters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_group3_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_group3_name = table.Column<string>(type: "varchar(255)", nullable: true),
                    printer_name = table.Column<string>(type: "varchar(255)", nullable: true),
                    department_id = table.Column<int>(type: "int(11)", nullable: true),
                    department_name = table.Column<string>(type: "varchar(100)", nullable: true),
                    active = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    printer_ip = table.Column<string>(type: "varchar(50)", nullable: true),
                    printer_test_time_out = table.Column<int>(type: "int(11)", nullable: true),
                    alternative_printer = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_restaurant_printer_parameters", x => x.Id);
                    table.ForeignKey(
                        name: "restaurant_printer_parameters_ibfk_1",
                        column: x => x.department_id,
                        principalTable: "restaurant_department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "restaurant_printer_parameters_ibfk_2",
                        column: x => x.product_group3_id,
                        principalTable: "product_groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "restaurant_order",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    restaurant_order_list_basket_id = table.Column<int>(type: "int(11)", nullable: true),
                    open_date = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    close_date = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    discount_rate = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    bill_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    pay_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    count_of_customer = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    table_id = table.Column<int>(type: "int(11)", nullable: true),
                    status = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    bill_collection = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    is_moved_new_basket = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_restaurant_order", x => x.Id);
                    table.ForeignKey(
                        name: "restaurant_order_ibfk_2",
                        column: x => x.table_id,
                        principalTable: "restaurant_tables",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "safe_input",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    safe_id = table.Column<int>(type: "int(11)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(400)", nullable: true),
                    currency = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    datetime = table.Column<DateTime>(type: "datetime", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    type_ = table.Column<string>(type: "varchar(2)", nullable: true, defaultValueSql: "'in'"),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: true),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    detail_id = table.Column<int>(type: "int(11)", nullable: true),
                    privaite_code = table.Column<string>(type: "varchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_safe_input", x => x.Id);
                    table.ForeignKey(
                        name: "safe_input_ibfk_1",
                        column: x => x.safe_id,
                        principalTable: "safe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "safe_output",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    safe_id = table.Column<int>(type: "int(11)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(400)", nullable: true),
                    currency = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    datetime = table.Column<DateTime>(type: "datetime", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    type_ = table.Column<string>(type: "varchar(3)", nullable: true, defaultValueSql: "'out'"),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: true),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    detail_id = table.Column<int>(type: "int(11)", nullable: true),
                    privaite_code = table.Column<string>(type: "varchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_safe_output", x => x.Id);
                    table.ForeignKey(
                        name: "safe_output_ibfk_1",
                        column: x => x.safe_id,
                        principalTable: "safe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "services_category_sub_definition",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    defination = table.Column<string>(type: "varchar(255)", nullable: true),
                    code = table.Column<string>(type: "varchar(100)", nullable: true),
                    color = table.Column<string>(type: "varchar(20)", nullable: true),
                    order = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    services_category_definition_id = table.Column<int>(type: "int(11)", nullable: true),
                    active = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    force_qr_code = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    service_time_out_minutes = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    force_ask_piece_change = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    Deletion = table.Column<sbyte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_category_sub_definition", x => x.Id);
                    table.ForeignKey(
                        name: "services_category_sub_definition_ibfk_1",
                        column: x => x.services_category_definition_id,
                        principalTable: "services_category_definition",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "services_setup_define_parameters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    services_setup_defines_id = table.Column<int>(type: "int(11)", nullable: true),
                    code = table.Column<string>(type: "varchar(150)", nullable: true),
                    defination = table.Column<string>(type: "varchar(200)", nullable: true),
                    integer_in = table.Column<int>(type: "int(11)", nullable: true),
                    decimal_in = table.Column<int>(type: "int(11)", nullable: true),
                    text_in = table.Column<int>(type: "int(11)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_setup_define_parameters", x => x.Id);
                    table.ForeignKey(
                        name: "services_setup_define_parameters_ibfk_1",
                        column: x => x.services_setup_defines_id,
                        principalTable: "services_setup_defines",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "services_request_defination",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    defination = table.Column<string>(type: "varchar(255)", nullable: true),
                    code = table.Column<string>(type: "varchar(100)", nullable: true),
                    color = table.Column<string>(type: "varchar(20)", nullable: true),
                    hex_color = table.Column<string>(type: "varchar(10)", nullable: true),
                    color_code = table.Column<string>(type: "varchar(50)", nullable: true),
                    order = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    services_category_definition_id = table.Column<int>(type: "int(11)", nullable: true),
                    services_category_name = table.Column<string>(type: "varchar(255)", nullable: true),
                    period = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    forced_contract = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    component_dialog = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    mobile_visible = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    open_without_personel = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    Customizable_sub_module = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    allow_not_accept = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    auto_insert_service_in_contract = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    sms_themes_id = table.Column<int>(type: "int(11)", nullable: true),
                    sms_themes_name = table.Column<string>(type: "varchar(100)", nullable: true),
                    contract_report_file = table.Column<string>(type: "varchar(255)", nullable: true),
                    service_report_file = table.Column<string>(type: "varchar(255)", nullable: true),
                    service_report_file_active = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'1'"),
                    service_report_excel_file = table.Column<string>(type: "varchar(255)", nullable: true),
                    service_report_excel_file_active = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    task_accept_timeout = table.Column<int>(type: "int(11)", nullable: true),
                    sms_start_themes_id = table.Column<int>(type: "int(11)", nullable: true),
                    sms_start_themes_name = table.Column<string>(type: "varchar(100)", nullable: true),
                    automatic_charging = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    operation_time = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'30'"),
                    piece_bills_are_merged_automatically = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'30'"),
                    request_group = table.Column<string>(type: "varchar(100)", nullable: true),
                    open_service_without_point = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    offer_excel_template = table.Column<string>(type: "varchar(255)", nullable: true),
                    offer_excel_products_page_no = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    offer_excel_products_start_column = table.Column<string>(type: "varchar(3)", nullable: true),
                    offer_excel_products_start_row = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    offer_excel_products_print_pages = table.Column<string>(type: "varchar(255)", nullable: true),
                    offer_excel_controls_page_no = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    offer_excel_controls_start_column = table.Column<string>(type: "varchar(3)", nullable: true),
                    offer_excel_controls_start_row = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    offer_excel_print_pages = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_request_defination", x => x.Id);
                    table.ForeignKey(
                        name: "services_request_defination_ibfk_1",
                        column: x => x.services_category_definition_id,
                        principalTable: "services_category_definition",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "services_request_defination_ibfk_2",
                        column: x => x.sms_themes_id,
                        principalTable: "sms_themes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "customer_subscription",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    subscription_id = table.Column<int>(type: "int(11)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    Stats = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'1'"),
                    Customer_Price = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    Deletion = table.Column<sbyte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_subscription", x => x.Id);
                    table.ForeignKey(
                        name: "customer_subscription_ibfk_2",
                        column: x => x.customer_id,
                        principalTable: "customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "customer_subscription_ibfk_1",
                        column: x => x.subscription_id,
                        principalTable: "subscription_groups",
                        principalColumn: "Subscription_Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "pos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    bank_account_id = table.Column<int>(type: "int(10)", nullable: true),
                    pos_name = table.Column<string>(type: "varchar(255)", nullable: true),
                    pos_exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    pos_block_time = table.Column<int>(type: "int(11)", nullable: true),
                    pos_code = table.Column<string>(type: "varchar(20)", nullable: true),
                    pos_bank_name = table.Column<string>(type: "varchar(60)", nullable: true),
                    pos_department = table.Column<string>(type: "varchar(60)", nullable: true),
                    pos_account_holder = table.Column<string>(type: "varchar(255)", nullable: true),
                    pos_department_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_pos", x => x.Id);
                    table.ForeignKey(
                        name: "pos_ibfk_1",
                        column: x => x.bank_account_id,
                        principalTable: "bank_account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "customer_current_output_service_notes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    doc_number = table.Column<string>(type: "varchar(50)", nullable: true),
                    ins_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    customer_current_output_id = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_current_output_service_notes", x => x.Id);
                    table.ForeignKey(
                        name: "customer_current_output_service_notes_ibfk_1",
                        column: x => x.customer_current_output_id,
                        principalTable: "customer_current_output",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "customer_offset",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    current_input_id = table.Column<int>(type: "int(11)", nullable: true),
                    current_output_id = table.Column<int>(type: "int(11)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    Deletion = table.Column<sbyte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_offset", x => x.Id);
                    table.ForeignKey(
                        name: "customer_offset_ibfk_2",
                        column: x => x.current_input_id,
                        principalTable: "customer_current_input",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "customer_offset_ibfk_3",
                        column: x => x.current_output_id,
                        principalTable: "customer_current_output",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "customer_offset_ibfk_1",
                        column: x => x.customer_id,
                        principalTable: "customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "sms_balance",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    sms_task_list_id = table.Column<int>(type: "int(11)", nullable: true),
                    qty = table.Column<int>(type: "int(11)", nullable: true),
                    date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    exp = table.Column<string>(type: "varchar(1000)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sms_balance", x => x.Id);
                    table.ForeignKey(
                        name: "sms_balance_ibfk_1",
                        column: x => x.sms_task_list_id,
                        principalTable: "sms_task_list",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "management_roles_service_request_authorization",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    service_request_id = table.Column<int>(type: "int(11)", nullable: true),
                    service_request_defination = table.Column<string>(type: "varchar(255)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    ins_datetime = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion_time = table.Column<DateTime>(type: "timestamp", nullable: true),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    management_roles_authorization_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_management_roles_service_request_authorization", x => x.Id);
                    table.ForeignKey(
                        name: "management_roles_service_request_authorization_ibfk_1",
                        column: x => x.management_roles_authorization_id,
                        principalTable: "management_roles_authorization",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "cargo_fee_defines",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ds1_1 = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    ds1_2 = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    currency1 = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    ds2_1 = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    ds22 = table.Column<int>(name: "ds2-2", type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    currency2 = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    ds3_1 = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    ds3_2 = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    currency3 = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    ds4_1 = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    ds4_2 = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    currency4 = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    ds5_1 = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    ds5_2 = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    currency5 = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    Use_cargo_fee_defines = table.Column<string>(type: "varchar(20)", nullable: true),
                    cargo_product_id = table.Column<int>(type: "int(11)", nullable: true),
                    ds6_1 = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    ds6_2 = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    currency6 = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    currency7 = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    Receive_Cargo_Difference = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    ds7_1 = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    ds7_2 = table.Column<int>(type: "int(11)", nullable: true),
                    ds8_1 = table.Column<int>(type: "int(11)", nullable: true),
                    currency8 = table.Column<decimal>(type: "decimal(10,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cargo_fee_defines", x => x.Id);
                    table.ForeignKey(
                        name: "cargo_fee_defines_ibfk_1",
                        column: x => x.cargo_product_id,
                        principalTable: "product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "invoice_product",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    unit_price = table.Column<decimal>(type: "decimal(18,5)", nullable: true),
                    qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    tax_rate = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    withholding_rate = table.Column<decimal>(type: "decimal(10,2)", nullable: true, defaultValueSql: "'0.00'"),
                    discount_rate1 = table.Column<decimal>(type: "decimal(10,5)", nullable: true, defaultValueSql: "'0.00000'"),
                    discount_rate2 = table.Column<decimal>(type: "decimal(10,5)", nullable: true, defaultValueSql: "'0.00000'"),
                    discount_rate3 = table.Column<decimal>(type: "decimal(10,5)", nullable: true, defaultValueSql: "'0.00000'"),
                    discount_rate4 = table.Column<decimal>(type: "decimal(10,5)", nullable: true, defaultValueSql: "'0.00000'"),
                    discount_rate5 = table.Column<decimal>(type: "decimal(10,5)", nullable: true, defaultValueSql: "'0.00000'"),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    product_name = table.Column<string>(type: "varchar(140)", nullable: true),
                    product_code = table.Column<string>(type: "varchar(100)", nullable: true),
                    barcode = table.Column<string>(type: "varchar(100)", nullable: true),
                    storage_id = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    unit_name = table.Column<string>(type: "varchar(20)", nullable: true),
                    color_id = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    product_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    detail_id = table.Column<int>(type: "int(11)", nullable: true),
                    size = table.Column<string>(type: "varchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_invoice_product", x => x.Id);
                    table.ForeignKey(
                        name: "invoice_product_ibfk_1",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "invoice_shipment_trace",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    invoice_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    storage_id = table.Column<int>(type: "int(11)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    invoice_product_basket = table.Column<int>(type: "int(11)", nullable: true),
                    shipping_datetime = table.Column<DateTime>(type: "datetime", nullable: true),
                    driver_name = table.Column<string>(type: "varchar(100)", nullable: true),
                    plate = table.Column<string>(type: "varchar(60)", nullable: true),
                    deletion = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_invoice_shipment_trace", x => x.Id);
                    table.ForeignKey(
                        name: "invoice_shipment_trace_ibfk_1",
                        column: x => x.invoice_id,
                        principalTable: "invoice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "invoice_shipment_trace_ibfk_2",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "invoice_shipment_trace_ibfk_3",
                        column: x => x.storage_id,
                        principalTable: "storage",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "product_nutritional_values",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    dry_matter = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    metabolic_energy = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    crude_protein = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    crude_fiber = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    ash = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    hcl = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    crude_oil = table.Column<decimal>(type: "decimal(10,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_nutritional_values", x => x.Id);
                    table.ForeignKey(
                        name: "product_nutritional_values_ibfk_1",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "product_serials",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    basket_id = table.Column<int>(type: "int(11)", nullable: true),
                    serial_code = table.Column<string>(type: "varchar(50)", nullable: true),
                    ins_date = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    status = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    status_change_detail_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_serials", x => x.Id);
                    table.ForeignKey(
                        name: "product_serials_ibfk_1",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "product_unit",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    unit_name = table.Column<string>(type: "varchar(25)", nullable: true),
                    unit_factor = table.Column<decimal>(type: "decimal(10,5)", nullable: true),
                    unit_barcode = table.Column<string>(type: "varchar(30)", nullable: true),
                    unit_exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_product_unit", x => x.Id);
                    table.ForeignKey(
                        name: "product_unit_ibfk_1",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "production_recipe_defines",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(type: "varchar(50)", nullable: true),
                    define = table.Column<string>(type: "varchar(150)", nullable: true),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_name = table.Column<string>(type: "varchar(190)", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    production_unit = table.Column<int>(type: "int(11)", nullable: true),
                    production_unit_define = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_production_recipe_defines", x => x.Id);
                    table.ForeignKey(
                        name: "production_recipe_defines_ibfk_1",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "restaurant_product_flavor_details",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    flavor_define = table.Column<string>(type: "varchar(100)", nullable: true),
                    price = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    visible = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_restaurant_product_flavor_details", x => x.Id);
                    table.ForeignKey(
                        name: "restaurant_product_flavor_details_ibfk_1",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "stock_activities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    detail_id = table.Column<int>(type: "int(11)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    datetime = table.Column<DateTime>(type: "datetime", nullable: true),
                    storage = table.Column<int>(type: "int(11)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: true),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    storage_transfer_basket_id = table.Column<string>(type: "varchar(255)", nullable: true),
                    doc_seri = table.Column<string>(type: "varchar(15)", nullable: true),
                    doc_num = table.Column<string>(type: "varchar(10)", nullable: true),
                    color_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_stock_activities", x => x.Id);
                    table.ForeignKey(
                        name: "stock_activities_ibfk_1",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "stock_activities_ibfk_2",
                        column: x => x.storage,
                        principalTable: "storage",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "service_control_defines",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    order_number = table.Column<int>(type: "int(11)", nullable: true),
                    is_important = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    control_define = table.Column<string>(type: "varchar(255)", nullable: false),
                    active = table.Column<sbyte>(type: "tinyint(3)", nullable: false, defaultValueSql: "'0'"),
                    code = table.Column<string>(type: "varchar(30)", nullable: true),
                    color = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    services_request_defination_id = table.Column<int>(type: "int(11)", nullable: true),
                    services_category_sub_definition_id = table.Column<int>(type: "int(11)", nullable: true),
                    photo_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    checkbox_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    text_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    integer_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    decimal_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    sub_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    force_select = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    Deletion = table.Column<sbyte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_service_control_defines", x => x.Id);
                    table.ForeignKey(
                        name: "service_control_defines_ibfk_2",
                        column: x => x.services_category_sub_definition_id,
                        principalTable: "services_category_sub_definition",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "service_control_defines_ibfk_1",
                        column: x => x.services_request_defination_id,
                        principalTable: "services_request_defination",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "services_contract",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    start_date = table.Column<DateTime>(type: "datetime", nullable: true),
                    end_date = table.Column<DateTime>(type: "datetime", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    service_day = table.Column<int>(type: "int(11)", nullable: true),
                    contract_fee = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    notes = table.Column<string>(type: "varchar(255)", nullable: true),
                    services_count = table.Column<int>(type: "int(11)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    active = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'1'"),
                    services_category_definition_id = table.Column<int>(type: "int(11)", nullable: true),
                    services_request_defination_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    tax = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'1'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_contract", x => x.Id);
                    table.ForeignKey(
                        name: "services_contract_ibfk_1",
                        column: x => x.customer_id,
                        principalTable: "customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "services_contract_ibfk_2",
                        column: x => x.services_category_definition_id,
                        principalTable: "services_category_definition",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "services_contract_ibfk_3",
                        column: x => x.services_request_defination_id,
                        principalTable: "services_request_defination",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "services_last_status_defination",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    defination = table.Column<string>(type: "varchar(255)", nullable: true),
                    code = table.Column<string>(type: "varchar(100)", nullable: true),
                    color = table.Column<string>(type: "varchar(20)", nullable: true),
                    order = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    sevices_request_defination_id = table.Column<int>(type: "int(11)", nullable: true),
                    sevices_request_defination_defination = table.Column<string>(type: "varchar(255)", nullable: true),
                    services_category_definition_id = table.Column<int>(type: "int(11)", nullable: true),
                    services_category_name = table.Column<string>(type: "varchar(255)", nullable: true),
                    open_new_service = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    service_impact = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    open_new_service_request_defination_id = table.Column<string>(type: "varchar(255)", nullable: true),
                    open_new_service_request_defination_defination = table.Column<string>(type: "varchar(255)", nullable: true),
                    color_code = table.Column<string>(type: "varchar(50)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion_datetime = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    sms_themes_id = table.Column<int>(type: "int(11)", nullable: true),
                    sms_themes_name = table.Column<string>(type: "varchar(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_last_status_defination", x => x.Id);
                    table.ForeignKey(
                        name: "services_last_status_defination_ibfk_2",
                        column: x => x.services_category_definition_id,
                        principalTable: "services_category_definition",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "services_last_status_defination_ibfk_1",
                        column: x => x.sevices_request_defination_id,
                        principalTable: "services_request_defination",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "services_last_status_defination_ibfk_3",
                        column: x => x.sms_themes_id,
                        principalTable: "sms_themes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "services_request_defination_infs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    caption = table.Column<string>(type: "varchar(100)", nullable: true),
                    text_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    numeric_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    integer_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    services_request_defination_id = table.Column<int>(type: "int(11)", nullable: true),
                    personel_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    date_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    force_entery = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    order_number = table.Column<sbyte>(type: "tinyint(3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_request_defination_infs", x => x.Id);
                    table.ForeignKey(
                        name: "services_request_defination_infs_ibfk_1",
                        column: x => x.services_request_defination_id,
                        principalTable: "services_request_defination",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "services_setup_request_list",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    services_setup_defines_id = table.Column<int>(type: "int(11)", nullable: true),
                    services_request_defination_id = table.Column<int>(type: "int(11)", nullable: true),
                    request_defination = table.Column<string>(type: "varchar(255)", nullable: true),
                    order = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    minute_difference = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    hour_difference = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    day_difference = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_setup_request_list", x => x.Id);
                    table.ForeignKey(
                        name: "services_setup_request_list_ibfk_1",
                        column: x => x.services_request_defination_id,
                        principalTable: "services_request_defination",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "customer_subscription_detail",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    customer_subscription_id = table.Column<int>(type: "int(11)", nullable: true),
                    contract_date = table.Column<DateTime>(type: "timestamp", nullable: true),
                    subscription_start_date = table.Column<DateTime>(type: "timestamp", nullable: true),
                    pay_day = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    option_day = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    exp = table.Column<string>(type: "varchar(150)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(150)", nullable: true),
                    exp3 = table.Column<string>(type: "varchar(155)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_subscription_detail", x => x.Id);
                    table.ForeignKey(
                        name: "customer_subscription_detail_ibfk_1",
                        column: x => x.customer_subscription_id,
                        principalTable: "customer_subscription",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "management",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    user_name = table.Column<string>(type: "varchar(100)", nullable: true),
                    mail = table.Column<string>(type: "varchar(200)", nullable: true),
                    password = table.Column<string>(type: "varchar(300)", nullable: true),
                    user_type = table.Column<int>(type: "int(11)", nullable: true),
                    default_safe = table.Column<int>(type: "int(11)", nullable: true),
                    currency_type = table.Column<string>(type: "varchar(15)", nullable: true),
                    safe_name = table.Column<string>(type: "varchar(50)", nullable: true),
                    pass2 = table.Column<string>(type: "varchar(300)", nullable: true),
                    skin = table.Column<string>(type: "varchar(60)", nullable: true),
                    invoice_serial = table.Column<string>(type: "varchar(5)", nullable: true),
                    tax_invoice_check_box = table.Column<string>(type: "varchar(10)", nullable: true),
                    default_storage = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    default_storage_name = table.Column<string>(type: "varchar(50)", nullable: true),
                    elevator_all_privileges = table.Column<sbyte>(type: "tinyint(1)", nullable: true, defaultValueSql: "'0'"),
                    customer_visible = table.Column<string>(type: "varchar(20)", nullable: true),
                    veterinary = table.Column<string>(type: "varchar(20)", nullable: true),
                    no_customer_current = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    no_customer_cards = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    invoice_serial_number = table.Column<int>(type: "int(11)", nullable: true),
                    active = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'1'"),
                    management_roles_id = table.Column<int>(type: "int(11)", nullable: false, defaultValueSql: "'0'"),
                    management_roles_role_defination = table.Column<string>(type: "varchar(255)", nullable: true),
                    user_role = table.Column<int>(type: "int(11)", nullable: true),
                    default_pos = table.Column<int>(type: "int(11)", nullable: true),
                    default_pos_name = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_management", x => x.Id);
                    table.ForeignKey(
                        name: "management_ibfk_2",
                        column: x => x.default_pos,
                        principalTable: "pos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "management_ibfk_1",
                        column: x => x.default_safe,
                        principalTable: "safe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "pos_detail",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    number_of_installments = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    bank_commission = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    customer_commission = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    pos_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_pos_detail", x => x.Id);
                    table.ForeignKey(
                        name: "pos_detail_ibfk_1",
                        column: x => x.pos_id,
                        principalTable: "pos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "pos_in",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    pos_id = table.Column<int>(type: "int(11)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(400)", nullable: true),
                    currency = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    datetime = table.Column<DateTime>(type: "datetime", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    installment_number = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: true),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    detail_id = table.Column<int>(type: "int(11)", nullable: true),
                    net_currency = table.Column<decimal>(type: "decimal(10,4)", nullable: true),
                    type_ = table.Column<string>(type: "varchar(2)", nullable: true, defaultValueSql: "'in'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_pos_in", x => x.Id);
                    table.ForeignKey(
                        name: "pos_in_ibfk_1",
                        column: x => x.pos_id,
                        principalTable: "pos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "pos_out",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    pos_id = table.Column<int>(type: "int(11)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(400)", nullable: true),
                    currency = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    datetime = table.Column<DateTime>(type: "datetime", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: true),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    type_ = table.Column<string>(type: "varchar(2)", nullable: true, defaultValueSql: "'ou'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_pos_out", x => x.Id);
                    table.ForeignKey(
                        name: "pos_out_ibfk_1",
                        column: x => x.pos_id,
                        principalTable: "pos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "production_orders_temp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    production_recipe_defines_id = table.Column<int>(type: "int(11)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    production_qty = table.Column<decimal>(type: "decimal(10,5)", nullable: true),
                    production_recipe_defines = table.Column<string>(type: "varchar(255)", nullable: true),
                    unit = table.Column<string>(type: "varchar(20)", nullable: true),
                    product_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_production_orders_temp", x => x.Id);
                    table.ForeignKey(
                        name: "production_orders_temp_ibfk_1",
                        column: x => x.production_recipe_defines_id,
                        principalTable: "production_recipe_defines",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "production_recipe_formula",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    production_recipe_defines_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_id = table.Column<int>(type: "int(255)", nullable: true),
                    formula_qty = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    product_name = table.Column<string>(type: "varchar(150)", nullable: true),
                    wastage_rate = table.Column<decimal>(type: "decimal(10,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_production_recipe_formula", x => x.Id);
                    table.ForeignKey(
                        name: "production_recipe_formula_ibfk_2",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "production_recipe_formula_ibfk_3",
                        column: x => x.production_recipe_defines_id,
                        principalTable: "production_recipe_defines",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "service_control_sub_defines",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    order_number = table.Column<int>(type: "int(11)", nullable: true),
                    is_important = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    control_define = table.Column<string>(type: "varchar(255)", nullable: false),
                    active = table.Column<sbyte>(type: "tinyint(3)", nullable: false, defaultValueSql: "'0'"),
                    service_control_defines_id = table.Column<int>(type: "int(11)", nullable: true),
                    photo_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    checkbox_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    text_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    integer_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    decimal_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    tick_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    force = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    Deletion = table.Column<sbyte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_service_control_sub_defines", x => x.Id);
                    table.ForeignKey(
                        name: "service_control_sub_defines_ibfk_1",
                        column: x => x.service_control_defines_id,
                        principalTable: "service_control_defines",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "services_open_control_defines_temp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    basket_id = table.Column<int>(type: "int(11)", nullable: true),
                    service_control_defines_id = table.Column<int>(type: "int(11)", nullable: true),
                    control_define = table.Column<string>(type: "varchar(255)", nullable: true),
                    module_defination = table.Column<string>(type: "varchar(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_open_control_defines_temp", x => x.Id);
                    table.ForeignKey(
                        name: "services_open_control_defines_temp_ibfk_1",
                        column: x => x.service_control_defines_id,
                        principalTable: "service_control_defines",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "services_points_base_information",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    acceptance_date = table.Column<DateTime>(type: "date", nullable: true),
                    manufacturer = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    warranty = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    warranty_end_date = table.Column<DateTime>(type: "date", nullable: true),
                    due_date = table.Column<DateTime>(type: "date", nullable: true),
                    installation_date = table.Column<DateTime>(type: "date", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    service_order = table.Column<int>(type: "int(11)", nullable: true),
                    identification_number = table.Column<string>(type: "varchar(50)", nullable: true),
                    service_code = table.Column<string>(type: "varchar(100)", nullable: true),
                    service_name = table.Column<string>(type: "varchar(200)", nullable: true),
                    direction = table.Column<string>(type: "varchar(100)", nullable: true),
                    services_category_definition_id = table.Column<int>(type: "int(11)", nullable: true),
                    services_category_name = table.Column<string>(type: "varchar(100)", nullable: true),
                    services_contract_id = table.Column<int>(type: "int(11)", nullable: true),
                    active = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'1'"),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    latitude = table.Column<string>(type: "varchar(100)", nullable: true),
                    longitude = table.Column<string>(type: "varchar(100)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_points_base_information", x => x.Id);
                    table.ForeignKey(
                        name: "services_points_base_information_ibfk_4",
                        column: x => x.customer_id,
                        principalTable: "customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "services_points_base_information_ibfk_3",
                        column: x => x.services_category_definition_id,
                        principalTable: "services_category_definition",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "services_points_base_information_ibfk_2",
                        column: x => x.services_contract_id,
                        principalTable: "services_contract",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "services_last_status_defination_infs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    caption = table.Column<string>(type: "varchar(100)", nullable: true),
                    text_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    numeric_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    integer_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    services_last_status_defination_id = table.Column<int>(type: "int(11)", nullable: true),
                    personel_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    date_in = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    force_entery = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    order_number = table.Column<sbyte>(type: "tinyint(3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_last_status_defination_infs", x => x.Id);
                    table.ForeignKey(
                        name: "services_last_status_defination_infs_ibfk_1",
                        column: x => x.services_last_status_defination_id,
                        principalTable: "services_last_status_defination",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "check_in",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    bank_id = table.Column<int>(type: "int(11)", nullable: true),
                    currency = table.Column<decimal>(type: "decimal(18,5)", nullable: true),
                    check_number = table.Column<int>(type: "int(11)", nullable: true),
                    check_bank_account = table.Column<string>(type: "varchar(50)", nullable: true),
                    exp = table.Column<string>(type: "varchar(400)", nullable: true),
                    pay_date = table.Column<DateTime>(type: "date", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    status = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'1'"),
                    type_ = table.Column<string>(type: "varchar(3)", nullable: true, defaultValueSql: "'in'"),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    ins_date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    reminder = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    reminder_user = table.Column<int>(type: "int(11)", nullable: true),
                    exit_customer_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_check_in", x => x.Id);
                    table.ForeignKey(
                        name: "check_in_ibfk_1",
                        column: x => x.reminder_user,
                        principalTable: "management",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "check_output",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    bank_id = table.Column<int>(type: "int(11)", nullable: true),
                    currency = table.Column<decimal>(type: "decimal(18,5)", nullable: true),
                    check_number = table.Column<int>(type: "int(11)", nullable: true),
                    check_bank_account = table.Column<string>(type: "varchar(50)", nullable: true),
                    exp = table.Column<string>(type: "varchar(400)", nullable: true),
                    pay_date = table.Column<DateTime>(type: "date", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    status = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'1'"),
                    type_ = table.Column<string>(type: "varchar(3)", nullable: true, defaultValueSql: "'out'"),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: true),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    check_bank_account_id = table.Column<int>(type: "int(11)", nullable: true),
                    ins_date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    reminder = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    reminder_user = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_check_output", x => x.Id);
                    table.ForeignKey(
                        name: "check_output_ibfk_1",
                        column: x => x.reminder_user,
                        principalTable: "management",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "commercial_paper_in",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    currency = table.Column<decimal>(type: "decimal(18,5)", nullable: true),
                    pay_date = table.Column<DateTime>(type: "date", nullable: true),
                    notary_warning = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    document_number = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(300)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    type_ = table.Column<string>(type: "varchar(5)", nullable: true, defaultValueSql: "'in'"),
                    status = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'1'"),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: true),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    ins_date = table.Column<DateTime>(type: "timestamp(3)", nullable: true),
                    ins_date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    reminder = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    reminder_user = table.Column<int>(type: "int(11)", nullable: true),
                    exit_customer_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_commercial_paper_in", x => x.Id);
                    table.ForeignKey(
                        name: "commercial_paper_in_ibfk_1",
                        column: x => x.reminder_user,
                        principalTable: "management",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "commercial_paper_out",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    currency = table.Column<decimal>(type: "decimal(18,5)", nullable: true),
                    pay_date = table.Column<DateTime>(type: "date", nullable: true),
                    notary_warning = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    document_number = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(300)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    type_ = table.Column<string>(type: "varchar(5)", nullable: true, defaultValueSql: "'out'"),
                    status = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'1'"),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: true),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    ins_date = table.Column<DateTime>(type: "timestamp(3)", nullable: true),
                    ins_date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    reminder = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    reminder_user = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_commercial_paper_out", x => x.Id);
                    table.ForeignKey(
                        name: "commercial_paper_out_ibfk_1",
                        column: x => x.reminder_user,
                        principalTable: "management",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "elevator_revision_offer_parameters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(type: "varchar(100)", nullable: true),
                    defination = table.Column<string>(type: "varchar(200)", nullable: true),
                    offer_caption = table.Column<string>(type: "varchar(255)", nullable: true),
                    fee = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    max_discount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    elevator_order = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    elevator_revision_parameters_id = table.Column<int>(type: "int(11)", nullable: true),
                    offer_visible = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    stats = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    stats_change_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    stats_change_datetime = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_elevator_revision_offer_parameters", x => x.Id);
                    table.ForeignKey(
                        name: "elevator_revision_offer_parameters_ibfk_1",
                        column: x => x.stats_change_user_id,
                        principalTable: "management",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "parameters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    customer_code = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    Scale_Use_On_Frm_Speed_Retail_Sales = table.Column<string>(type: "varchar(6)", nullable: true, defaultValueSql: "'False'"),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    Chane_Use_On_Frm_Speed_Retail_Sales = table.Column<string>(type: "varchar(10)", nullable: true, defaultValueSql: "'False'"),
                    Visible_Product_Grid_On_Frm_Speed_Retail_Sales = table.Column<string>(type: "varchar(10)", nullable: true, defaultValueSql: "'False'"),
                    Update_Prices_Always_On_Frm_Speed_Retail_Sales = table.Column<string>(type: "varchar(10)", nullable: true, defaultValueSql: "'False'"),
                    Frm_Invoice_Customer_List = table.Column<string>(type: "varchar(10)", nullable: true, defaultValueSql: "'False'"),
                    Frm_Invoice_Product_List = table.Column<string>(type: "varchar(10)", nullable: true, defaultValueSql: "'False'"),
                    Auto_Print_Ticket_On_Frm_Speed_Retail_Sales = table.Column<string>(type: "varchar(10)", nullable: true, defaultValueSql: "'False'"),
                    Frm_Product_Store_Module = table.Column<string>(type: "varchar(10)", nullable: true, defaultValueSql: "'False'"),
                    Frm_Product_Stationery_Module = table.Column<string>(type: "varchar(10)", nullable: true, defaultValueSql: "'False'"),
                    Frm_Product_Main_Stock_Module = table.Column<string>(type: "varchar(10)", nullable: true, defaultValueSql: "'False'"),
                    Frm_Main_Background_Image = table.Column<string>(type: "text", nullable: true),
                    Frm_Customer_Current_Send_Sms_On_Curr_In = table.Column<string>(type: "varchar(10)", nullable: true),
                    Frm_Customer_Current_Send_Sms_On_Curr_Out = table.Column<string>(type: "varchar(10)", nullable: true),
                    Frm_Invoice_Preview_Invoice = table.Column<string>(type: "varchar(10)", nullable: true),
                    Frm_Product_Ean13 = table.Column<string>(type: "varchar(10)", nullable: true, defaultValueSql: "'False'"),
                    Frm_Invoice_checked_receipt = table.Column<string>(type: "varchar(10)", nullable: true),
                    Frm_Speed_Retail_Sales_Print_Dialog = table.Column<string>(type: "varchar(10)", nullable: true),
                    Frm_Invoice_Unit_Price_Is_Retail_Price = table.Column<string>(type: "varchar(10)", nullable: true),
                    Frm_Check_Cp_Entry_Pay_Warning_Option = table.Column<string>(type: "varchar(10)", nullable: true),
                    Frm_Check_Cp_Entry_Option_Time = table.Column<string>(type: "varchar(10)", nullable: true, defaultValueSql: "'0'"),
                    Frm_Invoice_Qty_Dialog = table.Column<string>(type: "varchar(10)", nullable: true),
                    Frm_Invoice_Price_Diaolog = table.Column<string>(type: "varchar(255)", nullable: true),
                    Frm_Storafe_Transfer_Print_Dialog = table.Column<string>(type: "varchar(15)", nullable: true),
                    Frm_Customer_Card_B_L1_1 = table.Column<string>(type: "varchar(15)", nullable: true),
                    Frm_Customer_Card_B_L1_2 = table.Column<string>(type: "varchar(15)", nullable: true),
                    Frm_Customer_Card_B_L2_1 = table.Column<string>(type: "varchar(15)", nullable: true),
                    Frm_Customer_Card_B_L2_2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    Frm_Customer_Card_B_L3_1 = table.Column<string>(type: "varchar(255)", nullable: true),
                    Frm_Customer_Card_B_L3_2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    Frm_Customer_Card_B_L1_C = table.Column<string>(type: "varchar(255)", nullable: true),
                    Frm_Customer_Card_B_L2_C = table.Column<string>(type: "varchar(255)", nullable: true),
                    Frm_Customer_Card_B_L3_C = table.Column<string>(type: "varchar(255)", nullable: true),
                    Frm_Customer_Card_A_L1_1 = table.Column<string>(type: "varchar(15)", nullable: true),
                    Frm_Customer_Card_A_L1_2 = table.Column<string>(type: "varchar(15)", nullable: true),
                    Frm_Customer_Card_A_L2_1 = table.Column<string>(type: "varchar(15)", nullable: true),
                    Frm_Customer_Card_A_L2_2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    Frm_Customer_Card_A_L3_1 = table.Column<string>(type: "varchar(255)", nullable: true),
                    Frm_Customer_Card_A_L3_2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    Frm_Customer_Card_A_L1_C = table.Column<string>(type: "varchar(255)", nullable: true),
                    Frm_Customer_Card_A_L2_C = table.Column<string>(type: "varchar(255)", nullable: true),
                    Frm_Customer_Card_A_L3_C = table.Column<string>(type: "varchar(255)", nullable: true),
                    Frm_Inv_Preview_C_P = table.Column<string>(type: "varchar(10)", nullable: true),
                    Frm_Invoice_Shipping_Document = table.Column<string>(type: "varchar(10)", nullable: true),
                    Frm_Invoice_Cargo_Pay_Limit = table.Column<decimal>(type: "decimal(10,2)", nullable: true, defaultValueSql: "'0.00'"),
                    Frm_ProductCard_Order_Code = table.Column<string>(type: "varchar(25)", nullable: true),
                    Frm_Invoice_Select_Pay_Method = table.Column<string>(type: "varchar(25)", nullable: true),
                    Frm_Product_Card_Piece_Trace = table.Column<string>(type: "varchar(25)", nullable: true),
                    Frm_Customer_Card_Order_ByCode = table.Column<string>(type: "varchar(25)", nullable: true),
                    Frm_Invoice_Trace_Shipment = table.Column<string>(type: "varchar(20)", nullable: true, defaultValueSql: "'False'"),
                    Frm_Invoice_Trace_Stock_Counting_Via_Shipment = table.Column<string>(type: "varchar(25)", nullable: true, defaultValueSql: "'False'"),
                    Frm_Invoice_Product_Grid_Auto_Product_Insert = table.Column<string>(type: "varchar(25)", nullable: true),
                    Frm_Invoice_Default_Tax_Rate = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    Frm_Invoice_Invoice_Row_Count = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    Frm_Product_Shelf_Track = table.Column<string>(type: "varchar(50)", nullable: true),
                    Frm_ProductCard_Order_By_Product_Name = table.Column<string>(type: "varchar(200)", nullable: true),
                    Elevator_Mobile_Send_Sms = table.Column<string>(type: "varchar(20)", nullable: true, defaultValueSql: "'True'"),
                    Elevator_Sender_Title = table.Column<string>(type: "varchar(11)", nullable: true),
                    Frm_Invoice_Speed_Insert = table.Column<string>(type: "varchar(20)", nullable: true),
                    Frm_Invoice_Update_Purchase_Price = table.Column<string>(type: "varchar(20)", nullable: true),
                    Frm_Invoice_Update_Personel_Price = table.Column<string>(type: "varchar(20)", nullable: true),
                    Frm_Invoice_Personel_Price_Discount_Rate = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    Frm_Farm_Day_Pregnant = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    Frm_Product_Card_Serials_Monitor = table.Column<string>(type: "varchar(10)", nullable: true),
                    Frm_Speed_Retail_Sales_Ask_Product_Price_Change = table.Column<string>(type: "varchar(20)", nullable: true),
                    focus_color = table.Column<string>(type: "varchar(100)", nullable: true),
                    Frm_Speed_Sales_If_Barcode_Not_Only_Sound = table.Column<string>(type: "varchar(10)", nullable: true),
                    Frm_Product_Auto_Calc_Retail_price = table.Column<string>(type: "varchar(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_parameters", x => x.Id);
                    table.ForeignKey(
                        name: "parameters_ibfk_1",
                        column: x => x.user_id,
                        principalTable: "management",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "restaurant_order_list",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_name = table.Column<string>(type: "varchar(255)", nullable: true),
                    qty = table.Column<decimal>(type: "decimal(12,5)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    portion = table.Column<decimal>(type: "decimal(10,1)", nullable: true),
                    price = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    flavor_id = table.Column<int>(type: "int(11)", nullable: true),
                    ins_date = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_restaurant_order_list", x => x.Id);
                    table.ForeignKey(
                        name: "restaurant_order_list_ibfk_2",
                        column: x => x.user_id,
                        principalTable: "management",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "services_speed_buttons",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    request_id = table.Column<int>(type: "int(11)", nullable: true),
                    defination = table.Column<string>(type: "varchar(100)", nullable: true),
                    active = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'1'"),
                    glyph = table.Column<byte[]>(nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    button_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_speed_buttons", x => x.Id);
                    table.ForeignKey(
                        name: "services_speed_buttons_ibfk_1",
                        column: x => x.user_id,
                        principalTable: "management",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "temp_basket",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: false, defaultValueSql: "'0'"),
                    customer_id = table.Column<int>(type: "int(11)", nullable: false, defaultValueSql: "'0'"),
                    basket_group = table.Column<int>(type: "int(11)", nullable: true),
                    unit_price = table.Column<decimal>(type: "decimal(12,2)", nullable: true),
                    operation_type = table.Column<int>(type: "int(11)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: false, defaultValueSql: "'0'"),
                    date_ti = table.Column<DateTime>(type: "datetime", nullable: true),
                    qty = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'1'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_temp_basket", x => x.Id);
                    table.ForeignKey(
                        name: "temp_basket_ibfk_1",
                        column: x => x.Id,
                        principalTable: "customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "temp_basket_ibfk_3",
                        column: x => x.Id,
                        principalTable: "management",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "temp_basket_ibfk_2",
                        column: x => x.Id,
                        principalTable: "product2",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "vehicle",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Plate = table.Column<string>(type: "varchar(35)", nullable: true),
                    mark = table.Column<string>(type: "varchar(50)", nullable: true),
                    model = table.Column<string>(type: "varchar(50)", nullable: true),
                    model_year = table.Column<int>(type: "mediumint(4)", nullable: true),
                    visa_finish_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    visa_start_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    insurance_finish_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    insurance_start_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: true),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    exp1 = table.Column<string>(type: "varchar(255)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    type = table.Column<string>(type: "varchar(100)", nullable: true),
                    species = table.Column<string>(type: "varchar(100)", nullable: true),
                    engine_serial = table.Column<string>(type: "varchar(255)", nullable: true),
                    frame_serial = table.Column<string>(type: "varchar(255)", nullable: true),
                    traffic_document_number = table.Column<string>(type: "varchar(50)", nullable: true),
                    tax_date1 = table.Column<DateTime>(type: "date", nullable: true),
                    tax_date2 = table.Column<DateTime>(type: "date", nullable: true),
                    insurance2_finish_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    insurance2_start_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    vehicle_group = table.Column<string>(type: "varchar(100)", nullable: true),
                    purchase_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    purchase_date = table.Column<DateTime>(type: "timestamp", nullable: true),
                    vehicle_owner_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    vehicle_owner_customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    vehicle_owner_account_title = table.Column<string>(type: "varchar(150)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_vehicle", x => x.Id);
                    table.ForeignKey(
                        name: "vehicle_ibfk_1",
                        column: x => x.deletion_user_id,
                        principalTable: "management",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "elevator_customer_card_parameters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    elevator_card_parameters_id = table.Column<int>(type: "int(11)", nullable: true),
                    elevator_card_sub_parameters_id = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    elevator_card_parameters_id_for_type_2 = table.Column<int>(type: "int(11)", nullable: true),
                    ins_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    elevator_order = table.Column<int>(type: "int(11)", nullable: true),
                    picture = table.Column<byte[]>(nullable: true),
                    other_definition = table.Column<string>(type: "varchar(100)", nullable: true),
                    service_points_base_information_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_elevator_customer_card_parameters", x => x.Id);
                    table.ForeignKey(
                        name: "elevator_customer_card_parameters_ibfk_1",
                        column: x => x.service_points_base_information_id,
                        principalTable: "services_points_base_information",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "services_open",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    services_request_definatin_id = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    insert_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    start_date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    plan_finish_date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    finish_date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    opened_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    responsible_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    last_valid_date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    status = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    services_open_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    services_open_tunnel = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion_datetime = table.Column<DateTime>(type: "timestamp", nullable: true),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    services_points_base_information_id = table.Column<int>(type: "int(11)", nullable: true),
                    service_fee = table.Column<decimal>(type: "decimal(18,5)", nullable: true),
                    opens_id = table.Column<int>(type: "int(11)", nullable: true),
                    fault_stats = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    invoice_id = table.Column<int>(type: "int(11)", nullable: true),
                    options = table.Column<int>(type: "int(11)", nullable: true),
                    event_type = table.Column<int>(type: "int(11)", nullable: true),
                    master_services_open_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_open", x => x.Id);
                    table.ForeignKey(
                        name: "services_open_ibfk_3",
                        column: x => x.customer_id,
                        principalTable: "customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "services_open_ibfk_5",
                        column: x => x.invoice_id,
                        principalTable: "invoice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "services_open_ibfk_4",
                        column: x => x.master_services_open_id,
                        principalTable: "services_open",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "services_open_ibfk_2",
                        column: x => x.services_points_base_information_id,
                        principalTable: "services_points_base_information",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "services_open_ibfk_1",
                        column: x => x.services_request_definatin_id,
                        principalTable: "services_request_defination",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "services_product_change_list",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    first_photo = table.Column<string>(type: "longtext", nullable: true),
                    last_photo = table.Column<string>(type: "longtext", nullable: true),
                    invoice_stats = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    invoice_id = table.Column<int>(type: "int(11)", nullable: true),
                    note = table.Column<string>(type: "varchar(255)", nullable: true),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    qty = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    ins_date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    services_points_base_information_id = table.Column<int>(type: "int(11)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    services_open_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    detail_id = table.Column<int>(type: "int(11)", nullable: true),
                    unit_price_tax = table.Column<decimal>(type: "decimal(10,2)", nullable: true, defaultValueSql: "'0.00'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_product_change_list", x => x.Id);
                    table.ForeignKey(
                        name: "services_product_change_list_ibfk_3",
                        column: x => x.invoice_id,
                        principalTable: "invoice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "services_product_change_list_ibfk_1",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "services_product_change_list_ibfk_2",
                        column: x => x.services_points_base_information_id,
                        principalTable: "services_points_base_information",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "services_product_change_list_temp",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "varchar(255)", nullable: true),
                    first_photo = table.Column<string>(type: "longtext", nullable: true),
                    last_photo = table.Column<string>(type: "longtext", nullable: true),
                    invoice_stats = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    invoice_id = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    note = table.Column<string>(type: "varchar(255)", nullable: true),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    qty = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    ins_date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    services_points_base_information_id = table.Column<int>(type: "int(11)", nullable: true),
                    basket = table.Column<int>(type: "int(11)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    services_open_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    detail_id = table.Column<int>(type: "int(11)", nullable: true),
                    unit_price_tax = table.Column<decimal>(type: "decimal(10,2)", nullable: true, defaultValueSql: "'0.00'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_product_change_list_temp", x => x.Id);
                    table.ForeignKey(
                        name: "services_product_change_list_temp_ibfk_1",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "services_product_change_list_temp_ibfk_2",
                        column: x => x.services_points_base_information_id,
                        principalTable: "services_points_base_information",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "income_and_expense_types",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    definition = table.Column<string>(type: "varchar(150)", nullable: true),
                    range_again_day = table.Column<int>(type: "int(11)", nullable: true),
                    range_again_month = table.Column<int>(type: "int(11)", nullable: true),
                    range_again_year = table.Column<int>(type: "int(11)", nullable: true),
                    fixed_expenses = table.Column<string>(type: "varchar(15)", nullable: true),
                    variable_expenses = table.Column<string>(type: "varchar(15)", nullable: true),
                    group_ = table.Column<string>(type: "varchar(255)", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    exp3 = table.Column<string>(type: "varchar(255)", nullable: true),
                    type_income = table.Column<string>(type: "varchar(15)", nullable: true),
                    type_expense = table.Column<string>(type: "varchar(15)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    code = table.Column<string>(type: "varchar(60)", nullable: true),
                    is_vehicle_account = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    vehicle_id = table.Column<int>(type: "int(11)", nullable: true),
                    fuel_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    service_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    tire_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_income_and_expense_types", x => x.Id);
                    table.ForeignKey(
                        name: "income_and_expense_types_ibfk_1",
                        column: x => x.vehicle_id,
                        principalTable: "vehicle",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "mobile_scale_device",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Vehicle_Id = table.Column<int>(type: "int(11)", nullable: true),
                    Device_ID = table.Column<int>(type: "int(11)", nullable: true),
                    Name = table.Column<string>(type: "varchar(35)", nullable: true),
                    Device_Serial = table.Column<string>(type: "varchar(35)", nullable: true),
                    Device_Key = table.Column<string>(type: "varchar(35)", nullable: true),
                    Licance_Plate = table.Column<string>(type: "varchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mobile_scale_device", x => x.Id);
                    table.ForeignKey(
                        name: "vehicle_foreign",
                        column: x => x.Vehicle_Id,
                        principalTable: "vehicle",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "vehicle_kilometer",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    vehicle_id = table.Column<int>(type: "int(11)", nullable: true),
                    last_km = table.Column<int>(type: "int(11)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    ins_datetime = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    measurement_date = table.Column<DateTime>(type: "datetime", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    exp = table.Column<string>(type: "varchar(100)", nullable: true),
                    ins_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    detail_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_vehicle_kilometer", x => x.Id);
                    table.ForeignKey(
                        name: "vehicle_kilometer_ibfk_1",
                        column: x => x.vehicle_id,
                        principalTable: "vehicle",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "services_open_activity",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    services_open_id = table.Column<int>(type: "int(255)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    user_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_open_activity", x => x.Id);
                    table.ForeignKey(
                        name: "services_open_activity_ibfk_1",
                        column: x => x.services_open_id,
                        principalTable: "services_open",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "services_open_activity_ibfk_2",
                        column: x => x.user_id,
                        principalTable: "management",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "services_open_close",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    services_open_id = table.Column<int>(type: "int(11)", nullable: true),
                    closed_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    services_closed_tunnel = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    ins_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    closed_date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    services_last_status_defination_id = table.Column<int>(type: "int(11)", nullable: false, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_open_close", x => x.Id);
                    table.ForeignKey(
                        name: "services_open_close_ibfk_3",
                        column: x => x.closed_user_id,
                        principalTable: "management",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "services_open_close_ibfk_1",
                        column: x => x.services_last_status_defination_id,
                        principalTable: "services_last_status_defination",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "services_open_close_ibfk_2",
                        column: x => x.services_open_id,
                        principalTable: "services_open",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "services_open_close_control_defines",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    service_control_defines_id = table.Column<int>(type: "int(11)", nullable: true),
                    services_open_id = table.Column<int>(type: "int(11)", nullable: true),
                    result1 = table.Column<string>(type: "varchar(255)", nullable: true),
                    result2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    result3 = table.Column<string>(type: "varchar(255)", nullable: true),
                    start_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    service_control_defines_sub_id = table.Column<int>(type: "int(11)", nullable: true),
                    photo = table.Column<string>(type: "longtext", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_open_close_control_defines", x => x.Id);
                    table.ForeignKey(
                        name: "services_open_close_control_defines_ibfk_1",
                        column: x => x.service_control_defines_id,
                        principalTable: "service_control_defines",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "services_open_close_control_defines_ibfk_2",
                        column: x => x.services_open_id,
                        principalTable: "services_open",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "services_open_close_parameters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    services_open_id = table.Column<int>(type: "int(11)", nullable: true),
                    services_last_status_defination_infs_id = table.Column<int>(type: "int(11)", nullable: true),
                    field1 = table.Column<string>(type: "varchar(255)", nullable: true),
                    field2 = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_open_close_parameters", x => x.Id);
                    table.ForeignKey(
                        name: "services_open_close_parameters_ibfk_2",
                        column: x => x.services_last_status_defination_infs_id,
                        principalTable: "services_last_status_defination_infs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "services_open_close_parameters_ibfk_1",
                        column: x => x.services_open_id,
                        principalTable: "services_open",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "services_open_control_defines",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    service_control_defines_id = table.Column<int>(type: "int(11)", nullable: true),
                    services_open_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_open_control_defines", x => x.Id);
                    table.ForeignKey(
                        name: "services_open_control_defines_ibfk_1",
                        column: x => x.service_control_defines_id,
                        principalTable: "service_control_defines",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "services_open_control_defines_ibfk_2",
                        column: x => x.services_open_id,
                        principalTable: "services_open",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "services_open_parameters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    services_open_id = table.Column<int>(type: "int(11)", nullable: true),
                    services_request_defination_infs_id = table.Column<int>(type: "int(11)", nullable: true),
                    field1 = table.Column<string>(type: "varchar(255)", nullable: true),
                    field2 = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_services_open_parameters", x => x.Id);
                    table.ForeignKey(
                        name: "services_open_parameters_ibfk_1",
                        column: x => x.services_open_id,
                        principalTable: "services_open",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "services_open_parameters_ibfk_2",
                        column: x => x.services_request_defination_infs_id,
                        principalTable: "services_request_defination_infs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "mobile_scale_sync",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    sync_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    last_sync_id = table.Column<int>(type: "int(11)", nullable: true),
                    mobile_scale_device_id = table.Column<int>(type: "int(11)", nullable: true),
                    date = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    progress = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mobile_scale_sync", x => x.Id);
                    table.ForeignKey(
                        name: "mobile_scale_sync_ibfk_1",
                        column: x => x.mobile_scale_device_id,
                        principalTable: "mobile_scale_device",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "personel_cards",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    picture = table.Column<byte[]>(nullable: true),
                    personel_code = table.Column<string>(type: "varchar(100)", nullable: true),
                    department = table.Column<string>(type: "varchar(100)", nullable: true),
                    start_date = table.Column<DateTime>(type: "date", nullable: true),
                    exit_date = table.Column<DateTime>(type: "date", nullable: true),
                    reference_customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    reference_customer_account_title = table.Column<string>(type: "varchar(255)", nullable: true),
                    group_name = table.Column<string>(type: "varchar(100)", nullable: true),
                    task = table.Column<string>(type: "varchar(100)", nullable: true),
                    barcode = table.Column<string>(type: "varchar(100)", nullable: true),
                    close_exp = table.Column<string>(type: "varchar(200)", nullable: true),
                    driving_license = table.Column<string>(type: "varchar(100)", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    management_id = table.Column<int>(type: "int(11)", nullable: true),
                    managament_user_name = table.Column<string>(type: "varchar(256)", nullable: true),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    branch_id = table.Column<int>(type: "int(11)", nullable: true),
                    branch_defination = table.Column<string>(type: "varchar(150)", nullable: true),
                    visible = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    group_order = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    Deletion = table.Column<sbyte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_personel_cards", x => x.Id);
                    table.ForeignKey(
                        name: "personel_cards_ibfk_1",
                        column: x => x.customer_id,
                        principalTable: "customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "branch",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(type: "varchar(100)", nullable: true),
                    defination = table.Column<string>(type: "varchar(255)", nullable: true),
                    authorized_personel_id = table.Column<int>(type: "int(11)", nullable: true),
                    authorized_personel_name = table.Column<string>(type: "varchar(100)", nullable: true),
                    brach_adress = table.Column<string>(type: "varchar(255)", nullable: true),
                    exp1 = table.Column<string>(type: "varchar(255)", nullable: true),
                    exp2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion_date_time = table.Column<DateTime>(type: "timestamp", nullable: true),
                    phone_number = table.Column<string>(type: "varchar(15)", nullable: true),
                    fax_number = table.Column<string>(type: "varchar(15)", nullable: true),
                    mail_adress = table.Column<string>(type: "varchar(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_branch", x => x.Id);
                    table.ForeignKey(
                        name: "branch_ibfk_3",
                        column: x => x.authorized_personel_id,
                        principalTable: "personel_cards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "branch_ibfk_2",
                        column: x => x.deletion_user_id,
                        principalTable: "management",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "personel_cards_fee_information",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    monthly_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true, defaultValueSql: "'0.00'"),
                    weekly_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true, defaultValueSql: "'0.00'"),
                    daily_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true, defaultValueSql: "'0.00'"),
                    hours_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true, defaultValueSql: "'0.00'"),
                    minute_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true, defaultValueSql: "'0.00'"),
                    second_amount = table.Column<decimal>(type: "decimal(10,2)", nullable: true, defaultValueSql: "'0.00'"),
                    payment_method = table.Column<int>(type: "int(10)", nullable: true, defaultValueSql: "'0'"),
                    payment_period = table.Column<int>(type: "int(10)", nullable: true, defaultValueSql: "'0'"),
                    default_customer_bank_id = table.Column<int>(type: "int(11)", nullable: true),
                    payment_exp = table.Column<string>(type: "varchar(500)", nullable: true),
                    card_barcode = table.Column<string>(type: "varchar(255)", nullable: true),
                    card_name_surname = table.Column<string>(type: "varchar(255)", nullable: true),
                    card_exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    since_work = table.Column<DateTime>(type: "date", nullable: true),
                    date_of_departure = table.Column<DateTime>(type: "date", nullable: true),
                    agreement_exp = table.Column<string>(type: "varchar(255)", nullable: true),
                    daily_working_time = table.Column<decimal>(type: "decimal(10,2)", nullable: true, defaultValueSql: "'8.00'"),
                    customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    hours_of_overtime = table.Column<decimal>(type: "decimal(10,2)", nullable: true, defaultValueSql: "'0.00'"),
                    personel_task = table.Column<string>(type: "varchar(255)", nullable: true),
                    refarence_customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    refarance_name_surname = table.Column<string>(type: "varchar(255)", nullable: true),
                    personel_account_day = table.Column<string>(type: "varchar(40)", nullable: true, defaultValueSql: "'0'"),
                    add_auto_personel_fee = table.Column<string>(type: "varchar(10)", nullable: true),
                    personel_cards_id = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_personel_cards_fee_information", x => x.Id);
                    table.ForeignKey(
                        name: "personel_cards_fee_information_ibfk_1",
                        column: x => x.personel_cards_id,
                        principalTable: "personel_cards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "personel_cards_identity_information",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    personel_cards_id = table.Column<int>(type: "int(11)", nullable: true),
                    Wallet_serial_number = table.Column<string>(type: "varchar(10)", nullable: true),
                    Wallet_no = table.Column<int>(type: "int(7)", nullable: true),
                    TC_Identification_number = table.Column<long>(type: "bigint(11)", nullable: true),
                    Father_name = table.Column<string>(type: "varchar(50)", nullable: true),
                    mother_name = table.Column<string>(type: "varchar(50)", nullable: true),
                    Place_of_birth = table.Column<string>(type: "varchar(100)", nullable: true),
                    date_of_birth = table.Column<DateTime>(type: "date", nullable: true),
                    Marital_status = table.Column<string>(type: "varchar(50)", nullable: true),
                    religious = table.Column<string>(type: "varchar(100)", nullable: true),
                    blood_group = table.Column<string>(type: "varchar(10)", nullable: true),
                    place_of_issue = table.Column<string>(type: "varchar(100)", nullable: true),
                    province = table.Column<string>(type: "varchar(100)", nullable: true),
                    town = table.Column<string>(type: "varchar(100)", nullable: true),
                    number_of_children = table.Column<sbyte>(type: "tinyint(3)", nullable: true),
                    Military_status = table.Column<string>(type: "varchar(100)", nullable: true),
                    Neighborhood_village = table.Column<string>(type: "varchar(100)", nullable: true),
                    Vol = table.Column<string>(type: "varchar(10)", nullable: true),
                    Family_sequence_no = table.Column<string>(type: "varchar(10)", nullable: true),
                    Wallet_location = table.Column<string>(type: "varchar(50)", nullable: true),
                    The_reason_for_the_issuance_of_a_wallet = table.Column<string>(type: "varchar(100)", nullable: true),
                    Enrollment_number = table.Column<string>(type: "varchar(10)", nullable: true),
                    Wallet_issue_date = table.Column<DateTime>(type: "date", nullable: true),
                    Driving_license_serial_and_no = table.Column<string>(type: "varchar(100)", nullable: true),
                    document_no = table.Column<string>(type: "varchar(100)", nullable: true),
                    date_of_issue = table.Column<DateTime>(type: "date", nullable: true),
                    Drivers_license_class = table.Column<string>(type: "varchar(10)", nullable: true),
                    Sgk_number = table.Column<string>(type: "varchar(20)", nullable: true),
                    Educational_knowledge = table.Column<string>(type: "varchar(100)", nullable: true),
                    Sequenceno = table.Column<int>(name: "Sequence no", type: "int(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_personel_cards_identity_information", x => x.Id);
                    table.ForeignKey(
                        name: "personel_cards_identity_information_ibfk_1",
                        column: x => x.personel_cards_id,
                        principalTable: "personel_cards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "personel_cards_starting_ending",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    personel_cards_id = table.Column<int>(type: "int(11)", nullable: true),
                    ins_date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion_user_id = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    activity_date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_personel_cards_starting_ending", x => x.Id);
                    table.ForeignKey(
                        name: "personel_cards_starting_ending_ibfk_2",
                        column: x => x.deletion_user_id,
                        principalTable: "management",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "personel_cards_starting_ending_ibfk_1",
                        column: x => x.personel_cards_id,
                        principalTable: "personel_cards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "personel_meterial_debit",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    personel_cards_id = table.Column<int>(type: "int(11)", nullable: true),
                    qty = table.Column<int>(type: "int(11)", nullable: true),
                    explanation = table.Column<string>(type: "varchar(255)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    ins_date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_personel_meterial_debit", x => x.Id);
                    table.ForeignKey(
                        name: "personel_meterial_debit_ibfk_2",
                        column: x => x.personel_cards_id,
                        principalTable: "personel_cards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "personel_meterial_debit_ibfk_1",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "personel_permission_current",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    personel_cards_id = table.Column<int>(type: "int(255)", nullable: true),
                    permission_date = table.Column<DateTime>(type: "date", nullable: true),
                    ins_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    permission_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    exp = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_personel_permission_current", x => x.Id);
                    table.ForeignKey(
                        name: "personel_permission_current_ibfk_1",
                        column: x => x.personel_cards_id,
                        principalTable: "personel_cards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "personel_permission_current_ibfk_2",
                        column: x => x.user_id,
                        principalTable: "management",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "vehicle_debit",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    vehicle_id = table.Column<int>(type: "int(11)", nullable: true),
                    personel_cards_id = table.Column<int>(type: "int(11)", nullable: true),
                    ins_date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    active = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'1'"),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_vehicle_debit", x => x.Id);
                    table.ForeignKey(
                        name: "vehicle_debit_ibfk_2",
                        column: x => x.personel_cards_id,
                        principalTable: "personel_cards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "vehicle_debit_ibfk_1",
                        column: x => x.vehicle_id,
                        principalTable: "vehicle",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "vehicle_fuel_expense",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    income_and_expense_types_id = table.Column<int>(type: "int(11)", nullable: true),
                    vehicle_id = table.Column<int>(type: "int(11)", nullable: true),
                    storage_id = table.Column<int>(type: "int(11)", nullable: true),
                    fuel_company_customer_id = table.Column<int>(type: "int(11)", nullable: true),
                    fuel_product_id = table.Column<int>(type: "int(11)", nullable: true),
                    personel_cards_id = table.Column<int>(type: "int(11)", nullable: true),
                    liter = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    unit_price = table.Column<decimal>(type: "decimal(10,5)", nullable: true),
                    document_no = table.Column<string>(type: "varchar(15)", nullable: true),
                    supply_type = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    Purchase_Reasons = table.Column<string>(type: "varchar(255)", nullable: true, defaultValueSql: "'alım nedeni'"),
                    date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    ins_date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_vehicle_fuel_expense", x => x.Id);
                    table.ForeignKey(
                        name: "vehicle_fuel_expense_ibfk_4",
                        column: x => x.fuel_company_customer_id,
                        principalTable: "customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "vehicle_fuel_expense_ibfk_5",
                        column: x => x.fuel_product_id,
                        principalTable: "product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "vehicle_fuel_expense_ibfk_2",
                        column: x => x.income_and_expense_types_id,
                        principalTable: "income_and_expense_types",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "vehicle_fuel_expense_ibfk_6",
                        column: x => x.personel_cards_id,
                        principalTable: "personel_cards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "vehicle_fuel_expense_ibfk_3",
                        column: x => x.storage_id,
                        principalTable: "storage",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "vehicle_fuel_expense_ibfk_1",
                        column: x => x.vehicle_id,
                        principalTable: "vehicle",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "vehicle_product_expense",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    vehicle_id = table.Column<int>(type: "int(11)", nullable: true),
                    income_and_expense_types_id = table.Column<int>(type: "int(11)", nullable: true),
                    storage_id = table.Column<int>(type: "int(11)", nullable: true),
                    product_id = table.Column<int>(type: "int(11)", nullable: true),
                    personel_cards_id = table.Column<int>(type: "int(11)", nullable: true),
                    qty = table.Column<int>(type: "int(11)", nullable: true),
                    unit_price = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    doc_number = table.Column<string>(type: "varchar(16)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    ins_date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_vehicle_product_expense", x => x.Id);
                    table.ForeignKey(
                        name: "vehicle_product_expense_ibfk_2",
                        column: x => x.income_and_expense_types_id,
                        principalTable: "income_and_expense_types",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "vehicle_product_expense_ibfk_5",
                        column: x => x.personel_cards_id,
                        principalTable: "personel_cards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "vehicle_product_expense_ibfk_4",
                        column: x => x.product_id,
                        principalTable: "product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "vehicle_product_expense_ibfk_3",
                        column: x => x.storage_id,
                        principalTable: "storage",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "vehicle_product_expense_ibfk_6",
                        column: x => x.user_id,
                        principalTable: "management",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "vehicle_product_expense_ibfk_1",
                        column: x => x.vehicle_id,
                        principalTable: "vehicle",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "vehicle_tire_expense",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    vehicle_id = table.Column<int>(type: "int(11)", nullable: true),
                    income_and_expense_types_id = table.Column<int>(type: "int(11)", nullable: true),
                    storage_id = table.Column<int>(type: "int(11)", nullable: true),
                    tire_product_id = table.Column<int>(type: "int(11)", nullable: true),
                    personel_cards_id = table.Column<int>(type: "int(11)", nullable: true),
                    tire_life_km = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    qty = table.Column<int>(type: "int(11)", nullable: true),
                    unit_price = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    doc_number = table.Column<string>(type: "varchar(16)", nullable: true),
                    deletion = table.Column<sbyte>(type: "tinyint(3)", nullable: true, defaultValueSql: "'0'"),
                    user_id = table.Column<int>(type: "int(11)", nullable: true),
                    date_time = table.Column<DateTime>(type: "datetime", nullable: true),
                    ins_date_time = table.Column<DateTime>(type: "timestamp", nullable: true, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_vehicle_tire_expense", x => x.Id);
                    table.ForeignKey(
                        name: "vehicle_tire_expense_ibfk_2",
                        column: x => x.income_and_expense_types_id,
                        principalTable: "income_and_expense_types",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "vehicle_tire_expense_ibfk_5",
                        column: x => x.personel_cards_id,
                        principalTable: "personel_cards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "vehicle_tire_expense_ibfk_3",
                        column: x => x.storage_id,
                        principalTable: "storage",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "vehicle_tire_expense_ibfk_4",
                        column: x => x.tire_product_id,
                        principalTable: "product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "vehicle_tire_expense_ibfk_6",
                        column: x => x.user_id,
                        principalTable: "management",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "vehicle_tire_expense_ibfk_1",
                        column: x => x.vehicle_id,
                        principalTable: "vehicle",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "agricultural_id",
                table: "agricultural_customer",
                column: "agricultural_id");

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "agricultural_customer",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "arcihive_groups_id",
                table: "archive_management",
                column: "arcihive_groups_id");

            migrationBuilder.CreateIndex(
                name: "bank_name",
                table: "bank",
                column: "bank_name");

            migrationBuilder.CreateIndex(
                name: "bank_id",
                table: "bank_account",
                column: "bank_id");

            migrationBuilder.CreateIndex(
                name: "bank_name",
                table: "bank_department",
                column: "bank_name");

            migrationBuilder.CreateIndex(
                name: "country",
                table: "bank_department",
                column: "country");

            migrationBuilder.CreateIndex(
                name: "town",
                table: "bank_department",
                column: "town");

            migrationBuilder.CreateIndex(
                name: "authorized_personel_id",
                table: "branch",
                column: "authorized_personel_id");

            migrationBuilder.CreateIndex(
                name: "deletion_user_id",
                table: "branch",
                column: "deletion_user_id");

            migrationBuilder.CreateIndex(
                name: "cargo_fee_defines_ibfk_1",
                table: "cargo_fee_defines",
                column: "cargo_product_id");

            migrationBuilder.CreateIndex(
                name: "bank_id",
                table: "check_in",
                column: "bank_id");

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "check_in",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "deletion",
                table: "check_in",
                column: "deletion");

            migrationBuilder.CreateIndex(
                name: "reminder_user",
                table: "check_in",
                column: "reminder_user");

            migrationBuilder.CreateIndex(
                name: "type_",
                table: "check_in",
                column: "type_");

            migrationBuilder.CreateIndex(
                name: "bank_id",
                table: "check_output",
                column: "bank_id");

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "check_output",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "deletion",
                table: "check_output",
                column: "deletion");

            migrationBuilder.CreateIndex(
                name: "reminder_user",
                table: "check_output",
                column: "reminder_user");

            migrationBuilder.CreateIndex(
                name: "type_",
                table: "check_output",
                column: "type_");

            migrationBuilder.CreateIndex(
                name: "reminder_user",
                table: "commercial_paper_in",
                column: "reminder_user");

            migrationBuilder.CreateIndex(
                name: "reminder_user",
                table: "commercial_paper_out",
                column: "reminder_user");

            migrationBuilder.CreateIndex(
                name: "Currency_Id",
                table: "currency_rates_of_exchange",
                column: "Currency_Id");

            migrationBuilder.CreateIndex(
                name: "Basket",
                table: "currency_rates_of_exchange_temp",
                column: "Basket");

            migrationBuilder.CreateIndex(
                name: "customer_index3",
                table: "customer",
                column: "account_grup");

            migrationBuilder.CreateIndex(
                name: "customer_index2",
                table: "customer",
                column: "account_title");

            migrationBuilder.CreateIndex(
                name: "customer_index4",
                table: "customer",
                column: "account_type");

            migrationBuilder.CreateIndex(
                name: "contact person",
                table: "customer",
                column: "contact_person");

            migrationBuilder.CreateIndex(
                name: "ispersonel",
                table: "customer",
                column: "ispersonel");

            migrationBuilder.CreateIndex(
                name: "ndx_customer_main_customer_id",
                table: "customer",
                column: "main_customer_id");

            migrationBuilder.CreateIndex(
                name: "route_id",
                table: "customer",
                column: "route_id");

            migrationBuilder.CreateIndex(
                name: "visible",
                table: "customer",
                column: "visible");

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "customer_adress",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "customer_bank",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "bank_id",
                table: "customer_creditcard",
                column: "bank_id");

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "customer_creditcard",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "customer_current_input",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "deletion",
                table: "customer_current_input",
                column: "deletion");

            migrationBuilder.CreateIndex(
                name: "ndx_input_type",
                table: "customer_current_input",
                column: "type");

            migrationBuilder.CreateIndex(
                name: "ndx_input_type_2",
                table: "customer_current_input",
                columns: new[] { "type", "customer_id" });

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "customer_current_output",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "deletion",
                table: "customer_current_output",
                column: "deletion");

            migrationBuilder.CreateIndex(
                name: "ndx_output_type",
                table: "customer_current_output",
                column: "type");

            migrationBuilder.CreateIndex(
                name: "ndx_output_type_customer_id",
                table: "customer_current_output",
                columns: new[] { "type", "customer_id" });

            migrationBuilder.CreateIndex(
                name: "customer_current_output_id",
                table: "customer_current_output_service_notes",
                column: "customer_current_output_id");

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "customer_document_definitions",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "current_input_id",
                table: "customer_offset",
                column: "current_input_id");

            migrationBuilder.CreateIndex(
                name: "current_output_id",
                table: "customer_offset",
                column: "current_output_id");

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "customer_offset",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "customer_producer_receipt_tax_defines",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "producer_receipt_tax_defines_id",
                table: "customer_producer_receipt_tax_defines",
                columns: new[] { "producer_receipt_tax_defines_id", "customer_id" });

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "customer_subscription",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "subscription_id",
                table: "customer_subscription",
                column: "subscription_id");

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "customer_subscription_current",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "customer_subscription_id",
                table: "customer_subscription_detail",
                column: "customer_subscription_id");

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "device_location",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "device_imei",
                table: "device_location",
                column: "device_imei");

            migrationBuilder.CreateIndex(
                name: "ins_date_time",
                table: "device_location",
                column: "ins_date_time");

            migrationBuilder.CreateIndex(
                name: "customer_document_definitions_id",
                table: "documents",
                column: "customer_document_definitions_id");

            migrationBuilder.CreateIndex(
                name: "elevator_card_parameters_id",
                table: "elevator_card_sub_parameters",
                column: "elevator_card_parameters_id");

            migrationBuilder.CreateIndex(
                name: "service_points_base_information_id",
                table: "elevator_customer_card_parameters",
                column: "service_points_base_information_id");

            migrationBuilder.CreateIndex(
                name: "offer_temp_id",
                table: "elevator_installation_list",
                column: "offer_temp_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "stats_change_user_id",
                table: "elevator_revision_offer_parameters",
                column: "stats_change_user_id");

            migrationBuilder.CreateIndex(
                name: "elevator_revision_parameters_id",
                table: "elevator_revision_offer_parameters_temp",
                column: "elevator_revision_parameters_id");

            migrationBuilder.CreateIndex(
                name: "elevator_revision_parameters_id",
                table: "elevator_revision_parameters_product",
                column: "elevator_revision_parameters_id");

            migrationBuilder.CreateIndex(
                name: "in_ex_types_id",
                table: "expense",
                column: "in_ex_types_id");

            migrationBuilder.CreateIndex(
                name: "farm_animals_id",
                table: "farm_animals_activity",
                column: "farm_animals_id");

            migrationBuilder.CreateIndex(
                name: "farm_feed_mixture_id",
                table: "farm_feed_mixture_products",
                column: "farm_feed_mixture_id");

            migrationBuilder.CreateIndex(
                name: "farms_id",
                table: "farm_paddock",
                column: "farms_id");

            migrationBuilder.CreateIndex(
                name: "Form_Name",
                table: "form_button_defines",
                column: "Form_Name");

            migrationBuilder.CreateIndex(
                name: "in_ex_types_id",
                table: "income",
                column: "in_ex_types_id");

            migrationBuilder.CreateIndex(
                name: "vehicle_id",
                table: "income_and_expense_types",
                column: "vehicle_id");

            migrationBuilder.CreateIndex(
                name: "installments_plan_basket_id",
                table: "installments",
                column: "installments_plan_basket_id");

            migrationBuilder.CreateIndex(
                name: "ndx_installments_plan_basket_id",
                table: "installments_plan",
                column: "basket_id");

            migrationBuilder.CreateIndex(
                name: "ndx_installments_plan_customer_id",
                table: "installments_plan",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "ndx_installments_plan_pay_date",
                table: "installments_plan",
                column: "pay_date");

            migrationBuilder.CreateIndex(
                name: "ndx_installments_plan_user_id",
                table: "installments_plan",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "ndx_installments_plan_temp_basket_id",
                table: "installments_plan_temp",
                column: "basket_id");

            migrationBuilder.CreateIndex(
                name: "nx_invoice_customer_id",
                table: "invoice",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "deletion",
                table: "invoice",
                column: "deletion");

            migrationBuilder.CreateIndex(
                name: "if_edit_new_invoice_id",
                table: "invoice",
                column: "if_edit_new_invoice_id");

            migrationBuilder.CreateIndex(
                name: "ndx_invoice_invoice_product_basket",
                table: "invoice",
                column: "invoice_product_basket");

            migrationBuilder.CreateIndex(
                name: "ndx_invoice_type",
                table: "invoice",
                column: "type");

            migrationBuilder.CreateIndex(
                name: "ndx_invoice_user_id",
                table: "invoice",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "basket",
                table: "invoice_product",
                column: "basket");

            migrationBuilder.CreateIndex(
                name: "deletion",
                table: "invoice_product",
                column: "deletion");

            migrationBuilder.CreateIndex(
                name: "product_id",
                table: "invoice_product",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "invoice_id",
                table: "invoice_shipment_trace",
                column: "invoice_id");

            migrationBuilder.CreateIndex(
                name: "invoice_product_basket",
                table: "invoice_shipment_trace",
                column: "invoice_product_basket");

            migrationBuilder.CreateIndex(
                name: "product_id",
                table: "invoice_shipment_trace",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "storage_id",
                table: "invoice_shipment_trace",
                column: "storage_id");

            migrationBuilder.CreateIndex(
                name: "invoice_id",
                table: "invoice_shipment_trace_temp",
                column: "invoice_id");

            migrationBuilder.CreateIndex(
                name: "product_id",
                table: "invoice_shipment_trace_temp",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "storage_id",
                table: "invoice_shipment_trace_temp",
                column: "storage_id");

            migrationBuilder.CreateIndex(
                name: "basket",
                table: "label_temp",
                column: "basket");

            migrationBuilder.CreateIndex(
                name: "load_type_definition_id",
                table: "load_activity_temp",
                column: "load_type_definition_id");

            migrationBuilder.CreateIndex(
                name: "defination",
                table: "load_package_definitions",
                column: "defination");

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "maintenance",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "default_pos",
                table: "management",
                column: "default_pos");

            migrationBuilder.CreateIndex(
                name: "default_safe",
                table: "management",
                column: "default_safe");

            migrationBuilder.CreateIndex(
                name: "management_roles_id",
                table: "management_roles_authorization",
                column: "management_roles_id");

            migrationBuilder.CreateIndex(
                name: "management_roles_authorization_id",
                table: "management_roles_service_request_authorization",
                column: "management_roles_authorization_id");

            migrationBuilder.CreateIndex(
                name: "ndx_message_3",
                table: "message",
                column: "receiver");

            migrationBuilder.CreateIndex(
                name: "ndx_message_2",
                table: "message",
                column: "sender_id");

            migrationBuilder.CreateIndex(
                name: "ndx_message_1",
                table: "message",
                column: "status");

            migrationBuilder.CreateIndex(
                name: "vehicle_foreign",
                table: "mobile_scale_device",
                column: "Vehicle_Id");

            migrationBuilder.CreateIndex(
                name: "Customer_id",
                table: "mobile_scale_product_purchase",
                column: "Customer_id");

            migrationBuilder.CreateIndex(
                name: "Mobile_Scale_Customer_Id",
                table: "mobile_scale_product_purchase",
                column: "Mobile_Scale_Customer_Id");

            migrationBuilder.CreateIndex(
                name: "mobile_scale_product_purchase_id",
                table: "mobile_scale_product_purchase",
                column: "mobile_scale_product_purchase_id");

            migrationBuilder.CreateIndex(
                name: "Pickup_Time",
                table: "mobile_scale_product_purchase",
                column: "Pickup_Time");

            migrationBuilder.CreateIndex(
                name: "stats",
                table: "mobile_scale_product_purchase",
                column: "stats");

            migrationBuilder.CreateIndex(
                name: "user_id",
                table: "mobile_scale_product_purchase",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "Verification_Number",
                table: "mobile_scale_product_purchase_temp",
                column: "Verification_Number",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "mobile_scale_device",
                table: "mobile_scale_sync",
                column: "mobile_scale_device_id");

            migrationBuilder.CreateIndex(
                name: "sync_type",
                table: "mobile_scale_sync",
                column: "sync_type");

            migrationBuilder.CreateIndex(
                name: "ndx_orders_customer_id",
                table: "orders",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "ndx_orders_stats",
                table: "orders",
                column: "stats");

            migrationBuilder.CreateIndex(
                name: "ndx_orders_type",
                table: "orders",
                column: "type");

            migrationBuilder.CreateIndex(
                name: "ndx_orders_user_id",
                table: "orders",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "ndx_ordeR_products_basket",
                table: "orders_products",
                column: "basket");

            migrationBuilder.CreateIndex(
                name: "ndx_order_products_product_id",
                table: "orders_products",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "ndx_orders_products_sub_barcode",
                table: "orders_products_sub",
                column: "barcode");

            migrationBuilder.CreateIndex(
                name: "ndx_orders_products_sub_order_products_basket_id",
                table: "orders_products_sub",
                column: "order_products_basket_id");

            migrationBuilder.CreateIndex(
                name: "ndx_orders_products_sub_product_id",
                table: "orders_products_sub",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "ndx_orders_products_sub_product_name",
                table: "orders_products_sub",
                column: "product_name");

            migrationBuilder.CreateIndex(
                name: "ndx_orders_products_sub_basket",
                table: "orders_products_sub",
                column: "qty");

            migrationBuilder.CreateIndex(
                name: "user_id",
                table: "parameters",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "branch_id",
                table: "personel_cards",
                column: "branch_id");

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "personel_cards",
                column: "customer_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "personel_cards_id",
                table: "personel_cards_fee_information",
                column: "personel_cards_id");

            migrationBuilder.CreateIndex(
                name: "personel_cards_id",
                table: "personel_cards_identity_information",
                column: "personel_cards_id");

            migrationBuilder.CreateIndex(
                name: "deletion_user_id",
                table: "personel_cards_starting_ending",
                column: "deletion_user_id");

            migrationBuilder.CreateIndex(
                name: "personel_cards_id",
                table: "personel_cards_starting_ending",
                column: "personel_cards_id");

            migrationBuilder.CreateIndex(
                name: "personel_cards_id",
                table: "personel_meterial_debit",
                column: "personel_cards_id");

            migrationBuilder.CreateIndex(
                name: "product_id",
                table: "personel_meterial_debit",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "personel_cards_id",
                table: "personel_permission_current",
                column: "personel_cards_id");

            migrationBuilder.CreateIndex(
                name: "user_id",
                table: "personel_permission_current",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "bank_account_id",
                table: "pos",
                column: "bank_account_id");

            migrationBuilder.CreateIndex(
                name: "pos_id",
                table: "pos_detail",
                column: "pos_id");

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "pos_in",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "deletion",
                table: "pos_in",
                column: "deletion");

            migrationBuilder.CreateIndex(
                name: "detail_id",
                table: "pos_in",
                column: "detail_id");

            migrationBuilder.CreateIndex(
                name: "pos_id",
                table: "pos_in",
                column: "pos_id");

            migrationBuilder.CreateIndex(
                name: "user_id",
                table: "pos_in",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "pos_id_2",
                table: "pos_in",
                columns: new[] { "pos_id", "customer_id" });

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "pos_out",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "deletion",
                table: "pos_out",
                column: "deletion");

            migrationBuilder.CreateIndex(
                name: "pos_id",
                table: "pos_out",
                column: "pos_id");

            migrationBuilder.CreateIndex(
                name: "user_id",
                table: "pos_out",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "pos_id_2",
                table: "pos_out",
                columns: new[] { "pos_id", "customer_id" });

            migrationBuilder.CreateIndex(
                name: "price_code",
                table: "pricing",
                column: "price_code");

            migrationBuilder.CreateIndex(
                name: "price_description",
                table: "pricing",
                column: "price_description");

            migrationBuilder.CreateIndex(
                name: "barcode",
                table: "product",
                column: "barcode");

            migrationBuilder.CreateIndex(
                name: "code",
                table: "product",
                column: "code");

            migrationBuilder.CreateIndex(
                name: "group1",
                table: "product",
                column: "group1");

            migrationBuilder.CreateIndex(
                name: "group2",
                table: "product",
                column: "group2");

            migrationBuilder.CreateIndex(
                name: "product_ndx_main_product_id",
                table: "product",
                column: "main_product_id");

            migrationBuilder.CreateIndex(
                name: "mark",
                table: "product",
                column: "mark");

            migrationBuilder.CreateIndex(
                name: "name",
                table: "product",
                column: "name");

            migrationBuilder.CreateIndex(
                name: "visible",
                table: "product",
                column: "visible");

            migrationBuilder.CreateIndex(
                name: "ndx_product_color_product_id",
                table: "product_color",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "parent_id",
                table: "product_groups",
                column: "parent_id");

            migrationBuilder.CreateIndex(
                name: "product_id",
                table: "product_nutritional_values",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "product_id",
                table: "product_serials",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "product_id",
                table: "product_unit",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "production_recipe_defines_id",
                table: "production_orders_temp",
                column: "production_recipe_defines_id");

            migrationBuilder.CreateIndex(
                name: "product_id",
                table: "production_recipe_defines",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "product_id",
                table: "production_recipe_formula",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "production_recipe_defines_id",
                table: "production_recipe_formula",
                column: "production_recipe_defines_id");

            migrationBuilder.CreateIndex(
                name: "basket",
                table: "restaurant_order",
                column: "restaurant_order_list_basket_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "status",
                table: "restaurant_order",
                column: "status");

            migrationBuilder.CreateIndex(
                name: "table_id",
                table: "restaurant_order",
                columns: new[] { "table_id", "status" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "restaurant_order_temp_basket_id",
                table: "restaurant_order_list",
                column: "basket");

            migrationBuilder.CreateIndex(
                name: "deletion",
                table: "restaurant_order_list",
                column: "deletion");

            migrationBuilder.CreateIndex(
                name: "user_id",
                table: "restaurant_order_list",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "department_id",
                table: "restaurant_printer_parameters",
                column: "department_id");

            migrationBuilder.CreateIndex(
                name: "product_group3_id",
                table: "restaurant_printer_parameters",
                column: "product_group3_id");

            migrationBuilder.CreateIndex(
                name: "product_id",
                table: "restaurant_product_flavor_details",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "user_id",
                table: "restaurant_speed_group_buttons",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "table_code",
                table: "restaurant_tables",
                columns: new[] { "table_code", "table_number" });

            migrationBuilder.CreateIndex(
                name: "definition",
                table: "route",
                column: "definition");

            migrationBuilder.CreateIndex(
                name: "responsible_id",
                table: "route",
                column: "responsible_id");

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "safe_input",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "deletion",
                table: "safe_input",
                column: "deletion");

            migrationBuilder.CreateIndex(
                name: "safe_id",
                table: "safe_input",
                column: "safe_id");

            migrationBuilder.CreateIndex(
                name: "user_id",
                table: "safe_input",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "safe_id_2",
                table: "safe_input",
                columns: new[] { "safe_id", "customer_id" });

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "safe_output",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "deletion",
                table: "safe_output",
                column: "deletion");

            migrationBuilder.CreateIndex(
                name: "safe_id",
                table: "safe_output",
                column: "safe_id");

            migrationBuilder.CreateIndex(
                name: "user_id",
                table: "safe_output",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "safe_id_2",
                table: "safe_output",
                columns: new[] { "safe_id", "customer_id" });

            migrationBuilder.CreateIndex(
                name: "services_category_sub_definition_id",
                table: "service_control_defines",
                column: "services_category_sub_definition_id");

            migrationBuilder.CreateIndex(
                name: "service_request_id",
                table: "service_control_defines",
                column: "services_request_defination_id");

            migrationBuilder.CreateIndex(
                name: "service_control_defines_id",
                table: "service_control_sub_defines",
                column: "service_control_defines_id");

            migrationBuilder.CreateIndex(
                name: "services_category_definition_id",
                table: "services_category_sub_definition",
                column: "services_category_definition_id");

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "services_contract",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "services_category_definition_id",
                table: "services_contract",
                column: "services_category_definition_id");

            migrationBuilder.CreateIndex(
                name: "services_request_defination_id",
                table: "services_contract",
                column: "services_request_defination_id");

            migrationBuilder.CreateIndex(
                name: "services_category_definition_id",
                table: "services_last_status_defination",
                column: "services_category_definition_id");

            migrationBuilder.CreateIndex(
                name: "sevices_request_defination_id",
                table: "services_last_status_defination",
                column: "sevices_request_defination_id");

            migrationBuilder.CreateIndex(
                name: "sms_themes_id",
                table: "services_last_status_defination",
                column: "sms_themes_id");

            migrationBuilder.CreateIndex(
                name: "services_last_status_defination_id",
                table: "services_last_status_defination_infs",
                column: "services_last_status_defination_id");

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "services_open",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "invoice_id",
                table: "services_open",
                column: "invoice_id");

            migrationBuilder.CreateIndex(
                name: "master_services_open_id",
                table: "services_open",
                column: "master_services_open_id");

            migrationBuilder.CreateIndex(
                name: "services_points_base_information_id",
                table: "services_open",
                column: "services_points_base_information_id");

            migrationBuilder.CreateIndex(
                name: "services_request_definatin_id",
                table: "services_open",
                column: "services_request_definatin_id");

            migrationBuilder.CreateIndex(
                name: "services_open_id",
                table: "services_open_activity",
                column: "services_open_id");

            migrationBuilder.CreateIndex(
                name: "user_id",
                table: "services_open_activity",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "closed_user_id",
                table: "services_open_close",
                column: "closed_user_id");

            migrationBuilder.CreateIndex(
                name: "services_last_status_defination_id",
                table: "services_open_close",
                column: "services_last_status_defination_id");

            migrationBuilder.CreateIndex(
                name: "services_open_id",
                table: "services_open_close",
                column: "services_open_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "service_control_defines_id",
                table: "services_open_close_control_defines",
                column: "service_control_defines_id");

            migrationBuilder.CreateIndex(
                name: "services_open_id",
                table: "services_open_close_control_defines",
                column: "services_open_id");

            migrationBuilder.CreateIndex(
                name: "services_last_status_defination_infs_id",
                table: "services_open_close_parameters",
                column: "services_last_status_defination_infs_id");

            migrationBuilder.CreateIndex(
                name: "services_open_id",
                table: "services_open_close_parameters",
                column: "services_open_id");

            migrationBuilder.CreateIndex(
                name: "service_control_defines_id",
                table: "services_open_control_defines",
                column: "service_control_defines_id");

            migrationBuilder.CreateIndex(
                name: "services_open_id",
                table: "services_open_control_defines",
                column: "services_open_id");

            migrationBuilder.CreateIndex(
                name: "service_control_defines_id",
                table: "services_open_control_defines_temp",
                column: "service_control_defines_id");

            migrationBuilder.CreateIndex(
                name: "services_open_id",
                table: "services_open_parameters",
                column: "services_open_id");

            migrationBuilder.CreateIndex(
                name: "services_request_defination_infs_id",
                table: "services_open_parameters",
                column: "services_request_defination_infs_id");

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "services_points_base_information",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "services_category_definition_id",
                table: "services_points_base_information",
                column: "services_category_definition_id");

            migrationBuilder.CreateIndex(
                name: "services_contract_id",
                table: "services_points_base_information",
                column: "services_contract_id");

            migrationBuilder.CreateIndex(
                name: "invoice_id",
                table: "services_product_change_list",
                column: "invoice_id");

            migrationBuilder.CreateIndex(
                name: "product_id",
                table: "services_product_change_list",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "services_points_base_information_id",
                table: "services_product_change_list",
                column: "services_points_base_information_id");

            migrationBuilder.CreateIndex(
                name: "product_id",
                table: "services_product_change_list_temp",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "services_points_base_information_id",
                table: "services_product_change_list_temp",
                column: "services_points_base_information_id");

            migrationBuilder.CreateIndex(
                name: "services_category_definition_id",
                table: "services_request_defination",
                column: "services_category_definition_id");

            migrationBuilder.CreateIndex(
                name: "sms_theme_id",
                table: "services_request_defination",
                column: "sms_themes_id");

            migrationBuilder.CreateIndex(
                name: "services_request_defination_id",
                table: "services_request_defination_infs",
                column: "services_request_defination_id");

            migrationBuilder.CreateIndex(
                name: "services_setup_defines_id",
                table: "services_setup_define_parameters",
                column: "services_setup_defines_id");

            migrationBuilder.CreateIndex(
                name: "services_request_defination_id",
                table: "services_setup_request_list",
                column: "services_request_defination_id");

            migrationBuilder.CreateIndex(
                name: "user_id",
                table: "services_speed_buttons",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "sms_task_list_id",
                table: "sms_balance",
                column: "sms_task_list_id");

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "sms_task_list",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "user_id",
                table: "speed_sales_buttons",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "deletion",
                table: "stock_activities",
                column: "deletion");

            migrationBuilder.CreateIndex(
                name: "detail_id",
                table: "stock_activities",
                column: "detail_id");

            migrationBuilder.CreateIndex(
                name: "ndx_stock_activities_product_id_2",
                table: "stock_activities",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "ndx_stock_activities_storage",
                table: "stock_activities",
                column: "storage");

            migrationBuilder.CreateIndex(
                name: "ndx_stock_activities_type",
                table: "stock_activities",
                column: "type");

            migrationBuilder.CreateIndex(
                name: "ndx_stock_activities_product_id",
                table: "stock_activities",
                columns: new[] { "product_id", "type" });

            migrationBuilder.CreateIndex(
                name: "invoice_product_sub_id",
                table: "stock_activities_sub",
                column: "invoice_product_sub_id");

            migrationBuilder.CreateIndex(
                name: "product_id",
                table: "stock_activities_sub",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "stock_counting_sub_id",
                table: "stock_activities_sub",
                column: "stock_counting_sub_id");

            migrationBuilder.CreateIndex(
                name: "storage",
                table: "stock_activities_sub",
                column: "storage");

            migrationBuilder.CreateIndex(
                name: "user_id",
                table: "stock_activities_sub",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "invoice_product_sub_id",
                table: "stock_activities_sub_main",
                column: "invoice_product_sub_main_id");

            migrationBuilder.CreateIndex(
                name: "product_id",
                table: "stock_activities_sub_main",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "storage",
                table: "stock_activities_sub_main",
                column: "storage");

            migrationBuilder.CreateIndex(
                name: "user_id",
                table: "stock_activities_sub_main",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "product_id",
                table: "stock_counting",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "user_id",
                table: "stock_counting",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "basket",
                table: "stock_counting_temp",
                column: "basket");

            migrationBuilder.CreateIndex(
                name: "product_id",
                table: "stock_counting_temp",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "feed_back",
                table: "support",
                column: "feed_back");

            migrationBuilder.CreateIndex(
                name: "Index_3",
                table: "support",
                column: "receiver_unit");

            migrationBuilder.CreateIndex(
                name: "customer_id",
                table: "temp_basket",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "product_id",
                table: "temp_basket",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "user_id",
                table: "temp_basket",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "product_tax_list",
                table: "temp_basket_tax",
                column: "product_tax_list_id");

            migrationBuilder.CreateIndex(
                name: "temp_basket_id",
                table: "temp_basket_tax",
                column: "temp_basket_id");

            migrationBuilder.CreateIndex(
                name: "deletion_user_id",
                table: "vehicle",
                column: "deletion_user_id");

            migrationBuilder.CreateIndex(
                name: "personel_cards_id",
                table: "vehicle_debit",
                column: "personel_cards_id");

            migrationBuilder.CreateIndex(
                name: "vehicle_id",
                table: "vehicle_debit",
                column: "vehicle_id");

            migrationBuilder.CreateIndex(
                name: "fuel_company_customer_id",
                table: "vehicle_fuel_expense",
                column: "fuel_company_customer_id");

            migrationBuilder.CreateIndex(
                name: "fuel_product_id",
                table: "vehicle_fuel_expense",
                column: "fuel_product_id");

            migrationBuilder.CreateIndex(
                name: "income_and_expense_types_id",
                table: "vehicle_fuel_expense",
                column: "income_and_expense_types_id");

            migrationBuilder.CreateIndex(
                name: "personel_cards_id",
                table: "vehicle_fuel_expense",
                column: "personel_cards_id");

            migrationBuilder.CreateIndex(
                name: "storage_id",
                table: "vehicle_fuel_expense",
                column: "storage_id");

            migrationBuilder.CreateIndex(
                name: "vehicle_id",
                table: "vehicle_fuel_expense",
                column: "vehicle_id");

            migrationBuilder.CreateIndex(
                name: "vehicle_id",
                table: "vehicle_kilometer",
                column: "vehicle_id");

            migrationBuilder.CreateIndex(
                name: "income_and_expense_types_id",
                table: "vehicle_product_expense",
                column: "income_and_expense_types_id");

            migrationBuilder.CreateIndex(
                name: "personel_cards_id",
                table: "vehicle_product_expense",
                column: "personel_cards_id");

            migrationBuilder.CreateIndex(
                name: "tire_product_id",
                table: "vehicle_product_expense",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "storage_id",
                table: "vehicle_product_expense",
                column: "storage_id");

            migrationBuilder.CreateIndex(
                name: "user_id",
                table: "vehicle_product_expense",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "vehicle_id",
                table: "vehicle_product_expense",
                column: "vehicle_id");

            migrationBuilder.CreateIndex(
                name: "income_and_expense_types_id",
                table: "vehicle_tire_expense",
                column: "income_and_expense_types_id");

            migrationBuilder.CreateIndex(
                name: "personel_cards_id",
                table: "vehicle_tire_expense",
                column: "personel_cards_id");

            migrationBuilder.CreateIndex(
                name: "storage_id",
                table: "vehicle_tire_expense",
                column: "storage_id");

            migrationBuilder.CreateIndex(
                name: "tire_product_id",
                table: "vehicle_tire_expense",
                column: "tire_product_id");

            migrationBuilder.CreateIndex(
                name: "user_id",
                table: "vehicle_tire_expense",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "vehicle_id",
                table: "vehicle_tire_expense",
                column: "vehicle_id");

            migrationBuilder.CreateIndex(
                name: "ins_date_time",
                table: "waybill",
                column: "ins_date_time");

            migrationBuilder.CreateIndex(
                name: "type",
                table: "waybill",
                column: "type");

            migrationBuilder.CreateIndex(
                name: "waybill_product_basket",
                table: "waybill",
                column: "waybill_products_basket");

            migrationBuilder.AddForeignKey(
                name: "personel_cards_ibfk_2",
                table: "personel_cards",
                column: "branch_id",
                principalTable: "branch",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "personel_cards_ibfk_1",
                table: "personel_cards");

            migrationBuilder.DropForeignKey(
                name: "bank_account_ibfk_1",
                table: "bank_account");

            migrationBuilder.DropForeignKey(
                name: "branch_ibfk_3",
                table: "branch");

            migrationBuilder.DropTable(
                name: "agricultural_customer");

            migrationBuilder.DropTable(
                name: "agricultural_detail");

            migrationBuilder.DropTable(
                name: "archive_groups");

            migrationBuilder.DropTable(
                name: "archive_management");

            migrationBuilder.DropTable(
                name: "bank_account_in");

            migrationBuilder.DropTable(
                name: "bank_account_out");

            migrationBuilder.DropTable(
                name: "bank_department");

            migrationBuilder.DropTable(
                name: "basket");

            migrationBuilder.DropTable(
                name: "business_creditcard");

            migrationBuilder.DropTable(
                name: "business_creditcard_output");

            migrationBuilder.DropTable(
                name: "business_creditcard_output_payment_plan");

            migrationBuilder.DropTable(
                name: "business_creditcard_output_payment_plan_temp");

            migrationBuilder.DropTable(
                name: "caller_id_log");

            migrationBuilder.DropTable(
                name: "cargo_fee_defines");

            migrationBuilder.DropTable(
                name: "check_in");

            migrationBuilder.DropTable(
                name: "check_output");

            migrationBuilder.DropTable(
                name: "check_temp");

            migrationBuilder.DropTable(
                name: "coming_document");

            migrationBuilder.DropTable(
                name: "commercial_paper_in");

            migrationBuilder.DropTable(
                name: "commercial_paper_out");

            migrationBuilder.DropTable(
                name: "commercial_paper_temp");

            migrationBuilder.DropTable(
                name: "currency_in");

            migrationBuilder.DropTable(
                name: "currency_out");

            migrationBuilder.DropTable(
                name: "currency_rates_of_exchange");

            migrationBuilder.DropTable(
                name: "currency_rates_of_exchange_temp");

            migrationBuilder.DropTable(
                name: "customer_adress");

            migrationBuilder.DropTable(
                name: "customer_bank");

            migrationBuilder.DropTable(
                name: "customer_correspondence_report_files");

            migrationBuilder.DropTable(
                name: "customer_creditcard");

            migrationBuilder.DropTable(
                name: "customer_current_output_service_notes");

            migrationBuilder.DropTable(
                name: "customer_document_definitions");

            migrationBuilder.DropTable(
                name: "customer_offset");

            migrationBuilder.DropTable(
                name: "customer_personel_activity");

            migrationBuilder.DropTable(
                name: "customer_personel_activity_temp");

            migrationBuilder.DropTable(
                name: "customer_producer_receipt_tax_defines");

            migrationBuilder.DropTable(
                name: "customer_subscription_current");

            migrationBuilder.DropTable(
                name: "customer_subscription_detail");

            migrationBuilder.DropTable(
                name: "device_location");

            migrationBuilder.DropTable(
                name: "documents");

            migrationBuilder.DropTable(
                name: "elevator_card_sub_parameters");

            migrationBuilder.DropTable(
                name: "elevator_customer_card_parameters");

            migrationBuilder.DropTable(
                name: "elevator_fault_intervention_definitions");

            migrationBuilder.DropTable(
                name: "elevator_installation_list");

            migrationBuilder.DropTable(
                name: "elevator_offer_selected_product_properties_temp");

            migrationBuilder.DropTable(
                name: "elevator_offer_selected_product_sizes");

            migrationBuilder.DropTable(
                name: "elevator_offer_temp_product_list");

            migrationBuilder.DropTable(
                name: "elevator_revision_offer");

            migrationBuilder.DropTable(
                name: "elevator_revision_offer_parameters");

            migrationBuilder.DropTable(
                name: "elevator_revision_offer_parameters_temp");

            migrationBuilder.DropTable(
                name: "elevator_revision_parameters_product");

            migrationBuilder.DropTable(
                name: "elevator_sub_control_history");

            migrationBuilder.DropTable(
                name: "elevator_subcontractor_defines");

            migrationBuilder.DropTable(
                name: "email_parameters");

            migrationBuilder.DropTable(
                name: "endofthedayreport");

            migrationBuilder.DropTable(
                name: "expense");

            migrationBuilder.DropTable(
                name: "expiring");

            migrationBuilder.DropTable(
                name: "farm_animals_activity");

            migrationBuilder.DropTable(
                name: "farm_feed_activity");

            migrationBuilder.DropTable(
                name: "farm_feed_mixture_products");

            migrationBuilder.DropTable(
                name: "farm_paddock");

            migrationBuilder.DropTable(
                name: "farm_veterinary");

            migrationBuilder.DropTable(
                name: "farm_veterinary_applications");

            migrationBuilder.DropTable(
                name: "form_button_defines");

            migrationBuilder.DropTable(
                name: "general_parameters");

            migrationBuilder.DropTable(
                name: "income");

            migrationBuilder.DropTable(
                name: "installments");

            migrationBuilder.DropTable(
                name: "installments_plan");

            migrationBuilder.DropTable(
                name: "installments_plan_temp");

            migrationBuilder.DropTable(
                name: "invoice_product");

            migrationBuilder.DropTable(
                name: "invoice_shipment_trace");

            migrationBuilder.DropTable(
                name: "invoice_shipment_trace_temp");

            migrationBuilder.DropTable(
                name: "label_temp");

            migrationBuilder.DropTable(
                name: "licance_info");

            migrationBuilder.DropTable(
                name: "load_activity");

            migrationBuilder.DropTable(
                name: "load_activity_list");

            migrationBuilder.DropTable(
                name: "load_activity_temp");

            migrationBuilder.DropTable(
                name: "load_document_definitions");

            migrationBuilder.DropTable(
                name: "load_location_definations");

            migrationBuilder.DropTable(
                name: "load_package_definitions");

            migrationBuilder.DropTable(
                name: "load_status_definitions");

            migrationBuilder.DropTable(
                name: "load_units");

            migrationBuilder.DropTable(
                name: "maintenance");

            migrationBuilder.DropTable(
                name: "management_roles_service_request_authorization");

            migrationBuilder.DropTable(
                name: "message");

            migrationBuilder.DropTable(
                name: "mobile_scale_product_purchase");

            migrationBuilder.DropTable(
                name: "mobile_scale_product_purchase_temp");

            migrationBuilder.DropTable(
                name: "mobile_scale_sync");

            migrationBuilder.DropTable(
                name: "orders");

            migrationBuilder.DropTable(
                name: "orders_products");

            migrationBuilder.DropTable(
                name: "orders_products_sub");

            migrationBuilder.DropTable(
                name: "orders_products_sub_main");

            migrationBuilder.DropTable(
                name: "parameters");

            migrationBuilder.DropTable(
                name: "personel_cards_fee_information");

            migrationBuilder.DropTable(
                name: "personel_cards_identity_information");

            migrationBuilder.DropTable(
                name: "personel_cards_starting_ending");

            migrationBuilder.DropTable(
                name: "personel_holiday_parameters");

            migrationBuilder.DropTable(
                name: "personel_meterial_debit");

            migrationBuilder.DropTable(
                name: "personel_permission_current");

            migrationBuilder.DropTable(
                name: "personel_permission_day_parameters");

            migrationBuilder.DropTable(
                name: "personel_permission_parameters");

            migrationBuilder.DropTable(
                name: "pos_detail");

            migrationBuilder.DropTable(
                name: "pos_in");

            migrationBuilder.DropTable(
                name: "pos_out");

            migrationBuilder.DropTable(
                name: "premium_tracking");

            migrationBuilder.DropTable(
                name: "premium_tracking_temp");

            migrationBuilder.DropTable(
                name: "pricing");

            migrationBuilder.DropTable(
                name: "product_color");

            migrationBuilder.DropTable(
                name: "product_nutritional_values");

            migrationBuilder.DropTable(
                name: "product_pricing_change_history");

            migrationBuilder.DropTable(
                name: "product_serials");

            migrationBuilder.DropTable(
                name: "product_serials_temp");

            migrationBuilder.DropTable(
                name: "product_sizes");

            migrationBuilder.DropTable(
                name: "product_unit");

            migrationBuilder.DropTable(
                name: "production_history");

            migrationBuilder.DropTable(
                name: "production_orders_temp");

            migrationBuilder.DropTable(
                name: "production_recipe_formula");

            migrationBuilder.DropTable(
                name: "report_designs");

            migrationBuilder.DropTable(
                name: "restaurant_devices");

            migrationBuilder.DropTable(
                name: "restaurant_discount_rates");

            migrationBuilder.DropTable(
                name: "restaurant_order");

            migrationBuilder.DropTable(
                name: "restaurant_order_list");

            migrationBuilder.DropTable(
                name: "restaurant_order_list_temp_deletes");

            migrationBuilder.DropTable(
                name: "restaurant_parameters");

            migrationBuilder.DropTable(
                name: "restaurant_premium_calc");

            migrationBuilder.DropTable(
                name: "restaurant_premium_calc_temp");

            migrationBuilder.DropTable(
                name: "restaurant_premium_defines");

            migrationBuilder.DropTable(
                name: "restaurant_premium_defines_conditions");

            migrationBuilder.DropTable(
                name: "restaurant_premium_defines_non_conditions");

            migrationBuilder.DropTable(
                name: "restaurant_premium_defines_sub_conditions");

            migrationBuilder.DropTable(
                name: "restaurant_printer_parameters");

            migrationBuilder.DropTable(
                name: "restaurant_product_flavor_details");

            migrationBuilder.DropTable(
                name: "restaurant_speed_group_buttons");

            migrationBuilder.DropTable(
                name: "restaurant_user");

            migrationBuilder.DropTable(
                name: "route");

            migrationBuilder.DropTable(
                name: "route_parameters");

            migrationBuilder.DropTable(
                name: "safe_input");

            migrationBuilder.DropTable(
                name: "safe_output");

            migrationBuilder.DropTable(
                name: "scale_settings");

            migrationBuilder.DropTable(
                name: "scheduler");

            migrationBuilder.DropTable(
                name: "service_control_sub_defines");

            migrationBuilder.DropTable(
                name: "services_direction_list");

            migrationBuilder.DropTable(
                name: "services_open_activity");

            migrationBuilder.DropTable(
                name: "services_open_close");

            migrationBuilder.DropTable(
                name: "services_open_close_control_defines");

            migrationBuilder.DropTable(
                name: "services_open_close_parameters");

            migrationBuilder.DropTable(
                name: "services_open_control_defines");

            migrationBuilder.DropTable(
                name: "services_open_control_defines_temp");

            migrationBuilder.DropTable(
                name: "services_open_parameters");

            migrationBuilder.DropTable(
                name: "services_product_change_list");

            migrationBuilder.DropTable(
                name: "services_product_change_list_temp");

            migrationBuilder.DropTable(
                name: "services_setup_define_parameters");

            migrationBuilder.DropTable(
                name: "services_setup_defines_formula");

            migrationBuilder.DropTable(
                name: "services_setup_defines_functions");

            migrationBuilder.DropTable(
                name: "services_setup_request_list");

            migrationBuilder.DropTable(
                name: "services_speed_buttons");

            migrationBuilder.DropTable(
                name: "settings");

            migrationBuilder.DropTable(
                name: "sms_balance");

            migrationBuilder.DropTable(
                name: "sms_parameters");

            migrationBuilder.DropTable(
                name: "sms_task");

            migrationBuilder.DropTable(
                name: "speed_sales_buttons");

            migrationBuilder.DropTable(
                name: "speed_sales_tabsheet_captions");

            migrationBuilder.DropTable(
                name: "stock_activities");

            migrationBuilder.DropTable(
                name: "stock_activities_sub");

            migrationBuilder.DropTable(
                name: "stock_activities_sub_main");

            migrationBuilder.DropTable(
                name: "stock_counting");

            migrationBuilder.DropTable(
                name: "stock_counting_sub");

            migrationBuilder.DropTable(
                name: "stock_counting_sub_main");

            migrationBuilder.DropTable(
                name: "stock_counting_temp");

            migrationBuilder.DropTable(
                name: "stock_label");

            migrationBuilder.DropTable(
                name: "storage_transfer");

            migrationBuilder.DropTable(
                name: "storage_transfer_temp");

            migrationBuilder.DropTable(
                name: "support");

            migrationBuilder.DropTable(
                name: "temp_basket");

            migrationBuilder.DropTable(
                name: "temp_basket_tax");

            migrationBuilder.DropTable(
                name: "token");

            migrationBuilder.DropTable(
                name: "update_history");

            migrationBuilder.DropTable(
                name: "vehicle_debit");

            migrationBuilder.DropTable(
                name: "vehicle_fuel_expense");

            migrationBuilder.DropTable(
                name: "vehicle_kilometer");

            migrationBuilder.DropTable(
                name: "vehicle_product_expense");

            migrationBuilder.DropTable(
                name: "vehicle_tire_expense");

            migrationBuilder.DropTable(
                name: "waybill");

            migrationBuilder.DropTable(
                name: "waybill_products");

            migrationBuilder.DropTable(
                name: "waybill_products_sub");

            migrationBuilder.DropTable(
                name: "agricultural");

            migrationBuilder.DropTable(
                name: "currency");

            migrationBuilder.DropTable(
                name: "customer_current_input");

            migrationBuilder.DropTable(
                name: "customer_current_output");

            migrationBuilder.DropTable(
                name: "customer_subscription");

            migrationBuilder.DropTable(
                name: "elevator_card_parameters");

            migrationBuilder.DropTable(
                name: "elevator_revision_parameters");

            migrationBuilder.DropTable(
                name: "farm_animals");

            migrationBuilder.DropTable(
                name: "farm_feed_mixture");

            migrationBuilder.DropTable(
                name: "farms");

            migrationBuilder.DropTable(
                name: "load_type_definition");

            migrationBuilder.DropTable(
                name: "management_roles_authorization");

            migrationBuilder.DropTable(
                name: "mobile_scale_device");

            migrationBuilder.DropTable(
                name: "production_recipe_defines");

            migrationBuilder.DropTable(
                name: "restaurant_tables");

            migrationBuilder.DropTable(
                name: "restaurant_department");

            migrationBuilder.DropTable(
                name: "services_last_status_defination_infs");

            migrationBuilder.DropTable(
                name: "service_control_defines");

            migrationBuilder.DropTable(
                name: "services_open");

            migrationBuilder.DropTable(
                name: "services_request_defination_infs");

            migrationBuilder.DropTable(
                name: "services_setup_defines");

            migrationBuilder.DropTable(
                name: "sms_task_list");

            migrationBuilder.DropTable(
                name: "product2");

            migrationBuilder.DropTable(
                name: "product_tax_list2");

            migrationBuilder.DropTable(
                name: "income_and_expense_types");

            migrationBuilder.DropTable(
                name: "storage");

            migrationBuilder.DropTable(
                name: "subscription_groups");

            migrationBuilder.DropTable(
                name: "management_roles");

            migrationBuilder.DropTable(
                name: "product");

            migrationBuilder.DropTable(
                name: "services_last_status_defination");

            migrationBuilder.DropTable(
                name: "services_category_sub_definition");

            migrationBuilder.DropTable(
                name: "invoice");

            migrationBuilder.DropTable(
                name: "services_points_base_information");

            migrationBuilder.DropTable(
                name: "vehicle");

            migrationBuilder.DropTable(
                name: "product_groups");

            migrationBuilder.DropTable(
                name: "services_contract");

            migrationBuilder.DropTable(
                name: "services_request_defination");

            migrationBuilder.DropTable(
                name: "services_category_definition");

            migrationBuilder.DropTable(
                name: "sms_themes");

            migrationBuilder.DropTable(
                name: "customer");

            migrationBuilder.DropTable(
                name: "bank");

            migrationBuilder.DropTable(
                name: "personel_cards");

            migrationBuilder.DropTable(
                name: "branch");

            migrationBuilder.DropTable(
                name: "management");

            migrationBuilder.DropTable(
                name: "pos");

            migrationBuilder.DropTable(
                name: "safe");

            migrationBuilder.DropTable(
                name: "bank_account");
        }
    }
}
