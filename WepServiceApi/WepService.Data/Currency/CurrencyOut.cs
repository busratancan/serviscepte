﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class CurrencyOut
    {
        public int Id { get; set; }
        public int? CurrencyId { get; set; }
    }
}
