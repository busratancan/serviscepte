﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class CustomerOffset : BaseEntity
    {
        public override int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int? CurrentInputId { get; set; }
        public int? CurrentOutputId { get; set; }
        public int? CustomerId { get; set; }

        public CustomerCurrentInput CurrentInput { get; set; }
        public CustomerCurrentOutput CurrentOutput { get; set; }
        public CustomerEntity Customer { get; set; }
        public override sbyte? Deletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}
