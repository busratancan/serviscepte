﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class CustomerAdress : BaseEntity
    {
        public override int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int? CustomerId { get; set; }
        public string AdressType { get; set; }
        public string Adress { get; set; }
        public string Exp { get; set; }
        public string Province { get; set; }
        public string Town { get; set; }
        public string Country { get; set; }
        public override sbyte? Deletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string MobilePhone { get; set; }

        public CustomerEntity Customer { get; set; }
    }
}
