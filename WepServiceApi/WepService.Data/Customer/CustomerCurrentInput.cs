﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class CustomerCurrentInput : BaseEntity
    {
        public CustomerCurrentInput()
        {
            CustomerOffset = new HashSet<CustomerOffset>();
        }

        public override int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int? UserId { get; set; }
        public sbyte? Type { get; set; }
        public int? DetailId { get; set; }
        public decimal? Currency { get; set; }
        public int? CustomerId { get; set; }
        public DateTime? Datetime { get; set; }
        public string Exp { get; set; }
        public string Type1 { get; set; }
        public override sbyte? Deletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public DateTime? DeletionDateTime { get; set; }
        public int? DeletionUserId { get; set; }
        public DateTime? InsDatetime { get; set; }
        public string DocNumber { get; set; }

        public CustomerEntity Customer { get; set; }
        public ICollection<CustomerOffset> CustomerOffset { get; set; }

    }
}
