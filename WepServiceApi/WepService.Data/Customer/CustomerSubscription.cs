﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class CustomerSubscription : BaseEntity
    {
        public CustomerSubscription()
        {
            CustomerSubscriptionDetail = new HashSet<CustomerSubscriptionDetail>();
        }

        public override int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int? SubscriptionId { get; set; }
        public int? CustomerId { get; set; }
        public sbyte? Stats { get; set; }
        public decimal? CustomerPrice { get; set; }

        public CustomerEntity Customer { get; set; }
        public SubscriptionGroups Subscription { get; set; }
        public ICollection<CustomerSubscriptionDetail> CustomerSubscriptionDetail { get; set; }
        public override sbyte? Deletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}
