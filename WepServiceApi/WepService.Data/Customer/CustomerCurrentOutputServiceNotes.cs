﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class CustomerCurrentOutputServiceNotes
    {
        public int Id { get; set; }
        public string DocNumber { get; set; }
        public DateTime? InsTime { get; set; }
        public int? CustomerCurrentOutputId { get; set; }
        public string Exp { get; set; }

        public CustomerCurrentOutput CustomerCurrentOutput { get; set; }
    }
}
