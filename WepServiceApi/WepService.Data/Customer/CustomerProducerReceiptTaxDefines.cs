﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class CustomerProducerReceiptTaxDefines : BaseEntity
    {
        public override int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int? ProducerReceiptTaxDefinesId { get; set; }
        public int? CustomerId { get; set; }
        public string TaxName { get; set; }
        public sbyte? Exempt { get; set; }
        public sbyte? Discount { get; set; }

        public CustomerEntity Customer { get; set; }
        public override sbyte? Deletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}
