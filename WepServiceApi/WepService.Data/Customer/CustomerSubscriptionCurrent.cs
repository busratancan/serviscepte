﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class CustomerSubscriptionCurrent : BaseEntity
    {
        public override int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int? CustomerId { get; set; }
        public decimal? Currency { get; set; }
        public DateTime? InsDate { get; set; }
        public override sbyte? Deletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public DateTime? LastPayDate { get; set; }
        public DateTime? PayDate { get; set; }
        public sbyte? Type { get; set; }

        public CustomerEntity Customer { get; set; }
    }
}
