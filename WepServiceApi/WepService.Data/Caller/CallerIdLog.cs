﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class CallerIdLog
    {
        public int Id { get; set; }
        public DateTime? DateTime { get; set; }
        public string Number { get; set; }
        public string LineNumber { get; set; }
        public int? UserId { get; set; }
    }
}
