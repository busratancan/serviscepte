﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class DeviceLocation
    {
        public int Id { get; set; }
        public string DeviceImei { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public DateTime? InsDateTime { get; set; }
        public int? CustomerId { get; set; }
        public int? UserId { get; set; }
    }
}
