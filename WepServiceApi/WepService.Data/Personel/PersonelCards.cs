﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class PersonelCards : BaseEntity
    {
        public PersonelCards()
        {
            Branch = new HashSet<Branch>();
            PersonelCardsFeeInformation = new HashSet<PersonelCardsFeeInformation>();
            PersonelCardsIdentityInformation = new HashSet<PersonelCardsIdentityInformation>();
            PersonelCardsStartingEnding = new HashSet<PersonelCardsStartingEnding>();
            PersonelMeterialDebit = new HashSet<PersonelMeterialDebit>();
            PersonelPermissionCurrent = new HashSet<PersonelPermissionCurrent>();
            VehicleDebit = new HashSet<VehicleDebit>();
            VehicleFuelExpense = new HashSet<VehicleFuelExpense>();
            VehicleProductExpense = new HashSet<VehicleProductExpense>();
            VehicleTireExpense = new HashSet<VehicleTireExpense>();
        }

        
        public override int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public byte[] Picture { get; set; }
        public string PersonelCode { get; set; }
        public string Department { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExitDate { get; set; }
        public int? ReferenceCustomerId { get; set; }
        public string ReferenceCustomerAccountTitle { get; set; }
        public string GroupName { get; set; }
        public string Task { get; set; }
        public string Barcode { get; set; }
        public string CloseExp { get; set; }
        public string DrivingLicense { get; set; }
        public string Exp { get; set; }
        public int? ManagementId { get; set; }
        public string ManagamentUserName { get; set; }
        public int? CustomerId { get; set; }
        public int? BranchId { get; set; }
        public string BranchDefination { get; set; }
        public sbyte? Visible { get; set; }
        public sbyte? GroupOrder { get; set; }

        public Branch BranchNavigation { get; set; }
        public CustomerEntity Customer { get; set; }
        public ICollection<Branch> Branch { get; set; }
        public ICollection<PersonelCardsFeeInformation> PersonelCardsFeeInformation { get; set; }
        public ICollection<PersonelCardsIdentityInformation> PersonelCardsIdentityInformation { get; set; }
        public ICollection<PersonelCardsStartingEnding> PersonelCardsStartingEnding { get; set; }
        public ICollection<PersonelMeterialDebit> PersonelMeterialDebit { get; set; }
        public ICollection<PersonelPermissionCurrent> PersonelPermissionCurrent { get; set; }
        public ICollection<VehicleDebit> VehicleDebit { get; set; }
        public ICollection<VehicleFuelExpense> VehicleFuelExpense { get; set; }
        public ICollection<VehicleProductExpense> VehicleProductExpense { get; set; }
        public ICollection<VehicleTireExpense> VehicleTireExpense { get; set; }
        public override sbyte? Deletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}
