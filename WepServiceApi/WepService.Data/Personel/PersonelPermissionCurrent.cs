﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class PersonelPermissionCurrent
    {
        public int Id { get; set; }
        public int? PersonelCardsId { get; set; }
        public DateTime? PermissionDate { get; set; }
        public DateTime? InsTime { get; set; }
        public int? UserId { get; set; }
        public sbyte? Deletion { get; set; }
        public sbyte? PermissionType { get; set; }
        public string Exp { get; set; }

        public PersonelCards PersonelCards { get; set; }
        public Management User { get; set; }
    }
}
