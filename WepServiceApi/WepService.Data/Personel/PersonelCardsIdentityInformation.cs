﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class PersonelCardsIdentityInformation
    {
        public int Id { get; set; }
        public int? PersonelCardsId { get; set; }
        public string WalletSerialNumber { get; set; }
        public int? WalletNo { get; set; }
        public long? TcIdentificationNumber { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string PlaceOfBirth { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string MaritalStatus { get; set; }
        public string Religious { get; set; }
        public string BloodGroup { get; set; }
        public string PlaceOfIssue { get; set; }
        public string Province { get; set; }
        public string Town { get; set; }
        public sbyte? NumberOfChildren { get; set; }
        public string MilitaryStatus { get; set; }
        public string NeighborhoodVillage { get; set; }
        public string Vol { get; set; }
        public string FamilySequenceNo { get; set; }
        public string WalletLocation { get; set; }
        public string TheReasonForTheIssuanceOfAWallet { get; set; }
        public string EnrollmentNumber { get; set; }
        public DateTime? WalletIssueDate { get; set; }
        public string DrivingLicenseSerialAndNo { get; set; }
        public string DocumentNo { get; set; }
        public DateTime? DateOfIssue { get; set; }
        public string DriversLicenseClass { get; set; }
        public string SgkNumber { get; set; }
        public string EducationalKnowledge { get; set; }
        public int? SequenceNo { get; set; }

        public PersonelCards PersonelCards { get; set; }
    }
}
