﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class PersonelCardsStartingEnding
    {
        public int Id { get; set; }
        public int? PersonelCardsId { get; set; }
        public DateTime? InsDateTime { get; set; }
        public sbyte? Deletion { get; set; }
        public int? DeletionUserId { get; set; }
        public sbyte? Type { get; set; }
        public DateTime? ActivityDateTime { get; set; }
        public string Exp { get; set; }

        public Management DeletionUser { get; set; }
        public PersonelCards PersonelCards { get; set; }
    }
}
