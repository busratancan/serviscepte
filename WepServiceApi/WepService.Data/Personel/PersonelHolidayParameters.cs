﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class PersonelHolidayParameters
    {
        public int Id { get; set; }
        public DateTime? Date { get; set; }
        public string Exp { get; set; }
        public sbyte? Deletion { get; set; }
        public DateTime? InsDateTime { get; set; }
    }
}
