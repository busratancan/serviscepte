﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class PersonelPermissionDayParameters
    {
        public int Id { get; set; }
        public sbyte Monday { get; set; }
        public sbyte Tuesday { get; set; }
        public sbyte Wednesday { get; set; }
        public sbyte Thursday { get; set; }
        public sbyte Friday { get; set; }
        public sbyte Saturday { get; set; }
        public sbyte Sunday { get; set; }
    }
}
