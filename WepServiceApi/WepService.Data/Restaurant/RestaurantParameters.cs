﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class RestaurantParameters
    {
        public int Id { get; set; }
        public int? OrderProductGroupId { get; set; }
        public string OrderProductGroupName { get; set; }
        public string CustomerOnTrustGroup { get; set; }
        public string ReceiptTopNote1 { get; set; }
        public string ReceiptTopNote2 { get; set; }
        public string ReceiptBottomNote1 { get; set; }
        public string ReceiptBottomNote2 { get; set; }
    }
}
