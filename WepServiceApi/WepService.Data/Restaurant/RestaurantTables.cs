﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class RestaurantTables
    {
        public RestaurantTables()
        {
            RestaurantOrder = new HashSet<RestaurantOrder>();
        }

        public int Id { get; set; }
        public string TableCode { get; set; }
        public int? DepartmanId { get; set; }
        public string TableNumber { get; set; }
        public string TableExample { get; set; }
        public string TableCapacity { get; set; }
        public sbyte? Visible { get; set; }

        public ICollection<RestaurantOrder> RestaurantOrder { get; set; }
    }
}
