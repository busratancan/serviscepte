﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class RestaurantPremiumDefinesNonConditions
    {
        public int Id { get; set; }
        public int? RestaurantPremiumDefinesConditionsId { get; set; }
        public int? SourceId { get; set; }
        public sbyte? SourceType { get; set; }
        public sbyte? Condition { get; set; }
        public string ConditionDefine { get; set; }
    }
}
