﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class RestaurantPremiumDefinesConditions
    {
        public int Id { get; set; }
        public int? SourceId { get; set; }
        public int? SourceType { get; set; }
        public sbyte? Condition { get; set; }
        public int? Qty { get; set; }
        public sbyte? QtyCondition { get; set; }
        public int? RestaurantPremiumDefinesId { get; set; }
        public string ConditionDefine { get; set; }
        public int? Basket { get; set; }
        public decimal? PremiumAmount { get; set; }
        public decimal? WaiterPercent { get; set; }
    }
}
