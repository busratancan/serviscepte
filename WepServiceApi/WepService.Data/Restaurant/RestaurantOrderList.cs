﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class RestaurantOrderList
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal? Qty { get; set; }
        public int? Basket { get; set; }
        public decimal? Portion { get; set; }
        public decimal? Price { get; set; }
        public int? UserId { get; set; }
        public int? FlavorId { get; set; }
        public DateTime? InsDate { get; set; }
        public sbyte? Deletion { get; set; }

        public Management User { get; set; }
    }
}
