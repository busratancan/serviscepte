﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class RestaurantPremiumDefines
    {
        public int Id { get; set; }
        public string PreCode { get; set; }
        public string PreDefination { get; set; }
        public int? Group2ConditionId { get; set; }
        public string ConditionBased { get; set; }
        public string SalesBased { get; set; }
    }
}
