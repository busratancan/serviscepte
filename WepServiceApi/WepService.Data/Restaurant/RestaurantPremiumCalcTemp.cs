﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class RestaurantPremiumCalcTemp
    {
        public int Id { get; set; }
        public int? RestaurantPremiumDefinesSubConditionsId { get; set; }
        public int? Basket { get; set; }
        public int? UserId { get; set; }
        public int? Qty { get; set; }
        public decimal? TotalPremiumAmount { get; set; }
        public decimal? UnitPremiumAmount { get; set; }
        public int? RestaurantOrderBasketId { get; set; }
        public sbyte? ConditionType { get; set; }
    }
}
