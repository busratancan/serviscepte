﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class RestaurantPremiumDefinesSubConditions
    {
        public int Id { get; set; }
        public string PremiumDefineCode { get; set; }
        public string PremiumDefine { get; set; }
        public int? MinConditions { get; set; }
        public decimal? PremiumAmount { get; set; }
        public decimal? WaiterPercent { get; set; }
        public decimal? PoolPercent { get; set; }
        public int? RestaurantPremiumDefinesId { get; set; }
        public sbyte? ConditionForm { get; set; }
    }
}
