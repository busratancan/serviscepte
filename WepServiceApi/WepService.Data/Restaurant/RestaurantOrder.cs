﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class RestaurantOrder
    {
        public int Id { get; set; }
        public int? RestaurantOrderListBasketId { get; set; }
        public DateTime? OpenDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public decimal? DiscountRate { get; set; }
        public int? UserId { get; set; }
        public sbyte? Deletion { get; set; }
        public int? BillUserId { get; set; }
        public sbyte? PayType { get; set; }
        public sbyte? CountOfCustomer { get; set; }
        public int? TableId { get; set; }
        public sbyte? Status { get; set; }
        public sbyte? BillCollection { get; set; }
        public int? IsMovedNewBasket { get; set; }

        public RestaurantTables Table { get; set; }
    }
}
