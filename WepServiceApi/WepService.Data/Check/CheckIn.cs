﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class CheckIn
    {
        public int Id { get; set; }
        public int? BankId { get; set; }
        public decimal? Currency { get; set; }
        public int? CheckNumber { get; set; }
        public string CheckBankAccount { get; set; }
        public string Exp { get; set; }
        public DateTime? PayDate { get; set; }
        public int? UserId { get; set; }
        public sbyte? Status { get; set; }
        public string Type { get; set; }
        public int? CustomerId { get; set; }
        public DateTime? DeletionDateTime { get; set; }
        public int? DeletionUserId { get; set; }
        public sbyte? Deletion { get; set; }
        public DateTime? InsDateTime { get; set; }
        public sbyte? Reminder { get; set; }
        public int? ReminderUser { get; set; }
        public int? ExitCustomerId { get; set; }

        public Management ReminderUserNavigation { get; set; }
    }
}
