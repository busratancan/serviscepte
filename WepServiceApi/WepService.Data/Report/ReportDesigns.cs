﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ReportDesigns
    {
        public int Id { get; set; }
        public int? Type { get; set; }
        public string FileUrl { get; set; }
        public string Defination { get; set; }
        public byte[] File { get; set; }
        public sbyte? Isondatabase { get; set; }
    }
}
