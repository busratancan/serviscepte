﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{ 
    public partial class Endofthedayreport
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public int? Basket { get; set; }
        public decimal? Currency { get; set; }
        public string SubType { get; set; }
    }
}
