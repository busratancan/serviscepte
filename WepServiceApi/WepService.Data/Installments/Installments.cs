﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class Installments
    {
        public int Id { get; set; }
        public int? InstallmentsPlanBasketId { get; set; }
        public sbyte? InstallmentsType { get; set; }
        public sbyte? InvoiceType { get; set; }
        public decimal? TotalCurrency { get; set; }
        public decimal? TotalInterest { get; set; }
        public decimal? TotalInterestAmount { get; set; }
        public sbyte? DownPaymentType { get; set; }
        public decimal? DownCurrency { get; set; }
        public int? DownDetailId { get; set; }
        public sbyte? InstallmentAssurance { get; set; }
        public int? InstallmentAssuranceId { get; set; }
        public sbyte? InstallmentsSource { get; set; }
        public int? InstallmentsSourceId { get; set; }
    }
}
