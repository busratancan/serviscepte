﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class InstallmentsPlanTemp
    {
        public int Id { get; set; }
        public int? NumberOfInstallments { get; set; }
        public decimal? InstallmentsCurrency { get; set; }
        public int? UserId { get; set; }
        public DateTime? PayDate { get; set; }
        public int? BasketId { get; set; }
        public int? TheDifferenceInDays { get; set; }
        public decimal? InterestRate { get; set; }
        public decimal? AmountOfInterest { get; set; }
        public decimal? InstallmentsCurrencyInterest { get; set; }
    }
}
