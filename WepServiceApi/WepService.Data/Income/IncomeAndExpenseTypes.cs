﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class IncomeAndExpenseTypes
    {
        public IncomeAndExpenseTypes()
        {
            VehicleFuelExpense = new HashSet<VehicleFuelExpense>();
            VehicleProductExpense = new HashSet<VehicleProductExpense>();
            VehicleTireExpense = new HashSet<VehicleTireExpense>();
        }

        public int Id { get; set; }
        public string Definition { get; set; }
        public int? RangeAgainDay { get; set; }
        public int? RangeAgainMonth { get; set; }
        public int? RangeAgainYear { get; set; }
        public string FixedExpenses { get; set; }
        public string VariableExpenses { get; set; }
        public string Group { get; set; }
        public string Exp { get; set; }
        public string Exp2 { get; set; }
        public string Exp3 { get; set; }
        public string TypeIncome { get; set; }
        public string TypeExpense { get; set; }
        public int? UserId { get; set; }
        public string Code { get; set; }
        public sbyte? IsVehicleAccount { get; set; }
        public int? VehicleId { get; set; }
        public sbyte? FuelType { get; set; }
        public sbyte? ServiceType { get; set; }
        public sbyte? TireType { get; set; }

        public Vehicle Vehicle { get; set; }
        public ICollection<VehicleFuelExpense> VehicleFuelExpense { get; set; }
        public ICollection<VehicleProductExpense> VehicleProductExpense { get; set; }
        public ICollection<VehicleTireExpense> VehicleTireExpense { get; set; }
    }
}
