﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class Income
    {
        public int Id { get; set; }
        public string InExTypesId { get; set; }
        public int? DetailId { get; set; }
        public decimal? Currency { get; set; }
        public sbyte? Type { get; set; }
        public int? UserId { get; set; }
        public DateTime? InsDateTime { get; set; }
        public DateTime? DateTime { get; set; }
        public sbyte? PayType { get; set; }
        public int? PayDetailId { get; set; }
        public sbyte? Deletion { get; set; }
        public string Exp { get; set; }
        public string DocNo { get; set; }
    }
}
