﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class LoadActivity
    {
        public int Id { get; set; }
        public string DocNumber { get; set; }
        public int? CustomerId { get; set; }
        public int? VehicleId { get; set; }
        public int? PersonelId { get; set; }
        public int? ListBasketId { get; set; }
        public int? UserId { get; set; }
        public DateTime? InsDateTime { get; set; }
        public sbyte? Deletion { get; set; }
        public DateTime? DeletionDateTime { get; set; }
        public DateTime? DateTime { get; set; }
    }
}
