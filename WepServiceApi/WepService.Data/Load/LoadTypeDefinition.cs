﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class LoadTypeDefinition
    {
        public LoadTypeDefinition()
        {
            LoadActivityTemp = new HashSet<LoadActivityTemp>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Defination { get; set; }
        public string Color { get; set; }
        public string Exp { get; set; }
        public sbyte? Deletion { get; set; }

        public ICollection<LoadActivityTemp> LoadActivityTemp { get; set; }
    }
}
