﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class LoadActivityList
    {
        public int Id { get; set; }
        public int? LoadTypeDefinitionId { get; set; }
        public string LoadTypeDefination { get; set; }
        public int? SenderLoadLocationDefinationsId { get; set; }
        public string SenderLoadLocationDefinations { get; set; }
        public int? ReceiverLoadLocationDefinationsId { get; set; }
        public string ReceiverLoadLocationDefinations { get; set; }
        public decimal? Qty { get; set; }
        public int? Km { get; set; }
        public sbyte? PriceType { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? RentUnitPrice { get; set; }
        public int? Basket { get; set; }
        public string PriceTypeExp { get; set; }
        public sbyte? Deletion { get; set; }
    }
}
