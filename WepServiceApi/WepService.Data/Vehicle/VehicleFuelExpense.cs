﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class VehicleFuelExpense : BaseEntity
    {
        public override int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int? IncomeAndExpenseTypesId { get; set; }
        public int? VehicleId { get; set; }
        public int? StorageId { get; set; }
        public int? FuelCompanyCustomerId { get; set; }
        public int? FuelProductId { get; set; }
        public int? PersonelCardsId { get; set; }
        public decimal? Liter { get; set; }
        public decimal? UnitPrice { get; set; }
        public string DocumentNo { get; set; }
        public sbyte? SupplyType { get; set; }
        public override sbyte? Deletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int? UserId { get; set; }
        public string PurchaseReasons { get; set; }
        public DateTime? DateTime { get; set; }
        public DateTime? InsDateTime { get; set; }

        public CustomerEntity FuelCompanyCustomer { get; set; }
        public Product FuelProduct { get; set; }
        public IncomeAndExpenseTypes IncomeAndExpenseTypes { get; set; }
        public PersonelCards PersonelCards { get; set; }
        public Storage Storage { get; set; }
        public Vehicle Vehicle { get; set; }
       
    }
}
