﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class VehicleDebit
    {
        public int Id { get; set; }
        public int? VehicleId { get; set; }
        public int? PersonelCardsId { get; set; }
        public DateTime? InsDateTime { get; set; }
        public int? Active { get; set; }
        public sbyte? Deletion { get; set; }

        public PersonelCards PersonelCards { get; set; }
        public Vehicle Vehicle { get; set; }
    }
}
