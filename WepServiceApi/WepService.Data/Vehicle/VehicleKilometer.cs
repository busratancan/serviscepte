﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class VehicleKilometer
    {
        public int Id { get; set; }
        public int? VehicleId { get; set; }
        public int? LastKm { get; set; }
        public int? UserId { get; set; }
        public DateTime? InsDatetime { get; set; }
        public DateTime? MeasurementDate { get; set; }
        public sbyte? Deletion { get; set; }
        public int? DeletionUserId { get; set; }
        public string Exp { get; set; }
        public sbyte? InsType { get; set; }
        public int? DetailId { get; set; }

        public Vehicle Vehicle { get; set; }
    }
}
