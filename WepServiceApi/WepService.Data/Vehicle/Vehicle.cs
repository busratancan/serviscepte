﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class Vehicle
    {
        public Vehicle()
        {
            IncomeAndExpenseTypes = new HashSet<IncomeAndExpenseTypes>();
            MobileScaleDevice = new HashSet<MobileScaleDevice>();
            VehicleDebit = new HashSet<VehicleDebit>();
            VehicleFuelExpense = new HashSet<VehicleFuelExpense>();
            VehicleKilometer = new HashSet<VehicleKilometer>();
            VehicleProductExpense = new HashSet<VehicleProductExpense>();
            VehicleTireExpense = new HashSet<VehicleTireExpense>();
        }

        public int Id { get; set; }
        public string Plate { get; set; }
        public string Mark { get; set; }
        public string Model { get; set; }
        public int? ModelYear { get; set; }
        public DateTime? VisaFinishTime { get; set; }
        public DateTime? VisaStartTime { get; set; }
        public DateTime? InsuranceFinishTime { get; set; }
        public DateTime? InsuranceStartTime { get; set; }
        public sbyte? Deletion { get; set; }
        public DateTime? DeletionDateTime { get; set; }
        public int? DeletionUserId { get; set; }
        public string Exp1 { get; set; }
        public string Exp2 { get; set; }
        public string Type { get; set; }
        public string Species { get; set; }
        public string EngineSerial { get; set; }
        public string FrameSerial { get; set; }
        public string TrafficDocumentNumber { get; set; }
        public DateTime? TaxDate1 { get; set; }
        public DateTime? TaxDate2 { get; set; }
        public DateTime? Insurance2FinishTime { get; set; }
        public DateTime? Insurance2StartTime { get; set; }
        public string VehicleGroup { get; set; }
        public decimal? PurchaseAmount { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public sbyte? VehicleOwnerType { get; set; }
        public int? VehicleOwnerCustomerId { get; set; }
        public string VehicleOwnerAccountTitle { get; set; }

        public Management DeletionUser { get; set; }
        public ICollection<IncomeAndExpenseTypes> IncomeAndExpenseTypes { get; set; }
        public ICollection<MobileScaleDevice> MobileScaleDevice { get; set; }
        public ICollection<VehicleDebit> VehicleDebit { get; set; }
        public ICollection<VehicleFuelExpense> VehicleFuelExpense { get; set; }
        public ICollection<VehicleKilometer> VehicleKilometer { get; set; }
        public ICollection<VehicleProductExpense> VehicleProductExpense { get; set; }
        public ICollection<VehicleTireExpense> VehicleTireExpense { get; set; }
    }
}
