﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class VehicleTireExpense
    {
        public int Id { get; set; }
        public int? VehicleId { get; set; }
        public int? IncomeAndExpenseTypesId { get; set; }
        public int? StorageId { get; set; }
        public int? TireProductId { get; set; }
        public int? PersonelCardsId { get; set; }
        public decimal? TireLifeKm { get; set; }
        public int? Qty { get; set; }
        public decimal? UnitPrice { get; set; }
        public string DocNumber { get; set; }
        public sbyte? Deletion { get; set; }
        public int? UserId { get; set; }
        public DateTime? DateTime { get; set; }
        public DateTime? InsDateTime { get; set; }

        public IncomeAndExpenseTypes IncomeAndExpenseTypes { get; set; }
        public PersonelCards PersonelCards { get; set; }
        public Storage Storage { get; set; }
        public Product TireProduct { get; set; }
        public Management User { get; set; }
        public Vehicle Vehicle { get; set; }
    }
}
