﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ElevatorOfferSelectedProductSizes
    {
        public int Id { get; set; }
        public int? Basket { get; set; }
        public int? OfferGroupOrderId { get; set; }
        public int? TabOrder { get; set; }
        public string Size { get; set; }
        public sbyte? Deletion { get; set; }
    }
}
