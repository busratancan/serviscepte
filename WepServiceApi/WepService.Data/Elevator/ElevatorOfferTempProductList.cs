﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ElevatorOfferTempProductList
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public string GroupName { get; set; }
        public string Size { get; set; }
        public decimal? Qty { get; set; }
        public sbyte? Type { get; set; }
        public string SelectedMark { get; set; }
        public int? Basket { get; set; }
        public sbyte? OfferGroup { get; set; }
        public sbyte? ApplicationPart { get; set; }
    }
}
