﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ElevatorRevisionOffer
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public decimal? Currency { get; set; }
        public int? ElevatorRevisionOfferParametersBasket { get; set; }
        public sbyte? Stats { get; set; }
        public int? ElevatorControlHistoryId { get; set; }
        public DateTime? OfferDate { get; set; }
        public decimal? LaborCosts { get; set; }
        public decimal? Profit { get; set; }
        public sbyte? TargetColor { get; set; }
        public sbyte? StatusColor { get; set; }
    }
}
