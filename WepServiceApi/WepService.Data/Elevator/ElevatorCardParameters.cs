﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ElevatorCardParameters
    {
        public ElevatorCardParameters()
        {
            ElevatorCardSubParameters = new HashSet<ElevatorCardSubParameters>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Defination { get; set; }
        public string Exp { get; set; }
        public string Exp2 { get; set; }
        public sbyte? ControlType { get; set; }
        public int? ElevatorOrder { get; set; }

        public ICollection<ElevatorCardSubParameters> ElevatorCardSubParameters { get; set; }
    }
}
