﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WepService.Data
{
    public abstract class ServiceBaseEntity : BaseEntity
    {
        public override int Id { get; set; }
        public abstract string Defination { get; set; }
        public abstract string Code { get; set; }
        public abstract string Color { get; set; }
        public abstract int? Order { get; set; }
        public abstract string Exp { get; set; }
    }
}
