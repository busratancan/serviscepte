﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ProductGroups
    {
        public ProductGroups()
        {
            InverseParent = new HashSet<ProductGroups>();
            ProductGroup1Navigation = new HashSet<Product>();
            ProductGroup2Navigation = new HashSet<Product>();
            RestaurantPrinterParameters = new HashSet<RestaurantPrinterParameters>();
        }

        public int Id { get; set; }
        public string GroupName { get; set; }
        public int? ParentId { get; set; }

        public ProductGroups Parent { get; set; }
        public ICollection<ProductGroups> InverseParent { get; set; }
        public ICollection<Product> ProductGroup1Navigation { get; set; }
        public ICollection<Product> ProductGroup2Navigation { get; set; }
        public ICollection<RestaurantPrinterParameters> RestaurantPrinterParameters { get; set; }
    }
}
