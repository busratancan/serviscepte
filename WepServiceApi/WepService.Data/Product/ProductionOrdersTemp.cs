﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ProductionOrdersTemp
    {
        public int Id { get; set; }
        public int? ProductionRecipeDefinesId { get; set; }
        public int? Basket { get; set; }
        public decimal? ProductionQty { get; set; }
        public string ProductionRecipeDefines { get; set; }
        public string Unit { get; set; }
        public int? ProductId { get; set; }

        public ProductionRecipeDefines ProductionRecipeDefinesNavigation { get; set; }
    }
}
