﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ProductUnit
    {
        public int Id { get; set; }
        public string UnitName { get; set; }
        public decimal? UnitFactor { get; set; }
        public string UnitBarcode { get; set; }
        public string UnitExp { get; set; }
        public int? ProductId { get; set; }
        public int? UserId { get; set; }

        public Product Product { get; set; }
    }
}
