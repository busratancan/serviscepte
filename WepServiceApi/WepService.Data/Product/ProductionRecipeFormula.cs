﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ProductionRecipeFormula
    {
        public int Id { get; set; }
        public int? ProductionRecipeDefinesId { get; set; }
        public int? ProductId { get; set; }
        public decimal? FormulaQty { get; set; }
        public string ProductName { get; set; }
        public decimal? WastageRate { get; set; }

        public Product Product { get; set; }
        public ProductionRecipeDefines ProductionRecipeDefines { get; set; }
    }
}
