﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ProductSizes
    {
        public int Id { get; set; }
        public string Size { get; set; }
        public sbyte? Deletion { get; set; }
        public int? UserId { get; set; }
        public DateTime? InsDatetime { get; set; }
        public string GroupName { get; set; }
    }
}
