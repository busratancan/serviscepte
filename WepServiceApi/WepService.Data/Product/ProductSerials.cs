﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ProductSerials
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public int? BasketId { get; set; }
        public string SerialCode { get; set; }
        public DateTime? InsDate { get; set; }
        public int? UserId { get; set; }
        public sbyte? Deletion { get; set; }
        public sbyte? Type { get; set; }
        public sbyte? Status { get; set; }
        public int? StatusChangeDetailId { get; set; }

        public Product Product { get; set; }
    }
}
