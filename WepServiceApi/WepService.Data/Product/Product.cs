﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class Product
    {
        public Product()
        {
            CargoFeeDefines = new HashSet<CargoFeeDefines>();
            InvoiceProduct = new HashSet<InvoiceProduct>();
            InvoiceShipmentTrace = new HashSet<InvoiceShipmentTrace>();
            PersonelMeterialDebit = new HashSet<PersonelMeterialDebit>();
            ProductNutritionalValues = new HashSet<ProductNutritionalValues>();
            ProductSerials = new HashSet<ProductSerials>();
            ProductUnit = new HashSet<ProductUnit>();
            ProductionRecipeDefines = new HashSet<ProductionRecipeDefines>();
            ProductionRecipeFormula = new HashSet<ProductionRecipeFormula>();
            RestaurantProductFlavorDetails = new HashSet<RestaurantProductFlavorDetails>();
            ServicesProductChangeList = new HashSet<ServicesProductChangeList>();
            ServicesProductChangeListTemp = new HashSet<ServicesProductChangeListTemp>();
            StockActivities = new HashSet<StockActivities>();
            VehicleFuelExpense = new HashSet<VehicleFuelExpense>();
            VehicleProductExpense = new HashSet<VehicleProductExpense>();
            VehicleTireExpense = new HashSet<VehicleTireExpense>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Barcode { get; set; }
        public string Name { get; set; }
        public decimal? PurchasePrice { get; set; }
        public decimal? GrossSalesPrice { get; set; }
        public decimal? RetailPrice { get; set; }
        public int? PurchaseTaxPercent { get; set; }
        public int? SalesTaxPercent { get; set; }
        public int? SellerCustomerId { get; set; }
        public string SellerCustomerTitle { get; set; }
        public string Mark { get; set; }
        public string PrivateCode { get; set; }
        public string PrivateCode2 { get; set; }
        public string PrivateCode3 { get; set; }
        public string PrivateCode4 { get; set; }
        public string PrivateCode5 { get; set; }
        public string PrivateCode6 { get; set; }
        public string PrivateCode7 { get; set; }
        public string PrivateCode8 { get; set; }
        public string PrivateCode9 { get; set; }
        public string PrivateCode10 { get; set; }
        public string Unit { get; set; }
        public int? ProducerId { get; set; }
        public string ProducerAccountTitle { get; set; }
        public int? MaxStock { get; set; }
        public int? MinStock { get; set; }
        public int? MinOrder { get; set; }
        public int? MaxOrder { get; set; }
        public string Picture { get; set; }
        public string ActiveScale { get; set; }
        public string BarcodeScale { get; set; }
        public decimal? RetailPriceTax { get; set; }
        public string Author { get; set; }
        public string BookType { get; set; }
        public sbyte? Type { get; set; }
        public int? MainProductId { get; set; }
        public int? MainProductIdTemp { get; set; }
        public int? GroupQty { get; set; }
        public sbyte? PricePrivileged { get; set; }
        public decimal? PurchasePriceTax { get; set; }
        public sbyte? Visible { get; set; }
        public string TechnicOrder { get; set; }
        public string TechnicTitle { get; set; }
        public string TechnicTitle2 { get; set; }
        public string TechnicOrder2 { get; set; }
        public string TechnicTitle3 { get; set; }
        public string TechnicOrder3 { get; set; }
        public decimal? Weight { get; set; }
        public string Group { get; set; }
        public string Size { get; set; }
        public sbyte? MobileVisible { get; set; }
        public string ShelfInf { get; set; }
        public decimal? GrossSalesPriceTax { get; set; }
        public decimal? UnitFactor { get; set; }
        public sbyte? Ean { get; set; }
        public decimal? PersonalPrice { get; set; }
        public decimal? PersonalPriceTax { get; set; }
        public int? Group1 { get; set; }
        public int? Group2 { get; set; }
        public int? Group3 { get; set; }
        public int? Group4 { get; set; }
        public decimal? FixedDiscountRate { get; set; }
        public decimal? HalfPortion { get; set; }
        public decimal? OnePortion { get; set; }
        public decimal? OneHalfPortion { get; set; }
        public decimal? DoublePortion { get; set; }
        public string HalfPortionOrder { get; set; }
        public string OnePortionOrder { get; set; }
        public string OneHalfPortionOrder { get; set; }
        public string DoublePortionOrder { get; set; }
        public string ForceSerialTrack { get; set; }
        public decimal? PremiumAmount { get; set; }
        public decimal? WaiterPercent { get; set; }
        public int? CurrenySource { get; set; }
        public int? SyncId { get; set; }
        public sbyte? WarrantyPeriod { get; set; }
        public sbyte? Variant { get; set; }
        public decimal? ProfitRate { get; set; }
        public decimal? LaborRate { get; set; }

        public ProductGroups Group1Navigation { get; set; }
        public ProductGroups Group2Navigation { get; set; }
        public ICollection<CargoFeeDefines> CargoFeeDefines { get; set; }
        public ICollection<InvoiceProduct> InvoiceProduct { get; set; }
        public ICollection<InvoiceShipmentTrace> InvoiceShipmentTrace { get; set; }
        public ICollection<PersonelMeterialDebit> PersonelMeterialDebit { get; set; }
        public ICollection<ProductNutritionalValues> ProductNutritionalValues { get; set; }
        public ICollection<ProductSerials> ProductSerials { get; set; }
        public ICollection<ProductUnit> ProductUnit { get; set; }
        public ICollection<ProductionRecipeDefines> ProductionRecipeDefines { get; set; }
        public ICollection<ProductionRecipeFormula> ProductionRecipeFormula { get; set; }
        public ICollection<RestaurantProductFlavorDetails> RestaurantProductFlavorDetails { get; set; }
        public ICollection<ServicesProductChangeList> ServicesProductChangeList { get; set; }
        public ICollection<ServicesProductChangeListTemp> ServicesProductChangeListTemp { get; set; }
        public ICollection<StockActivities> StockActivities { get; set; }
        public ICollection<VehicleFuelExpense> VehicleFuelExpense { get; set; }
        public ICollection<VehicleProductExpense> VehicleProductExpense { get; set; }
        public ICollection<VehicleTireExpense> VehicleTireExpense { get; set; }
    }
}
