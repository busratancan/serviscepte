﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ProductTaxList2
    {
        public int Id { get; set; }
        public string TaxName { get; set; }
        public decimal? TaxRate { get; set; }

        public TempBasketTax TempBasketTax { get; set; }
    }
}
