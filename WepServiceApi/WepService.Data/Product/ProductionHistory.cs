﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ProductionHistory
    {
        public int Id { get; set; }
        public int? ProductionRecipeDefinesId { get; set; }
        public int? Basket { get; set; }
        public decimal? ProductionQty { get; set; }
        public string ProductionRecipeDefines { get; set; }
        public string Unit { get; set; }
        public int? ProductId { get; set; }
        public DateTime? InsDateTime { get; set; }
        public sbyte? Deletion { get; set; }
        public int? DeletionUserId { get; set; }
        public DateTime? DeletionDateTime { get; set; }
    }
}
