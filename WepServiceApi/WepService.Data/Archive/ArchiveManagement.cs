﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ArchiveManagement : BaseEntity
    {
        public override int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public byte[] Document { get; set; }
        public sbyte? DocType { get; set; }
        public int? ArcihiveGroupsId { get; set; }
        public int? CustomerId { get; set; }
        public string DocExplanation { get; set; }
        public DateTime? InsertDatetime { get; set; }
        public int? UserId { get; set; }
        public sbyte? ArciveType { get; set; }
        public override sbyte? Deletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string DocSerial { get; set; }
        public int? DocNumner { get; set; }
    }
}
