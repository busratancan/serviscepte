﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class ArchiveGroups : BaseEntity
    {
        public override int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        public string GroupExplanation { get; set; }
        public string ForCustomer { get; set; }
        public string ForPersonel { get; set; }
        public string ForVehicle { get; set; }
        public override sbyte? Deletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}
