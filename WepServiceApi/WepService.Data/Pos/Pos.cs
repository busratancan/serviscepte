﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class Pos
    {
        public Pos()
        {
            Management = new HashSet<Management>();
            PosDetail = new HashSet<PosDetail>();
            PosIn = new HashSet<PosIn>();
            PosOut = new HashSet<PosOut>();
        }

        public int Id { get; set; }
        public int? BankAccountId { get; set; }
        public string PosName { get; set; }
        public string PosExp { get; set; }
        public int? PosBlockTime { get; set; }
        public string PosCode { get; set; }
        public string PosBankName { get; set; }
        public string PosDepartment { get; set; }
        public string PosAccountHolder { get; set; }
        public int? PosDepartmentId { get; set; }

        public BankAccount BankAccount { get; set; }
        public ICollection<Management> Management { get; set; }
        public ICollection<PosDetail> PosDetail { get; set; }
        public ICollection<PosIn> PosIn { get; set; }
        public ICollection<PosOut> PosOut { get; set; }
    }
}
