﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class LicanceInfo
    {
        public int Id { get; set; }
        public DateTime? DateTime { get; set; }
        public string Ks { get; set; }
        public string Vs { get; set; }
        public string Message { get; set; }
    }
}
