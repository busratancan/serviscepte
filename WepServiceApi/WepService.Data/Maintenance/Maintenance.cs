﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class Maintenance : BaseEntity
    {
        public override int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int? CustomerId { get; set; }
        public decimal? Fee { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? FinishDate { get; set; }
        public DateTime? InsDate { get; set; }
        public int? ElevatorCount { get; set; }

        public CustomerEntity Customer { get; set; }
        public override sbyte? Deletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}
