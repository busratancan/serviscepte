﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class Orders : BaseEntity
    {
        public override int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int? Type { get; set; }
        public string OrdersProductsBasket { get; set; }
        public DateTime? InsDateTime { get; set; }
        public DateTime? OrderDateTime { get; set; }
        public int? UserId { get; set; }
        public int? CustomerId { get; set; }
        public string Barcode { get; set; }
        public decimal? OrderAmount { get; set; }
        public string OrderSerial { get; set; }
        public string OrderNumber { get; set; }
        public sbyte? Stats { get; set; }
        public DateTime? InsDatetime1 { get; set; }

        public CustomerEntity Customer { get; set; }
        public override sbyte? Deletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}
