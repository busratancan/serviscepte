﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class FarmPaddock
    {
        public int Id { get; set; }
        public int? FarmsId { get; set; }
        public string PaddockCode { get; set; }
        public string PaddockDefination { get; set; }
        public string Exp { get; set; }
        public string Rfid { get; set; }

        public Farms Farms { get; set; }
    }
}
