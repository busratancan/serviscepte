﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class FarmVeterinaryApplications
    {
        public int Id { get; set; }
        public string AppCode { get; set; }
        public string AppDefination { get; set; }
        public string AppType { get; set; }
        public string Exp1 { get; set; }
        public string Exp2 { get; set; }
        public sbyte? AppTypeCode { get; set; }
    }
}
