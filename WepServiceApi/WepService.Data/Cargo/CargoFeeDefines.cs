﻿using System;
using System.Collections.Generic;

namespace WepService.Data
{
    public partial class CargoFeeDefines
    {
        public int Id { get; set; }
        public int? Ds11 { get; set; }
        public int? Ds12 { get; set; }
        public decimal? Currency1 { get; set; }
        public int? Ds21 { get; set; }
        public int? Ds22 { get; set; }
        public decimal? Currency2 { get; set; }
        public int? Ds31 { get; set; }
        public int? Ds32 { get; set; }
        public decimal? Currency3 { get; set; }
        public int? Ds41 { get; set; }
        public int? Ds42 { get; set; }
        public decimal? Currency4 { get; set; }
        public int? Ds51 { get; set; }
        public int? Ds52 { get; set; }
        public decimal? Currency5 { get; set; }
        public string UseCargoFeeDefines { get; set; }
        public int? CargoProductId { get; set; }
        public int? Ds61 { get; set; }
        public int? Ds62 { get; set; }
        public decimal? Currency6 { get; set; }
        public decimal? Currency7 { get; set; }
        public decimal? ReceiveCargoDifference { get; set; }
        public int? Ds71 { get; set; }
        public int? Ds72 { get; set; }
        public int? Ds81 { get; set; }
        public decimal? Currency8 { get; set; }

        public Product CargoProduct { get; set; }
    }
}
