﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Repositories.Base;
using WepService.Data;

namespace WebService.Repositories.Services.InterfaceRepo
{
    public interface IRequestDefinationRepo : IBaseRepository<ServicesRequestDefination>
    {
    }
}
