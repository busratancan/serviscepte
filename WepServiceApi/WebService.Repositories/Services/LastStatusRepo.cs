﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Repositories.Base;
using WebService.Repositories.Services.InterfaceRepo;
using WepService.Data;

namespace WebService.Repositories.Services
{
    public class LastStatusRepo : GenericRepository<ServicesLastStatusDefination>, ILastStatusRepo
    {
        public LastStatusRepo(

            crocodileContext context) : base(
                context)
        {
        }
    }
}