﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Repositories.Base;
using WepService.Data;

namespace WebService.Repositories.Services
{
    public class SubLastStatusRepo : GenericRepository<ServicesLastStatusDefinationInfs>, ISubLastStatusRepo
    {
        public SubLastStatusRepo(

            crocodileContext context) : base(
                context)
        {
        }
    }
}
