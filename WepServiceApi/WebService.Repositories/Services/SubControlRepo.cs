﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Repositories.Base;
using WepService.Data;

namespace WebService.Repositories.Services
{
    public class SubControlRepo : GenericRepository<ServiceControlSubDefines>, ISubControlDefinesRepo
    {
        public SubControlRepo(

            crocodileContext context) : base(
                context)
        {
        }
    }
}


