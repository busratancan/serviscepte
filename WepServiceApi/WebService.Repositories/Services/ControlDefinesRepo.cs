﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Repositories.Base;
using WepService.Data;

namespace WebService.Repositories.Services
{
    public class ControlDefinesRepo : GenericRepository<ServiceControlDefines>, IControlDefinesRepo
    {
        public ControlDefinesRepo(

            crocodileContext context) : base(
                context)
        {
        }
    }
}
