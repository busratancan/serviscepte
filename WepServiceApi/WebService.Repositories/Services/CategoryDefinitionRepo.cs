﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Repositories.Base;
using WepService.Data;

namespace WebService.Repositories.Services
{
    public class CategoryDefinitionRepo : GenericRepository<ServicesCategoryDefinition>, ICategoryDefinitionRepo
    {
        public CategoryDefinitionRepo(

            crocodileContext context) : base(
                context)
        {
        }
    }
}
