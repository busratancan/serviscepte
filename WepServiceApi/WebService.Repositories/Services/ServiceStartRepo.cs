﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Repositories.Base;
using WebService.Repositories.Services.InterfaceRepo;
using WepService.Data;

namespace WebService.Repositories.Services
{
    public class ServiceStartRepo : GenericRepository<ServicesOpen>, IServiceStartRepo
    {
        public ServiceStartRepo(
            crocodileContext context) : base(
                context)
        {

        }
    }
}
