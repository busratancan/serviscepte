﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace WebService.Repositories.Base
{
    public abstract class RepositoryBase<TContext> : IRepositoryInjection
         where TContext : DbContext
    {
        protected RepositoryBase(
            TContext context)
        {
            this.Context = context;
        }

        protected TContext Context { get; private set; }

        public IRepositoryInjection SetContext(DbContext context)
        {
            this.Context = (TContext)context;
            return this;
        }
    }
}

