﻿
using Microsoft.Extensions.DependencyInjection;
using WebService.Repositories.Customer;
using WebService.Repositories.Services;
using WebService.Repositories.Services.InterfaceRepo;

namespace WebService.Repositories.Base
{
    public class RepositoryProvider
    {
        public static void SetRepositories(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ICategoryDefinitionRepo, CategoryDefinitionRepo>();
            serviceCollection.AddTransient<IControlDefinesRepo, ControlDefinesRepo>();
            serviceCollection.AddTransient<IModuleDefineRepo, ModuleDefineRepo>();
            serviceCollection.AddTransient<IRequestDefinationRepo, RequestDefinationRepo>();
            serviceCollection.AddTransient<IServiceStartRepo, ServiceStartRepo>();
            serviceCollection.AddTransient<ISubControlDefinesRepo, SubControlRepo>();
            serviceCollection.AddTransient<ISubLastStatusRepo, SubLastStatusRepo>();
            serviceCollection.AddTransient<ICustomerRepository, CustomerRepository>();
            serviceCollection.AddTransient<IAccountActivitiesRepo, AccountActivitiesRepo>();
            serviceCollection.AddTransient<IUserPictureRepo, UserPictureRepo>();
        }
    }
}

