﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using WepService.Data;

namespace WebService.Repositories.Base
{
    public class GenericRepository<TEntity> : EntityRepositoryBase2<DbContext, TEntity>
       where TEntity : BaseEntity, new()
    {
        public GenericRepository(
            DbContext context) : base(context)
        {
        }

        protected override Expression<Func<TEntity, bool>> defaultFilter
        {
            get
            {
                return (flt => flt.Deletion == 0);
            }
        }
    }
}

