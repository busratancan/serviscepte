﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WebService.Repositories.Base.Query;
using WepService.Data;

namespace WebService.Repositories.Base
{
    public abstract class
         EntityRepositoryBase2<TContext, TEntity> : RepositoryBase<TContext>, IBaseRepository<TEntity>
             where TContext : DbContext
             where TEntity : BaseEntity, new()
    {
        private readonly OrderBy<TEntity> defaultOrderBy = new OrderBy<TEntity>(qry => qry.OrderBy(e => e.Id));
        protected virtual Expression<Func<TEntity, bool>> defaultFilter { get { return (flt => flt.Deletion == 0); } }

        public EntityRepositoryBase2(TContext context) : base(context)
        {
        }

        private IQueryable<TEntity> Set()
        {
            DbSet<TEntity> query = Context.Set<TEntity>();
            return query.Where(defaultFilter);
            //return query;
        }

        public virtual void Add(TEntity entity)
        {
            if (entity == null) throw new InvalidOperationException("Unable to add a null entity to the repository.");
           // entity.CreatedDate = DateTime.Now;
            entity.Deletion = 0;
            Context.Set<TEntity>().Add(entity);
        }

        public bool Any(Expression<Func<TEntity, bool>> filter = null)
        {
            IQueryable<TEntity> query = Set();

            if (filter != null)
                query.Where(filter);

            return query.Any();
        }
        public Task<bool> AnyAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            IQueryable<TEntity> query = QueryDb(filter, null, null);

            return query.AnyAsync();
        }

        public virtual TEntity Get(int id, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            IQueryable<TEntity> query = Set();

            if (includes != null)
                query = includes(query);

            return query.SingleOrDefault(x => x.Id == id);
        }
        public virtual Task<TEntity> GetAsync(int id, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            IQueryable<TEntity> query = Set();

            if (includes != null)
                query = includes(query);

            return query.SingleOrDefaultAsync(x => x.Id == id);
        }

        private IQueryable<TEntity> QueryDb(
            Expression<Func<TEntity, bool>> filter,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includes)
        {
            IQueryable<TEntity> query = Set();

            if (filter != null)
                query = query.Where(filter);

            if (orderBy != null)
                query = orderBy(query);

            if (includes != null)
                query = includes(query);

            return query;
        }

        public virtual IQueryable<TEntity> GetQuery(
            Expression<Func<TEntity, bool>> filter,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            if (orderBy == null) orderBy = defaultOrderBy.Expression;
            return QueryDb(filter, orderBy, includes);
        }


        public int QueryCount(Expression<Func<TEntity, bool>> filter = null)
        {
            IQueryable<TEntity> query = QueryDb(filter, null, null);
            return query.Count();
        }

        public async Task<int> QueryCountAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            IQueryable<TEntity> query = QueryDb(filter, null, null);
            return await query.CountAsync();
        }

        public void Remove(TEntity entity)
        {
            Context.Set<TEntity>().Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
            Context.Set<TEntity>().Remove(entity);
            entity.Deletion = 1;
            Context.SaveChanges();
        }

        public void Remove(int id)
        {
            var entity = new TEntity() { Id = id };
            this.Remove(entity);
        }

        public void SetUnchanged(TEntity entity)
        {
            base.Context.Entry<TEntity>(entity).State = EntityState.Unchanged;
        }

        public virtual TEntity Update(TEntity entity)
        {
            return Context.Set<TEntity>().Update(entity).Entity;
        }

        public IEnumerable<TEntity> QueryFecth(IQueryable<TEntity> query)
        {
            return query.ToList();
        }

        public async Task<IEnumerable<TEntity>> QueryFecthAsync(IQueryable<TEntity> query)
        {
            return await query.ToListAsync();
        }

        public virtual IQueryable<TEntity> GetQueryPaged(int pageNumber, int pageCount, Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null)
        {
            var query = GetQuery(filter, orderBy, includes);
            return query.Skip((pageNumber - 1) * pageCount).Take(pageCount);
        }
    }
}