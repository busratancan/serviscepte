﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace WebService.Repositories.Base
{
    public interface IBaseRepository<TEntity>
    {
        IQueryable<TEntity> GetQuery(
            Expression<Func<TEntity, bool>> filter,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null);
        IQueryable<TEntity> GetQueryPaged(
            int pageNumber,
            int pageCount,
            Expression<Func<TEntity, bool>> filter,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null);

        IEnumerable<TEntity> QueryFecth(IQueryable<TEntity> query);
        Task<IEnumerable<TEntity>> QueryFecthAsync(IQueryable<TEntity> query);

        TEntity Get(
            int id,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null);
        Task<TEntity> GetAsync(
            int id,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includes = null);

        int QueryCount(Expression<Func<TEntity, bool>> filter = null);
        Task<int> QueryCountAsync(Expression<Func<TEntity, bool>> filter = null);

        void Add(TEntity entity);
        TEntity Update(TEntity entity);
        void Remove(TEntity entity);
        void Remove(int id);

        bool Any(Expression<Func<TEntity, bool>> filter = null);
        Task<bool> AnyAsync(Expression<Func<TEntity, bool>> filter = null);

        void SetUnchanged(TEntity entity);

    }
}
