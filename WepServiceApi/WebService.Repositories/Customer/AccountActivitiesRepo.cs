﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Repositories.Base;
using WepService.Data;

namespace WebService.Repositories.Customer
{
    public class AccountActivitiesRepo : GenericRepository<CustomerCurrentInput>,IAccountActivitiesRepo
    {
        public AccountActivitiesRepo(
            crocodileContext context) : base(
                context)
        {

        }
    }
}
