﻿using System;
using System.Collections.Generic;
using System.Text;
using WebService.Repositories.Base;
using WepService.Data;

namespace WebService.Repositories.Customer
{
    public class UserPictureRepo : GenericRepository<CustomerEntity>,IUserPictureRepo
    {
        public UserPictureRepo(crocodileContext context) : base(
                context)
        {

        }
    }
}
