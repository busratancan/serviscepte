﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WebService.Repositories.Base;
using WepService.Data;

namespace WebService.Repositories.Customer
{
    public interface ICustomerRepository : IBaseRepository<CustomerEntity>
    {
    }
}